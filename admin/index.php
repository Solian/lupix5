<?php

// Basisdefinition
define('CALLED_FROM_ADMIN', TRUE);
define('BASIC_PATH',substr(__DIR__,0,-5));

// Entwickler-Einstellungen
define('DEVELOPING_MODE',TRUE);      // Set FALSE für Produktion
error_reporting(E_ALL);         // SET E_NON für System

// Autoloader für die Klassen
require __DIR__ . '/../vendor/autoload.php';

// Registriere die Fehlerbehandlung
require_once('../../lib/standardv5/fehlerbehandlung.lib.php');

try {

    //Lade benutzerfunktionen TODO Prüfe, welche überhaupt verwendet werden!
    require_once "../../lib/standardv5/funktionen.lib.php";

#################################################
##
##	Skript- und Modulkennung
##
#################################################

    $_BEREICH = "admin";
    $_MODUL = "main_administration";
    $_SKRIPT = "main_index";
    $_VERSION = "2.6.0";

    $admin = new adminklasse();

    $admin->registerScript($_MODUL, $_SKRIPT, $_VERSION, $_BEREICH);

    $admin->catchVariables();
    $admin->identifyUser();
    $admin->checkAdministrationPermissions();
    $admin->renderAdminPage();

}catch(Throwable $exception){
    LupixExceptionHandler($exception);
}

