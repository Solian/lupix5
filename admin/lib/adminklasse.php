<?php


/**
 * Class adminklasse
 */

class adminklasse extends seitenklasse
{

    function __construct()
    {
        parent::__construct();
        //Load Credentials from file
        $path=__DIR__.'/../../inc/config.ini.php';
        $this->inhalt = new adminSiteContent();
        //Wenn die Config-Datei nicht existiert, muss erstmal der Datenbankserver-Konfiguriert werden.
        if (!file_exists($path)) {
            $this->catchVariables();
            $this->renderAdminPage();
        }




    }

    function catchVariables($argFangVariablen = array()) {

        parent::catchVariables($argFangVariablen);

        //Zusätzlich wird noch das Modul gefangen und eingelesen
        $vars = array("mod");
        foreach ($vars as $vari) {
            if (!empty($this->_GLOBAL_VARS["POST"][$vari])) {
                $this->inhalt->$vari = $this->_GLOBAL_VARS["POST"][$vari];
            } elseif (!empty($this->_GLOBAL_VARS["GET"][$vari])) {
                $this->inhalt->$vari = $this->_GLOBAL_VARS["GET"][$vari];
            }else{
                $this->inhalt->$vari='';
            }
        }
    }


    function identifyUser(){

        //Wenn man installiert, muss die Identifikation via DB umgangen werden.
        if($this->inhalt->mod=='install' || $this->inhalt->mod=='main_install'){

            try {
                $this->session = lulo::loadSession($this->_GLOBAL_VARS['COOKIE']['sessionkey']);

                //wenn es keine Session gibt, eine neue initialisieren
                if ($this->session === false) {
                    $this->session=lulo::createNewSession();
                }

            }

            catch(Throwable $e){
                //Falls es noch keine Datenbank gibt
                // muss die Session also immer wechseln
                $this->session=new session(zufallszahl(5),'');

            }

            //benutzer identifizieren
            $this->userIdenty = new UserIdentity();

            ##      Sets the Usercookie     ##
            if(DEVELOPING_MODE===true){
                setcookie('sessionkey', $this->session->sessionkey);
            }else {
                setcookie('sessionkey', $this->session->sessionkey, time() + 86400, '', '', true, true);
            }

        }else {

            parent::identifyUser();
        }

    }


    /**
     * @return |null
     * @throws Exception
     */

    function checkAdministrationPermissions() {

        ## Zuerst werden die Benutzer geprüft
        ##

        ## Gesperrte Benutzer fliegen raus
        ##

        if($this->userIdenty->group_id==3){
            throw new Exception($this->userIdenty->id,902);
        }
        ## Admins bekommen den Blankoschein
        ##
        elseif($this->userIdenty->group_id == 1) {
            $this->inhalt->zugangVerboten = false;
        }
        ## alle anderen Gruppen müssen registriert sein
        ##
        elseif($this->userIdenty->group_id>0){
            if (benutzerArchive::get_benutzer_adminrecht($this->userIdenty->id, $this->inhalt->mod)) {
                $this->inhalt->zugangVerboten = false;
            } else {
                throw new Exception('Userid:' . $this->userIdenty->id . ' Modul:' . $this->inhalt->mod, 911);
            }
        }

        ## Wenn der Zugang nicht gestattet ist und es auch nciht zum Root-Nutezr gehen soll-> Login
        ##
        if($this->inhalt->zugangVerboten===true && $this->inhalt->mod!='install' && $this->inhalt->mod!='main_install')
        {
            $this->inhalt->mod='start';
            $this->inhalt->aktion='login';
        }
        ## Skript unterscheidung
        ##
        else {
            ## Wenn nix angegeben wurde Admin Starten lassen
            ##
            if ($this->inhalt->mod === '') {
                $this->inhalt->mod = 'start';
                $this->inhalt->aktion = '';
                $this->registerScript('Start', 'main_start', '5.0.6', 'admin');
            }

            ## Installieren des Root Users
            ##
            elseif ($this->inhalt->mod === 'install' || $this->inhalt->mod === 'main_install') {
                $this->registerScript('Install', 'main_install', '5.0.6', 'admin');
            }

            ## Wenn es ein anderes Modul ist, wird es geladen
            ##

            elseif (!($this->inhalt->mod === 'navi' || $this->inhalt->mod === 'start')) {

                $moduleDaten = db::querySingle('SELECT * FROM main_admin_module WHERE Modulkennung=:kennung', ['kennung' => $this->inhalt->mod]);
                if (db::lastRowCount() == 0) {
                    throw new Exception($this->inhalt->mod, 915);
                } elseif (db::lastRowCount() > 1) {
                    throw new Exception($this->inhalt->mod, 916);
                }

                $this->registerScript($moduleDaten['Name'], $moduleDaten['Scriptdatei'], $moduleDaten['Version'], 'admin');

            }
        }


    }

    /**
     * @return array
     * @throws Exception
     */

    public function getAllAdminModules(){
        return db::query(/** @lang text */ 'select * from main_admin_module where 1 ORDER BY opt_main_modul DESC, id ASC;');
    }



    ##  Startet die Ausgabe. Eigentlich darf keine Ausgabe direkt erfolgen. Sie wird immer abgefangen und zum Schluss der Funktion �bergeben. Diese muss pr�fen, ob ein Fehler aufgetreten ist. Wenn ja, muss dieser angezeigt werden.
    ##
    /**
     * @throws ReflectionException|Exception
     */

    function renderAdminPage()
    {


        //Alle Pfade werden relativ zur Index-Admin angegebene
        $adminTemplate = wolfi::empty_template_from_file('standardv5/admin.tpl');


        $adminTemplate->entries['titleBild_'] = wolfi::create_templateEntry('string', '../template/standardv5/img/cover.png');
        $adminTemplate->entries['pageTitle'] = wolfi::create_templateEntry('string', 'Lupix5 - Administration');
        $adminTemplate->entries['pageName'] = wolfi::create_templateEntry('string', 'Administration ~ '.$this->inhalt->modul);
        $adminTemplate->entries['navi'] = wolfi::create_templateEntry('controller', 'navi');
        $adminTemplate->entries['footer'] = wolfi::create_templateEntry('string', '');
        $adminTemplate->entries['menu'] = wolfi::create_templateEntry('string', '');



        //Prüfen ob Root user und DB da sind.
        $installStatus['noDatabase']=false;
        $installStatus['noRoot']=false;
        try{
            db::getCredentials(9);
            if (db::test_connect() !==true) {
                $installStatus['noDatabase']=true;
            }
            //Wenn es doch eine DB Verbindung gibt, prüfen ob ein Root user existiert!
            elseif (benutzerArchive::get_benutzer_daten(1,'nickname') == null) {
                $installStatus['noRoot']=true;
            }

        }catch (Throwable $exception){
            $installStatus['noDatabase']=true;
        }


        //Wenn ein Datenbankfehler vorliegt, meldung anzeigen
        if($installStatus['noDatabase'] &&  $this->inhalt->mod !== 'install' &&  $this->inhalt->mod !== 'main_install') {
            $adminTemplate->entries['modulTitle']= wolfi::create_templateEntry('string','Install-Modul');
            $adminTemplate->entries['content'] = wolfi::create_templateEntry('string', "<div style=\"padding:15pt;\"><div class=\"alert alert-warning\"><i class=\"text-warning fa fa-database\"></i> Die Datenbank wurde nicht installiert.</div><div><p class=\"h7\">Lösung:</p><ul>
                <li><p>Installieren Sie die Datenbank und konfigurieren Sie das Lupix5-CMS. Öffnen Sie dazu das <a href=\"index.php?mod=install\">Install-Tool</a> und folgenden Sie den Anweisungen.</p></li></ul></div>");
            $this->inhalt->modul='install';
        }
        //Wenn kein Root-Nutzer vorliegt, meldung anzeigen
        elseif($installStatus['noRoot'] && $this->inhalt->mod !== 'install' &&  $this->inhalt->mod !== 'main_install') {
            $adminTemplate->entries['modulTitle']= wolfi::create_templateEntry('string','Install-Modul');
            $adminTemplate->entries['content'] = wolfi::create_templateEntry('string', "<div style=\"padding:15pt;\"><div class=\"alert alert-warning\"><i class=\"text-warning fa fa-user-secret\"></i> Der Root-Benutzer wurde nicht eingerichtet.</div><div><p class=\"h7\">Lösung:</p><ul>
                <li><p>Installieren Sie die Datenbank und konfigurieren Sie das Lupix5-CMS. Öffnen Sie dazu das <a href=\"index.php?mod=install\">Install-Tool</a> und folgenden Sie den Anweisungen.</p></li></ul></div></div>");
            $this->inhalt->modul='install';
        }else {

            if ($this->inhalt->mod == '') {
                throw new Exception('Userid:' . $this->userIdenty->id, 913);
            } elseif ($this->inhalt->mod == 'start') {
                $adminTemplate->entries['modulTitle'] = wolfi::create_templateEntry('string', 'Lupix Überblick');
                $adminTemplate->entries['content'] = wolfi::create_templateEntry('controller', 'start', '%');
                $this->inhalt->modul = 'Start';
            } elseif ($this->inhalt->mod == 'install' || $this->inhalt->mod === 'main_install') {
                $adminTemplate->entries['modulTitle'] = wolfi::create_templateEntry('string', 'Setup / Install Tool');
                $adminTemplate->entries['content'] = wolfi::create_templateEntry('controller', 'main_install', '%');
                $adminTemplate->entries['footer'] = wolfi::create_templateEntry('string', '');
                $this->inhalt->modul = 'Install';
            } else {
                if ($this->inhalt->zugangVerboten == true) {
                    throw new Exception('Modul:' . $this->inhalt->modul, 911);
                } else {

                    //Wenn ein Template in ver Vorschau angezeigt werden soll, wird ein leeres Template geladen
                    if ($this->inhalt->mod == 'main_seiten' && $this->inhalt->aktion == 'show_template') {

                        //Lädt die Daten aus dem Parameter
                        list($isSet, $this->inhalt->templateDatei) = $this->loadParameter('file', 'string', false);
                        $this->registerPageTemplate($this->inhalt->templateDatei);
                        if ($isSet) {

                            //Die Templatedatei verwenden
                            $this->inhalt->templateDaten = wolfi::empty_template_from_file($this->templateFile);

                            //Ausgabe des Scriptes und Ende der Ausführung
                            die(
                                //Die Dateipfade werden angepasst
                            preg_replace(
                                '/(href|src)="(?!\.\.)([A-Za-z0-9_\-.\/]+)"/',
                                '$1="../$2"',
                                preg_replace(
                                    '/url\((?!\.\.)([A-Za-z0-9_\-.\/]+)\)/',
                                    'url(../$1)',
                                    $this->parsePage($this->inhalt->modul, $this->inhalt->modul, __DIR__ . '/../')
                                )
                            )
                            );
                        } else {
                            throw new Exception('file', 214);
                        }

                    } //Wenn ein Template komplett geparst werden soll, wird die Standard-Funktionalität verwendet
                    elseif ($this->inhalt->mod == 'main_seiten' && $this->inhalt->aktion == 'preview_page') {

                        //Lädt die Daten aus dem Parameter
                        list($isSet, $this->inhalt->id) = $this->loadParameter('id', 'int', false);

                        if ($isSet) {

                            $seitenDaten = seitenArchive::seiten_seitendaten_laden($this->inhalt->id);

                            $this->inhalt->templateDatei = $seitenDaten['Templatedatei'];
                            $this->inhalt->templateDaten = unserialize($seitenDaten['Templatedaten']);

                            //Ausgabe des Scriptes und Ende der Ausführung
                            die(
                                //Die Dateipfade werden angepasst
                            preg_replace(
                                '/(href|src)="(?!\.\.)([A-Za-z0-9_\-.\/]+)"/',
                                '$1="../$2"',
                                preg_replace(
                                    '/url\((?!\.\.)([A-Za-z0-9_\-.\/]+)\)/',
                                    'url(../$1)',
                                    $this->parsePage($seitenDaten['Name'], $seitenDaten['Titel'], __DIR__ . '/../../controller/')
                                )
                            )
                            );


                        } else {
                            throw new Exception('file', 214);
                        }
                    } //Wenn ein Template komplett geparst werden soll aber mit Änderungsfunktionen, wird die Standard-Funktionalität ergänzt
                    elseif ($this->inhalt->mod == 'main_seiten' && $this->inhalt->aktion == 'editPage') {


                        //Lädt die Daten aus dem Parameter
                        list($isSet, $this->inhalt->id) = $this->loadParameter('id', 'int', false);

                        if ($isSet && $this->inhalt->id > 0) {

                            ##  Edit Javascript wrappen
                            ##

                            require_once(__DIR__ . '/../module/main_seiten.php');


                            $seitenController = new main_seiten($this->userIdenty, $this);
                            $javascriptEditorFunctions = $seitenController->start('showPageJavascriptInline');


                            ##  Seiteninhalte wrappen lassen
                            ##

                            $seitenDaten = seitenArchive::seiten_seitendaten_laden($this->inhalt->id);
                            $this->inhalt->templateDatei = $seitenDaten['Templatedatei'];
                            $this->inhalt->templateDaten = unserialize($seitenDaten['Templatedaten']);

                            require_once(__DIR__ . '/../module/main_seiten.php');
                            $seitenController = new main_seiten($this->userIdenty, $this);
                            //Die Templatedaten auswerten und dann als String einfach übergeben!

                            $this->inhalt->templateDaten->entries = $seitenController->wrappingInlineEditorFields($this->inhalt->templateDaten->entries, true);

                            unset($this->inhalt->templateDaten->entries['coveredEntriesEditorFields']);

                            //Ausgabe des Scriptes und Ende der Ausführung
                            die(
                                //Die Dateipfade werden angepasst
                            preg_replace(
                                '/(href|src)="(?!\.\.)([A-Za-z0-9_\-.\/]+)"/',
                                '$1="../$2"',
                                preg_replace(
                                    '/url\((?!\.\.)([A-Za-z0-9_\-.\/]+)\)/',
                                    'url(../$1)',
                                    $this->parsePageInlineEdit($seitenDaten['Name'], $seitenDaten['Titel'], $javascriptEditorFunctions)
                                )
                            )
                            );

                        } elseif (!$isSet && $this->inhalt->id > 0) {
                            throw new Exception('Die Seiten-Id wurde nicht angegeben', 214);
                        } else {
                            throw new Exception('Die Seite existiert nicht!', 10);
                        }

                    } //Wenn ein Template komplett geparst werden soll aber mit Änderungsfunktionen, wird das Overlay-geparst und danach editPage in einem iFrame aufgerufen
                    elseif ($this->inhalt->mod == 'main_seiten' && $this->inhalt->aktion == 'showPage') {

                        try {

                            //Lädt die Daten aus dem Parameter
                            list($isSet, $this->inhalt->id) = $this->loadParameter('id', 'int', false);

                            if ($isSet && $this->inhalt->id > 0) {

                                ## Overlay rednern lassen
                                ##

                                $adminTemplate = wolfi::empty_template_from_file('standardv5/seiten_seite_anzeigen_inline.tpl');

                                $adminTemplate->set_entry('pagetree', 'controller', 'main_seiten', 'showPageTree');
                                $adminTemplate->set_entry('seitenEigenschaften', 'controller', 'main_seiten', 'showPageProperties');
                                $adminTemplate->set_entry('seitenJavascript', 'controller', 'main_seiten', 'showPageJavascript');
                                $adminTemplate->set_entry('navi', 'controller', 'navi');
                                $adminTemplate->set_entry('pageId_', 'string', $this->inhalt->id);
                                $this->inhalt->templateDatei = $adminTemplate->file;
                                $this->inhalt->templateDaten = $adminTemplate;

                                die($this->parsePage('Seiten-Editor', 'Seiten-Editor', __DIR__ . '/../module/'));


                            } elseif (!$isSet && $this->inhalt->id > 0) {
                                throw new Exception('Die Seiten-Id wurde nicht angegeben', 214);
                            } else {
                                throw new Exception('Die Seite existiert nicht!', 10);
                            }
                        } //Falls ein Fehler auftritt beim darstellen der Seite wird die Seite nicht gerendert und stattdessen in der alten Liste angezeigt
                        catch (Throwable $e) {
                            $logGesetzt = events_log_setzen(
                                zufallszahl(3),
                                time(),
                                $e->getFile(),
                                $e->getCode(),
                                "1",
                                $e->getLine(),
                                json_encode($e->getTrace(), 1),
                                $e->getMessage());

                            $this->setDispatchingMessage($e->getMessage());
                            $this->addReferringParameter('mod', 'main_seiten');
                            $this->referTo('showPageSettingsList');
                        }
                    } //normaler Controlleraufruf
                    else {

                        $adminTemplate->entries['modulTitle'] = wolfi::create_templateEntry('string', $this->inhalt->modul);
                        $adminTemplate->entries['content'] = wolfi::create_templateEntry('controller', $this->inhalt->modulskript, '%');
                        $adminTemplate->entries['menu'] = wolfi::create_templateEntry('controller', $this->inhalt->modulskript, 'menu');
                    }

                }
            }

        }

        $this->inhalt->templateDatei='standardv5/admin';
        $this->inhalt->templateDaten=$adminTemplate;
//        die($this->inhalt->modul.' '.$this->inhalt->modul.' '.__DIR__.'/../');

        die($this->parsePage($this->inhalt->modul,$this->inhalt->modul,__DIR__.'/../module/'));
    }





    /**
     * @param string $seitenName
     * @param string $seitenTitel
     * @param string $javascriptCode
     * @param string $offset
     * @return string|string[]|null
     * @throws Exception
     */

    function parsePageInlineEdit(string $seitenName,string $seitenTitel,string $javascriptCode,string $offset=__DIR__.'/../../controller/'){

        //Generate the Page with Editor Buttons
        $inlineEditorModifiedPage = parent::parsePage($seitenName,$seitenTitel,$offset);

        //define TheHeadInjetor
        $headInjector='';

        //Check for necessary components
        if(!preg_match('/font-awesome.css/',$inlineEditorModifiedPage))$headInjector.='<link rel="stylesheet" href="../template/standardv5/fonts/fontawesome/font-awesome.css" type="text/css" />';
        if(!preg_match('/jquery-ui.css/',$inlineEditorModifiedPage))$headInjector.='<link rel="stylesheet" href="../template/standardv5/scripts/jquery-ui/jquery-ui.css">';
        if(!preg_match('/bootstrap.css/',$inlineEditorModifiedPage))$headInjector.='<link rel="stylesheet" href="../template/standardv5/css/bootstrap/bootstrap.css">';
        if(!preg_match('/standardv5\/css\/editor.css/',$inlineEditorModifiedPage))$headInjector.='<link rel="stylesheet" href="../template/standardv5/css/editor.css">';
        if(!preg_match('/standardv5\/css\/buttons.css/',$inlineEditorModifiedPage))$headInjector.='<link rel="stylesheet" href="../template/standardv5/css/buttons.css" type="text/css" />';

        if(!preg_match('/standardv5\/scripts\/funktionen.js/',$inlineEditorModifiedPage))$headInjector.='<script src="../template/standardv5/scripts/funktionen.js"></script>';
        if(!preg_match('/jquery.js/',$inlineEditorModifiedPage))$headInjector.='<script src="../template/standardv5/scripts/jquery.js"></script>';
        if(!preg_match('/jquery-ui.js/',$inlineEditorModifiedPage))$headInjector.='<script src="../template/standardv5/scripts/jquery-ui/jquery-ui.js"></script>';
        if(!preg_match('/popper.min.js/',$inlineEditorModifiedPage))$headInjector.='<script src="../template/standardv5/scripts/popper.min.js"></script>';
        if(!preg_match('/bootstrap.min.js/',$inlineEditorModifiedPage))$headInjector.='<script src="../template/standardv5/scripts/bootstrap.min.js"></script>';


        return preg_replace(
            '/\<\/head\>/',
            $headInjector.$javascriptCode.'</head>',
            preg_replace(
                '/\<\/body\>/',
                "<div id=\"dialog\"></div>\n</body>",
                $inlineEditorModifiedPage
            )
        );
    }


    /**
     * @param $templateOrSeitenElement
     * @param $pathOffset
     * @throws ReflectionException|Exception
     */

    public function parseTemplateEntriesToTemplateVars($templateOrSeitenElement,$pathOffset)
    {

        $templateVars = [];

        //Alle Template Variablen nacheinander durchgehen
        //throw new Exception('$this->>inhalt->templateDaten:'.print_r($this->inhalt->templateDaten,true),50000);
        foreach ($templateOrSeitenElement->entries as $templateBereich => $templateBereichsdaten) {

            //echo" $templateBereich: ";

            //Wenn es ein statischer String ist
            // TODO diese erhalten keine Cache-Funktion, weil damit auch andere Daten geschleust werden --> Lösung dafür finden
            if ($templateBereichsdaten->type == 'string') {

                //$this->registerPageTemplateVariable($templateBereich, $templateBereichsdaten->value);
                $templateVars[$templateBereich] = $templateBereichsdaten->value;
            } //wenn es ein text ist
            elseif ($templateBereichsdaten->type == 'text') {

                $cacheFilePath = $this->createCachePath($this->inhalt->id, $templateBereich, 'T' . $templateBereichsdaten->value);

                if (file_exists($cacheFilePath)) {

                    //$this->registerPageTemplateVariable($templateBereich, file_get_contents($cacheFilePath));
                    $templateVars[$templateBereich] = file_get_contents($cacheFilePath);

                } else {

                    $text = html_entity_decode(seitenArchive::seiten_text_laden($templateBereichsdaten->value, $this->inhalt->id)['Text']);
                    if (db::lastRowCount() > 0) {

                        //$this->registerPageTemplateVariable($templateBereich, $text);
                        $templateVars[$templateBereich] = $text;

                        file_put_contents($cacheFilePath, $text);

                    } else {
                        throw new Exception('Text-ID:' . $templateBereichsdaten->value, 34);
                    }

                }
            } //Führt die einzelnen Controller aus und lädt ihren Inhalt in den angebeben Bereich
            elseif ($templateBereichsdaten->type == 'controller') {

                //echo"Controller";
                //ob_start();
                $controllerFileName = $pathOffset . $templateBereichsdaten->value . '.php';


                if (file_exists($controllerFileName)) {
                    //echo'Lade:' .$controllerFileName .' ';
                    /** @noinspection PhpIncludeInspection */
                    require_once($controllerFileName);
                    $classname=$templateBereichsdaten->value;
                    //die(print_r(class_parents($classname),true) .' '. var_dump($classname instanceof frontendControllerClass));

                    //If there is a frontendController to be loaded it will be loaded with the new paramter
                    if(in_array('frontendControllerClass',class_parents($classname))) {
                        $controller = new $classname($this);
                    }//backendController will be loadad with the old ones
                    else{
                        $controller = new $classname($this->userIdenty, $this);
                    }

                } else {

                    throw new Exception($controllerFileName, 35);
                }


                //ob_end_clean();
                //Ausnahme werfen, wenn kein Controller in der Datei initialisiert wurde, dies wird für Standard-Controller erwartet
                /** @var controllerBaseClass $controller */
                if (!isset($controller)) {
                    throw new Exception($controllerFileName . '<br>' . print_r($templateBereichsdaten, true), 3);
                }

                //Die Seitenaktion soll hier verwertet werden
                if ($templateBereichsdaten->action === '%') {
                    //echo"Rufe SEitenaktion auf: ".$this->inhalt->aktion;
                    //$this->registerPageTemplateVariable($templateBereich, $controller->start($this->inhalt->aktion));
                    $templateVars[$templateBereich] = $controller->start($this->inhalt->aktion);

                }


                //Eine vordefinierte Aktion soll hier verwertet werden
                else {
                    //echo"Rufe für \"$templateBereich\" definiert Aktion ".get_class($controller)."->".$templateBereichsdaten->action;
                    //$this->registerPageTemplateVariable($templateBereich, $controller->start($templateBereichsdaten->action));
                    $templateVars[$templateBereich] = $controller->start($templateBereichsdaten->action);

                }

            } //parst rekursiv die seitenElementTexte
            elseif ($templateBereichsdaten->type == 'set') {

                $setId = (int)$templateBereichsdaten->value;

                $templateVars[$templateBereich] =  $this->parseSet($setId,$pathOffset);

            }
        }

        return $templateVars;

    }



}






