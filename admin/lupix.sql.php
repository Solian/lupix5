<?php
function DbInstallQuery($DBName)
{
    $DBName=htmlspecialchars($DBName);
    return <<<DBQuery
-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 17, 2020 at 05:18 PM
-- Server version: 10.2.31-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `$DBName`
--

DROP DATABASE IF EXISTS `$DBName`;
CREATE DATABASE `$DBName` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `$DBName`;

-- --------------------------------------------------------

--
-- Table structure for table `main_admin_ber`
--

CREATE TABLE `main_admin_ber` (
  `benutzer_id` int(11) NOT NULL,
  `bereich_id` int(11) NOT NULL,
  `aendern` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Table structure for table `main_admin_module`
--

CREATE TABLE `main_admin_module` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Modulkennung` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `opt_main_modul` tinyint(1) NOT NULL DEFAULT 0,
  `Scriptdatei` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Version` varchar(8) COLLATE latin1_general_cs NOT NULL,
  `opt_nur_admin` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Dumping data for table `main_admin_module`
--

INSERT INTO `main_admin_module` (`id`, `Name`, `Modulkennung`, `opt_main_modul`, `Scriptdatei`, `Version`, `opt_nur_admin`) VALUES
(1, 'Benutzer', 'main_benutzer', 1, 'main_benutzer', '2.6.0', 1),
(2, 'Log', 'main_events', 1, 'main_events', '2.6.0', 1),
(3, 'Seiten', 'main_seiten', 1, 'main_seiten', '2.6.0', 0),
(4, 'Install', 'main_install', 1, 'main_install', '2.6.0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `main_benutzer`
--

CREATE TABLE `main_benutzer` (
  `id` int(11) NOT NULL,
  `nickname` varchar(40) COLLATE latin1_general_cs DEFAULT NULL,
  `style` varchar(25) COLLATE latin1_general_cs DEFAULT NULL,
  `email` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `new_email_key` varchar(200) COLLATE latin1_general_cs NOT NULL DEFAULT '""',
  `new_email_date` int(11) NOT NULL DEFAULT 0,
  `new_email` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `passwort` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `salt` varchar(255) COLLATE latin1_general_cs NOT NULL DEFAULT '',
  `adminkey` varchar(100) COLLATE latin1_general_cs NOT NULL DEFAULT '',
  `sessionkey` varchar(255) COLLATE latin1_general_cs NOT NULL DEFAULT '',
  `nutzergruppe_id` int(11) DEFAULT 2,
  `regdatum` varchar(11) COLLATE latin1_general_cs DEFAULT NULL,
  `letztbesuch` int(11) DEFAULT NULL,
  `realername` varchar(255) COLLATE latin1_general_cs NOT NULL DEFAULT '',
  `avatar` text COLLATE latin1_general_cs NOT NULL DEFAULT '',
  `preavatar` text COLLATE latin1_general_cs DEFAULT '',
  `avatar_ok` varchar(1) COLLATE latin1_general_cs DEFAULT NULL,
  `opt_flash_navi` tinyint(1) NOT NULL DEFAULT 0,
  `zeig_pers_daten` tinyint(1) NOT NULL DEFAULT 0,
  `neue_pns_anzeigen` tinyint(1) NOT NULL DEFAULT 1,
  `neue_pns_mail` tinyint(1) NOT NULL DEFAULT 0,
  `attempts` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1 CHECKSUM=1 COLLATE=latin1_general_cs PACK_KEYS=0;

TRUNCATE TABLE `main_benutzer`;

--
-- Dumping data for table `main_benutzer`
--

INSERT INTO `main_benutzer` (`id`, `nickname`, `style`, `email`, `new_email_key`, `new_email_date`, `new_email`, `passwort`, `salt`, `adminkey`, `sessionkey`, `nutzergruppe_id`, `regdatum`, `letztbesuch`, `realername`, `avatar`, `preavatar`, `avatar_ok`, `opt_flash_navi`, `zeig_pers_daten`, `neue_pns_anzeigen`, `neue_pns_mail`) VALUES
(0, 'jeder', NULL, NULL, '', 0, '', NULL, '', '', '', 0, NULL, NULL, '', '', '', '', 0, 0, 0, 0),
(1, 'root', NULL, NULL, '', 0, '', NULL, '', '', '', 1, NULL, NULL, '', '', '', '', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `main_logs`
--

CREATE TABLE `main_logs` (
  `Fehlerkey` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `datum` int(11) NOT NULL,
  `fehlernummer` int(11) NOT NULL,
  `Modul` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Skriptname` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Fehlernachricht` varchar(512) COLLATE latin1_general_cs NOT NULL,
  `zeilennummer` int(11) NOT NULL,
  `Verfolgung` varchar(2000) COLLATE latin1_general_cs NOT NULL,
  `ExecCodeAusgabe` varchar(1000) COLLATE latin1_general_cs NOT NULL,
  `nutzer_id` int(11) NOT NULL,
  `fehlerniveau` int(11) NOT NULL,
  `checked_datum` int(11) NOT NULL DEFAULT 0,
  `checked_admin_userid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Table structure for table `main_logs_niveaus`
--

CREATE TABLE `main_logs_niveaus` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Kommentar` varchar(2500) COLLATE latin1_general_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Truncate table before insert `main_logs_niveaus`
--

TRUNCATE TABLE `main_logs_niveaus`;
--
-- Dumping data for table `main_logs_niveaus`
--

INSERT INTO `main_logs_niveaus` (`id`, `Name`, `Kommentar`) VALUES
(0, 'kritische Fehler', 'Krtische Fehler treten auf, wenn das zugrunde liegende Computersystem fehlerhaft arbeitet, ein schwerer Programmierfehler begangen wurde oder die Integrität des Systems beeiträchtigt ist.'),
(1, 'Schwere Fehler', 'Zeigen Fehler auf, die die Stabilität des Systems gefährden können.'),
(2, 'Mittelschwere Fehler', 'Die zeigen auf Caching-Probleme und Resourucenzeiger (Pfade, Resourcen-Tabelen...), wodurch die Webseite ggf. gerade nicht benutzbar ist.'),
(3, 'Mittlere Fehler ', 'Diese Fehler zeigen falsche Benutzereingaben in eines der Kernmodule an.'),
(4, 'Mittelleichte Fehler', 'Diese Fehler werden von einem Ergänzungsmodul ausgelöst. Dadurch ist die Integrität des Basis-Systems zwar nicht gefährdet, aber die der Ergänzungsmodule eventuell.'),
(5, 'Leichte Fehler', 'Falsche Benutzereingaben und Resourcenfehler werden abgefangen und ausgegeben. ');

-- --------------------------------------------------------

--
-- Table structure for table `main_nutzergruppen`
--

DROP TABLE IF EXISTS `main_nutzergruppen`;
CREATE TABLE `main_nutzergruppen` (
                                      `id` int(11) NOT NULL,
                                      `Name` varchar(255) COLLATE latin1_general_cs NOT NULL,
                                      `Kommentar` text COLLATE latin1_general_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs DELAY_KEY_WRITE=1;

--
-- Truncate table before insert `main_nutzergruppen`
--

TRUNCATE TABLE `main_nutzergruppen`;
--
-- Dumping data for table `main_nutzergruppen`
--

INSERT INTO `main_nutzergruppen` (`id`, `Name`, `Kommentar`) VALUES
(0, 'jeder', 'Das sind unbekannte Gäste. Er darf nur wenig lesen und nichts ausführen. Alle nicht angemeldeten Nutzer sind automatisch unbekannte Gäste.'),
(1, 'Administrator', 'Die Admins dürfen alles.'),
(2, 'registriert', 'Alle benutzer werden zunächst in diese Gruppe einsortiert.'),
(3, 'gebannt', 'Diese Nutzer dürfen sich nichteinmal einloggen. Sofort gibt es einen Fehler! Diese haben keine Rechte mehr!');


-- --------------------------------------------------------

--
-- Table structure for table `main_picto_bilder`
--

CREATE TABLE `main_picto_bilder` (
  `id` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Name` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Bildtitel` varchar(500) COLLATE latin1_general_cs NOT NULL,
  `Beschriftung` varchar(500) COLLATE latin1_general_cs NOT NULL,
  `picto_kategorie` int(11) NOT NULL,
  `Cssid` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Pfad` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Alternativtext` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `size` int(11) NOT NULL,
  `width` int(11) DEFAULT 0,
  `height` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Table structure for table `main_picto_kategorien`
--

CREATE TABLE `main_picto_kategorien` (
  `id` int(11) NOT NULL,
  `Name` varchar(300) COLLATE latin1_general_cs NOT NULL,
  `Kommentar` text COLLATE latin1_general_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Table structure for table `main_seiten`
--

CREATE TABLE `main_seiten` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Titel` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `navigruppe_id` int(11) NOT NULL,
  `opt_cachen` tinyint(1) NOT NULL DEFAULT 0,
  `opt_verbergen` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 Sichtbar, 1 Verborgen (sehen nur Admins im editieren modus oder so)',
  `opt_zus_navi_position` tinyint(11) NOT NULL DEFAULT 1 COMMENT '1 oben (Standard), 2 drunter, 0 unterdrücke Navicontainer',
  `Templatedatei` varchar(512) COLLATE latin1_general_cs NOT NULL,
  `Templatedaten` text COLLATE latin1_general_cs NOT NULL,
  `Titelbild` text COLLATE latin1_general_cs NOT NULL,
  `ueber_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `opt_edit_nur_admin` tinyint(11) NOT NULL COMMENT '1 Nur Admins dürfen aendern'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Table structure for table `main_seiten_ber`
--

CREATE TABLE `main_seiten_ber` (
  `seite_id` int(11) DEFAULT NULL,
  `texte_id` int(11) DEFAULT NULL,
  `nutzergruppe_id` int(11) DEFAULT NULL,
  `benutzer_id` int(11) DEFAULT NULL,
  `lesen` tinyint(1) DEFAULT 1,
  `aendern` tinyint(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Table structure for table `main_sessions`
--

CREATE TABLE `main_sessions` (
  `sessionkey` varchar(255) NOT NULL,
  `challenge` varchar(255) NOT NULL,
  `loginStatus` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `firstTime` int(11) NOT NULL,
  `privKey` text NOT NULL,
  `pubKey` text NOT NULL,
  `attempts` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_sets`
--

CREATE TABLE `main_sets` (
  `id` int(11) NOT NULL,
  `elements` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `main_texte`
--

DROP TABLE IF EXISTS `main_texte`;
CREATE TABLE `main_texte` (
                              `id` int(11) NOT NULL,
                              `seite_id` int(11) NOT NULL,
                              `autor_id` int(11) NOT NULL,
                              `erstell_datum` int(11) NOT NULL,
                              `aenderung_datum` int(11) NOT NULL,
                              `Titel` text COLLATE latin1_general_cs NOT NULL,
                              `Text` text COLLATE latin1_general_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `main_admin_module`
--
ALTER TABLE `main_admin_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_benutzer`
--
ALTER TABLE `main_benutzer`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `main_benutzer` ADD FULLTEXT KEY `nickname` (`nickname`);

--
-- Indexes for table `main_logs`
--
ALTER TABLE `main_logs`
  ADD PRIMARY KEY (`Fehlerkey`);
ALTER TABLE `main_logs` ADD FULLTEXT KEY `Fehlerkey` (`Fehlerkey`);

--
-- Indexes for table `main_logs_niveaus`
--
ALTER TABLE `main_logs_niveaus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_picto_bilder`
--
ALTER TABLE `main_picto_bilder`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `main_picto_bilder` ADD FULLTEXT KEY `Volltextindex` (`Name`,`Bildtitel`,`Beschriftung`,`Alternativtext`);

--
-- Indexes for table `main_picto_kategorien`
--
ALTER TABLE `main_picto_kategorien`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `main_nutzergruppen`
--
ALTER TABLE `main_nutzergruppen`
    ADD PRIMARY KEY (`id`);


--
-- Indexes for table `main_seiten`
--
ALTER TABLE `main_seiten`
  ADD PRIMARY KEY (`id`);


--
-- Indexes for table `main_sessions`
--
ALTER TABLE `main_sessions`
  ADD PRIMARY KEY (`sessionkey`);
COMMIT;


--
-- Indizes für die Tabelle `main_sets`
--
ALTER TABLE `main_sets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_texte`
--
ALTER TABLE `main_texte`
    ADD PRIMARY KEY (`id`);


--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `main_benutzer`
--
ALTER TABLE `main_benutzer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `main_picto_kategorien`
--
ALTER TABLE `main_picto_kategorien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


--
-- AUTO_INCREMENT for table `main_nutzergruppen`
--
ALTER TABLE `main_nutzergruppen`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


--
-- AUTO_INCREMENT for table `main_seiten`
--
ALTER TABLE `main_seiten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
--
-- AUTO_INCREMENT for table `main_sets`
--
ALTER TABLE `main_sets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_texte`
--
ALTER TABLE `main_texte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
DBQuery;
}
