<?php


#################################################
##
##	Inhalt des Skripts. Die Ausgabe wird in einen
##	Puffer umgeleitet. echo kann bedenkenlos
##	eingesetzt werden!!!!
##
#################################################




$_BEREICH="admin";
$_MODUL="main_administration";
$_SKRIPT="main_start";
$_VERSION="2.6.0";

class start extends adminControllerClass
{


    function __construct(UserIdentity $userident, $adminObject)
    {
        parent::__construct($userident, $adminObject);
        benutzerArchive::init($userident);
        seitenArchive::init($userident);
    }


    ###################################################################################################################################################
    ##
    ##	Es soll standardmäßig die Übersicht angezeigt werden.
    ##
    ###################################################################################################################################################

    /**
     * @throws Exception
     */

    function Action()
    {
        $this->registerTemplate('standardv5/admin/start');

        $this->registerTemplateVariable('domain',$this->main->get_('SERVER','HTTP_HOST'));

        $this->registerTemplateVariable('lupixVersion',$this->main->getLupixVersion());

        $this->registerTemplateVariable('fehlermeldungen',log::anzahl_ungelesen());
        $this->registerTemplateVariable('gesamtFehlermeldungen',log::anzahl_alle());

        $totalSpaceRaw = disk_total_space(__DIR__);
        $availableSpaceRaw = disk_free_space(__DIR__);
        $this->registerTemplateVariable('usedSpace',$this->convertBytes($totalSpaceRaw-$availableSpaceRaw));
        $this->registerTemplateVariable('usedSpaceRaw',$totalSpaceRaw-$availableSpaceRaw);
        $this->registerTemplateVariable('availableSpace',$this->convertBytes($availableSpaceRaw));
        $this->registerTemplateVariable('availableSpaceRaw',$availableSpaceRaw);
        $this->registerTemplateVariable('totalSpace',$this->convertBytes(disk_total_space(__DIR__)));
        $this->registerTemplateVariable('totalSpaceRaw',$totalSpaceRaw);


        $this->registerTemplateVariable('benutzerZahl',benutzerArchive::get_benutzer_anzahl());
        $this->registerTemplateVariable('seitenZahl',seitenArchive::main_seiten_anzahl());


    }

    function convertBytes($number)
    {
        $len = strlen($number);
        if($len < 4)
        {
            return sprintf("%d b", $number);
        }
        if($len >= 4 && $len <=6)
        {
            return sprintf("%0.2f KiB", $number/1024);
        }
        if($len >= 7 && $len <=9)
        {
            return sprintf("%0.2f MiB", $number/1024/1024);
        }

        return sprintf("%0.2f GiB", $number/1024/1024/1024);

    }

    /**
     * @throws Exception
     */

    function loginAction(){


        if($this->main->session->loginStatus==lulo::AUTH_AUTHENTICATED){

            $this->referTo('');
        }
        else {
            $this->registerTemplateVariable('challenge', $this->main->session->challenge);
            $this->registerTemplateVariable('sessionkey', $this->main->session->sessionkey);
            $messageBegin = '<div class="alert alert-danger">';
            $messageEnd = '</div>';
            switch ($this->main->session->loginStatus) {
                case lulo::AUTH_TO_MANY_ATTEMPTS:
                    $this->registerTemplateVariable('loginMessage', $messageBegin . 'Zu viele Loginversuche, setzen sie das Passwort zurück!' . $messageEnd);
                    break;
                case lulo::AUTH_LOGIN_TIMEOUT:
                    $this->registerTemplateVariable('loginMessage', $messageBegin . 'Der Login hat zu lange gebraucht. Schließen Sie das Browser-Fenster und laden sie die Seite neu!' . $messageEnd);
                    break;
                case lulo::AUTH_NO_USER_FOUND:
                case lulo::AUTH_WRONG_PW:
                    $this->registerTemplateVariable('loginMessage', $messageBegin . 'Das Benutzername oder Passwort ist falsch! Versuchen Sie es nochmal. Beachten Sie, dass Sie nur fünf Versuche haben!' . $messageEnd);
                    break;
                default:
                    $this->registerTemplateVariable('loginMessage', '');
                    break;
            }
            $this->registerTemplate('standardv5/admin/lulo_login');
        }
    }

}

#################################################
##
##	Ende der Datei!!
##
#################################################

