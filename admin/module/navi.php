<?php
ini_set('display_errors',false);

#################################################
##
##	Inhalt des Skripts. Die Ausgabe wird in einen
##	Puffer umgeleitet. echo kann bedenkenlos
##	eingesetzt werden!!!!
##
#################################################

$_BEREICH="admin";
$_MODUL="main_administration";
$_SKRIPT="main_navi";
$_VERSION="2.6.0";


class navi extends adminControllerClass
{


    /**
     * naviControllerClass constructor.
     * @param UserIdentity $userident
     * @param adminklasse $adminObject
     */

    function __construct(UserIdentity $userident, $adminObject)
    {
        parent::__construct($userident, $adminObject);
    }


    ###################################################################################################################################################
    ##
    ##	Es soll standardmäßig die Übersicht angezeigt werden.
    ##
    ###################################################################################################################################################

    //if (empty($aktion))
    //$aktion = 'uebersicht';

    /**
     * @throws Exception
     */

    function Action()
    {
        $adminModule=array();
        if($this->userident->group_id==1)
        {
            $adminModule = $this->main->getAllAdminModules();

        } elseif($this->userident->group_id!=3 && $this->userident->group_id!=0) {
            $adminModule = db::query(
                "SELECT main_admin_module.*
                     FROM main_admin_module, main_admin_ber
                     WHERE main_admin_ber.benutzer_id=:userid AND main_admin_ber.bereich_id=main_admin_module.id AND main_admin_ber.aendern=1
                     ORDER BY main_admin_module.opt_main_modul DESC, main_admin_module.id ASC;",
                ['userid' => $this->userident->id]);
        }


            $this->registerTemplate('standardv5/admin/navi_navigation');
            $this->registerTemplateVariable('modulDaten', $adminModule);
            $this->registerTemplateVariable('versionsNummer', $this->main->getLupixVersion());
    }

}

#################################################
##
##	Ende der Datei!!
##
#################################################

