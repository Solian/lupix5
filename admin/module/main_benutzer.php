wee<?php


#################################################
##
##	Skript- und Modulkennung
##
#################################################



class main_benutzer extends adminControllerClass
{

    /**
     * @var string
     */
    protected $_BEREICH="admin";
    /**
     * @var string
     */
    protected $_MODUL="main_benutzer";
    /**
     * @var string
     */
    protected $_SKRIPT="main_benutzer";
    /**
     * @var string
     */
    protected $_VERSION="2.6.0";

    /**
     * userControllerClass constructor.
     * @param UserIdentity $userident
     * @param adminklasse $adminObject
     */
    function __construct(UserIdentity $userident, adminklasse $adminObject)
    {
        parent::__construct($userident, $adminObject);
    }

    /**
     *
     */
    function preload()
    {
        benutzerArchive::init($this->userident);

        $this->registerTemplateVariable('nachricht', $this->getDispatchingMessage());

    }

    function menuAction(string $aktion='uebersicht'){

        //Menueinträge
        $menuEntries=[];
        //Anzeige Name,     Navflag,         link             ist Standard
        $menuEntries[]=['title'=>'Übersicht','navFlag'=>'uebersicht','link'=>'?mod=main_benutzer'];
        $menuEntries[]=['title'=>'Benutzer','navFlag'=>'benutzerListe','link'=>'?mod=main_benutzer&aktion=benutzerliste'];
        $menuEntries[]=['title'=>'Gruppen','navFlag'=>'gruppenListe','link'=>'?mod=main_benutzer&aktion=liste_der_benutzergruppen'];
        $menuEntries[]=['title'=>'Neuer Nutzer','navFlag'=>'neuerBenutzer','link'=>'?mod=main_benutzer&aktion=neuerBenutzerFormular'];
        $menuEntries[]=['title'=>'Suche','navFlag'=>'sucheBenutzer','link'=>'?mod=main_benutzer&aktion=suche_form'];

        switch($aktion){
            default:
                $navFlag='uebersicht';
                break;
            case 'benutzerliste':
            case 'show_user_daten':
                $navFlag='benutzerListe';
                break;
            case 'liste_der_benutzergruppen':
                $navFlag='gruppenListe';
                break;
            case 'neuerBenutzerFormular':
                $navFlag='neuerBenutzer';
                break;
            case 'suche_form':
                $navFlag='sucheBenutzer';
                break;

        }


        $this->registerTemplateVariable('menuEntries',$menuEntries);
        $this->registerTemplateVariable('navFlag',$navFlag);
        $this->registerTemplate('standardv5/admin/modul_menu');

    }


    ###################################################################################################################################################
    ##
    ##	Es soll standardmäßig die Übersicht angezeigt werden.
    ##
    ###################################################################################################################################################

    /**
     * @throws Exception
     */

    function Action()
    {
        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer');
        $this->registerTemplateVariable('benutzerZahl', benutzerArchive::get_benutzer_anzahl());
        $this->registerTemplateVariable('benutzerGruppen', benutzerArchive::get_benutzergruppen_alle());
        $this->registerTemplateVariable('navFlag','uebersicht');

    }

    /**
     * @param string $orderBy
     * @param string $asc
     * @throws Exception
     */

    function benutzerlisteAction(string $orderBy = '', string $asc = '')
    {

        ##Sortierkriterium laden
        ##
        $orderBy = explode(';', $orderBy)[0];
        if ($orderBy == "") $sOrderBy = "main_benutzer.regdatum";
        else $sOrderBy = $orderBy;

        ## Sortierreihenfolge laden
        ##
        $sAufsteigend = explode(';', $asc)[0];

        $aAlleBenutzer = benutzerArchive::get_benutzer_alle_mitgruppe($sOrderBy, $sAufsteigend);
        $alleGruppen = benutzerArchive::get_benutzergruppen_alle();


        ## Sortierreihenfolge umkehren für die Links in der Anzeige.
        ##
        if ($sAufsteigend == "desc") $sAufsteigend = "asc";
        else $sAufsteigend = "desc";

        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_benutzerliste');
        $this->registerTemplateVariable('aufAb', $sAufsteigend);
        $this->registerTemplateVariable('alleBenutzer', $aAlleBenutzer);
        $this->registerTemplateVariable('alleGruppen',$alleGruppen);

        $this->registerTemplateVariable('navFlag','benutzerListe');

    }

    /**
     * @throws Exception
     */

    function neuerBenutzerFormularAction(){

        $res = openssl_pkey_new(["private_key_bits" => 2048]);

        // Extract the private key from $res to $privKey
        $privKey='';
        openssl_pkey_export($res, $privKey);
        $this->main->session->privKey = $privKey;

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);

        //openssl_free_key($res);
        $this->main->session->pubKey = $pubKey["key"];

        lulo::saveSession($this->main->session);


        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_erstellen');
        $this->registerTemplateVariable('navFlag','neuerBenutzer');
        $this->registerTemplateVariable('form_action','?mod=main_benutzer&aktion=neuerBenutzer');
        $this->registerTemplateVariable('k_pub',preg_replace("/\n/","'+\n'",$this->main->session->pubKey));

    }

    /**
     * @param string $nickname
     * @param string $passwort1
     * @param string $passwort2
     * @param string $email
     * @throws Exception
     */

    function neuerBenutzerAction(string $nickname, string $c2, string $email){

        if (preg_match('/^([a-zA-Z0-9]+([a-zA-Z0-9]+([_\- ])[a-zA-Z0-9]+)*[a-zA-Z0-9]+){2,20}/', $nickname)) {
            //if(preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/',$neuespw1txt)){

            //$pw = '';
            openssl_private_decrypt(base64_decode($c2), $pw, $this->main->session->privKey);
            //echo 'PW:'.$pw."<br>\n";

            list($userID, $neueEMailKeyString) = benutzerArchive::create_benutzer($nickname, $pw, $email);

            $this->registerTemplate('standardv5/login/confirm_mail');
            $this->registerTemplateVariable('neueEMailKeyString', $neueEMailKeyString);
            $nachricht = $this->parseTemplate();

            $extra = "From: derwirt@heldenschenke.de\r\n";

            set_error_handler('LupixSilencedErrorHandler');
            $mailSend = mail($email, "Anmeldung in der Heldenschenke", $nachricht, $extra, "-f derwirt@heldenschenke.de");
            restore_error_handler();
            if ($mailSend !== true) {

                $this->setDispatchingMessage('<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Die E-Mail-bestätigung konnte nicht gesendet werden!</div>');
            }
            $this->addReferringParameter('userId', $userID);
            $this->referTo('show_user_daten', 'main_benutzer');

        } else {
            throw new Exception('Userid:'.$this->main->userIdenty->id.' NeuerUser:'.$nickname.' E-Mail:'.$email,910);
        }
    }

    /**
     * @param int $userId
     * @param int $gruppeId
     * @throws Exception|ReflectionException
     */

    function benutzergruppe_aendernAction(int $userId,int $gruppeId){

        //Einige Änderungen dürfen nicht durchgeführt werden
        if($userId===0 and $gruppeId>0){
            throw new Exception('Userid:'.$this->userident->id,892);
        }elseif($userId===1 and $gruppeId!==1){
            throw new Exception('Userid:'.$this->userident->id,893);
        }

        benutzerArchive::set_benutzer_gruppe($userId,$gruppeId);
        $this->referTo('benutzerliste');
    }

    /**
     * @param int $userId
     * @throws Exception|ReflectionException
     */

    function benutzer_loeschenAction(int $userId){
        benutzerArchive::benutzer_loeschen($userId);
        $this->referTo('benutzerliste');
    }

    /**
     * @param int $userId
     * @throws Exception
     */

    function show_user_datenAction(int $userId)
    {

        $aBenutzer = benutzerArchive::get_benutzer_alle_daten($userId);

        $aBenutzer['regdatum'] = date("d.m.Y H:i", $aBenutzer['regdatum']);
        $aBenutzer["letztbesuch"] = zeit_format($aBenutzer["letztbesuch"]);

        $aBenutzer['zeig_pers_daten'] = checkbox_from_bool("zeig_pers_daten", $aBenutzer['zeig_pers_daten']);
        $aBenutzer['neue_pns_anzeigen'] = checkbox_from_bool("neue_pns_anzeigen", $aBenutzer['neue_pns_anzeigen']);
        $aBenutzer['neue_pns_mail'] = checkbox_from_bool("neue_pns_mail", $aBenutzer['neue_pns_mail']);


        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_anzeige');
        $this->registerTemplateVariable('benutzer', $aBenutzer);

        $this->registerTemplateVariable('navFlag','benutzerListe');


    }

    /**
     * @param int $userId
     * @param string $nickname
     * @param string $realername
     * @param bool $zeig_pers_daten
     * @param bool $neue_pns_anzeigen
     * @param bool $neue_pns_mail
     * @param string $email
     * @throws Exception|ReflectionException
     */

    function set_user_datenAction(int $userId, string $nickname, string $realername, bool $zeig_pers_daten = false, bool $neue_pns_anzeigen = false, bool $neue_pns_mail = false, string $email = '')
    {

        foreach (array("zeig_pers_daten", "neue_pns_anzeigen", "neue_pns_mail") as $sOptName) {
            if ($$sOptName == true) $$sOptName = 1;
            else $$sOptName = 0;
        }

        $betroffeneZeile = benutzerArchive::set_benutzer_daten($userId, $nickname, $realername, $zeig_pers_daten, $neue_pns_anzeigen, $neue_pns_mail, $email);

        if ($betroffeneZeile == 1) {
            $this->setDispatchingMessage('<p class="hinweis erfolg fa fa-info-circle"> Benutzerdaten von ' . $nickname . ' erfolgreich geändert.</p>');
            $this->dispatch('benutzerliste');
        } else {
            $this->setDispatchingMessage('<p class="fehler fa fa-info-circle"> Die Benutzerdaten von ' . $nickname . ' wurden entweder nicht verändert oder nicht gespeichert.</p>');
            $this->dispatch('show_user_daten');
        }
    }

    /**
     * @param int $userId
     * @throws Throwable|Exception
     */

    function new_user_pwAction(int $userId)
    {

        try {
            $res = benutzerArchive::set_benutzer_zufallspasswort($userId);

            if ($res) {
                $this->setDispatchingMessage("<p class=\"erfolg  fa fa-check-square-o\" style=\"text-align:center;\">Ein neues Passwort wurde erstellt und an den Benutzer geschickt!</p>");

            } else {
                $this->setDispatchingMessage("<p class=\"fehler fa fa-info-circle\" style=\"text-align:center;\">Das Passwort konnte nicht geändert oder zugestellt werden.</p>");
            }

        } catch (Throwable $e) {
            if ($e->getCode() == 30) {
                $this->setDispatchingMessage("<p class=\"fehler fa fa-info-circle\" style=\"text-align:center;\"> Das neue Passwort konnte nicht in der Datenbank gespeichert.</p>");
            } elseif ($e->getCode() == 32) {
                $this->setDispatchingMessage("<p class=\"fehler fa fa-info-circle\" style=\"text-align:center;\"> Das neue Passwort konnte nicht an die E-Mail gesendet werden.</p>");
            } else {
                throw $e;
            }
        }

        $this->addReferringParameter('userId', $userId);
        $this->referTo('show_user_daten', 'main_benutzer');

    }

    /**
     * @param string $mailkey
     * @throws Exception
     */

    function confirm_mailAction(string $mailkey)
    {

        $eMailDatenArray = db::querySingle("SELECT id, new_email_date FROM main_benutzer WHERE new_email_key=:key;", [':key' => $mailkey]);

        if (!benutzerArchive::confirm_benutzer_mail($mailkey)) {

            if (empty($eMailDatenArray)) {
                $this->setDispatchingMessage('<p class="fehler fa fa-info-circle" style="width:96%"> Der Link ist ungültig. Fordern Sie einen euen an oder wenden Sie sich an die Administratoren.</p>');
            } elseif ($eMailDatenArray["new_email_date"] < (time() - 86400)) {
                $this->setDispatchingMessage('<p class="fehler fa fa-info-circle" style="width:96%">Der Link ist nicht mehr gültig. Erstellen Sie einen neuen!</p>');
            } else {
                $this->setDispatchingMessage('<p class="fehler fa fa-info-circle" style="width:96%">Der Link ist fehlerhaft.  Fordern Sie einen euen an oder wenden Sie sich an die Administratoren.</p>');
            }
        } else {
            $this->setDispatchingMessage('<p class="erfolg fa fa-check-square-o" style="width:96%"> Die E-Mailadresse wurde bestätigt. Nun könnt ihr euch damit einloggen.</p>');
        }
        $this->addReferringParameter('mod', 'main_benutzer');
        $this->addReferringParameter('userId', $eMailDatenArray['id']);
        $this->referTo('show_user_daten');
    }

    /**
     * @throws Exception
     */

    function suche_formAction()
    {
        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_suche');
        $this->registerTemplateVariable('suchName','');
        $this->registerTemplateVariable('ergebnisListe', '');
        $this->registerTemplateVariable('navFlag','sucheBenutzer');
    }

    /**
     * @param string $query
     * @throws Exception
     */

    function suche_nutzerAction(string $query)
    {

        $ergebnisListe = benutzerArchive::suche_benutzer('%' . $query . '%');
        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_suche');
        $this->registerTemplateVariable('suchName', $query );
        $this->registerTemplateVariable('ergebnisListe', $ergebnisListe);
        $this->registerTemplateVariable('navFlag','sucheBenutzer');

    }

    /**
     * @param int $userId
     * @throws Exception
     */
    function show_user_rechteAction(int $userId)
    {


        $nutzerdaten = benutzerArchive::get_benutzer_alle_daten($userId);

        $benutzerRechte = benutzerArchive::benutzer_seitenrechte_anzeigen($userId);
        $gruppenRechte = benutzerArchive::get_benutzergruppe_rechte($nutzerdaten['nutzergruppe_id']);

        $rechte = array();

        foreach ($benutzerRechte as $aRecht) {
            array_push($rechte, $aRecht);//array($aRecht["id"],$aRecht["Name"],$aRecht["lesen"],$aRecht["aendern"]));
        }

        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_seiten_rechte');
        $this->registerTemplateVariable('nutzerdaten', $nutzerdaten);
        $this->registerTemplateVariable('rechte', $rechte);
        $this->registerTemplateVariable('gruppenRechte', $gruppenRechte);

        $this->registerTemplateVariable('navFlag','benutzerListe');
    }

    /**
     * @param int $uid
     * @throws ReflectionException|Exception
     */

    function set_benutzer_rechteAction(int $uid){

        //alte Berechtigungen laden, um damit die neuen zu schreiben
        $aRechte= benutzerArchive::benutzer_seitenrechte_anzeigen($uid);

        // L�schen der gesamten Gruppenzugriffsberechtigung f�r Seiten
        benutzerArchive::benutzer_seitenberechtigungen_loeschen($uid);

        // Alle Rechte neu einf�gen
        foreach($aRechte as $aRecht)
        {
            if($this->main->get_("POST","lesen{$aRecht['id']}")=="on"){
                $iLesen=1;
            } else $iLesen=0;

            if($this->main->get_("POST","aendern{$aRecht['id']}")=="on"){
                $iAendern=1;
            } else $iAendern=0;
            benutzerArchive::benutzer_seitenberechtigung_setzen( $aRecht['id'], $uid, $iLesen, $iAendern);
        }

        $this->setDispatchingMessage('<p class="erfolg">Die Benutzerrechte wurden aktualisiert.</p>');
        $this->dispatch('show_user_rechte');

    }

    /**
     * @param string $order
     * @param string $asc
     * @throws Exception
     */

    function liste_der_benutzergruppenAction(string $order='', string $asc = '')
    {
        ##Sortierkriterium laden
        ##


        ## Sortierreihenfolge laden
        ##
        $sAufsteigend = explode(';', $asc)[0];

        $aAlleGruppen = benutzerArchive::get_benutzergruppen_alle($sAufsteigend,$order);

        ## Sortierreihenfolge umkehren für die Links in der Anzeige.
        ##
        if ($sAufsteigend == "desc") $sAufsteigend = "asc";
        else $sAufsteigend = "desc";

        $this->registerTemplateVariable('alleGruppen', $aAlleGruppen);
        $this->registerTemplateVariable('sAufsteigend',$sAufsteigend);
        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_gruppenliste');

        $this->registerTemplateVariable('navFlag','gruppenListe');


    }

    /**
     * @param int $userId
     * @throws Exception
     */
    public function show_user_adminrechteAction(int $userId)
    {

        //Nickname laden ist standard
        $nickname = benutzerArchive::get_benutzer_daten($userId, 'nickname');
        $groupId = benutzerArchive::get_benutzer_daten($userId, 'nutzergruppe_id');

        //Adminrechte laden --> werden von Gruppen nicht über die Tabelle vererbt!
        $rechte = benutzerArchive::get_benutzer_adminrechte($userId);

        $alleModule = $this->main->getAllAdminModules();

        $aAlleRechte = array();

        foreach ($alleModule as $modul) {

            array_push($aAlleRechte,
                array(
                    'id' => $modul["id"],
                    'Name' => $modul["Name"],
                    'recht' => (array_key_exists(intval($modul["id"]),$rechte)?$rechte[intval($modul["id"])]["aendern"]:0),
                    'recht_admingroup' => ($groupId == 1 ? true : false)     // Adminvererbte Regeln
                )
            );
        }

        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_adminrechte');
        $this->registerTemplateVariable('alleRechte',$aAlleRechte);
        $this->registerTemplateVariable('nickname',$nickname);
        $this->registerTemplateVariable('userId',$userId);

        $this->registerTemplateVariable('navFlag','benutzerListe');

    }

    /**
     * @param int $userId
     * @throws ReflectionException|Exception
     */

    function set_user_adminrechteAction(int $userId)
    {
        benutzerArchive::benutzer_adminberechtigungen_loeschen($userId);

        $alleModule = $this->main->getAllAdminModules();

        foreach ($alleModule as $modul) {
            if ($this->main->get_("POST", 'aendern'.$modul['id']) == "on"){
                benutzerArchive::benutzer_adminberechtigung_setzen(intval($modul['id']), $userId, 1);
            }
        }

        $this->setDispatchingMessage("<p class=\"erfolg fa fa-check-square-o\" style=\"text-align:center;\">Administrationsrechte ge&auml;ndert.</p>");
        $this->dispatch("show_user_adminrechte");

    }

    /**
     * @param int $gid
     * @throws Exception
     */

    function show_gruppen_datenAction(int $gid){
        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_gruppe_anzeige');
        $this->registerTemplateVariable('aGruppe',benutzerArchive::nutzergruppe_daten_laden($gid));
        $this->registerTemplateVariable('navFlag','gruppenListe');
    }

    /**
     * @param int $gid
     * @param string $name
     * @param string $kommentar
     * @throws ReflectionException|Exception
     */

    function set_gruppen_datenAction(int $gid, string $name, string $kommentar){

        if($name=="" || $kommentar=="")
        {
            throw new Exception('Gruppenname:'.$name.' Kommentar:'.$kommentar,890);
        }
        else
        {
            if(benutzerArchive::nutzergruppe_daten_aendern($gid,$name,$kommentar)){
                $this->setDispatchingMessage('<p class="erfolg">Die Daten der Benutzergruppe wurden geändert.</p>');
                $this->dispatch('liste_der_benutzergruppen');
            }else{
                $this->setDispatchingMessage('<p class="fehler">Die Daten der Benutzergruppe konnten nicht gespeichert werden!</p>');
                $this->dispatch('show_gruppen_daten');
            }

        }

    }

    /**
     * @param int $gid
     * @throws Exception
     */

    function show_gruppen_rechteAction(int $gid){

        $aAlleRechte = benutzerArchive::nutzergruppe_seitenrechte_anzeigen($gid);
        $aGruppendaten=benutzerArchive::nutzergruppe_daten_laden($gid);

        $this->registerTemplate('standardv5/admin/main_benutzer/benutzer_gruppenrechte_anzeige');
        $this->registerTemplateVariable('aGruppendaten',$aGruppendaten);
        $this->registerTemplateVariable('aAlleRechte',$aAlleRechte);
        $this->registerTemplateVariable('gid',$gid);

        $this->registerTemplateVariable('navFlag','gruppenListe');

    }

    /**
     * @param int $gid
     * @throws ReflectionException|Exception
     */
    function set_gruppen_rechteAction(int $gid){

        //alte Berechtigungen laden, um damit die neuen zu schreiben
        $aRechte= benutzerArchive::benutzer_seitenrechte_anzeigen($gid);

        // L�schen der gesamten Gruppenzugriffsberechtigung f�r Seiten
        benutzerArchive::nutzergruppe_seitenberechtigungen_loeschen($gid);

        // Alle Rechte neu einf�gen
        foreach($aRechte as $aRecht)
        {
            if($this->main->get_("POST","lesen{$aRecht['id']}")=="on")$iLesen=1; else $iLesen=0;
            if($this->main->get_("POST","aendern{$aRecht['id']}")=="on")$iAendern=1; else $iAendern=0;
            benutzerArchive::nutzergruppe_seitenberechtigung_setzen( $aRecht['id'], $gid, $iLesen, $iAendern);
        }

        $this->setDispatchingMessage('<p class="alert alert-success fa fa-check-circle-o">Die Gruppenrechte wurden aktualisiert.</p>');
        $this->dispatch('show_gruppen_rechte');

    }



}



    #################################################
    ##
    ##	Ende der Datei!!
    ##
    #################################################
