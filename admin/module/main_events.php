<?php

#################################################
##
##	 Standardskript der Administration
##
#################################################
##
##	 Name:		   	StandardSkript der Administration
##
##	 Dateiname:  	adminstandardv3.php
##
##	 Standard:	 	3.2
##
##	 Datum:			28.09.2010
##
##	 Bearbeiter: 	Remus
##
##
#################################################


#################################################
##
##	Skript- und Modulkennung
##
#################################################




class main_events extends adminControllerClass
{

    protected $_BEREICH="admin";
    protected $_MODUL="main_events";
    protected $_SKRIPT="main_events.php";
    protected $_VERSION="2.6.0";


    function __construct(UserIdentity $userident, $adminObject)
    {
        parent::__construct($userident, $adminObject);
    }

    /**
     * @throws Exception
     */
    function preload()
    {
        $this->registerTemplateVariable('existierendeFehlerniveaus',log::lade_existniveaus());
        $this->registerTemplateVariable('nachricht',$this->getDispatchingMessage());

    }


    function menuAction(string $aktion='uebersicht'){

        //Menueinträge
        $menuEntries=[];
        //Anzeige Name,     Navflag,         link             ist Standard
        $menuEntries[]=['title'=>'Übersicht','navFlag'=>'uebersicht','link'=>'?mod=main_events'];
        $menuEntries[]=['title'=>'Suche','navFlag'=>'fehlerSuche','link'=>'?mod=main_events&aktion=suche'];

        switch($aktion){
            case 'suche':
                $navFlag='fehlerSuche';
                break;
            default:
                $navFlag='uebersicht';
        }

        $this->registerTemplateVariable('menuEntries',$menuEntries);
        $this->registerTemplateVariable('navFlag',$navFlag);
        $this->registerTemplate('standardv5/admin/modul_menu');

    }

    ###################################################################################################################################################
    ##
    ##	Es soll standardmäßig die Übersicht angezeigt werden.
    ##
    ###################################################################################################################################################


    /**
     * @throws Exception
     */

    function Action()
    {

        $iUngeleseneEreignisse = log::anzahl_ungelesen();
        $iAlleEregnisse = log::anzahl_alle();
        $aFehlerniveaus = log::lade_existniveausanzahl();
        $aUngeleseneFehlerniveaus = log::lade_ungeleseneniveausanzahl();

        $this->registerTemplate('standardv5/admin/main_events/events');
        $this->registerTemplateVariable('maxLogLevel', log::get_loglevel());
        $this->registerTemplateVariable('anzahlAllerEregnisse', $iAlleEregnisse);
        $this->registerTemplateVariable('fehlerniveausMitAnzahl', $aFehlerniveaus);
        $this->registerTemplateVariable('anzahlUngelesenerEregnisse', $iUngeleseneEreignisse);
        $this->registerTemplateVariable('ungepruefterFehlerniveausMitAnzahl', $aUngeleseneFehlerniveaus);

        $this->registerTemplateVariable('navFlag','uebersicht');

    }

    /**
     * @param int $niveau
     * @throws Exception
     */

    function zeige_niveauAction(int $niveau)
    {

        //Lade die Niveau_Daten, wenn es das nicht gibt, wird intern eine Ausnahme geworfen
        $this->registerTemplateVariable('ueberschriftName', log::niveauDaten($niveau)['Name']);

        //Lade alle und füge den Nutzernamen noch ein
        $fehlerDesNiveaus = log::lade_fehlernachniveau($niveau);
        foreach ($fehlerDesNiveaus as $key => $event) {
            $fehlerDesNiveaus[$key]['nutzername'] = benutzerArchive::get_benutzer_daten($event['nutzer_id'],'nickname');
            $fehlerDesNiveaus[$key]['Beschreibung'] = log::fehlerDaten($event['fehlernummer'],'Beschreibung');
        }

        $this->registerTemplateVariable('fehler', $fehlerDesNiveaus);
        $this->registerTemplateVariable('niveau', $niveau);
        $this->registerTemplateVariable('unmarked','all');
        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehlerniveau');

        $this->registerTemplateVariable('navFlag','uebersicht');
    }

    /**
     * @param int $niveau
     * @throws Exception
     */

    function zeige_niveau_unmarkedAction(int $niveau)
    {
        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehlerniveau');
        //Lade die Niveau_Daten, wenn es das nicht gibt, wird intern eine Ausnahme geworfen
        $this->registerTemplateVariable('ueberschriftName', log::niveauDaten($niveau)['Name']);

        //Lade alle und füge den Nutzernamen noch ein
        $fehlerDesNiveaus = log::lade_ungepruefte_fehler_nach_niveau($niveau);
        foreach ($fehlerDesNiveaus as $key => $event) {
            //Versuche Benutzerdaten zu laden
            try {
                $fehlerDesNiveaus[$key]['nutzername'] = benutzerArchive::get_benutzer_daten($event['nutzer_id'], 'nickname');
            }
                //Wenn der Benutzer nicht existiert oder nicht geladen werden kann, wird eben nur die alte ID geladen!
            catch(Throwable $exception){
                $fehlerDesNiveaus[$key]['nutzername']='N/A';
            }
            $fehlerDesNiveaus[$key]['Beschreibung'] = log::fehlerDaten($event['fehlernummer'],'Beschreibung');
        }

        $this->registerTemplateVariable('fehler', $fehlerDesNiveaus);
        $this->registerTemplateVariable('niveau', $niveau);
        $this->registerTemplateVariable('unmarked','unmarked');

        $this->registerTemplateVariable('navFlag','uebersicht');
    }

    /**
     * @param string $key
     * @throws Exception
     */

    function zeige_fehlerAction(string $key)
    {
        $ereignisDaten = log::lade_fehlerauskey($key);

        $benutzername = benutzerArchive::get_benutzer_daten($ereignisDaten["nutzer_id"]);
        ## Eigen definierte Fehler haben eine Beschreibung usw. sodass, diese aus der DB geladen werden kann.
        ##
        if ($ereignisDaten["fehlernummer"] < 1000 or $ereignisDaten["fehlernummer"] > 5000) {
            $fehlerDaten=log::fehler($ereignisDaten["fehlernummer"]);
            $sNachricht =$fehlerDaten['Nachricht'];
            $sBeschreibung = $fehlerDaten['Beschreibung'];
            $iFehlerlevel =$fehlerDaten['NIV'];
        }
        ##	Mysql-Fehler haben keine reproduzierbaren Beschreibungstexte. Hier sollte man nochmal im Netz nachschlagen.
        ##
        else {
            $sNachricht = "Gibt keine Nachricht.";
            $sBeschreibung = "Steht höchstens bei ExecCode, sonst muss man bei <a href=\"http://dev.mysql.com/doc/refman/5.1/de/error-messages-server.html\">mysql.com</a> suchen.";
            $iFehlerlevel = 0;
        }

        if ($ereignisDaten["checked_datum"] > 0){
            $checkedAdminName = benutzerArchive::get_benutzer_daten($ereignisDaten["checked_admin_userid"]);
        }else{
            $checkedAdminName='';
        }

        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehler');
        $this->registerTemplateVariable('key',$key);
        $this->registerTemplateVariable('ereignisDaten',$ereignisDaten);
        $this->registerTemplateVariable('benutzername',$benutzername);
        $this->registerTemplateVariable('checkedAdminName',$checkedAdminName);
        $this->registerTemplateVariable('nachricht',$sNachricht);
        $this->registerTemplateVariable('beschreibung',$sBeschreibung);
        $this->registerTemplateVariable('fehlerlevel',$iFehlerlevel);
        $this->registerTemplateVariable('fehlerlevelName',log::niveauDaten($iFehlerlevel)['Name']);

        $this->registerTemplateVariable('navFlag','uebersicht');
    }

    /**
     * @param string $key
     * @param int|null $niveau
     * @param string $unmarked
     * @throws ReflectionException|Exception
     */

    function mark_fehler_checkedAction(string $key, int $niveau=null, string $unmarked=''){
        log::checke_fehler_aus($key,$this->userident->id);
        if($niveau==null){
            $this->dispatch('zeige_fehler');
        }
        else {
            $this->addReferringParameter('niveau',$niveau);
            if ($unmarked == 'all') $this->referTo('zeige_niveau');
            else $this->referTo('zeige_niveau_unmarked');
        }
    }

    /**
     * @throws Exception
     */
    function sucheAction()
    {

        $this->registerTemplate('standardv5/admin/main_events/events_suche');

        $this->registerTemplateVariable('navFlag','fehlerSuche');
    }

    /**
     * @param string $query
     * @throws Exception
     */

    function suche_eventAction(string $query){

        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehlerniveau');
        $this->registerTemplateVariable('ueberschriftName', 'Suche nach "'.$query.'"');

        //Lade alle und füge den Nutzernamen noch ein
        $ergebnisListe = log::suche_ereignis('%'.$query.'%');
        foreach ($ergebnisListe as $key => $event) {
            $ergebnisListe[$key]['nutzername'] = benutzerArchive::get_benutzer_daten($event['nutzer_id']);
            $ergebnisListe[$key]['Beschreibung'] = log::fehlerDaten($event['fehlernummer'],'Beschreibung');
        }

        $this->registerTemplateVariable('fehler', $ergebnisListe);
        $this->registerTemplateVariable('niveau', null);
        $this->registerTemplateVariable('unmarked','unmarked');
        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehlerniveau');

    }

    /**
     * @param int $query
     * @throws Exception
     */

    function suche_fehlerAction(int $query){
        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehlerniveau');

        $this->registerTemplateVariable('ueberschriftName', 'Suche nach Fehler "'.$query.'"');
        //Lade alle und füge den Nutzernamen noch ein
        $ergebnisListe = log::suche_fehlernummer($query);
        foreach ($ergebnisListe as $key => $event) {
            $ergebnisListe[$key]['nutzername'] = benutzerArchive::get_benutzer_daten($event['nutzer_id']);
            $ergebnisListe[$key]['Beschreibung'] = log::fehlerDaten($event['fehlernummer'],'Beschreibung');
        }

        $this->registerTemplateVariable('fehler', $ergebnisListe);
        $this->registerTemplateVariable('niveau', null);
        $this->registerTemplateVariable('unmarked','unmarked');
        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehlerniveau');
    }

    /**
     * @param string $query
     * @throws Exception
     */

    function suche_nutzer_eventAction(string $query){

        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehlerniveau');

        $this->registerTemplateVariable('ueberschriftName', 'Suche nach "'.$query.'"');
        //Lade alle und füge den Nutzernamen noch ein
        $ergebnisListe = log::suche_nutzer_ereignis('%'.$query.'%');
        foreach ($ergebnisListe as $key => $event) {
            $ergebnisListe[$key]['nutzername'] = benutzerArchive::get_benutzer_daten($event['nutzer_id']);
            $ergebnisListe[$key]['Beschreibung'] = log::fehlerDaten($event['fehlernummer'],'Beschreibung');
        }

        $this->registerTemplateVariable('fehler', $ergebnisListe);
        $this->registerTemplateVariable('niveau', null);
        $this->registerTemplateVariable('unmarked','unmarked');
        $this->registerTemplate('standardv5/admin/main_events/events_zeige_fehlerniveau');

    }
}


#################################################
##
##	Ende der Datei!!
##
#################################################
