<?php





class main_install extends adminControllerClass
{

    /**
     * @var string
     */
    protected $_BEREICH = "admin";

    /**
     * @var string
     */
    protected $_MODUL = "main_install";

    /**
     * @var string
     */
    protected $_SKRIPT = __FILE__;

    /**
     * @var string
     */
    protected $_VERSION = "2.2.0";





    /**
     * @var bool
     */
    protected $installFileExists = false;

    /**
     * @var bool
     */
    protected $installLoginOK = false;


    /**
     * @var string|null
     */
    protected $installPasswort=null;

    /**
     * installControllerClass constructor.
     * @param UserIdentity $userident
     * @param adminklasse $adminObject
     */

    function __construct(UserIdentity $userident, adminklasse $adminObject)
    {
        parent::__construct($userident, $adminObject);
    }

    /**
     * @return bool
     */

    function isInstallLoginOk(){
        return $this->installLoginOK;
    }

    /**
     * @return bool
     */
    public function isInstallFileExists(): bool
    {
        return $this->installFileExists;
    }

    function preload()
    {
        $this->registerTemplateVariable('dispatchMessage', $this->getDispatchingMessage());
        $this->registerTemplateVariable('nachricht', $this->getDispatchingMessage());

        //Wenn die Install-Datei existiert und ncihtzu alt ist
        if(file_exists(__DIR__ . '/../BACKEND_INSTALL.php')){

            if((time()-filemtime(__DIR__ . '/../BACKEND_INSTALL.php')<3600)){

                include(__DIR__ . '/../BACKEND_INSTALL.php');
                $this->installFileExists=true;

                //Wenn das Passwort existiert, wird es zwischengespeichert
                if(isset($backendInstallPasswort)) {
                    $this->installPasswort = $backendInstallPasswort;
                }
                 //Prüfen ob der Install key existiert,
                elseif(isset($installkey)) {

                    //Wenn der Key mit dem Cookie übereinstimmt
                    if ($installkey === $this->main->get_('COOKIE', 'installkey')) {

                        //Wird der Login freigegeben
                        $this->installLoginOK = true;
                    }

                }

            }
            //Wenn die Datei zu alt ist, wird sie gelöscht!
            else{
                unlink(__DIR__ . '/../BACKEND_INSTALL.php');
            }
        }
    }


    ###################################################################################################################################################
    ##
    ##	Es soll standardmäßig die Übersicht angezeigt werden.
    ##
    ###################################################################################################################################################


    /**
     * @throws Exception
     */

    function Action()
    {


        if($this->isInstallLoginOk()===false){

            $this->referTo('loginForm');
        }else {

            /* TODO Vor der Installation müssen die Voraussetzungen grpüft werden
             *
             * PHP Version
             * gibt es openssl als extension
             * ist doctrine2 installiert
             *
             * */

            try {

                //Wenn beide Dateien vorhanden sind, soll ein Fehler ausgegeben werden, da nun nicht mehr vernünftig gearbeitet werden kann!
                if (file_exists(__DIR__ . '/test_config.php') && file_exists(__DIR__ . '/../../inc/config.ini.php')) {

                    $this->referTo('errorTwoConfigs');

                } //Wenn die Datenbankeinstellungen nur in der normalen Version vorliegen, werden diese zur Installation verwendet
                elseif (!file_exists(__DIR__ . '/test_config.php') && file_exists(__DIR__ . '/../../inc/config.ini.php')) {

                    copy(__DIR__ . '/../../inc/config.ini.php', __DIR__ . '/test_config.php');
                    unlink(__DIR__ . '/../../inc/config.ini.php');
                }

                //das Installskript arbeitet immer im Test-Modus
                db::setConfigPath(__DIR__ . '/test_config.php');

                //Versuch, verbindung zur Datenbank herzustellen
                db::connect();

                try {

                    //Test ob die Benutzerdb da ist.
                    $result = db::query('select * from main_benutzer');
                    if (count($result) < 2) throw new Exception();

                    //Wenn die Configuration mit der Test-Config gemacht wurde, kann sie nun verschoben werden
                    db::disconnect();

                    copy(__DIR__ . '/test_config.php', __DIR__ . '/../../inc/config.ini.php');
                    unlink(__DIR__ . '/test_config.php');
                    if (file_exists(__DIR__ . '/test_config.php')) throw new Exception('Die TEst-Konfig lässt sich nicht ändern.', 40000);


                    db::setConfigPath(__DIR__ . '/../../inc/config.ini.php');
                    db::connect();

                    //Wenn alles da ist kann man auch die Benutzerdaten für den Root eingeben
                    $this->dispatch('installRootForm');

                } //Wenn die Benutzerdb nicht da ist, ist auch anders kaputt, die DB muss neu erstellt werden!
                catch (Throwable $exception) {
                    $this->referTo('installDatabaseForm');
                }
            } //Wenn keine Verbindung zu stande kommt, muss sie neu eingegeben werden
            catch (Throwable $exception) {
                $this->referTo('connectDatabaseForm');
            }
        }
    }

    /**
     * @throws Exception
     */

    function loginFormAction(){
        if($this->isInstallLoginOk()===true)$this->referTo('');

        if(!$this->isInstallFileExists())$this->dispatch('error');

        $this->registerTemplate('standardv5/admin/main_install/install_login');
    }


    /**
     * @param string $installPasswort
     * @throws ReflectionException|Exception
     */

    function installToolLoginAction(string $installPasswort){

        if(!$this->isInstallFileExists())$this->referTo('error');

        if($this->installPasswort===$installPasswort){

            //erstelle den Sessionkey
            $installkey = zufallszahl(2);

            if(!is_writable (__DIR__ . '/../BACKEND_INSTALL.php'))throw new Exception(date('d.m.Y H:i:s'),37);
            //schreibe ihn in die Datei
            file_put_contents(__DIR__ . '/../BACKEND_INSTALL.php','<?php
    $installkey="'.$installkey.'";
?>
            ');

            //setze das Cookie
            if(DEVELOPING_MODE===False){setcookie('installkey',$installkey,0,'','',true,true);}
            else setcookie('installkey',$installkey);

            $this->installLoginOK=true;

            $this->dispatch('');

        }else{
            $this->setDispatchingMessage('<p class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i>'."{$this->installPasswort}===$installPasswort");
            $this->referTo('loginForm');
        }



    }

    /**
     * @throws Exception
     */
    function connectDatabaseFormAction(){

        if($this->isInstallLoginOk()===false)$this->referTo('loginForm');

        $this->registerTemplate('standardv5/admin/main_install/install_connect_database');
    }



    /**
     * @param string $dbServer
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbName
     * @throws Exception
     */

    function connectDatabaseAction(string $dbServer, string $dbUser, string $dbPass)
    {

        if ($this->isInstallLoginOk() === false) $this->referTo('error');

        $configData = <<<CONF
;<?php
;die() /*
DBUSER = "$dbUser"
DBPASS = "$dbPass"
DBHOST = "$dbServer"
;*/?>
CONF;
        file_put_contents(__DIR__ . '/test_config.php', $configData);
        db::setConfigPath(__DIR__ . '/test_config.php');

        try {
            if (!db::connectForInstallDB()) {
                throw new Exception($dbUser . ':' . $dbPass . '@' . $dbServer, 6);
            }

            $this->registerTemplate('standardv5/admin/standard');
            $this->registerTemplateVariable('titel','Lupix - Installation');
            $this->registerTemplateVariable('inhalt','
                <h4>Auswahl der Vorgehensweise</h4>
               <div class="alert alert-success"><i class="fa fa-check-circle-o alert-success"></i> Die Verbindung zum Server <b>'.$dbServer. '</b> war erfolgreich. </div>
            <div class="alert alert-info">Wählen Sie zwischen folgenden Optionen:
             <ul>
                <li>Wollen Sie eine neue Datenbank installieren</li>
                <li>oder nur die Verbindungsdaten für eine existierende Datenbank eingeben.</li>
                </ul>
             </div>
             <div class="input-group mb-3">
                 <a class="lupix-btn form-control" href="?mod=install&aktion=installDatabaseForm">Neue Datenbank</a>
                 <a class="lupix-btn form-control"  href="?mod=install&aktion=chooseDatabaseForm">Bestehende Datenbank verbinden</a>
             </div>
            ');
            //$this->dispatch('installDatabaseForm');

        }catch(Throwable $e){
            $this->setDispatchingMessage(log::fehlerDaten($e->getCode(),'Nachricht'));
            $this->dispatch('');
        }
            //db::query('show DATABASES ');
    }

    /**
     * @throws ReflectionException
     */

    function chooseDatabaseFormAction(){

        if ($this->isInstallLoginOk() === false) $this->referTo('error');

        db::setConfigPath(__DIR__ . '/test_config.php');
        try {
            if (!db::connectForInstallDB()) throw new Exception('', 6);

            $this->registerTemplate('standardv5/admin/main_install/install_choose_database');
            $this->registerTemplateVariable('datenbanken', db::query('show databases'));
        }
        catch(Throwable $e){
            $this->setDispatchingMessage(log::fehlerDaten($e->getCode(),'Nachricht'));
            $this->dispatch('connectDatabaseForm')    ;
        }
    }

    /**
     * @param string $dbName
     * @throws Exception
     */

    function chooseDatabaseAction(string $dbName){

        if($this->isInstallLoginOk()===false)$this->referTo('loginForm');

        //Aktuelle Verbindungsdaten laden und um die DB ergänzen
        db::setConfigPath(__DIR__.'/test_config.php');
        $DB=db::getConnectionCredentials();

        $configData = <<<CONF
;<?php
;die() /*
DBUSER = "{$DB['DB_USER']}"
DBPASS = "{$DB['DB_PASS']}"
DBHOST = "{$DB['DB_HOST']}"
DBNAME = "$dbName"
;*/?>
CONF;
        file_put_contents(__DIR__.'/test_config.php',$configData);

        db::disconnect();
        db::connect();

        // TODO umfangreichere Prüfung schreiben

        if(count(db::query('select * from main_benutzer'))>=2) {

            //Die Config an den richtigen PLatz kopieren
            if(!is_dir(__DIR__ . '/../../inc'))mkdir(__DIR__ . '/../../inc');
            copy(__DIR__ . '/test_config.php', __DIR__ . '/../../inc/config.ini.php');
            unlink(__DIR__ . '/test_config.php');
            if(file_exists(__DIR__ . '/test_config.php'))throw new Exception('Die TEst-Konfig lässt sich nicht ändern.',40000);

            //Die Datei mit der ZUgangsberechtigung löschen
            try {
                unlink(__DIR__ . '/../BACKEND_INSTALL.php');
            }catch(Throwable $e){}

            //Den Root-Benutzer installieren
            $this->setDispatchingMessage('<p class="alert alert-success">Die Datenbank wurde ordentlich verbunden!');
            $this->referTo('','start');

        }else{
            $this->setDispatchingMessage('<p class="alert alert-danger">Die Datenbank konnte nicht ordentlich geladen werden.');
            $this->dispatch('');
        }

    }

    /**
     * @throws ReflectionException
     */

    function installDatabaseFormAction(){

        if ($this->isInstallLoginOk() === false)$this->referTo('loginForm');

        db::setConfigPath(__DIR__ . '/test_config.php');
        try {
            if (!db::connectForInstallDB()) throw new Exception('', 6);

            $this->registerTemplate('standardv5/admin/main_install/install_database');
            $this->registerTemplateVariable('datenbanken', db::query('show databases'));
        }
        catch(Throwable $e){
            $this->setDispatchingMessage(log::fehlerDaten($e->getCode(),'Nachricht'));
            $this->dispatch('connectDatabaseForm')    ;
        }
    }

    /**
     * @param string $dbName
     * @throws Exception
     */

    function installDataBaseAction(string $dbName){

        if($this->isInstallLoginOk()===false)$this->referTo('loginForm');

        //wenn die Konfiguration schon erstellt wurde
        if(file_exists(__DIR__.'/../../inc/config.ini.php')){

            //Lade die Daten aus aus der bestehenden Config
            db::setConfigPath(__DIR__.'/../../inc/config.ini.php');

        }
        //Wenn sie noch nciht erstellt wurde, ist die KOnfig schon getestet
        elseif(file_exists(__DIR__.'/test_config.php')) {

            //Lade die Daten aus der Testconfig
            db::setConfigPath(__DIR__.'/test_config.php');
        }
        //Wenn nix davon stimmt, dann müssen die Daten neu eingenegeben werden
        else{

            $this->referTo('connectDatabaseForm');
        }


        //Lade die Daten aus der Datenbank
        $DB=db::getConnectionCredentials();
        $configData = <<<CONF
;<?php
;die() /*
DBUSER = "{$DB['DB_USER']}"
DBPASS = "{$DB['DB_PASS']}"
DBHOST = "{$DB['DB_HOST']}"
DBNAME = "$dbName"
;*/?>
CONF;
        file_put_contents(__DIR__.'/../../inc/config.ini.php',$configData);
        db::setConfigPath(__DIR__.'/../../inc/config.ini.php');


        db::disconnect();
        db::connect();

        //Installiere die DB
        require_once(__DIR__.'/../lupix.sql.php');
        $sql = DbInstallQuery($dbName);
        db::insert($sql);

        if(count(db::query('select * from main_benutzer'))==2) {

            //Alte Config ggf. löschen und die neue Datei dorthin konfigureiren
            if(file_exists(__DIR__.'/test_config.php'))unlink(__DIR__ . '/test_config.php');
            if(file_exists(__DIR__ . '/test_config.php'))throw new Exception('Die TEst-Konfig lässt sich nicht ändern.',40000);


            //Den Root-Benutzer installieren
            $this->setDispatchingMessage('<p class="alert alert-success">Die Datenbank wurde erstellt!');
            $this->referTo('installRootForm');

        }else{
            $this->setDispatchingMessage('<p class="alert alert-danger">Die Datenbank konnte nicht ordentlich geladen werden.</p>');
            $this->referTo('');
        }

        //}catch( Throwable $exception){
        //    unlink(__DIR__.'/test_config.php');
        //    $this->setDispatchingMessage('<p class="alert alert-danger">Die Verbindung konnte nicht hergestellt werden.');
        //    $this->dispatch('');
        //}

    }


    /**
     * @throws Exception
     */
    function installRootFormAction(){

        if($this->isInstallLoginOk()===false)$this->referTo('loginForm');

        $res = openssl_pkey_new(["private_key_bits" => 2048]);

        // Extract the private key from $res to $privKey
        $privKey='';
        openssl_pkey_export($res, $privKey);

        file_put_contents(__DIR__.'/../../inc/installRootUser.key',$privKey);

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);

        $this->registerTemplate('standardv5/admin/main_install/install_root_passwort');
        $this->registerTemplateVariable('form_action','?mod=install&aktion=installRootUser');
        $this->registerTemplateVariable('k_pub',preg_replace("/\n/","'+\n'",$pubKey['key']));

    }

    /**
     * @param string $email
     * @param string $neuespw1
     * @param string $neuespw2
     * @throws ReflectionException|Exception
     */

    function installRootUserAction(string $email, string $c2){

        if($this->isInstallLoginOk()===false)$this->referTo('loginForm');
        else {

            if (!empty($email)) {

                $pw = '';
                openssl_private_decrypt(base64_decode($c2), $pw,file_get_contents(__DIR__.'/../../inc/installRootUser.key'));
                //echo 'PW:'.$pw."<br>\n";

                benutzerArchive::set_new_root($pw, $email);

                try {
                    if (file_exists(__DIR__ . '/../BACKEND_INSTALL.php')) unlink(__DIR__ . '/../BACKEND_INSTALL.php');
                    if (file_exists('../download.php')) unlink('../download.php');
                    if (file_exists(__DIR__.'/../../inc/installRootUser.key')) unlink(__DIR__.'/../../inc/installRootUser.key');
                } catch (Throwable $e) {
                    //Do nothing, if there are no files to remove
                }

                lulo::deleteSession($this->main->session);

                $this->referTo('', 'start');

            } else {
                $this->setDispatchingMessage('<p class="hinweis fehler">Es muss eine E-Mail angegeben werden!');
                $this->dispatch('');

            }
        }
    }


    /**
     * @throws Exception
     */
    function errorAction(){

        $this->registerTemplate('standardv5/admin/standard');
        $this->setBeHeaderText('Willkommen bei Lupix5');
        $this->registerTemplateVariable('titel', "Vorbereitung");
        $this->registerTemplateVariable('inhalt',
            '
<div class="p-3">
<div class="alert alert-warning"><i class="alert-warning fa fa-exclamation-triangle"></i> Die Installtion kann nicht beginnen, da zuerst die Datei <b>BACKEND_INSTALL.php</b> im Admin-Verzeichnis gesetzt werden muss. 
Sie muss das einmalige Login-Passwort wie im folgenden Code-Beispiel gezeigt.</div>
<div class="alert alert-danger"><i class="alert-danger fa fa-check"></i> Sie können das zufällig generierte Passwort nicht verwendet. Vor allem nicht, wenn sie die Seite unverschlüsselt aufrufen!</div>
<div class="alert alert-secondary"><code>'.highlight_string("<?php \n ".'$backendInstallPasswort'."='".zufallszahl()."'; \n ?>",true).'</code></div>
<a class="lupix-btn" href="?mod=main_install">Neu starten</a>
</div>
');
    }


    /**
     * @throws Exception
     */
    function errorTwoConfigsAction(){

        $this->registerTemplate('standardv5/standard');
        $this->setBeHeaderText('Willkommen bei Lupix5');
        $this->registerTemplateVariable('titel', "Lupix  - Installation");
        $this->registerTemplateVariable('inhalt',
            '<h4>Auswahl der Vorgehensweise</h4>
<div class="alert alert-warning"><i class="alert-warning fa fa-exclamation-triangle"></i> Die Installtion kann nicht beginnen, da zwei verschiedene Datenbank-Konfigurationen vorliegen. 
Sie müssen sich entscheiden, welche Sie verwenden möchten! Die andere Datei wird gelöscht!</div>
<p><a href="?mod=install&aktion=decideConfig&useTest=true" class="button">Test-Konfiguration</a> <a href="?mod=install&aktion=decideConfig&useTest=false" class="button">Produktiv-Konfiguration</a></p>');
    }

    /**
     * @param bool $useTest
     * @throws ReflectionException
     */

    function decideConfigAction(bool $useTest)
    {

        if ($this->isInstallLoginOk() === false) $this->referTo('loginForm');
        else {
            if($useTest===true)unlink(__DIR__ . '/../../inc/config.ini.php');
            else{unlink(__DIR__ . '/test_config.php');}
            $this->dispatch('');
        }
    }

}
