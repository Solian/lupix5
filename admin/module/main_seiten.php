<?php

#################################################
##
##	 Standardskript der Administration
##
#################################################
##
##	 Name:		   	StandardSkript der Administration
##
##	 Dateiname:  	adminstandardv3.php
##
##	 Standard:	 	3.2
##
##	 Datum:			28.09.2010
##
##	 Bearbeiter: 	Remus
##
##
#################################################

require_once(__DIR__. '/../../lib/spyc/Spyc.php');

#################################################
##
##	Skript- und Modulkennung
##
#################################################


class main_seiten extends adminControllerClass
{

    protected $_BEREICH = "admin";
    protected $_MODUL = "main_seiten";
    protected $_SKRIPT = "main_seiten.php";
    protected $_VERSION = "2.6.0";

    function __construct(UserIdentity $userident, $adminObject)
    {
        parent::__construct($userident, $adminObject);
    }


    function menuAction(string $aktion='uebersicht'){

        //Menueinträge
        $menuEntries=[];
        //Anzeige Name,     Navflag,         link             ist Standard
        $menuEntries[]=['title'=>'Übersicht','navFlag'=>'uebersicht','link'=>'?mod=main_seiten'];
        $menuEntries[]=['title'=>'Seite','navFlag'=>'seite','link'=>'#'];
        $menuEntries[]=['title'=>'Neue Seite erstellen','navFlag'=>'neueSeite','link'=>'?mod=main_seiten&aktion=newPageChooseHook'];

        switch($aktion){
            case 'newPageSetProperties':
            case 'newPageChooseHook':
                $navFlag='neueSeite';
                break;
            case 'showPageSettingsList':
            case 'pageUeberSeiteFormAction':
            case 'seite_loeschen':
            case 'changeTemplateForm':
            case 'changePageController':
            case 'changePageText':
            case 'edit_text':
            case 'deleteSetForNewSetForm':
                $navFlag='seite';
                break;
            default:
                $navFlag='uebersicht';
        }

        $this->registerTemplateVariable('menuEntries',$menuEntries);
        $this->registerTemplateVariable('navFlag',$navFlag);
        $this->registerTemplate('standardv5/admin/modul_menu');

    }


    /**
     * @throws Exception
     */

    function preload()
    {
        //initialisiert die Benutzerberechtigungen
        seitenArchive::init($this->userident);
        $this->registerTemplateVariable('aSeitenbaum', seitenArchive::seitenbaum_array());
        $this->registerTemplateVariable('tSeitenbaum', '');
        $this->registerTemplateVariable('SeitenbaumSpaltentitle', 'Seitenbaum');
        $this->registerTemplateVariable('navFlag','uebersicht');
        $this->registerTemplateVariable('nachricht',$this->getDispatchingMessage());


    }

    /**
     * @throws Exception
     */

    ###############################################################
    ##
    ##          Überblick - Funktionen
    ##
    ###############################################################

    function Action()
    {

        $this->registerTemplate('standardv5/admin/main_seiten/seiten_start');
        $this->registerTemplateVariable('tSpaltentitle', "&Uuml;berblick");
        $this->registerTemplateVariable('tSeitendaten', '
<ul class="list-group">
    <li class="list-group-item d-flex justify-content-between align-items-center">
       Seitenzahl
        <span class="badge badge-primary badge-pill">' . seitenArchive::main_seiten_anzahl() . '</span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
       Anzahl der Texte
        <span class="badge badge-primary badge-pill">' . seitenArchive::seiten_texte_anzahl() . '</span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
       <a href="?mod=main_seiten&aktion=deleteCache">Cache Loeschen</a>
        <span class="badge badge-primary badge-pill">'.(count(scandir($this->main->getCacheFolder()))-3).' Dateien</span>
    </li>
    
</ul>');
    }



###############################################################
##
##          NEUE SEITEN - Funktionen
##
###############################################################



    /**
     * @throws Exception
     */

    function newPageChooseHookAction()
    {
        $this->registerTemplateVariable('navFlag','neueSeite');

        if(seitenArchive::main_seiten_anzahl()>0) {

            $this->registerTemplate('standardv5/admin/main_seiten/seiten_position_auswahl');
            $this->registerTemplateVariable('tSeitenbaum', '');
            $this->registerTemplateVariable('tSeitendaten', "<h5>Schritt 1: Seitenposition</h5><div class=\"alert alert-info\">Wählen Sei die Position der neuen Seite <strong>links im Seitenbaum</strong>!</div>
		               <div class=\"alert alert-warning\"><i class=\"alert-warning fa fa-exclamation-triangle\"></i> Die Seiten werden hinter allen anderen bestehenden Unterseiten eingehangen. Dies wird noch geändert!</div>");
            $this->registerTemplateVariable('tSpaltentitle', "Neue Seite erstellen");


        }
        else{
            $this->referringParameters=['ueberid'=>0];
            $this->referTo('newPageSetProperties');
        }
    }



    /**
     * @param int $ueberid
     * @throws Exception
     */

    function newPageSetPropertiesAction(int $ueberid)
    {
        $this->registerTemplateVariable('navFlag','neueSeite');

        $this->registerTemplate('standardv5/admin/main_seiten/seiten_neue_seite_eigenschaften');
        $this->registerTemplateVariable('tSpaltentitle', "Neue Seite erstellen");
        $this->registerTemplateVariable('SeitenbaumSpaltentitle', 'Templateauswahl');
        $this->registerTemplateVariable('aTemplateDateien',wolfi::load_template_files(__DIR__.'/../../template'));
        $this->registerTemplateVariable('sAction', '');
        $this->registerTemplateVariable('ueberid', $ueberid);
        $this->registerTemplateVariable('modul', $this->_MODUL);
    }


    /**
     * @param int $ueberid
     * @param string $name
     * @param string $titel
     * @param string $templateFile
     * @throws Exception
     * */

    public function newPageSetContentTypeAction(int $ueberid, string $name, string $titel, string $templateFile)
    {

        $id = seitenArchive::seiten_leere_seite($name, $titel, $ueberid,$templateFile);

        if ($id !== false) // Wenn die Seite erfolgreich erstellt wurde.
        {
            $this->main->inhalt->id=$id;
            $this->referTo('showPage');

        } else  // Wenn die Seite nicht erfolgreich erstellt wurde.
        {
            $this->setDispatchingMessage("Fehler: Die Seite konnte nicht gespeichert werden.");
            $this->dispatch(' newPageSetProperties');
        }
    }





###############################################################
##
##          S E I T E N - Funktionen
##
###############################################################



    /**
     * @param int $id
     * @throws Throwable
     */

    public function showPageSettingsListAction(int $id)
    {
        if($id<1)$this->dispatch('');
        else {

            /** @var template $template */

            //Lade die Daten der Seite aus der Datenbank
            $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);
            $template=unserialize($aSeitendaten['Templatedaten']);


            //lade für die bereits gewählten controller die Aktionen und die Text die Titel
            $templateControllerActions=[];
            $templateTextTitel=[];
            $changeTypeDropdownButton=[];

            foreach($template->entries as $feldLabel => $feldDaten){
                //suche für die Controller alle Actionen heraus
                if($feldDaten->type=='controller'){

                    //Quelltext laden
                    $controllerFileName='../controller/'.$feldDaten->value.'.php';
                    if(file_exists($controllerFileName)) {//throw new Exception($controllerFileName,0);
                        $quelltext = file_get_contents('../controller/' . $feldDaten->value . '.php');

                        //Suche alle Aktionen heraus
                        $aktionen = [];
                        preg_match_all('/function ([A-Za-z_]*)Action\(\)/', $quelltext, $aktionen);


                        //Array vereinfachen
                        $templateControllerActions[$feldLabel] = $aktionen[1];
                        array_unshift($templateControllerActions[$feldLabel], '%');
                    }else{
                        $templateControllerActions[$feldLabel]=[];
                    }
                }
                //Lade für alle Texte den Titel heraus:
                elseif($feldDaten->type=='text') {
                    $textDaten = seitenArchive::seiten_text_laden($feldDaten->value,$id);
                    $templateTextTitel[$textDaten['id']]=$textDaten['Titel'];
                }
                $changeTypeDropdownButton[$feldLabel] = $this->changeTypeDropdownButtonViewerHelper($id,$feldDaten->type,$feldLabel);
            }


            ## Lade die verfügbaren Controller
            ##

            $aController = $this->main->getControllerListe();


            ## Lade die Dateien mit demselben Template
            ##

            $sameTemplateFiles=seitenArchive::loadPagesWithTemplate($aSeitendaten['Templatedatei']);

            $this->registerTemplateVariable('aController',$aController);

            $this->registerTemplate('standardv5/admin/main_seiten/seiten_seite_anzeigen');
            $this->registerTemplateVariable('tSpaltentitle', '');

            $this->registerTemplateVariable('aSeitendaten', $aSeitendaten);
            $this->registerTemplateVariable('template', $template);
            $this->registerTemplateVariable('templateControllerActions', $templateControllerActions);
            $this->registerTemplateVariable('templateTextTitel', $templateTextTitel);
            $this->registerTemplateVariable('userGroupId', $this->userident->group_id);


            $this->registerTemplateVariable('navFlag','seite');
            $this->registerTemplateVariable('SeitenTitel',seitenArchive::seiten_seitendaten_laden($id)["Titel"]);

            $this->registerTemplateVariable('changeTypeDropdownButton',$changeTypeDropdownButton);

            $this->registerTemplateVariable('sameTemplateFiles',$sameTemplateFiles);
        }
    }



    /**
     * @param int $id
     * @param string $Name
     * @param string $Titel
     * @param bool $opt_verbergen
     * @param bool $opt_edit_nur_admin
     * @throws Exception
     */

    public function set_seiten_datenAction(int $id,string $Name,string $Titel,bool $opt_verbergen=false,bool $opt_edit_nur_admin=false){

        if(seitenArchive::seiten_seitendaten_aendern($id,$Name,$Titel,(int) $opt_verbergen,(int) $opt_edit_nur_admin)>0){
            $this->setDispatchingMessage('Die Seitendaten wurden erfolgreich geändert!');
        }else{
            $this->setDispatchingMessage('Die Seitendaten konnten nicht geändert werden!');
        }
        $this->referTo('showPage');
    }


    /**
     * @param int $id
     * @param string $Name
     * @param string $Titel
     * @param bool $opt_verbergen
     * @param bool $opt_edit_nur_admin
     * @throws Exception
     */

    public function set_seiten_daten_listAction(int $id,string $Name,string $Titel,bool $opt_verbergen=false,bool $opt_edit_nur_admin=false){

        if(seitenArchive::seiten_seitendaten_aendern($id,$Name,$Titel,(int) $opt_verbergen,(int) $opt_edit_nur_admin)>0){
            $this->setDispatchingMessage('Die Seitendaten wurden erfolgreich geändert!');
        }else{
            $this->setDispatchingMessage('Die Seitendaten konnten nicht geändert werden!');
        }
        $this->referTo('showPageSettingsList');
    }


    /**
     * @param int $id
     * @throws Exception
     */

    public function pageUeberSeiteFormAction(int $id){

        $seitenAnzahl = seitenArchive::main_seiten_anzahl();
        if($seitenAnzahl>1) {

            $this->registerTemplate('standardv5/admin/main_seiten/seiten_neue_position_auswahl');
            $this->registerTemplateVariable('tSeitenbaum', '');
            $this->registerTemplateVariable('tSeitendaten', "<h5>Seitenposition Ändern</h5><p>Wählen Sie die neue Position der Seite links im Seitenbaum!</p>
		               <p class=\"hinweis\">Achtung: Die Seiten werden hinter allen anderen bestehenden Unterseiten eingehangen. Dies wird noch geändert!</p>");
            $this->registerTemplateVariable('tSpaltentitle', "Neue Seite erstellen");
            $this->registerTemplateVariable('seitenId',$id);

            $this->registerTemplateVariable('navFlag','seite');
            $this->registerTemplateVariable('SeitenTitel',seitenArchive::seiten_seitendaten_laden($id)["Titel"]);

        }
        elseif($seitenAnzahl===1){
            $this->addReferringParameter('id',$id);
            $this->referTo('showPage');
        }
    }

    /**
     * @param int $id
     * @param int $ueberId
     * @param int $neuPos
     * @param string $callBackAction
     * @param int|bool $callBackId
     * @throws Exception
     */

    public function setUeberSeiteAction(int $id, int $ueberId, int $neuPos, string $callBackAction='showPage'){

        //Damit nicht einfach irgendwas aufgerufen werden kann, werden die Daten bereinigt
        if( $callBackAction!='showPage' && $callBackAction!='editPage' && $callBackAction!='Standard')$callBackAction='showPage';
        if($callBackAction=='Standard')$callBackAction='';

        //wenn die Position erfolgreich geändert werden kann
        if(seitenArchive::set_seiten_ueberid($id,$ueberId,$neuPos)){
            $this->addReferringParameter('id',$id);
            $this->referTo($callBackAction);
        }
        //wenn nicht, zurück zur Seite und einen Fehler ausgeben
        else{
            $this->setDispatchingMessage('Die ÜberId konnte nicht geändert werden!');
            $this->referTo($callBackAction);
        }

    }

    /**
     * @param int $id
     * @param int $groupId
     * @param int $lesen
     * @param int $aendern
     * @throws Exception
     */


    public function setPagePermissionAction(int $id,int $groupId,int $lesen,int $aendern){

        seitenArchive::changeGroupPagePermission($id,$groupId,$lesen,$aendern);

        $this->addReferringParameter('id',$id);
        $this->referTo('showPage');

    }



    /**
     * @param int $id
     * @throws ReflectionException|Exception
     */

    public function set_seite_loeschenAction(int $id)
    {
        //Die Funktion gibt bei Erfolg true aus
        if (seitenArchive::seiten_seite_loeschen($id)) {

            $this->setDispatchingMessage('Die Seite wurde erfolgreich gelöscht.');
            $this->referTo('');

        } else {
            $this->setDispatchingMessage('Die Seite konnte nicht erfolgreich gelöscht werden.');
            $this->referTo('showPage');
        }
    }

    /**
     * @param int $id
     * @throws Exception
     */

    public function seite_loeschenAction(int $id)
    {
        $this->registerTemplateVariable('navFlag','seite');
        $this->registerTemplateVariable('SeitenTitel',seitenArchive::seiten_seitendaten_laden($id)["Titel"]);
        $this->registerTemplate('standardv5/admin/main_seiten/seiten');
        $this->registerTemplateVariable('tSpaltentitle', "Seite loeschen");
        $this->registerTemplateVariable('tSeitendaten', '<form method="POST" action="?mod=main_seiten">
            <p>Seite wirklich l&ouml;schen?<input type="hidden" name="aktion" value="set_seite_loeschen" /><input type="hidden" name="id" value="' . $id . '" /><input type="Submit" value="L&ouml;schen" /></p>
            </form>');

    }




###############################################################
##
##          Template - Funktionen
##
###############################################################


    /**
     * Die Funktion schleust die Daten durch, damit sie nachher in der Vorschau angezeigt werden können. (Vielleicht, stimmt das auch nicht und sie wird nie aufgerufen!)
     * @param string $file Datei-Rumpf des Tempaltes
     * @throws Exception
     */
    public function show_templateAction(string $file){
        $this->registerTemplate($file);
    }


    /**
     * @param int $id
     * @throws Exception
     */

    function changeTemplateFormAction(int $id)
    {


        if(!seitenArchive::main_seite_existiert($id))$this->dispatch('');
        else {

            $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);


            $this->registerTemplate('standardv5/admin/main_seiten/seiten_seite_template');
            $this->registerTemplateVariable('tSpaltentitle', "Neue Seite erstellen");
            $this->registerTemplateVariable('SeitenbaumSpaltentitle', 'Templateauswahl');
            $this->registerTemplateVariable('aTemplateDateien',wolfi::load_template_files(__DIR__.'/../../template'));
            $this->registerTemplateVariable('iFramePreviewLink',"index.php?mod=main_seiten&aktion=preview_page&id={$aSeitendaten["id"]}");
            $this->registerTemplateVariable('oldTemplateFile',$aSeitendaten['Templatedatei']);
            $this->registerTemplateVariable('sAction', '');
            $this->registerTemplateVariable('id', $id);
            $this->registerTemplateVariable('modul', $this->_MODUL);

            $this->registerTemplateVariable('navFlag','seite');
            $this->registerTemplateVariable('SeitenTitel',seitenArchive::seiten_seitendaten_laden($id)["Titel"]);
        }
    }

    /**
     * @param int $id
     * @param string $templateFile
     * @throws Exception
     * */

    public function pageSetContentTypeAction(int $id, string $templateFile)
    {

        seitenArchive::saveTemplate($id,wolfi::empty_template_from_file($templateFile.'.tpl'),$templateFile);

        $this->main->inhalt->id=$id;
        $this->referTo('showPage');
    }




    /**
     * @param int $id SeitenId
     * @throws Throwable
     */

    public function saveTemplateStringAction(int $id, string $label, string $value){

        $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);

        $template = unserialize($aSeitendaten['Templatedaten']);

        if(in_array($label,$template->fields)){
            $template->entries[$label]->type='string';
            $template->entries[$label]->value=$value;
        }else{
            throw new Exception("Seite: $id Label: $label Wert:'$value'",72);
        }

        seitenArchive::saveTemplate($id,$template,$aSeitendaten['Templatedatei']);

        $this->main->inhalt->id=$id;
        $this->referTo('showPage');
    }


    /**
     * @param int $id
     * @param string $label
     * @param string $value
     * @throws Exception
     */

    public function saveTemplateStringInlineAction(int $id, string $label, string $value){

        $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);

        $template = unserialize($aSeitendaten['Templatedaten']);

        if(in_array($label,$template->fields)){
            $template->entries[$label]->type='string';
            $template->entries[$label]->value=$value;
        }else{
            throw new Exception("Seite: $id Label: $label Wert:'$value'",72);
        }

        seitenArchive::saveTemplate($id,$template,$aSeitendaten['Templatedatei']);

        $this->main->inhalt->id=$id;
        $this->referTo('editPage');


    }

    /**
     * @param $id
     * @param $fromId
     * @throws Exception
     */

    public function inheritTemplateAction(int $id,int $fromId){

        seitenArchive::main_seite_existiert($id);
        seitenArchive::main_seite_existiert($fromId);

        seitenArchive::inheritTemplateFromPage($id,$fromId);

        $this->addReferringParameter('id',$id);
        $this->referTo('showPage','main_seiten');

    }


###############################################################
##
##          Controller - Funktionen
##
###############################################################

    /**
     * @param int $id
     * @param string $label
     * @throws Exception
     */

    public function changePageControllerAction(int $id,string $label)
    {
        ##	Laden aller Skripte im Ordner
        $aSkripte = getDirectoryTree("../controller", array("php"),true);
        $seitenDaten = seitenArchive::seiten_seitendaten_laden($id);

        $this->registerTemplate('standardv5/admin/main_seiten/seiten_controller_auswahl');
        $this->registerTemplateVariable('tSpaltentitle', 'Ändern Sie den Skript-Controller für die Seite "' . $seitenDaten['Name'] . '" (' . $seitenDaten['id'] . ')');
        $this->registerTemplateVariable('tSeitendaten', '');
        $this->registerTemplateVariable('aSkripte', $aSkripte);
        $this->registerTemplateVariable('id', $id);
        $this->registerTemplateVariable('label',$label);

        $this->registerTemplateVariable('navFlag','seite');
        $this->registerTemplateVariable('SeitenTitel',seitenArchive::seiten_seitendaten_laden($id)["Titel"]);
    }


    /**
     * @param int $id
     * @param string $label
     * @param string $controller
     * @param string $action
     * @throws Exception
     */

    public function saveControllerAction(int $id, string $label, string $controller, string $action=''){

        $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);

        /** @var template $template */
        $template = unserialize($aSeitendaten['Templatedaten']);

        if(in_array($label,$template->fields)){
            $template->entries[$label]->type='controller';
            $template->entries[$label]->value=$controller;
            $template->entries[$label]->action=$action;
        }else{
            throw new Exception("Seite: $id Label: $label Controller:'$controller' Aktion:'$action'",72);
        }

        seitenArchive::saveTemplate($id,$template,$aSeitendaten['Templatedatei']);

        $this->main->inhalt->id=$id;
        $this->referTo('showPage');

    }


    /**
     * @param int $id
     * @param string $label
     * @param string $controller
     * @param string $action
     * @throws Exception
     */

    public function saveControllerInlineAction(int $id, string $label, string $controller, string $action=''){

        $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);

        /** @var template $template */
        $template = unserialize($aSeitendaten['Templatedaten']);

        if(in_array($label,$template->fields)){
            $template->entries[$label]->type='controller';
            $template->entries[$label]->value=$controller;
            $template->entries[$label]->action=$action;
        }else{
            throw new Exception("Seite: $id Label: $label Controller:'$controller' Aktion:'$action'",72);
        }

        seitenArchive::saveTemplate($id,$template,$aSeitendaten['Templatedatei']);

        $this->main->inhalt->id=$id;
        $this->referTo('editPage');

    }



###############################################################
##
##          T E X T - Funktionen
##
###############################################################

    /**
     * @param int $id
     * @param string $label
     * @throws Exception
     */

    public function changePageTextAction(int $id,string $label)
    {

        $aAlleTexte = seitenArchive::texte_auflisten($id);
        $seitenDaten = seitenArchive::seiten_seitendaten_laden($id);

        $this->registerTemplate('standardv5/admin/main_seiten/seiten_text_auswahl');
        $this->registerTemplateVariable('tSpaltentitle', 'Text für das Feld "'.$label.' in "' . $seitenDaten['Name'] . '"');
        $this->registerTemplateVariable('tSeitendaten', '');
        $this->registerTemplateVariable('SeitenbaumSpaltentitle', 'Alle Texte');

        $this->registerTemplateVariable('textMeldung', '');
        $this->registerTemplateVariable('textInhalt', '<p>Dies ist ein neuer Text. Ändern Sie ihn, damit er gespeichert werden kann.</p>');
        $this->registerTemplateVariable('textTitel', 'Wichtiger Titel des Textes');
        $this->registerTemplateVariable('aAlleTexte', $aAlleTexte);
        $this->registerTemplateVariable('id', $id);
        $this->registerTemplateVariable('label', $label);
        $this->registerTemplateVariable('formAktionVar', 'create_text');
        $this->registerTemplateVariable('sAction', '');


        $this->registerTemplateVariable('navFlag','seite');
        $this->registerTemplateVariable('SeitenTitel',seitenArchive::seiten_seitendaten_laden($id)["Titel"]);
    }

    /**
     * @param int $id
     * @param int $tid
     * @param string $label
     * @throws Exception
     */

    public function edit_textAction(int $id, int $tid,string $label)
    {

        $aTextdaten = seitenArchive::seiten_text_laden($tid, $id);

        $this->registerTemplateVariable('tSpaltentitle', "{$aTextdaten["Titel"]}({$tid})");
        $this->registerTemplate('standardv5/admin/main_seiten/seiten_text_editor');
        $this->registerTemplateVariable('sAction', '');
        $this->registerTemplateVariable('formAktionVar', 'set_text');
        $this->registerTemplateVariable('id', $id);
        $this->registerTemplateVariable('iTextId', $tid);
        $this->registerTemplateVariable('textTitel', $aTextdaten["Titel"]);
        $this->registerTemplateVariable('textInhalt', html_entity_decode($aTextdaten["Text"]));
        $this->registerTemplateVariable('textMeldung','');
        $this->registerTemplateVariable('tSeitendaten','');
        $this->registerTemplateVariable('label',$label);

        $this->registerTemplateVariable('navFlag','seite');
        $this->registerTemplateVariable('SeitenTitel',seitenArchive::seiten_seitendaten_laden($id)["Titel"]);
    }


    /**
     * @param int $id
     * @param int $textId
     * @param string $text
     * @param string $textTitel
     * @param string $label
     * @throws Exception|ReflectionException
     */

    public function set_textAction(int $id, int $textId, string $text, string $textTitel, string $label)
    {

        if (seitenArchive::seiten_text_aendern($textId, $text, $textTitel,$id)) {

            $this->main->deleteCachePath($id,$label,'T'.$textId);
            $this->setDispatchingMessage('Der Text wurde erfolgreich gespeichert!');
            $this->referTo('showPage');

        } //Wenn der TExt nicht gespeichert wurde, dann kommt man zurück auf die Editorseite
        else {
            $this->setDispatchingMessage( 'Der Text konnte nicht gespeichert werden, da er nicht verändert wurde oder nicht zu der angegebenen Seite gehört. Der Text wird zur Sicherheit nocheinmal angezeigt. Bitte sichern Sie ihn jetzt.');
            $this->referTo('showPage');
        }
    }


    /**
     * @param int $id
     * @param int $textId
     * @param string $text
     * @param string $textTitel
     * @param string $label
     * @throws Exception|ReflectionException
     */

    public function set_textInlineAction(int $id, int $textId, string $text, string $textTitel, string $label)
    {
        //Offset wird geändert

        $text = preg_replace(
            '/(href|src)=\&quot\;(\.\.\/)([A-Za-z0-9_\-.\/]+)\&quot\;/',
            '$1="$3"',
            $text
        );

        if (seitenArchive::seiten_text_aendern($textId, $text, $textTitel,$id)) {

            $this->main->deleteCachePath($id,$label,'T'.$textId);
            $this->setDispatchingMessage('Der Text wurde erfolgreich gespeichert!');
            $this->referTo('editPage');

        } //Wenn der TExt nicht gespeichert wurde, dann kommt man zurück auf die Editorseite
        else {
            $this->setDispatchingMessage( 'Der Text konnte nicht gespeichert werden, da er nicht verändert wurde oder nicht zu der angegebenen Seite gehört. Der Text wird zur Sicherheit nocheinmal angezeigt. Bitte sichern Sie ihn jetzt.');
            $this->referTo('editPage');
        }
    }


    /**
     * @param int $id
     * @param int $textId
     * @param string $text
     * @param string $textTitel
     * @param string $label
     * @throws ReflectionException|Exception
     */

    public function create_textAction(int $id, string $text, string $textTitel, string $label)
    {

        $textId = seitenArchive::seiten_text_erstellen($text, $textTitel,$id);
        if($textId!==false){

            $this->addReferringParameter('textId',$textId);
            $this->addReferringParameter('id',$id);
            $this->addReferringParameter('label',$label);

            $this->referTo('saveTextToPageLabel');

        } //Wenn der TExt nicht gespeichert wurde, dann kommt man zurück auf die Editorseite
        else {
            $this->setDispatchingMessage( 'Der Text konnte nicht gespeichert werden, da er nicht verändert wurde oder nicht zu der angegebenen Seite gehört. Der Text wird zur Sicherheit nocheinmal angezeigt. Bitte sichern Sie ihn jetzt.');
            $this->dispatch('changePageText');

        }
    }


    /**
     * @param int $id
     * @param string $label
     * @param int $textId
     * @throws Exception
     */

    public function saveTextToPageLabelAction(int $id, string $label, int $textId){

        $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);

        /** @var template $template */
        $template = unserialize($aSeitendaten['Templatedaten']);

        if(in_array($label,$template->fields)){

            //Alten Text aus dem Cache löschen
            $this->main->deleteCachePath($id,$label,'T'.$textId);

            //Neue Daten speichern
            $template->entries[$label]->type='text';
            $template->entries[$label]->value=$textId;
        }else{
            throw new Exception("Seite: $id Label: $label Text-ID:'$textId'",72);
        }

        seitenArchive::saveTemplate($id,$template,$aSeitendaten['Templatedatei']);

        $this->main->inhalt->id=$id;
        $this->referTo('showPage');


    }


    /**
     * @throws Exception
     */

    function showAllTextsAction()
    {
        $aAlleTexte = seitenArchive::texte_auflisten();

        $this->registerTemplate('standardv5/admin/main_seiten/seiten_waise_text_auswahl');
        $this->registerTemplateVariable('tSpaltentitle', 'Liste der aller Texte');
        $this->registerTemplateVariable('tSeitendaten', $this->getDispatchingMessage());
        $this->registerTemplateVariable('aAlleTexte', $aAlleTexte);
    }

    ##########################################################
    ##
    ## SeitenElement-Text-Funktionen
    ##
    ###########################################################




    /**
     * @param int $id
     * @param string $label
     * @throws Exception
     */

    function createSeitenElementTextAction(int $id, string $label,bool $force=false){

        //Wenn ein verdecktes Label mit einem seitenElement-Text gefüllt werden soll, wirf einen Fehler aus!
        if(substr($label,-1)=='_'){

            $this->setDispatchingMessage('<div class="alert alert-danger fa fa-exclamation-triangle"> Ein verdecktes Feld darf nicht mit einem Seitenelement-Text gefüllt werden.</div>');
            $this->referTo('showPageSettingsList');

        }else{

            $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);
            /** @var template $template */
            $template = unserialize($aSeitendaten['Templatedaten']);

            //Wenn ein alter SE-Text bereits vorhanden ist und er noch nicht gelöscht wurde
            if($template->entries[$label]->type=='set' && $force==false){

                //TODO einen alten Seitenelement-Text zum Löschen anfragen!
                $this->addReferringParameter('label',$label);
                $this->referTo('deleteSetForNewSetForm');

            }elseif($template->entries[$label]->type=='set' && $force==true){

                bobArchiv::deleteSeitenElementText($template->entries[$label]->value);

            }

            $seitenElementText = new seitenElementText();
            $seitenElementText->setId(bobArchiv::saveSeitenElementText($seitenElementText));

            $template->set_entry($label,'set',$seitenElementText->getId());

            seitenArchive::saveTemplate($id,$template,$aSeitendaten['Templatedatei']);

            $this->main->inhalt->id=$id;
            $this->referTo('showPage');

        }
    }

    /**
     * @param int $id
     * @param string $label
     * @throws Exception
     */

    function deleteSetForNewSetFormAction(int $id, string $label){

        $this->registerTemplateVariable('navFlag','seite');
        $this->registerTemplateVariable('SeitenTitel',seitenArchive::seiten_seitendaten_laden($id)["Titel"]);
        $this->registerTemplate('standardv5/admin/main_seiten/seiten');
        $this->registerTemplateVariable('tSpaltentitle', "Ein Seitenelement-Text löschen");
        $this->registerTemplateVariable('tSeitendaten', '<form method="POST" action="?mod=main_seiten">
            <div class="alert alert-danger">Wollen Sie wirklich für den Bereich "'.$label.'" die Sammlung von Seitenelementen löschen?</div>
            <input type="hidden" name="aktion" value="createSeitenElementText" />
            <input type="hidden" name="force" value="1" />
            <input type="hidden" name="id" value="' . $id . '" />
            <input type="hidden" name="label" value="' . $label . '" />
            <input type="Submit" value="Ja" /><a class="lupix-btn ml-2" target="_top" href="?mod=main_seiten&aktion=showPage&id=' . $id . '">Nein</a>
            </form>');
    }

    /**
     * @param int $id
     * @param string $label
     * @param string $file
     * @param int $setId
     * @throws Exception
     */

    function addSeitenElementToSETAction(int $id,string $label,string $file,int $setId=0){

        if($setId===0){
            $seitenDaten = seitenArchive::seiten_seitendaten_laden($id);
            /* @var $template template */
            $template = unserialize($seitenDaten['Templatedaten']);

            if ($template->entries[$label]->type != 'set') throw new Exception("Die Datei $file auf Seite $id zum Label $label.", 78);

            $setId=$template->entries[$label]->value;

        }else{

        }

        //Lade den Seitenelement-Text aus der Datenbank
        $seitenElementText =  bobArchiv::loadSeitenElementText($setId);

        //Erstelle das neue seitenElement aus dem übergebenen Dateipfad
        $neuesSeitenElement = bobArchiv::emptySeitenElementFromFile($file);

        //Prüfe ob Texte dabei sind, für die dann ein neuer text generiert werden muss!
        foreach($neuesSeitenElement->entries as $entryLabel => $entry){
            if($entry->type=='text'){
                $textId = seitenArchive::seiten_text_erstellen(
                    '<p>Text</p>',
                    'SET-Text: Seite ('.$id.') -> <i>'.$label.'</i> -> '.$file.'->'.$entryLabel,
                    $id);
                $neuesSeitenElement->set_entry($entryLabel,'text',$textId);
            }elseif($entry->type=='set'){
                $neuerSeitenElementText = new seitenElementText();
                $neuerSeitenElementText->setId(bobArchiv::saveSeitenElementText($neuerSeitenElementText));
                $neuesSeitenElement->set_entry($entryLabel,'set',$neuerSeitenElementText->getId());
            }
        }

        $seitenElementText->addSet($neuesSeitenElement);
        bobArchiv::saveSeitenElementText($seitenElementText);

        $this->referTo('showPage');

    }



    /**
     * @param int $id
     * @param int $setId
     * @param int $setNumber
     * @throws Exception
     */

    public function shiftTextElementUpAction(int $id,int $setId,int $setNumber){

        //Wenn es nicht das erste Element ist!
        if($setNumber>0) {
            //SeitenElementText laden
            $seitenElementText = bobArchiv::loadSeitenElementText($setId);

            //Aktuelles textElement herausnehmen
            $dummyElement = $seitenElementText->elements[$setNumber];

            //Oberes Element nach unten verschieben
            $seitenElementText->elements[$setNumber] = $seitenElementText->elements[$setNumber - 1];

            //Das Element nach oben verschieben
            $seitenElementText->elements[$setNumber - 1]=$dummyElement;

            bobArchiv::saveSeitenElementText($seitenElementText);

        }
        $this->addReferringParameter('id',$id);
        $this->referTo('showPage');
    }

    /**
     * @param int $id
     * @param int $setId
     * @param int $setNumber
     * @throws Exception
     */

    public function shiftTextElementDownAction(int $id,int $setId,int $setNumber){

        //SeitenElementText laden
        $seitenElementText = bobArchiv::loadSeitenElementText($setId);

        //Wenn es nicht das letzte Element ist
        if($setNumber<count($seitenElementText->elements)-1) {

            //Aktuelles textElement herausnehmen
            $dummyElement = $seitenElementText->elements[$setNumber];

            //Unteres Element nach oben verschieben
            $seitenElementText->elements[$setNumber] = $seitenElementText->elements[$setNumber + 1];

            //Das Element nach unten verschieben
            $seitenElementText->elements[$setNumber + 1]=$dummyElement;

            bobArchiv::saveSeitenElementText($seitenElementText);

        }
        $this->addReferringParameter('id',$id);
        $this->referTo('showPage');
    }

    /**
     * @param int $id
     * @param int $setId
     * @param int $setNumber
     * @throws Exception
     */

    public function deleteTextElementAction(int $id,int $setId,int $setNumber){

        //SeitenElementText laden
        $seitenElementText = bobArchiv::loadSeitenElementText($setId);

        $seitenElementTextcount = count($seitenElementText->elements);

        //wenn das Seitenelement darin ist
        if($setNumber>=0 && $setNumber<$seitenElementTextcount) {

            // Wenn es nicht das letzte ist, müssen alle anderen Elemente
            // hochgeschoben werden.
            if ($setNumber >= 0 && $setNumber < $seitenElementTextcount - 1) {

                for ($i = $setNumber; $i < $seitenElementTextcount - 1; $i++) {
                    $seitenElementText->elements[$i] = $seitenElementText->elements[$i + 1];
                }
            }

            //Das letzte Element wird nun gelöscht!
            unset($seitenElementText->elements[ $seitenElementTextcount - 1]);

            //Seitenelement Speichern!
            bobArchiv::saveSeitenElementText($seitenElementText);

        }
        $this->addReferringParameter('id',$id);
        $this->referTo('showPage');
    }

    /**
     * @param int $id
     * @param int $setId
     * @param int $setNumber
     * @param string $label
     * @param string $value
     * @throws Exception
     */

    public function saveSETStringInlineAction(int $id, int $setId, int $setNumber, string $label, string $value)
    {
        // seitenElementText laden
        $seitenElementText = bobArchiv::loadSeitenElementText($setId);

        // Nur die Daten geändert werden sollen, soll auch eine Änderung
        // in die Datenbank beschrieben werden,
        // Sonst so wird das Eintragen übersprungen.
        if($seitenElementText->elements[$setNumber]->entries[$label]->value!==$value) {
            $seitenElementText->elements[$setNumber]->entries[$label]->value = $value;
            bobArchiv::saveSeitenElementText($seitenElementText);
        }

        //Weiterleitung zur Webseite
        $this->referTo('editPage');
    }


    ################################################
    ##
    ##      Inline-Editor-Funktionen
    ##
    ################################################

    /** Zeigt den SEitenbaum an
     * @throws Exception
     */

    function showPageTreeAction(int $id){


        $gruppenSeitenRechte=seitenArchive::getPagePermissions($id);

        $this->registerTemplate('standardv5/admin/main_seiten/seiten_seitenbaum');
        $this->registerTemplateVariable('id',$id);
        $this->registerTemplateVariable('gruppenSeitenRechte',$gruppenSeitenRechte);
    }

    /**
     * @param int $id
     * @throws Exception
     */

    function showPagePropertiesAction(int $id){


        //Seitendaten aus der Datenbank laden
        $aSeitendaten = seitenArchive::seiten_seitendaten_laden($id);


        ## Übergeordnete Seite laden
        ##

        //Wenn ein ordentliche übergeordnete ID angegeben wurde, werden die Daten geladen
        if($aSeitendaten['ueber_id']>0)$ueberSeite = seitenArchive::seiten_seitendaten_laden($aSeitendaten['ueber_id']);

        //Anderfalls wird einfach die Wurzel gesetzt
        else{
            $ueberSeite=['id'=>0,'Name'=>'Wurzel'];
        }


        ## Zusatzoptionen für Admins laden        TODO REchtemanagement überarbeiten?
        ##

        //Wenn ein Admin sich das ganze ansieht, werden die Optionen angezeigt
        if($this->userident->group_id===1)
        {
            $verbergen=custom_checkbox_from_bool(
                "opt_verbergen",
                $aSeitendaten["opt_verbergen"],
                ' class="custom-control-input" id="opt_verbergen" ');
            $admin_edit=custom_checkbox_from_bool(
                "opt_edit_nur_admin",
                $aSeitendaten["opt_edit_nur_admin"],
                ' class="custom-control-input" id="opt_edit_nur_admin" ');
        }

        //Bei anderen Benutezrgruppen werden einfach die Felder leer gesetzt
        else{
            $verbergen='';
            $admin_edit='';
        }

        ## verborgene Felder laden (die auf '_' enden werden aussortiert
        ##

        /** @var template $template */
        $template = unserialize($aSeitendaten['Templatedaten']);
        foreach($template->entries as $label => $entry ){
            if(substr($label,-1)==='_') {
                if ($entry->type === 'text') {
                    $template->set_entry(
                        $label,
                        'string',
                        $this->generateUserLabelOption($this->main->inhalt->id, $label, $entry).'<a class="lupix-btn fa fa-edit" href="?mod=main_seiten&aktion=edit_text&id='.$this->main->inhalt->id.'&tid='.$entry->value.'&label='.$label.'"></a>',
                        false
                    );
                }else{
                    $template->set_entry(
                        $label,'string',$this->generateUserLabelOption($this->main->inhalt->id, $label, $entry)
                    );
                }
            }else{
                unset($template->entries[$label]);
            }
        }



        //Template daten werden immer am Ende an einem Stück gearbeitet, damit vorherige parseTemplate-Aufrufe von internen Funktionen nicht
        $this->registerTemplate('standardv5/admin/main_seiten/seiten_seite_eigenschaften');
        $this->registerTemplateVariable('verbergen',$verbergen);
        $this->registerTemplateVariable('admin_edit',$admin_edit);
        $this->registerTemplateVariable('seitenName',$aSeitendaten["Name"]);
        $this->registerTemplateVariable('seitenTitel',$aSeitendaten["Titel"]);
        $this->registerTemplateVariable('seitenId',$id);
        $this->registerTemplateVariable('ueberSeiteId',$ueberSeite['id']);
        $this->registerTemplateVariable('ueberSeiteName',$ueberSeite['Name']);
        $this->registerTemplateVariable('verborgeneTemplateFelder',$template->entries);
        $this->registerTemplateVariable('sameTemplateFiles',seitenArchive::loadPagesWithTemplate($aSeitendaten['Templatedatei']));

    }

    /**
     * @param int $id
     * @throws Exception
     */


    function showPageJavascriptInlineAction(int $id){
        $this->registerTemplate('standardv5/admin/main_seiten/seiten_seite_javascript_inline');
        $this->registerTemplateVariable('id',$id);

        $aController=$this->main->getControllerListe();
        $this->registerTemplateVariable('aController',$aController);
    }


    /**
     * @param int $id
     * @throws Exception
     */


    function showPageJavascriptAction(int $id){

        $this->registerTemplate('standardv5/admin/main_seiten/seiten_seite_javascript');
        $this->registerTemplateVariable('id',$id);
        $aController=$this->main->getControllerListe();
        $this->registerTemplateVariable('aController',$aController);
    }



    /**
     * Die Funktion erstellt einen Bootstrap-Dropdown-Button
     * @param $pageId int Id der SEite
     * @param $type string  der Typ des templateFeldes
     * @param $label string  label, das hier betroffen ist
     * @param $inline bool
     * @return string   Gibt einen Dropdown-Button für Bootstrap aus
     */

    function changeTypeDropdownButtonViewerHelper(int $pageId,string $type,string $label, bool$inline=false){
        $dropDownIcon=array('controller'=>'fa fa-code','text'=>'fa fa-file-text-o','string'=>'fa fa-header','set'=>'fa fa-puzzle-piece');
        $typeLabel=ucfirst($type);
        $inline='';//!$inline?'':'Inline';

        $returnText= <<<dropDownButton
     <div class="dropdown" style="display: inline-block">
        <button class="lupix-btn dropdown-toggle {$dropDownIcon[$type]}" data-toggle="dropdown">
        
        </button>
        <div class="dropdown-menu">
            <div class="dropdown-header">Wählen Sie einen Type aus!</div>
dropDownButton;

        //Geht für dieses Label alle Möglichkeiten durch
        foreach(array('controller','text','string','seitenelement-Text') as $typeOption){

            //beim  String wird keine Auswahlzeile angegeben, da dort inline etwas stehen wird
            if($typeOption===$type && $typeOption ==='string'){
                $returnText.='<a class="dropdown-item disabled" href="#">'.ucfirst($typeOption).'</a>';
            }
            //Für alle anderen Fälle wird der Link angegeben
            else{
                switch ($typeOption){
                    case 'string':
                        $returnText.='<a class="dropdown-item" target="_top" href="index.php?mod=main_seiten&aktion=saveTemplateString'.$inline.'&id='.$pageId.'&label='.$label.'&value=">'.ucfirst($typeOption).'</a>';
                        break;
                    case 'controller':
                        $returnText.='<a class="dropdown-item" target="_top"  onclick="choosePageController(\''.$label.'\')">'.ucfirst($typeOption).'</a>';
                        break;
                    case 'text':
                        $returnText.='<a class="dropdown-item" target="_top" href="index.php?mod=main_seiten&aktion=changePageText'.$inline.'&id='.$pageId.'&label='.$label.'">'.ucfirst($typeOption).'</a>';
                        break;
                    case 'seitenelement-Text':
                        if(substr($label,-1)==='_')break;
                        $returnText.='<a class="dropdown-item" target="_top"  href="index.php?mod=main_seiten&aktion=createSeitenElementText'.$inline.'&id='.$pageId.'&label='.$label.'">'.ucfirst($typeOption).'</a>';
                        break;
                }
            }
        }

        $returnText.='</div></div>';

        return $returnText;
    }



    /**
     * @param int $id
     * @param string $label
     * @param templateEntry|textElement $entry
     * @throws Exception
     */

    public function generateUserLabelOption(int $seitenId, string $feldName, $feldEintrag, bool $inline=false, bool $isSET=false, $setId=null,$setNumber=null){



        ## InlineText einbauen
        ##

        # Während für den Text und den String die Javascripte geändert wurden, wird hier ja kein Javscript aufgerufen
        #
        #
        $inlineText=$inline?'Inline':'';

        ## Daten vorladen: Titel des Textes bzw. Controller-actions

        if($feldEintrag->type==='text') {
            $templateTextTitel=seitenArchive::seiten_text_laden($feldEintrag->value, $seitenId)['Titel'];
        }elseif($feldEintrag->type==='controller'){
            //Quelltext laden
            $controllerFileName=__DIR__.'/../../controller/'.$feldEintrag->value.'.php';

            if(file_exists($controllerFileName)) {//throw new Exception($controllerFileName,0);
                $quelltext = file_get_contents(__DIR__.'/../../controller/' . $feldEintrag->value . '.php');

                //Suche alle Aktionen heraus
                $aktionen = [];
                preg_match_all('/ function ([  A-Za-z_]*)Action\(\)/', $quelltext, $aktionen);


                //Array vereinfachen
                $templateControllerActions[$feldName] = $aktionen[1];
                array_unshift($templateControllerActions[$feldName], '%');
            }else{
                $templateControllerActions[$feldName]=[];

            }
        }


        ## Je nach Typ unterschiedliche Dinge anzeigen.
        ##

        ob_start();

        //Nur einen Button anzeigen, wenn es nciht Name und Titel sind. Die werden zusätzlich automatisch generiert und können immer verwendet werden
        if($feldName !=='pageName' && $feldName!=='pageTitle') {


            //Zeilenanfang
            //echo '<div class="p-2" style="display: inline-block">';

            // Die Möglichkeit zu Wechseln wird nur angezeigt,
            // wenn es sich nicht um einen SeitenElementText handelt
            // Dieser hat nämlich vorgegebene Feld-Werte

            if(!$isSET)echo $this->changeTypeDropdownButtonViewerHelper($seitenId,$feldEintrag->type,$feldName,$inline);

            //Fallunterscheidung je nach Typ
            if($feldEintrag->type=='string')
            {?>
                <input type="text" id="<?=$feldName.'_'.$setId.'_'.$setNumber;?>StringValue" value="<?=$feldEintrag->value;?>" class="" />
                <a class="lupix-btn btn-secondary fa fa-save" onclick="saveTemplateString('<?=$feldName;?>',<?=$setId===null?'null':$setId;?>,<?=$setNumber===null?'null':$setNumber;?>)"></a>
            <?php       }
            elseif($feldEintrag->type=='text')
            {?>
                <a class="lupix-btn btn-outline-primary fa fa-file-o" target="_top" href="?mod=main_seiten&aktion=changePageText&id=<?=$seitenId;?>&label=<?=$feldName;?>"></a>
                <?php
            }
            elseif($feldEintrag->type=='controller') {
                echo ' ' . $feldEintrag->value . '&rarr;
                            <div class="dropdown">
                                <a type="button" class="lupix-btn btn-secondary dropdown-toggle" data-toggle="dropdown">' . ($feldEintrag->action !== '' ? $feldEintrag->action : 'Standard') . '</a>
                                <div class="dropdown-menu">';


                foreach ($templateControllerActions[$feldName] as $aktion) {
                    if ($aktion == '') {
                        $aktionsName = 'Standard';
                    } elseif ($aktion == '%') {
                        $aktionsName = 'Seitenaktion (%)';
                    } else {
                        $aktionsName = $aktion;
                    }

                    echo '<a class="dropdown-item" onclick="savePageController(' . $seitenId . ',\'' . $feldName . '\',\'' . $feldEintrag->value . '\',\'' . $aktion . '\')">' . $aktionsName . '</a>';
                }

                echo '     </div>
                            </div>';
            }
            elseif($feldEintrag->type=='set')
            {
                // wenn es sich nicht um ein verdecktes Feld handelt,
                // werden einfach die Handlungsoptionen generiert
                if(substr($feldName,-1)!=='_') {
                    $elements = bobArchiv::load_elements(__DIR__ . '/../../template');
                    ?>
                    <a class="lupix-btn btn-secondary fa fa-plus-circle" title="Ein ein neues Element ans Ende hängen"
                       onclick="addNewSeitenElementToSET<?=$feldEintrag->value;?>()"></a>
                    <script>
                        function addNewSeitenElementToSET<?=$feldEintrag->value;?>() {

                            $('#dialog').dialog('option', 'title', 'Seitenelement-Text einfügen').dialog('option', 'minHeight', 400).dialog('option', 'minWidth', 400).html('<?php

                                foreach ($elements as $bereich => $bereichsElemente) {
                                    echo "<h5>$bereich:</h5><ul>";
                                    foreach ($bereichsElemente as $element) {
                                        echo "<li><a target=\"_top\" href=\"?mod=main_seiten&aktion=addSeitenElementToSET&id=$seitenId&setId={$feldEintrag->value}&file={$element['config']['file']}&label=\" data-placement=\"bottom\"  data-toggle=\"tooltip\" title=\"{$element['config']['desc']}\">{$element['config']['name']}</a></li>";
                                        /*if ($setId !== null && $setNumber !== null) {
                                            echo "<li><a target=\"_top\" href=\"?mod=main_seiten&aktion=addSeitenElementToSET&id=$seitenId&setId=$setId&setNumber=$setNumber&label=$feldName&file={$element['config']['file']}\" data-placement=\"bottom\"  data-toggle=\"tooltip\" title=\"{$element['config']['desc']}\">{$element['config']['name']}</a></li>";
                                        }else{
                                            echo "<li><a target=\"_top\" href=\"?mod=main_seiten&aktion=addSeitenElementToSET&id=$seitenId&label=$feldName&file={$element['config']['file']}\" data-placement=\"bottom\"  data-toggle=\"tooltip\" title=\"{$element['config']['desc']}\">{$element['config']['name']}</a></li>";
                                        }*/
                                    }
                                    echo "</ul>";
                                }
                                ?>').dialog('open');

                            $('[data-toggle="tooltip"]').tooltip();

                        }
                    </script>
                    <?php
                }
                //Wenn es dagegen ein verdecktes Feld ist, wird nur die Warnung angezeigt
                else{
                    echo '<div class="alert alert-danger fa fa-exclamation-triangle"> Ändern Sie den Typ. Verdeckte Seitenelement-Texte sind nicht möglich!</div>';
                }
            }

            //Zeilenende
            //echo '</div>';
        }

        return ob_get_clean();
    }

    /**
     * @param int $id
     * @param int $tid
     * @param string $label
     * @param bool $inline
     * @return false|string
     * @throws Exception
     */

    public function textEditor(int $id, int $tid, string $label,bool $inline=true)
    {
        $aTextdaten = seitenArchive::seiten_text_laden($tid, $id);
        $this->registerTemplate('standardv5/admin/main_seiten/seiten_texteditor');
        $this->registerTemplateVariable('formAktionVar', 'set_text');
        $this->registerTemplateVariable('id', $id);
        $this->registerTemplateVariable('textId', $tid);
        $this->registerTemplateVariable('textTitel', $aTextdaten['Titel']);
        $this->registerTemplateVariable('textInhalt', html_entity_decode($aTextdaten["Text"]));
        $this->registerTemplateVariable('label', $label);
        $this->registerTemplateVariable('inline', $inline?'true':'false');
        $this->registerTemplateVariable('setId', '');
        $this->registerTemplateVariable('setNumber', '');

        return $this->parseTemplate();
    }


    /**
     * @param int $id
     * @param int $tid
     * @param string $label
     * @param bool $inline
     * @return false|string
     * @throws Exception
     */

    public function textEditorInline(int $id, int $tid, string $label, $setId=null,$setNumber=null)
    {
        $aTextdaten = seitenArchive::seiten_text_laden($tid, $id);
        $this->registerTemplate('standardv5/admin/main_seiten/seiten_texteditor');
        $this->registerTemplateVariable('formAktionVar', 'set_textInline');
        $this->registerTemplateVariable('id', $id);
        $this->registerTemplateVariable('textId', $tid);
        $this->registerTemplateVariable('textTitel', $aTextdaten['Titel']);
        $this->registerTemplateVariable('textInhalt', html_entity_decode(html_entity_decode($aTextdaten["Text"])));
        $this->registerTemplateVariable('label', $label);
        $this->registerTemplateVariable('inline', 'true');
        $this->registerTemplateVariable('setId', $setId);
        $this->registerTemplateVariable('setNumber', $setNumber);

        return $this->parseTemplate();
    }



    public function deleteCacheAction()
    {
        $this->main->deleteCache();
        $this->referTo('','main_seiten');
    }

    /**
     * @param templateEntry[] $entries
     * @param bool $inline
     * @param bool $set
     * @param null|int $setId
     * @param null|int $setNumber
     * @return templateEntry[]
     * @throws Exception
     */

    public function wrappingInlineEditorFields(array $entries,bool $inline=false,bool $set=false,$setId=null,$setNumber=null)
    {
        $coveredEntriesEditorFields='';
        //für jeden Eintrag den Auswahldialog laden
        foreach ($entries as $label => $entry) {

            if(is_string($entry)){
                die($this->main->inhalt->id.', '.$label.', '.$entry.','.$inline.','.$set.','.$setId.','.$setNumber);
            }
            $test = $this->generateUserLabelOption($this->main->inhalt->id, $label, $entry,$inline,$set,$setId,$setNumber);

            if ($entry->type === 'text')
            {
                $test .= $this->textEditorInline($this->main->inhalt->id, intval($entry->value), $label,$setId,$setNumber);
            }
            elseif($entry->type === 'set')
            {
                $test .= $this->seitenElementTextInlineEditor($this->main->inhalt->id, $entry->value, $label);
            }

            //Wenn es sich nicht um einen verdeckten String handelt werden die Edit-Optionen angezeigt.
            if (substr($label, -1) !== '_') {
                $entries[$label]->type='string';
                $entries[$label]->value=$test;
            }
            //bei verdeckten Inhalten werden diese normale geparsed
            else{

                $entries[$label]->type='string';
                $entries[$label]->value=$this->main->parseTemplateEntry($label,$entry,'');
                $coveredEntriesEditorFields.='<div class="badge badge-info" >'.$label.'</div>'.$test;

            }
            //Bei einem im HTML-Code versteckten String oder Controller wird der String normal geparst!!!
        }
        $entries['coveredEntriesEditorFields']=$coveredEntriesEditorFields;

        return $entries;

    }

    /**
     * @param int $id
     * @param int $setId
     * @param string $label
     * @throws Exception
     */

    function seitenElementTextInlineEditor(int $id, int $setId, string $label)
    {
        $setId= (int) $setId;

        $output='<div class="badge badge-info" >'.$label.'</div> <div class="lupix-set">';

        $seitenElementText = bobArchiv::loadSeitenElementText($setId);

        //

        $coveredEntriesEditorFields='';

        $numberOfElements=count($seitenElementText->elements);
        for($i=0;$i<$numberOfElements;$i++){

            $textElement = $seitenElementText->elements[$i];

            $output.='<div class="lupix-se"><span class="badge badge-secondary  m-1 p-1">'.$textElement->getName().'</span><span class="lupix-se-menu"> ';

            if($i>0)$output.='<a class="lupix-btn fa fa-chevron-circle-up m-0 p-1" target="_top" href="?mod=main_seiten&id='.$id.'&aktion=shiftTextElementUp&setId='.$setId.'&setNumber='.$i.'"></a> ';
            if($i<$numberOfElements-1)$output.='<a class="lupix-btn fa fa-chevron-circle-down m-0 p-1" target="_top" href="?mod=main_seiten&id='.$id.'&aktion=shiftTextElementDown&setId='.$setId.'&setNumber='.$i.'"></a> ';
            $output.='<a class="lupix-btn fa fa-trash m-0 p-1"  target="_top" href="?mod=main_seiten&id='.$id.'&aktion=deleteTextElement&setId='.$setId.'&setNumber='.$i.'"></a> ';

            $output.='</span>';

            // Die Template-Einträge werden nun mit Editorfeldern gefüllt
            // Wenn die Einträge verdeckt sind, enthält der preparedEntry den normalen Inhalt und
            // das Editorfeld wird in $coveredEntriesEditorFields angehängt und unter dem Template angezeigt.

            $preparedEntries = $this->wrappingInlineEditorFields($textElement->entries,true,true,$setId,$i);

            foreach($preparedEntries as $entryLabel => $entry) {
                //Wenn es sich um die Felder handelt, sollen die Variablen normal registriert werden
                if($entryLabel!=='coveredEntriesEditorFields') {
                    $this->registerTemplateVariable($entryLabel, $entry->value);
                }else{

                    //Sonst in die gesonderte Variable speichern und nicht zum Parsen verwenden.
                    $coveredEntriesEditorFields=$entry;
                }
            }

            //textElement wird geparsed
            $this->registerTemplate($textElement->file);
            $output.=$this->parseTemplate();

            //Die Editorfelder der verdeckten Felder werden ergänzt.
            $output.=$coveredEntriesEditorFields;

            $output.='</div>';
        }


        $output.='</div>';

        return $output;
    }

}
/*
 *     public function textEditorInline(int $id, int $tid, string $label,bool $inline=true)
    {
        $aTextdaten = seitenArchive::seiten_text_laden($tid, $id);
        $this->registerTemplate('standardv5/admin/main_seiten/seiten_texteditor');
        $this->registerTemplateVariable('formAktionVar', 'set_textInline');
        $this->registerTemplateVariable('id', $id);
        $this->registerTemplateVariable('textId', $tid);
        $this->registerTemplateVariable('textTitel', $aTextdaten['Titel']);
        $this->registerTemplateVariable('textInhalt', html_entity_decode($aTextdaten["Text"]));
        $this->registerTemplateVariable('label', $label);
        $this->registerTemplateVariable('inline', $inline?'true':'false');

        return $this->parseTemplate();
    }
 */



#################################################
##
##	Ende der Datei!!
##
#################################################
