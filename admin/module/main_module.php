<?php




class main_module extends adminControllerClass
{

    /**
     * @var string
     */
    protected $_BEREICH = "admin";
    /**
     * @var string
     */
    protected $_MODUL = "main_module";
    /**
     * @var string
     */
    protected $_SKRIPT = "main_module";
    /**
     * @var string
     */
    protected $_VERSION = "2.6.0";

    /**
     * userControllerClass constructor.
     * @param UserIdentity $userident
     * @param adminklasse $adminObject
     */
    function __construct(UserIdentity $userident, adminklasse $adminObject)
    {
        parent::__construct($userident, $adminObject);
    }

    /**
     *
     */
    function preload()
    {
        modulArchive::init();
        $this->registerTemplateVariable('nachricht',$this->getDispatchingMessage());

    }

    function menuAction(string $aktion = 'uebersicht')
    {

        //Menueinträge
        $menuEntries = [];
        //Anzeige Name,     Navflag,         link             ist Standard
        $menuEntries[] = ['title' => 'Übersicht', 'navFlag' => 'uebersicht', 'link' => '?mod=main_module'];
        $menuEntries[] = ['title' => 'Modul', 'navFlag' => 'zeigeModul', 'link' => '#'];
        $menuEntries[] = ['title' => 'Erstellen', 'navFlag' => 'neuesModul', 'link' => '?mod=main_module&aktion=neuesModul'];

        $navFlag='uebersicht';


        $this->registerTemplateVariable('menuEntries', $menuEntries);
        $this->registerTemplateVariable('navFlag', $navFlag);
        $this->registerTemplate('standardv5/admin/modul_menu');

    }


    ###################################################################################################################################################
    ##
    ##	Es soll standardmäßig die Übersicht angezeigt werden.
    ##
    ###################################################################################################################################################

    /**
     * @throws Exception
     */

    function Action()
    {
        $this->registerTemplateVariable('alleModule',$this->main->getAllAdminModules());
        $this->registerTemplate('standardv5/admin/main_module/start');
    }

    function zeigeModulAction(){
        /**
         * Übersicht: Meta-Daten, Dateien in einer Ordneransicht, Speicherplatz
         * Export-Funktion
         * Aktualisierung laden
         */

    }

    function neuesModulAction(){

        /**
         * prüfen, ob der name schon besetzt ist oder ein geschützter Name wie main_ oder admin_
         * alle Ordner erstellen
         * basisController im Backend
         * basisController ggf. im Frontend
         */
    }
}
