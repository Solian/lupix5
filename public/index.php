<?php
try{
    define('CALLED_FROM_INDEX', TRUE);
    define('DEVELOPING_MODE', TRUE);

    //TODO muss auch geprüft und mit einer Fehlermeldung versehen werden.
    require __DIR__ . '/../vendor/autoload.php';

    require_once('../lib/standardv5/fehlerbehandlung.lib.php');
    require_once('../lib/standardv5/funktionen.lib.php');


    $_BEREICH="front";
    $_MODUL="main_core";
    $_SKRIPT="index";
    $_VERSION="2.6.0";

    $main = new seitenklasse();

    $main->registerScript($_MODUL,$_SKRIPT,$_VERSION,$_BEREICH);

    $main->catchVariables();
    $main->identifyUser();
    $main->checkUserPermissions();

    $main->renderPage();
}
catch (Throwable $programmFehler){
    LupixExceptionHandler($programmFehler);
}
?>
