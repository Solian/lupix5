<?php

if(!isset($_POST['a'])){

?>
<!doctype html>
<html>
<head>
    <title>JavaScript RSA Encryption</title>
    <script src="../jquery.js"></script>
    <script src="jsencrypt.min.js"></script>
    <script type="text/javascript">

        // Call this code when the page is done loading.
        $(function() {

            // Run a quick encryption/decryption when they click.
            $('#testme').click(function() {

                // Encrypt with the public key...
                var encrypt = new JSEncrypt({default_key_size: 1024});
                encrypt.getKey();

                $.post( "jsencrypt.client.php", { a: "auth", gpub: encrypt.getPublicKey() } ).done(function(answer){
                    $('#input').val(encrypt.decrypt(answer));
                });
            });
        });

    </script>
</head>
<body>

<form id="sendPW" method="post" action="">
    <input type="hidden" name="a" value="auth">
<label for="input">Text to encrypt:</label><br/>
<textarea id="input" name="input" type="text" rows=4 cols=70>This is a test!</textarea><br/>
<input id="testme" type="button" value="Test Me!!!" />
</form>
</body>
</html>
<?php
}elseif($_POST['a']==='auth'){
    openssl_public_encrypt('Noch etwas laDas ist ein Testtext, wenn der zu lang wird dürfte er doch gar nicht mehr dechriffriert werden koennen.',$geheimtext,$_POST['gpub']);
    echo base64_encode($geheimtext);
}