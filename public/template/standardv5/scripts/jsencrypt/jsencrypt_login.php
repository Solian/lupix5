<?php
include('../../../../lib/standardv5/funktionen.lib.php');
ob_start();


abstract class lulu {

    const AUTH_UNIDENTIFIED = 0;
    const AUTH_GOT_USER_WITH_PUB_KEY = 1;
    const AUTH_AUTHENTICATED = 2;
    const AUTH_NO_USER_FOUND = 3;

    /**
     * @var array
     */

    static $users=[];

    /**
     * @var session[]
     */
    static $sessions=[];

    // Lade die Benutzer
    public static function init(){
        self::$users= [
            0 => ['email'=>'',
                'salt'=>'',
                'passwort'=>''],
            1 => ['email'=>'root@lupix.de',
                'salt'=>hash('sha512','jfahdjkahckdfnhakdnasck5846551684fg616dafc3asf48ar34f3'),
                'passwort'=>hash('sha512','123'.hash('sha512','jfahdjkahckdfnhakdnasck5846551684fg616dafc3asf48ar34f3'))]
        ];
    }


    public static function addNewSession(session $session){
        self::$sessions[$session->sessionkey]=$session;
    }

    public static function saveSession(session $session){
        self::addNewSession($session);
    }

    public static function loadSessions(){
        if(file_exists('session.db')){
            self::$sessions = unserialize(file_get_contents('session.db'));
        }else{
            self::$sessions=[];
        }
    }

    /**
     * @param $k
     * @return session|false
     */

    public static function loadSession($k){
        self::loadSessions();
        if(array_key_exists($k,self::$sessions)){
            return self::$sessions[$k];
        }else return false;
    }

    public static function deleteOldSession(int $sessionkey){
        if(array_key_exists($sessionkey,self::$sessions)){
            unset(self::$sessions[$sessionkey]);
            return true;
        }else return false;
    }

    public static function saveSessions(){
        file_put_contents('session.db',serialize(self::$sessions));
    }


    public static function searchForSupposedUser(string $k, string $h1){

        //Lade die Sessions
        self::loadSessions();

        //Schaue bei der entsprechenden session nach
        if(array_key_exists($k,self::$sessions))
        {
            $session=self::$sessions[$k];

            foreach(self::$users as $userId => $userData){

                if(hash('sha512',$userData['email'].$session->challenge)===$h1){
                    return array($userId,$userData);
                }

            }
        }
        //Wenn es keine Session mit dieser ID gibt, dann gib Falsch aus!
        else {
            return array(0,false);
        }
    }

    public static function checkTheSupposedUser(session $session, string $h2)
    {

        //Wenn der übergebene Hash aus der E-Mail und der Challange o erzeugt wurde
        if (hash('sha512', self::$users[$session->userId]['email'] . $session->challenge) === $h2) {
            return true;
        }
        //Wenn es keine Session mit dieser ID gibt, dann gib Falsch aus!
        return false;

    }

    public static function checkUserCredentials($l,$pw){
        //Lade die Sessions
        self::loadSessions();

        //Schaue bei der entsprechenden session nach
        if(array_key_exists($l,self::$sessions))
        {

            $session = self::$sessions[$l];
            //das pw wird mit dem salt s aus der Benuzter-Tabelle gehasht h_pw=(pw,s) und mit dem hash in der Nutzer-Tabelle verglichen:
            //
            //echo $pw.self::$users[$session->userId]['salt'].'==123'.hash('sha512','jfahdjkahckdfnhakdnasck5846551684fg616dafc3asf48ar34f3')."\n";
            if(hash('sha512',$pw.self::$users[$session->userId]['salt'])===self::$users[$session->userId]['passwort']){
                return true;
            }
        }
        return false;
    }

    /**
     * @return session
     */

    public static function createNewSession(){
        $sessionkey=zufallszahl(5);
        $challenge=zufallszahl(5);

        $session = new session($sessionkey,$challenge);

        lulu::addNewSession($session);
        lulu::saveSessions();
        return $session;
    }

    public static function prepareLogin(session $session, $h1,$g_pub)
    {
        //es wird ein benutzer gesucht, der mit der Challange n den übergebenen hash-Wert h1 bildet:
        //

        list($userId, $userData) = lulu::searchForSupposedUser($session->sessionkey, $h1);

        //wird kein Benutzer gefunden,
        if ($userId == 0) {

            //der Status der Session wird auf status=AUTH_NO_USER_FOUND gesetzt
            //bleibt der Rest, damit man nicht jetzt sich schon verrät.
            $session->loginStatus = self::AUTH_NO_USER_FOUND;
        } //wird ein Benutzer gefunden,
        else {

            //wird der benutzer user_id= in der session eingetragen
            //der Status der Session wird auf status=AUTH_GOT_USER_WITH_PUB_KEY gesetzt
            $session->loginStatus = self::AUTH_GOT_USER_WITH_PUB_KEY;
            $session->userId = $userId;
        }

        //die Challenge wird von n in o geändert und mit dem g_pub verschlüsselt zu c1=enc(o,g_pub)
        //
        $session->challenge = zufallszahl(5);
        $c1 = '';
        openssl_public_encrypt($session->challenge, $c1, $g_pub);
        $c1 = base64_encode($c1);

        //der Sessionkey wird von k --> l geändert.
        //lulu::deleteOldSession($session->sessionkey);
        $session->sessionkey = zufallszahl(5);

        //Dazu generiert er ein RSA-2048bit-Schlüsselpaar k_priv, k_pub Der private SChlüssel wird in der Session gespeichert
        // Create the private and public key
        $res = openssl_pkey_new(["private_key_bits" => 2048]);

        // Extract the private key from $res to $privKey
        $privKey = '';
        openssl_pkey_export($res, $privKey);
        $session->privKey = $privKey;

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        $session->pubKey = $pubKey["key"];

        //Server speichert die Session

        lulu::saveSession($session);
        lulu::saveSessions();

        //Die Login-Daten ausgeben
        return [
            'l' => $session->sessionkey,
            'c1' => $c1,
            'k_pub' => $session->pubKey,
            'userid' => $session->userId
        ];
    }

    public static function login($session, $h2,$c2){
        //Der Server prüft, ob die Antwort h2 mit der gespeicherten Challange o und dem verlinkten Benutzernamen übereinstimmt:
        //
        //wenn ja,
        if (lulu::checkTheSupposedUser($session, $h2)) {

            //wird aus der Chiffre c2 mit dem gespeicherten privaten Schlüssel k_priv das Passwort pw entschlüsselt
            $pw = '';
            openssl_private_decrypt(base64_decode($c2), $pw, $session->privKey);
            if (lulu::checkUserCredentials($session->sessionkey, $pw)) {

                //Bei Erfolg:
                //
                //wir der Sessionstatus status=AUTH_AUTHENTICATED,
                $session->loginStatus = lulu::AUTH_AUTHENTICATED;
                //die Sessionkey von l --> m gesetzt,
                $session->sessionkey = zufallszahl(5);

                //die RSA-Schlüssel und die Challange o werden gelöscht
                $session->privKey = '';
                $session->pubKey = '';
                $session->challenge = '';

                lulu::saveSession($session);
                lulu::saveSessions();

                //die Zeit session_time=time() neu gesetzt
                //der neue Sessionkey m wird als Cookie gesetzt und die Standardseite geladen!
                setcookie('sessionkey', $session->sessionkey);

                return true;
            } else {
                return false;
                throw new Exception("Benutzerpasswort ist falsch");
            }

        } else {
            throw new Exception("Benutzer konnte nicht identifiert werden");
        }
    }

}

lulu::init();

class session{
    var $sessionkey='';
    var $challenge='';
    var $loginStatus = lulu::AUTH_UNIDENTIFIED;
    var $userId = 0;
    var $privKey='';
    var $pubKey='';

    public function __construct($sessionkey,$challenge)
    {
        $this->sessionkey=$sessionkey;
        $this->challenge=$challenge;
    }
}

$a = $_POST['a'].$_GET['a'];

//Session Identify
if(isset($_POST['l']))$session = lulu::loadSession($_POST['l']);
elseif(isset($_POST['k']))$session = lulu::loadSession($_POST['k']);
elseif(isset($_COOKIE['sessionkey']))$session = lulu::loadSession($_COOKIE['sessionkey']);
//Wenn es ncihtmal den gibt, ist die Session also wirklich nicht da!
else{ $session=false;}
//Wenn es keien Session gibt, die passt, wird eine neue erstellt!
if($session===false) {

    $session = lulu::createNewSession();
    setcookie('sessionkey',$session->sessionkey);
}

if($session->loginStatus!==lulu::AUTH_AUTHENTICATED) {

    if (empty($a)) {


        $sessionkey = $session->sessionkey;
        $challenge = $session->challenge;

        ?>
        <!doctype html>
        <html>
        <head>
            <title>JavaScript RSA Encryption</title>
            <link rel="stylesheet" href="../../css/bootstrap/bootstrap.min.css">
            <link rel="stylesheet" href="../../css/buttons.css">
            <script src="../jquery.js"></script>
            <script src="../bootstrap.min.js"></script>
            <script src="../js-sha512/sha512.min.js"></script>
            <script src="jsencrypt.min.js"></script>

        </head>
        <body>

        <div class="container-xl">
            <div class="jumbotron">
                <h1>Lupix Login</h1>
            </div>
            <div class="row">
                <div class="col">

                    <div>
                        <script type="text/javascript">
                            function startLogin() {
                                //erstellt aus Challange n und Benutzernamen u den hash h1=h(u,n).
                                let h1 = sha512($('#loginname').val() + '<?=$challenge;?>');

                                //Dazu generiert er ein RSA-1024bit-Schlüsselpaar g_priv, g_pub

                                let encrypt = new JSEncrypt({default_key_size: 1536});
                                encrypt.getKey();


                                //liefert k, h1, g_pub an den Server zurück!
                                $.post("jsencrypt_login.php", {
                                    'a': "einloggen",
                                    'g_pub': encrypt.getPublicKey(),
                                    'h1': h1,
                                    'k': '<?=$session->sessionkey;?>'
                                }).done(function (answer) {
                                    answer = JSON.parse(answer);
                                    $('#inputL').val(answer.l);

                                    //Der Client entschlüsselt mit seinem privaten RSA-SChlüssel g_priv aus dem Chiffrat c1 --> die Challange o
                                    let o = encrypt.decrypt(answer.c1);

                                    //er stellt erneut die Antwort h2=h(u,o) aus dem Benutzernamen und der neuen Challange o
                                    $('#inputH2').val(sha512($('#loginname').val() + o));

                                    //er verschlüsselt das Passwort c2=enc(pw,k_pub)
                                    // Encrypt with the public key...
                                    var encrypt2 = new JSEncrypt();
                                    encrypt2.setPublicKey(answer.k_pub);
                                    $('#inputC2').val(encrypt2.encrypt($('#loginpasswort').val()));

                                    //er sendet l,c2,h2 an den Server, neue Seite wird aufgerufen, die RSA-SChlüssel gehen verloren!
                                    $('#loginbox').submit();
                                });
                            }


                        </script>
                        <input type="hidden" name="aktion" value="einloggen"/>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">E-Mail</span>
                            </div>
                            <input type="text" class="form-control" placeholder="registrierte E-Mailadresse" id="loginname" name="loginname">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Passwort</span>
                            </div>
                            <input type="password" class="form-control" placeholder="Passwort" id="loginpasswort" name="loginpasswort">
                            <input type="button" class="lupix-btn btn-primary form-control" value="Login" onclick="startLogin()"/>
                        </div>
                        <form action="jsencrypt_login.php" method="post" id="loginbox" name="loginbox">
                            <input type="hidden" id="inputL" name="l" value="">
                            <input type="hidden" id="inputC2" name="c2" value="">
                            <input type="hidden" id="inputH2" name="h2" value="">
                            <input type="hidden" id="a" name="a" value="einloggen">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </body>
        </html>
        <?php

    } else {

        if ($a === 'einloggen') {

            if (isset($_POST['k'])) {

                if ($session !== false) {

                    //Der Server sendet l,c1,k_pub
                    die(json_encode(lulu::prepareLogin($session,$_POST['h1'],$_POST['g_pub'])));

                } else {
                    setcookie('sessionkey');
                    die(json_encode(['l' => false]));
                }

            } elseif (isset($_POST['l'])) {

                //Der Server ordnet mit dem Sessionkey l die Anfrage der Session zu, wenn die Zeit seit der ersten Anfrage nicht zu lange her ist.
                if ($session !== false) {

                    try {
                        lulu::login($session, $_POST['h2'], $_POST['c2']);
                        echo  'Erfolg';
                    }catch(Throwable $e){
                        echo $e->getMessage();
                    }

                } else {
                    echo "Sessions timmt nciht.";
                }
            } else {
                echo "der Sessionkey l oder k stimmen für den Login nicht";
            }
        }

    }
}
else{
    echo "Eingeloggt";
}
