<?php

if(!isset($_POST['a'])){


// Create the private and public key
$res = openssl_pkey_new(["private_key_bits" => 2048]);

// Extract the private key from $res to $privKey
openssl_pkey_export($res, $privKey);
file_put_contents('key.perm',$privKey);

// Extract the public key from $res to $pubKey
$pubKey = openssl_pkey_get_details($res);
$pubKey = $pubKey["key"];

?>
<!doctype html>
<html>
<head>
    <title>JavaScript RSA Encryption</title>
    <script src="../jquery.js"></script>
    <script src="jsencrypt.min.js"></script>
    <script type="text/javascript">

        // Call this code when the page is done loading.
        $(function() {

            // Run a quick encryption/decryption when they click.
            $('#testme').click(function() {

                // Encrypt with the public key...
                var encrypt = new JSEncrypt();

                encrypt.setPublicKey($('#pubkey').val());
                $('#input').val(encrypt.encrypt($('#input').val()));

                $('#sendPW').submit();

            });
        });
    </script>
</head>
<body>


<label for="pubkey">Public Key</label><br/>
<textarea id="pubkey" rows="15" cols="65"><?=$pubKey;?></textarea><br/>
<form id="sendPW" method="post" action="">
    <input type="hidden" name="a" value="auth">
<label for="input">Text to encrypt:</label><br/>
<textarea id="input" name="input" type="text" rows=4 cols=70>This is a test!</textarea><br/>
<input id="testme" type="button" value="Test Me!!!" />
</form>
</body>
</html>
<?php
}else{
    $privKey=file_get_contents('key.perm');
    openssl_private_decrypt(base64_decode($_POST['input']),$klartext,$privKey);
    echo $klartext;
}