<?php

#################################################
##
##	 Heldenschenke Skript Standard 3.0
##
#################################################
##
##	 Name:		   	Standard Version 3
##
##	 Dateiname:  	spielerprofil.php
##
##	 Standard:	 	3.0
##
##	 Datum:			16.10.2010
##
##	 Bearbeiter: 	Remus
##
##
#################################################
#################################################
##
##	Standardvariablen
##
##	Variablen, die zur Darstellung neben de
##	der eigentlichen Ausgabe gebraucht werden.
##
#################################################
##
##  Seitenüberschrift unter der Navigation
##  -->wird unten in den Controller eingefügt
##
#################################################

$show_be = 'Spielerprofil';

#################################################
##
##	Inhalt des Skripts. Die Ausgabe wird in einen
##	Puffer umgeleitet. echo kann bedenkenlos
##	eingesetzt werden!!!!
##
#################################################

require_once('../lib/standardv5/benutzer.lib.php');

class profilControllerClass extends frontendControllerClass
{




    ###################################################################################################################################################
    ##
    ##	Es soll standardmäßig die Übersicht angezeigt werden.
    ##
    ###################################################################################################################################################

    //if (empty($aktion))
    //$aktion = 'uebersicht';


    /**
     * @throws Exception|ReflectionException
     */

    function Action()
    {
        $this->dispatch('uebersicht');

    }


    ###################################################################################################################################################
    ##
    ##	Hauptübersicht --> Menü
    ##
    ###################################################################################################################################################

    /**
     * @throws Exception
     */

    function uebersichtAction()
    {
        //Prüfung, dass man eingeloggt ist
        if ($this->userident->id == 0) throw new Exception('Übersicht des Profils', 200);

        //$rs = new rechteseite($this->main, $this->userident->id);
        //$box = '';
        //if ($this->userident->id != 0) {
            //$box = $rs->get_nachrichten_vorschau();
        //}

        $this->registerTemplate('profil/uebersicht');
        $this->registerTemplateVariable('seiteId', $this->main->inhalt->id);
        //$this->registerTemplateVariable('dispatchMessage',$this->getDispatchingMessage());

    }

    ####################################################################################################################################################
    ##
    ##  Login-Formular anzeigen
    ##
    ####################################################################################################################################################

    /**
     * @throws ReflectionException|Exception
     */

    function loginFormAction()
    {
        //Prüfung, dass man eingeloggt ist, dann wird man weitergeleitet ins Profil
        if ($this->userident->id > 0) {

            $this->registerTemplate('login/adviceForSignedInUsers');
            $this->registerTemplateVariable('username',benutzerArchive::get_benutzer_daten($this->userident->id));
            $this->setDispatchingMessage($this->parseTemplate());
            $this->dispatch('uebersicht');

        } // Wenn man nicht eingeloggt ist, soll man ein Login angezeigt bekommen
        else {
            $this->registerTemplate('login/login');
            $this->registerTemplateVariable('mit_ueberschrift', true);
        }

    }

    ####################################################################################################################################################
    ##
    ##  Profil
    ##
    ####################################################################################################################################################



    function profil_aendernAction()
    {
        if ($this->userident->id == 0) throw new Exception('Einstellungen ändern', 200);

        echo "<h1>Profil ändern</h1>";

        //Benutzerdaten laden
        $benutzerDatenArray = db::querySingle("select email, style, realername from main_benutzer where id={$this->userident->id}");

        //Liste aller Stile laden
        //$listeDerStileNamenArray=db::query("select name from main_stile");
        $listeDerStileNamenArray = [];

        $this->registerTemplate('profil/profilAendernForm');
        $this->registerTemplateVariable('email',$benutzerDatenArray['email']);
        $this->registerTemplateVariable('lengthOfMail',strlen($benutzerDatenArray['email']) + 5);
        $this->registerTemplateVariable('realerName',$benutzerDatenArray['realername']);
        $this->registerTemplateVariable('listeDerStileNamenArray',$listeDerStileNamenArray);
        $this->registerTemplateVariable('style',$benutzerDatenArray['style']);
        //$this->registerTemplateVariable('dispatchMessage',$this->getDispatchingMessage());

    }

    /**
     * @param string $email
     * @param string $realername
     * @param string $stil
     * @throws Exception
     */

    function profil_speichernAction(string $email, string $realername, string $stil = '')
    {
        if ($this->userident->id == 0) throw new Exception('Einstellungen anzeigen', 200);

        echo "<h1>Profil ändern</h1>";
        /*
                $neuerStilString = $main->get_USER_VAR('stil');
                $neuerRealerNameString = $main->get_USER_VAR('realername');
                $neueEmailString = $main->get_USER_VAR('email');
                $loginnameString = $main->get_USER_IDENT("username");
        */
        $neuerStilString = $stil;
        $neuerRealerNameString = $realername;
        $neueEmailString = $email;
        $loginnameString = $this->main->userident->name;
        $neueEMailDatumInt = 0;
        $neueEMailKeyString = '';
        $userid = $this->userident->id;

        //Gibt es ein neue E-Mail? --> dann speichere es vorerst und schicke ein Bestätigungslink heraus!
        $aktuelleNutzerDaten = db::query("select email, style, realername from main_benutzer where id=$userid;");
        $aktuelleEmailString = $aktuelleNutzerDaten[0]['email'];
        $aktuellerStilString = $aktuelleNutzerDaten[0]['style'];
        $aktuellerRealerNameString = $aktuelleNutzerDaten[0]['realername'];

        //speichere alles außer der e-Mail ab!
        db::insert("update main_benutzer set style='$neuerStilString', realername='$neuerRealerNameString' where id=$userid;");


        //Prüfe vorher, ob diese E-Mail bereits gespeichert wird bei einem anderen Nutzer
        db::query("select email,style,realername from main_benutzer where not id=$userid and email='$neueEmailString';");
        if (db::lastRowCount() > 0) {
            //throw new Exception("Eine E-Mailadresse, die bereits benutzt wird, soll verwendet werden. Dies ist nicht erlaubt.", 40);
            $this->setDispatchingMessage('<p class="fehler"><a class="fa fa-universal-access button error"></a> Die vorgeschlagene E-Mail-Adresse dürfen Sie nicht verwenden.</p>');
            $this->dispatch('profil_aendern');
            return;
        }

        if (db::lastRowCount() > 0) {
            $this->setDispatchingMessage('<p class="hinweis erfolg"><span class="fa fa-check-square-o"> </span> Die Daten wurden aktualisiert!</p><!--<p class="hinweis">Hinweis: Loggen Sie sich neu ein, damit der Stil aktiviert wird.</p>-->');
        }elseif( $neuerStilString == $aktuellerStilString && $aktuellerRealerNameString == $neuerRealerNameString){
            $this->setDispatchingMessage('<p class="fehler"><span class="fa fa-info-circle"> </span> Sie haben keine Angabe geändert. <a href="javascript:history.back();">Zur&uuml;ck</a></p>');
        } else {
            $this->setDispatchingMessage('<p class="fehler"><span class="fa fa-info-circle"> </span> Die Daten konnte nicht akualisiert werden. <a href="javascript:history.back();">Zur&uuml;ck</a></p>');
        }

        //Key generieren, der zur Bestätigung aus dem E-Mail-Konto heraus eingegeben werden muss.
        if ($aktuelleEmailString !== $neueEmailString) {


            $eMailKeyOrFalse = benutzerArchive::set_benutzer_neue_mail($this->userident->id,$neueEmailString);
            if ($eMailKeyOrFalse!==false) {
                $this->setDispatchingMessage($this->getDispatchingMessage() . '<p class="hinweis erfolg"><span class="fa fa-check-square-o"> </span> Die E-Mail wurde aktualisiert!</p><!--<p class="hinweis">Hinweis: Loggen Sie sich neu ein, damit der Stil aktiviert wird.</p>-->');

                $sendEMailSuccessFull = mail($neueEmailString, "Bestätigung der E-Mailadresse", "Travia zum Gruße, $loginnameString! \n\n ihr habt diese E-Mail als neue Kontaktadresse in der Heldenschenke angegeben. Bestätigen Sie, dass dies ihre Adresse ist und Sie diese Adresse in der Heldenschenke verwenden dürfen, in dem sie folgenden Link auswählen. Dieser link verfält nach 23 Stunden.: \n\n http://www.heldenschenke.de/index.php?id=profil&aktion=confirm_email&emailkey='.$neueEMailKeyString.'. \n\n Viel Spass in der Schenke wuenscht \n\n Der Wirt", "From: derwirt@heldenschenke.de\r\n", "-f derwirt@heldenschenke.de");
                if ($sendEMailSuccessFull == true) {
                    $this->setDispatchingMessage($this->getDispatchingMessage() . '<p class="erfolg"><span class="fa fa-check-square-o"> </span> Eine E-Mail wurde an die neue Adresse gesendet. Sie haben 24h um den Link zu betätigen, danach verfällt er!</p>');
                } else {
                    $this->setDispatchingMessage($this->getDispatchingMessage() . '<p class="fehler"><span class="fa fa-info-circle"> </span>Es konnte keine E-Mail mit dem Bestätigungslink an die neue Adresse gesendet werden. Versuchen Sie es nochmals. Kontaktieren Sie gegebenenfalls die Administratoren.</p>');
                    //echo '<p class="hinweis erfolg><a href="http://www.heldenschenke.de/index.php?id=profil&aktion=confirm_email&emailkey=' . $neueEMailKeyString . '">Bestätigen</a></p>';
                }

            } elseif ('' != $neueEmailString ) {
                $this->setDispatchingMessage($this->getDispatchingMessage().'<p class="fehler"><span class="fa fa-info-circle"> </span> Die E-Mail konnte nicht akualisiert werden. <a href="javascript:history.back();">Zur&uuml;ck</a></p>');
            }


        }
        $this->referTo('profil_aendern');
    }

    ####################################################################################################################################################
    ##
    ##  Passwort im Profil
    ##
    ####################################################################################################################################################

    function pw_aendernAction()
    {
        $this->registerTemplate('profil/passwortAendern');
    }

    function pw_speichernAction(string $neuespw1, string $neuespw2, string $altespw)
    {

        if ($this->userident->id == 0) throw new Exception('Passwort speichern', 200);

        echo "<h1>Neues Passwort</h1>";

        //Skript
        if($neuespw1!==verschluesselung2('')) {

            if ($neuespw1 === $neuespw2) {

                $res = $this->userident->setNewPassword($neuespw1, $neuespw2, $altespw);

                if ($res == UserIdentity::PW_CHANGE_SUCC) {
                    $this->setDispatchingMessage('<p class="hinweis erfolg">Das Passwort wurde erfolgreich geändert.</p>');
                } elseif ($res == UserIdentity::PW_CHANGE_OLD_PW_WRONG) {
                    $this->setDispatchingMessage('<p class="fehler">Das aktuelle Passwort stimmt nicht. Geben Sie es erneut ein.</p>');
                } elseif ($res == UserIdentity::PW_CHANGE_UNEQUAL_PWS) {
                    $this->setDispatchingMessage('<p class="fehler">Das neue Passwort und die Wiederholung stimmen nicht überein. Geben Sie diese erneut ein.</p>');
                }
            } else {
                $this->setDispatchingMessage('<p class="fehler">Die neuen Passwörter stimmen nicht überein!</p>');
            }
        } else{
            $this->setDispatchingMessage('<p class="fehler">Das Passwort darf nicht leer sein!</p>');
        }

        $this->referTo('pw_aendern');
    }

    ####################################################################################################################################################
    ##
    ##  Passwort vergessen
    ##
    ####################################################################################################################################################

    function neues_passwortAction()
    {

        if($this->userident->id>0){

            $this->setDispatchingMessage( '<p class="hinweis">Wenn Ihr eingeloggt seid, sollt ihr das Passwort im Spielerprofil direkt ändern!<br>Habt ihr das Passwort vergessen, müsst ihr euch <a href="?aktion=logout">abmelden</a> und die erneut <i>"Neues Passwort"</i> aufrufen.</p>');
            $this->dispatch('pw_aendern');

        }else{

            $this->registerTemplate('profil/passwortVergessen');
        }
    }

    function neues_passwort_sendenAction(string $nickname, string $email){

        if($this->userident->id>0){

            $this->setDispatchingMessage( '<p class="hinweis">Wenn Ihr eingeloggt seid, sollt ihr das Passwort im Spielerprofil direkt ändern!<br>Habt ihr das Passwort vergessen, müsst ihr euch <a href="?aktion=logout">abmelden</a> und die erneut <i>"Neues Passwort"</i> aufrufen.</p>');
            $this->dispatch('neues_passwort');
        }
        else {

            echo "<h1>Neues Passwort</h1>";

            //Nötige Variablen laden
            $eMailString = $email;
            $loginnameString = $nickname;

            if ($loginnameString != "" && $eMailString != "") {

                $eMailAusDerDatenbankString = db::querySingle("select email from main_benutzer where nickname='$loginnameString';")["email"];
                if ($eMailAusDerDatenbankString === $eMailString) {
                    $neuesZufallspasswortString = zufallszahl();

                    // generate salt
                    $salt = uniqid(mt_rand(), true);
                    // generate salted pw
                    $pw = hash('sha256', verschluesselung2($neuesZufallspasswortString) . $salt);


                    $sql = 'UPDATE main_benutzer SET passwort = :pw, salt = :salt
                        WHERE email = :email';
                    $args = [':pw' => $pw, ':salt' => $salt, ':email' => $eMailString];
                    db::insert($sql, $args);
                    //echo "Das neue Passwort lautet: $neuesZufallspasswortString";


                    if (db::lastRowCount() == 1) {
                        $mail_sended = mail($eMailString, "Passwort der Heldenschenke vergessen", "Travia zum Gruße, $loginnameString! \n\n Ihr habt Euer Passwort vergessen. Um die Heldenschenk wieder nutzen zu koennen, ist folgendes Passwort generiert worden: \n\n \"$neuesZufallspasswortString\". \n\n Viel Spass in der Schenke wuenscht \n\n Der Wirt", "From: derwirt@heldenschenke.de\r\n", "-f derwirt@heldenschenke.de");
                        if ($mail_sended == 1) {
                            $this->setDispatchingMessage("<p class=\"erfolg\">Das neue Passwort <i>".$neuesZufallspasswortString."</i> wurde an die angegeben Adresse gesendet.</p>");
                        } else {
                            $this->setDispatchingMessage("<p class=\"fehler\">Das neue Passwort konnte nicht an die E-MailAdresse gesendet werden. Kontaktieren Sie die Administratoren.</p>");
                        }
                    } else {
                        $this->setDispatchingMessage('<p class="fehler">Das Zufallspasswort konnte nicht in der Datenbank gespeichert werden.</p>');
                    }
                } else {
                    $this->setDispatchingMessage('<p class="fehler">Nutzername und eMail sind leider falsch.</p>');
                }
            } else {
                $this->setDispatchingMessage('<p class="fehler">Geben Sie Ihren Nutzernamen und Ihre E-Mail-Adresse ein. Diese dürfen nicht leer bleiben.</p>');
            }
        }
        $this->referTo('neues_passwort');
    }

    ####################################################################################################################################################
    ##
    ##  Avatar
    ##
    ####################################################################################################################################################

    function avatar_hochladenAction(){


        if ($this->userident->id == 0) throw new Exception('Avatar auswählen und hochladen', 200);

        echo "<h1>Avatar hochladen</h1>";

        $userid=$this->userident->id;

        $avatarPicto=urldecode(
            $this->main->picto(
                '%' . db::querySingle(
                    "select avatar from main_benutzer where id=$userid;")["avatar"],
                'max-width:100pt;margin:10pt;'
            )
        );

        $this->registerTemplate('profil/avatarHochladen');
        $this->registerTemplateVariable('avatarPicto',$avatarPicto);
    }

    function avatar_speichernAction(){

        if ($this->userident->id == 0) throw new Exception('Avatar speichern', 200);

        $neues_bild = $this->main->get_("FILES", "neues_bild");

        // Allgemeine Upload-Errorauswertung des Browsers
        if ($neues_bild["error"] > 0) {
        throw new Exception("Fehler", (219 + $neues_bild["error"]));
        }

        // Maximale Größe, wird aber nur getestet, noch nicht verkleinert!
        if ($neues_bild["size"] > 512000) {
            echo '<p class="smalltext">Die Datei ist ' . ($neues_bild["tmp_name"]["size"] / 1024) . ' groß! Das ist zu groß!</p>';
        }

        //Wenn es nun kein unterstütztes Bild format ist: png,jpg mehr nicht. alle anderen sind doof und müssen umgewandelt werden.
        if (!strstr($neues_bild["type"], "image"))
            throw new Exception($neues_bild["type"], 225); //if(!strstr($$aSizeInfo["mime"],"image")) throw new Exception($$aSizeInfo["mime"],806);
        $aSizeInfo = getimagesize($neues_bild["tmp_name"]);
        if (($aSizeInfo[2] == IMAGETYPE_BMP))
            throw new Exception("Fehler", 226);
        if (!($aSizeInfo[2] == IMAGETYPE_JPEG || $aSizeInfo["2"] == IMAGETYPE_PNG ))
            throw new Exception("Fehler", 225);

        //Prüfung, ob ein Bild hochgeladen wurde!
        if (!is_uploaded_file($neues_bild["tmp_name"])) {
            throw new Exception("Fehler", 224);
        }

        // Die Datei wird registriert und in den Bildercache verschoben.
        $sBildId = $this->main->picto_upload_avatar_reg($neues_bild);

        //echo '<p class="smalltext">Das Bild wurde eingefügt und hat nun die ID:"'.$sBildId.'". Die Resourcentabelle wurde aktualisiert.</p>';
        if ($sBildId == false) {
            $this->setDispatchingMessage('<p class="fehler"><span class="fa fa-info-circle"> </span> Das Bild konnte nicht ordnungsgemäß hochgeladen werden.</p>');
        } else {
            $this->setDispatchingMessage('<p class="erfolg"><span class="fa fa-check-square-o"> </span> Der Avatar wurde erfolgreich hochgeladen.</p>');
        }

        $this->referTo('avatar_hochladen');
    }

    ####################################################################################################################################################
    ##
    ##  Registrieren
    ##
    ####################################################################################################################################################

    function registrierenAction()
    {

        if($this->userident->id>0){
            echo '<p class="hinweis">Sie könnt Sich als eingeloggter Spieler nicht noch einmal in der Heldenschenke anmelden!</p><p><a href="?aktion=logout">Logout</a> <a href="javascript:history.back();">Zurück</a></a></p>';
        }else {
            $this->setBeHeaderText("Registrieren");

            ##### Alte Captchaaufgaben werden entfernt
            db::insert("DELETE FROM main_captcha WHERE zeit<'".(time()-3600)."'");

            #### Neue Aufgabe wird erstellt und in Datenbank geschrieben
            $captchaid = zufallszahl(5);
            $operator = rand(1, 2);
            $eins = rand(0, 10);
            $zwei = rand(0, 10);
            if ($operator == 1) {
                $captchaErgebnisInteger = $eins + $zwei;
            } else {
                $captchaErgebnisInteger = $eins * $zwei;
            }

            db::insert("INSERT INTO main_captcha (`id`, `zeit`, `operator`,`zahl1`,`zahl2`,`ergebnis`) VALUES ('$captchaid', '" . time() . "',$operator,$eins,$zwei,$captchaErgebnisInteger);");

            $this->registerTemplate('login/registrierung');
            $this->registerTemplateVariable('captchaid',$captchaid);
        }
    }

    function spieler_speichernAction(string $passwort1, string $passwort2, string $nickname, string $email, string $antwort, string $captchaid)
    {

        $passwort1String = $passwort1;
        $passwort2String = $passwort2;
        $nicknameString = $nickname;
        $eMailString = $email;
        $antwortInteger = $antwort;
        $captchaIsString = $captchaid;

        #### Lösung des Captcha aus der Datenbank holen

        $richtigesErgebnisInteger = (integer)db::querySingle("select ergebnis from main_captcha where id='$captchaIsString'")["ergebnis"];

        if (preg_match('/^([a-zA-Z0-9]+([a-zA-Z0-9]+(_|-| )[a-zA-Z0-9]+)*[a-zA-Z0-9]+){2,20}/', $nicknameString)) {
            //if(preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/',$passwort1String)){
            if ($passwort1String === $passwort2String) {
                if (preg_match('/\S+@\S+\.\S+/', $eMailString)) {
                    if ($antwortInteger !== "") {
                        $antwortInteger = (integer)$antwortInteger;
                        if ($antwortInteger >= 0) {
                            //if(document.registerbox.nickname.value.match(nicknameRegex)){
                            //    if(document.registerbox.passwort1.value.match(passwRegex)){
                            //      if(document.registerbox.passwort1.value==document.registerbox.passwort2.value){
                            //        if(document.registerbox.email.value.match(emailRegex)){
                            //          if( parseInt(document.registerbox.antwort.value)>=0){
                            #### Wenn alle Eingaben richtig bzw. nicht leer sind
                            ##### Ob der User schon exestiert
                            db::query("select nickname from main_benutzer where nickname='$nicknameString';");


                            ##### Wenn er der erste mit dies dem Namen ist
                            if (db::lastRowCount() == 0) {
                                ##### Ob die eMail schon existiert
                                db::query("SELECT email FROM main_benutzer WHERE email='" . addslashes($eMailString) . "' or new_email='" . addslashes($eMailString) . "';");


                                ##### Wenn er der erste mit dieser Mail ist
                                if (db::lastRowCount() == 0) {


                                    ##Dann wird jetzt der Freischaltungscode für die E-Mailadresse erzeugt
                                    $neueEMailKeyString = zufallszahl(20);
                                    $neueEMailDatumInt = time();

                                    ##Die E-Mail wird nicht als normale Mail gespeichert, sondern als neue Mail,
                                    #die bestätigt werden muss. So kann man sich nicht in die Schenke einloggen!, bis man die E-Mail gespeichert hat. Dann gibt es allerdings auch keine neue E-Mail!
                                    db::insert("INSERT INTO main_benutzer (`nickname`,`new_email_key`,`new_email_date`,`new_email`,`nutzergruppe_id`,`regdatum`)" .
                                        " VALUES ('$nicknameString','$neueEMailKeyString',$neueEMailDatumInt,'$eMailString',2,$neueEMailDatumInt)");

                                    // generate salt
                                    $salt = uniqid(mt_rand(), true);
                                    // generate salted pw
                                    //beachte, dass die Passwörter bereits verschlüsselt sind.
                                    $pw = hash('sha256', $passwort1String . $salt);

                                    $sql = 'UPDATE main_benutzer SET passwort = :pw, salt = :salt WHERE new_email_key = :new_email_key';
                                    $args = [':pw' => $pw, ':salt' => $salt, ':new_email_key' => $neueEMailKeyString];
                                    db::insert($sql, $args);

                                    if (db::lastRowCount() > 0) {


                                        $nachricht = '
Willkommen in der Heldenschenke,
der Wirt und seine Helfer begrüßen Euch herzlichst beim Rollenspiel-Webprojekt Heldeschenke.
In dieser Internet-Taverne gemeinsam mit anderen Online zusammen das Rollenspiel das Schwarze Auge zu spielen.
Als erstes müsst Ihr Eure E-Mailadresse freischalten über den folgenden Link:

http://www.heldenschenke.de/index.php?id=profil&aktion=confirm_email&emailkey=' . $neueEMailKeyString . '"

Viel Spaß wünschen Euch
Der Wirt und seine Helfer';
                                        $extra = "From: derwirt@heldenschenke.de\r\n";
                                        if (mail($eMailString, "Anmeldung in der Heldenschenke", $nachricht, $extra, "-f derwirt@heldenschenke.de")) {
                                            $this->addReferringParameter('email',$eMailString);
                                            $this->referTo('show_mail_success');
                                        } else {
                                            $this->addReferringParameter('email',$eMailString);
                                            $this->referTo('show_mail_failure');
                                        }
                                    } else {
                                        echo '<p class="hinweis fehler">Bei der Registrierung ist ein Fehler aufgetreten. Bitte versucht es noch einmal.<p>
                                              <p><a href="javascript:history.back()">Zurück</a></p>';

                                    }
                                } ##### Wenn schon diese eMail vorhanden ist
                                else {
                                    echo '<p class="hinweis fehler">Sie können diese E-Mailadresse nicht verwendet!</p>
                                          <p><a href="javascript:history.back()">Zurück</a></p>';
                                }
                            } ##### Wenn schon ein User mit diesem Namen Vorhanden ist
                            else {
                                echo '<p class="hinweis fehler">Ein anderer Spieler mit diesem Namen ist bereits Spieler in der Heldenschenke.</p>
                                      <p><a href="javascript:history.back()">Zurück</a></p>';
                            }
                        } else {
                            echo '<p class="hinweis fehler">Gebt die Antwort auf die Rechenaufgabe ein. Sie ist nicht kleiner als Null!</p>
                                  <p><a href="javascript:history.back()">Zurück</a></p>';
                        }
                    } else {
                        echo '<p class="hinweis fehler">Gebt die Antwort auf die Rechenaufgabe ein.!</p>
                              <p><a href="javascript:history.back()">Zurück</a></p>';
                    }
                } else {
                    echo '<p class="hinweis fehler">Die Adressen muss im Format [...]@[...].[...] angegeben werden. Achten Sie darauf, dass sie Ihre E-Mailadresse richtig eingebt!</p>
                          <p><a href="javascript:history.back()">Zurück</a></p>';
                }
            } else {
                echo '<p class="hinweis fehler">Die Passwörter müssen übereinstimmen.</p>
                     <p><a href="javascript:history.back()">Zurück</a></p>';
            }
        } else {
            echo '<p class="hinweis fehler">Der Spielername darf nicht leer sein und kann Buchstaben, Zahlen, Bindestriche, Unterstriche und Leerzeichen enthalten.
                    Zwischen den Sonderzeichen müssen mindestens zwei Zeichen liegen.</p>
                    <p><a href="javascript:history.back()">Zurück</a></p>';
        }
    }

    /**
     * @param string $emailkey
     * @throws Exception
     */

    function confirm_emailAction(string $emailkey){

        echo "<h1>Bestätigung der E-Mailadresse</h1>";

        if (!benutzerArchive::confirm_benutzer_mail($emailkey)) {

            $eMailDatenArray=db::querySingle("select new_email_date from main_benutzer where new_email_key='" . $emailkey . "';");
            if(empty($eMailDatenArray)){
                echo '<p class="fehler fa fa-info-circle" style="width:96%"> Der Link ist ungültig. Fordern Sie einen euen an oder wenden Sie sich an die Administratoren.</p>';
            }elseif ($eMailDatenArray["new_email_date"] < (time() - 86400)) {
                echo '<p class="fehler fa fa-info-circle" style="width:96%">Der Link ist nicht mehr gültig. Erstellen Sie einen neuen!</p>';
            } else {
                echo '<p class="fehler fa fa-info-circle" style="width:96%">Der Link ist fehlerhaft.  Fordern Sie einen euen an oder wenden Sie sich an die Administratoren.</p>';
            }
        } else {
            echo '<p class="erfolg fa fa-check-square-o" style="width:96%"> Die E-Mailadresse wurde bestätigt. Nun könnt ihr euch damit einloggen.</p>';
        }
    }

    function show_mail_successAction(string $email){
        $this->setBeHeaderText('Registrierung');
        $email = filter_var($email,FILTER_SANITIZE_EMAIL);

        $this->registerTemplate('profil/mailErfolg');
        $this->registerTemplateVariable('email',$email);
    }

    function show_mail_failureAction(string $email){
        $this->setBeHeaderText('Registrierung');
        $email = filter_var($email,FILTER_SANITIZE_EMAIL);

        $this->registerTemplate('profil/mailFehler');
        $this->registerTemplateVariable('email',$email);
    }




}

#################################################
##
##	Ende der Datei!!
##
#################################################



?>
