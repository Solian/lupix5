<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */


//Standard-Seitencontroller für Lupix 5

//Der Frontend

class uploadController extends frontendControllerClass
{

    protected $_BEREICH = "front";
    protected $_MODUL = "main_seiten";
    protected $_SKRIPT = "main_standardv5.php";
    protected $_VERSION = "5.0.0";

     /**
     * @throws ReflectionException
     */

    function Action(){

        $this->registerTemplate('standardv5/upload/uploadForm');

    }

    function uploadAction(uploadedFile $datei){

        $oldName=$datei->getName();
        $datei->setName(zufallszahl(8));

        $this->registerTemplate('standardv5/standard');
        $this->setBeHeaderText('Willkommen bei Lupix5');
        $this->registerTemplateVariable('titel', "Upload");

        if(move_uploaded_file($datei->getTempName(),__DIR__.'/../public/media/'.$datei->getName())){

            $this->registerTemplateVariable('inhalt', '<p>Hochgeladene Datei ('.$oldName.' '.$datei->getType().'):</p>'.
                ($datei->isImage()?'<img src="media/'.$datei->getName().'">':
                (
                    $datei->isText()?'<pre>'.file_get_contents(__DIR__.'/../public/media/'.$datei->getName()).'</pre>':
                    '<p>'.$oldName.'</p>'
                ))
            );

        }else{
            $this->registerTemplateVariable('inhalt', '<p>Datei konnte nicht hochgeladen werden!</p><p>'.print_r($datei,true).'</p>');

        }

    }

    function multiUploadFormAction(){

        $this->registerTemplate('standardv5/upload/multiUploadForm');
        $this->registerTemplateVariable('count',ini_get('max_file_uploads'));
    }

    function multiUploadAction(uploadedFiles $dateien){

        $this->registerTemplate('standardv5/standard');
        $this->registerTemplateVariable('titel', "Upload");
        $contentString='';

        for($i=0;$i<$dateien->fileCount();$i++) {

            $datei = $dateien->getFile($i);

            $oldName = $datei->getName();
            $datei->setName(zufallszahl(8));

            if (move_uploaded_file($datei->getTempName(), __DIR__ . '/../public/media/' . $datei->getName())) {


                $contentString.='<p>Hochgeladene Datei (' . $oldName . ' ' . $datei->getType() . '):' .
                    ($datei->isImage() ? '<img width="150" src="media/' . $datei->getName() . '">' :
                        (
                        $datei->isText() ? '<pre>' . file_get_contents(__DIR__ . '/../public/media/' . $datei->getName()) . '</pre>' :
                            '<p>' . $oldName . '</p>'
                        )
                    )
                ;
                $contentString.='</p>';

            } else {

                $contentString.='<p>Datei '.$oldName.' konnte nicht hochgeladen werden!</p><p>' . print_r($datei, true) . '</p>';
            }

            $this->registerTemplateVariable('inhalt',$contentString );

        }

    }

    function arrayUploadFormAction(){

        $this->registerTemplate('standardv5/upload/arrayForm');
        $this->registerTemplateVariable('count',5);
    }

    function arrayPostAction(array $ganzeZahlen){

        $this->registerTemplate('standardv5/standard');
        $this->registerTemplateVariable('titel', "Upload");
        $contentString='<p>';

        for($i=0;$i<count($ganzeZahlen);$i++) {

            $contentString .= '<p>Integer String (' . $ganzeZahlen[$i] . ' )<br>';
            $this->registerTemplateVariable('inhalt', $contentString);

        }

        $contentString.='</p>';

    }

}


#################################################
##
##	Ende der Datei!!
##
#################################################
