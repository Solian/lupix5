<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */


//Standard-Seitencontroller für Lupix 5

//Der Frontend

class statistikController extends frontendControllerClass
{

    protected $_BEREICH = "front";
    protected $_MODUL = "main_statistik";
    protected $_SKRIPT = "statistik.php";
    protected $_VERSION = "5.0.0";


    /**
     * @throws Exception
     */

    function frontendAction(){
        $this->registerTemplate('standardv5/standard');
        $this->registerTemplateVariable('titel','');
        $this->registerTemplateVariable(
            'inhalt',
            'Abfragen: <a title="Mysqlabfragen">' . db::getQueryCount() . '</a>, </a> Ausf&uuml;hrungszeit: ' . round((microtime(true) - $this->main->exec_time), 6) . 's'
        );
    }

}


#################################################
##
##	Ende der Datei!!
##
#################################################
