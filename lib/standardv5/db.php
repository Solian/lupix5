<?php

require_once (__DIR__ . '/funktionen.lib.php');

/**
 * Description of db
 *
 * @author markus
 */
class db {

    /**
     * @var null|PDO
     */
    private static $dbh = NULL;

    /**
     * @var null|string
     */
    private static $config_path = NULL;

    /**
     * @var NULL|integer
     *
     */
    private static $rowCount = NULL;

    /**
     * @var integer
     */
    private static $queryCount = 0;


    /**
     * Connect to the database.
     *
     * @throws Exception|PDOException
     */
    public static function connectForInstallDB() {
        if (db::$dbh != NULL) {
            return true;
        }


        //try {
            $c = db::getConnectionCredentials();
            db::$dbh = new PDO('mysql:host=' . $c['DB_HOST'], $c['DB_USER'], $c['DB_PASS'], array(PDO::ATTR_PERSISTENT => false));

        db::$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        db::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        db::$dbh->exec("set names utf8");
        return true;
    }

    /**
     * Connect to the database.
     *
     * @throws Exception
     */
    public static function connect() {
        if (db::$dbh != NULL) {
            return true;
        }

        try {
            $c = db::getCredentials();
            db::$dbh = new PDO('mysql:host=' . $c['DB_HOST'] . ';dbname=' . $c['DB_NAME'], $c['DB_USER'], $c['DB_PASS'], array(PDO::ATTR_PERSISTENT => false));

        } catch (PDOException $e) {
            //db::$dbh = null;
            throw new Exception($e->getMessage(), $e->getCode());
        }

        db::$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        db::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        db::$dbh->exec("set names utf8");
        return true;
    }

    /**
     * Connects to the Mysql database
     *
     * @throws Exception
     */

    public static function test_connect() {
        if (db::$dbh != NULL) {
            return true;
        }

        // try connect to GitLab database
        //if (defined('GITLAB')) {
        //    db::connectGtiLab();
        //    return;
        //}

        //print ('... Try to use config credentials!\n');
        try {
            $c = db::getCredentials();
            db::$dbh = new PDO('mysql:host=' . $c['DB_HOST'] . ';dbname=' . $c['DB_NAME'], $c['DB_USER'], $c['DB_PASS'], array(PDO::ATTR_PERSISTENT => false));
            return true;

        } catch (PDOException $e) {
            db::$dbh = null;
            //throw new Exception($e->getMessage(), $e->getCode());
            return false;
        }
    }

    /**
     * Disconnects from the database.
     */
    public static function disconnect() {
        if (db::$dbh != NULL) {
            db::$dbh = NULL;
        }
    }

    /**
     * Set the name of the database to use.
     *
     * @param string $db_name The database name.
     * @return boolean
     * @throws Exception
     * @throws Exception
     */
    public static function setDBName($db_name) {
        if ($db_name != NULL && $db_name != "") {
            db::query("USE " . $db_name, NULL);
            return TRUE;
        }
        return FALSE;
    }
    /**
     * @throws Exception
     */

    public static function getCredentials() {

        $path = (db::$config_path != NULL) ? db::$config_path : __DIR__ . '/../../inc/config.ini.php';
        if (!file_exists($path)) {
            throw new Exception($path,90);
        }

        $config = parse_ini_file($path);
        if (!$config) {
            throw new Exception($path,91);
        }

        if (array_key_exists('DBHOST', $config) && array_key_exists('DBUSER', $config) &&
                array_key_exists('DBPASS', $config) && array_key_exists('DBNAME', $config)) {

            $c = [];
            $c['DB_USER'] = $config['DBUSER'];
            $c['DB_HOST'] = $config['DBHOST'];
            $c['DB_PASS'] = $config['DBPASS'];
            if(!isset($config['DBNAME']))  throw new Exception('',6);
            $c['DB_NAME'] = $config['DBNAME'];

            return $c;
        }

        throw new Exception('',6);
    }

    /**
     * @throws Exception
     */

    public static function getConnectionCredentials() {

        $path = (db::$config_path != NULL) ? db::$config_path : __DIR__ . '/../../inc/config.ini.php';
        if (!file_exists($path)) {
            throw new Exception($path,90);
        }

        $config = parse_ini_file($path);
        if (!$config) {
            throw new Exception($path,91);
        }

        if (array_key_exists('DBHOST', $config) && array_key_exists('DBUSER', $config) &&
                array_key_exists('DBPASS', $config)) {



            $c = [];
            $c['DB_USER'] = $config['DBUSER'];
            $c['DB_HOST'] = $config['DBHOST'];
            $c['DB_PASS'] = $config['DBPASS'];

            return $c;
        }

        throw new Exception($path,91);
    }

    /**
     * Set the path to the config file with the database credentials.
     *
     * @param string $path Path to the config file.
     */
    public static function setConfigPath($path) {
        if ($path != NULL && $path != "") {
            db::$config_path = $path;
        }
    }

    /**
     * The methode shoud used for queries with one excepted result row.
     *
     * @param string $sql The sql query.
     * @param array $args Optional parameters.
     * @return mixed
     * @throws Exception
     * @throws Exception
     */
    public static function querySingle($sql, $args = null) {
        self::$rowCount=0;
        $result = db::query($sql, $args);
        self::$queryCount++;
        if (count($result) == 1) {
            return $result[0];
        }
        return FALSE;
    }

    /**
     * The methode could used for select queries.
     *
     * @param string $sql The sql query.
     * @param array $args Optional parameters.
     * @return array
     * @throws Exception
     */
    public static function query($sql, $args = []) {
        if (self::$dbh === NULL) {
            self::connect();
        }

        if(preg_match('/(:[a-zA-Z]+[0-9_]*)+/',$sql)===1 and empty($args))throw new Exception($sql,92);

        self::$rowCount=0;
        try {

            self::$queryCount++;

            //try {
            $sth = db::$dbh->prepare($sql);
            $sth->execute($args);
            db::$rowCount = $sth->rowCount();
            if (db::$rowCount > 0) {
                return $sth->fetchAll();
            }
            return [];
        } catch (Throwable $e) {
            throw new Exception($e->getCode() . "\n" . $e->getMessage() . "\n" . showQuery($sql, $args)."\n".print_r( db::$dbh->errorInfo(),true), 1000);
        }
    }

    /**
     * This methode should used for delete queries.
     *
     * @param string $sql The sql query.
     * @param array $args Optional parameters.
     * @return integer The last inserted id.
     * @throws Exception
     */
    public static function delete($sql, $args = []) {
        return db::insert($sql, $args);
    }

    /**
     * This methode should used for update queries.
     *
     * @param string $sql The sql query.
     * @param array $args Optional parameters.
     * @return integer The last inserted id.
     * @throws Exception
     */
    public static function update($sql, $args = []) {
        return db::insert($sql, $args);
    }

    /**
     * This methode should used for insert queries.
     *
     * @param string $sql The sql query.
     * @param array $args Optional parameters.
     * @return integer The last inserted id.
     * @throws Exception
     */
    public static function insert($sql, $args = []) {

        if(preg_match('/(:[a-zA-Z]+[0-9_]*)+/',$sql)===1 and empty($args))throw new Exception($sql,92);

        if (db::$dbh == NULL) {
            db::connect();
        }

        self::$rowCount=0;

        self::$queryCount++;

        try {
            $sth = self::$dbh->prepare($sql);
            $sth->execute($args);
            self::$rowCount = $sth->rowCount();
            return  self::$dbh->lastInsertId();
        } catch (Throwable $e) {
            throw new Exception($e->getCode() . "\n" . $e->getMessage() . "\n" . showQuery($sql, $args), 1000);
        }
    }

    public static function lastRowCount() {
        return self::$rowCount;
    }


    public static function getQueryCount() {
        return self::$queryCount;
    }


}
