<?php


class seitenElementText {

    /**
     * @var integer|null
     */

    protected $id=null;

    /**
     * @var textElement[]
     */

    public $elements=[];

    /**
     * bausatzText constructor.
     */
    public function __construct($id=null)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return textElement[]
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @param textElement[] $elements
     */
    public function setElements(array $elements): void
    {
        $this->elements = $elements;
    }

    /**
     * @return bool
     */

    public function isIdNull(){
        return ($this->id===null);
    }

    /**
     * @param textElement $textElement
     */

    public function addSet($textElement){
        $this->elements[]=$textElement;
    }

    /**
     * @param int $position
     * @return textElement
     * @throws Exception
     */

    public function getElementAtPosition(int $position):textElement
    {
        if($position>=0 and $position<count($this->elements))return $this->elements[$position];
        else throw new Exception($position,74);
    }


}
