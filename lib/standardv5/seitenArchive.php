<?php

#################################################
##
##	 Bibliothek: Seitenzugriffs Funktionen
##
#################################################
##
##	 Name:		   		Seitenzugriffsfunktion
##
##	 Dateiname:    	seiten.lib.php
##
##	 Version:	   		1.0
##
##	 Datum:					9.3.2010
##
##	 Bearbeiter: 		Ralf
##
##
#################################################




class seitenArchive extends modulBaseClass
{

    protected static $MODUL = 'main_seiten';

    protected static $daUnsortierteSeiten=array();

    protected static $sortedPageTree=array();

    /**
     * @var array
     */
    static $userPermissionFromDatabase=array();
    static $userPermissionQuery='';

    /**
     * @param int $permissionLevel
     * @param null|int $seiteId
     * @param bool $schreiben
     * @throws Exception
     */

    static protected function checkPagePermission($seiteId=null,$schreiben=false) :void
    {
        //Soll eine Seitenprüfung durchgeführt werden
        if($seiteId!==null){

            //Parameter setzen
            if($schreiben===false)$schreiben='lesen';
            else $schreiben='aendern';

            //Liegt eine Benutzerberechtigung für die Seite vor?
            if(!self::get_seiten_userperm($seiteId,$schreiben)){

                //Zugriffsfehler, darf nicht lesen
                if($schreiben==='lesen')throw new Exception(self::$MODUL,203);

                //Zugriffsfehler, darf nicht schreiben
                else throw new Exception(self::$MODUL,204);
            }
        }else{
            throw new Exception('Seite: '.$seiteId,301);
        }
    }

    /**
     * @return int
     * @throws Exception
     */

    public static function main_seiten_anzahl(){
        self::checkPermission(USERGROUPS::GUEST);
        $sql = "select count(id) from main_seiten order by main_seiten.id;";
        return (int) db::querySingle($sql)['count(id)'];
    }

    /**
     * Die Funktion prüft, ob eine Seite in der Datenbank verfügbar ist.
     * @param $id int Die ID der Webseite
     * @return bool
     * @throws Exception
     */

    public static function main_seite_existiert(int $id){
        //Prüfen, ob die Seite existiert.
        $sql = 'SELECT main_seiten.id
                    FROM main_seiten
                    WHERE main_seiten.id=:sid;';
        db::query($sql, [':sid' => $id]);

        //Wenn sie nicht existiert
        if (db::lastRowCount() == 1)return true;
        elseif(db::lastRowCount() == 0)return false;
        else throw new Exception('Seiten-Id:'.$id,80);
    }

    /**
     * @param bool $force
     * @return array
     * @throws Exception
     */

    public static function main_seiten_seitenbaum($force=false)
    {

        ##	Nur Administratoren d�rfen alles, daher wird hier die Gruppenzugeh�rigkeit gepr�ft und dann die Ladebedingung gepr�ft
        ##

        self::checkPermission(USERGROUPS::GUEST);

        if(self::$userIdent->group_id!=1)
        {
            $sql = "select distinct main_seiten.id, main_seiten.Name, main_seiten.ueber_id from main_seiten, main_seiten_ber where (main_seiten_ber.benutzer_id=".self::$userIdent->id." or main_seiten_ber.nutzergruppe_id=".self::$userIdent->group_id.") and main_seiten_ber.seite_id=main_seiten.id and not main_seiten.id=0 order by main_seiten.position";
        }else{
            $sql = "select distinct main_seiten.id, main_seiten.Name, main_seiten.ueber_id from main_seiten order by main_seiten.position;";
        }

        //Lade den Seitenbaum des Benutzers, wenn er aktuell leer ist
        if(empty(self::$sortedPageTree) || $force===true) {

            ## Laden des Seitenbaums
            ##

            self::$daUnsortierteSeiten = db::query($sql);

            ## Seitenbaum wird in einen verschachtelten array umgewandelt
            ##
            /*$aSortierteSeiten = $this->get_teilbaum_array(0,0);

            $aSortierteSeiten=array_reverse($aSortierteSeiten,true);
            $aSortierteSeiten=array_reverse($aSortierteSeiten,true);

            $aSortierteSeiten = $this->get_teilbaum_array(0,0);

            $aSortierteSeiten=array_reverse($this->get_teilbaum_array(0,0),true);
            $aSortierteSeiten=array_reverse(array_reverse($this->get_teilbaum_array(0,0),true),true);

            */
            self::$sortedPageTree = array_merge(array(array("ebene" => 0, "id" => 0, "ueber_id" => 0, "Name" => "Wurzel")), array_reverse(array_reverse(self::get_teilbaum_array(0, 0), true), true));
        }

        return self::$sortedPageTree;
    }

    /**
     * @param bool $force
     * @return array
     * @throws Exception
     */

    static public function seitenbaum_array($force=false){
        ##	Nur Administratoren d�rfen alles, daher wird hier die Gruppenzugeh�rigkeit gepr�ft und dann die Ladebedingung gepr�ft
        ##

        self::checkPermission(USERGROUPS::GUEST);

        if(self::$userIdent->group_id!=1)
        {
            $sql = "select distinct main_seiten.id, main_seiten.Name, main_seiten.ueber_id from main_seiten, main_seiten_ber where (main_seiten_ber.benutzer_id=".self::$userIdent->id." or main_seiten_ber.nutzergruppe_id=".self::$userIdent->group_id.") and main_seiten_ber.seite_id=main_seiten.id and not main_seiten.id=0 order by main_seiten.position";
        }else{
            $sql = "select distinct main_seiten.id, main_seiten.Name, main_seiten.ueber_id from main_seiten order by main_seiten.position;";
        }

        //Lade den Seitenbaum des Benutzers, wenn er aktuell leer ist
        if(empty(self::$sortedPageTree) || $force===true) {

            ## Laden des Seitenbaums
            ##

            self::$daUnsortierteSeiten = db::query($sql);

            self::$sortedPageTree = array("Wurzel:0"=> self::sort_teilbaum_array(self::$daUnsortierteSeiten));
        }

        return self::$sortedPageTree;
    }

    /**
     * @param array $pageList
     * @param int $ueberseite
     * @return array
     */

    static public function sort_teilbaum_array(array $pageList,int $ueber_id=0){

        //result_array
        $resultArray = [];
        //lade alle Seiten, mit der aktuellen überseite
        foreach($pageList as $pageData){
            if($pageData['ueber_id']==$ueber_id){
                $resultArray[ $pageData['Name'] . ':' . $pageData['id'] ] = self::sort_teilbaum_array( $pageList , $pageData['id'] );
            }
        }
        return $resultArray;
    }

    /**
     * @param array $teilBaum
     * @return bool|mixed
     * @throws Exception
     */


    public static function get_startseite_id(array $teilBaum=array(),$modus='lesen'){

        $pageIdOrFalse=false;

        //wenn der Teilbaum nicht benannt ist, wird der SEitenbaum in seiner Gesamtheit geladen (ggf. gecached) und dann übernommen
        if(empty($teilBaum)) {

            //wenn der Baum leer ist, muss er natürlich geladen werden
            if (empty(self::$sortedPageTree)) self::seitenbaum_array(true);

            //Lade alle Berechtigungen, wen sei ncih
            self::load_benutzer_berechtigungen();

            //Das root Element ist keine Seite, und kann daher auch vergessen werden
            $teilBaum = self::$sortedPageTree["Wurzel:0"];

        }

        //ALle Seiten werden nun durchsucht
        foreach($teilBaum as $pageNameDotDotID=>$unterBaum){

            //Lade die Seiten ID
            list($pageName,$pageId)=explode(':',$pageNameDotDotID);
            unset($pageName);
            $pageId = (int) $pageId;

            //Wenn man lesen darf, gib die Unterseite zurück
            if(self::get_seiten_userperm($pageId,$modus)){
                return $pageId;
            }
            //Wenn man nicht lesen darf, dann suche in den unterseiten und nicht der teilbaum nicht leer ist!
            elseif(!empty($unterBaum)){
                //Wird geprüft, ob eine der Unterseiten lesbar ist
                $pageIdOrFalse=self::get_startseite_id($unterBaum,$modus);
            }
        }

        if($pageIdOrFalse===false)throw new Exception('Userid:'.self::$userIdent->id,206);
        return $pageIdOrFalse;

    }


    /**
     * @param int $iSeitenId
     * @param int $iAlteEbene
     * @return array
     * @throws Exception
     */

    public static function get_teilbaum_array(int $iSeitenId,int $iAlteEbene)
    {

        self::checkPagePermission($iSeitenId);
        //$aQuellArray=$$sNameQuellArray;

        $aUnterSeiten=array();
        $iAktuelleEbene=$iAlteEbene+1;
        $iArrayLen=count(self::$daUnsortierteSeiten);
        for($i=0;$i<$iArrayLen;$i++)
        {
            if( self::$daUnsortierteSeiten[$i]["ueber_id"]==$iSeitenId )
            {
                $aUnterSeiten[]=array_merge(array( "ebene" => $iAktuelleEbene ),self::$daUnsortierteSeiten[$i]);
                $aUnterUnterSeiten=self::get_teilbaum_array( self::$daUnsortierteSeiten[$i]["id"],$iAktuelleEbene);
                $aUnterSeiten=array_merge($aUnterSeiten,$aUnterUnterSeiten);
            }
        }
        return $aUnterSeiten;
    }

    /***
     * @param string $argName
     * @param string $argTitel
     * @param int $argUeberid
     * @param string $templateFile
     * @return int
     * @throws Exception
     */

    public static function seiten_leere_seite(string $argName,string $argTitel,int $argUeberid, string $templateFile)
    {
        self::checkPermission(USERGROUPS::ADMIN);

        $emptyTemplate = wolfi::empty_template_from_file($templateFile.'.tpl');
        $serializedTemplate = serialize($emptyTemplate);

        $seitenPosition = db::querySingle('select count(id) from main_seiten where ueber_id=:id',[':id'=>$argUeberid])['count(id)'];

        $seiteId = db::insert(
            "insert into main_seiten (Name, Titel, navigruppe_id, opt_cachen, opt_verbergen, opt_zus_navi_position, Titelbild, ueber_id,position, opt_edit_nur_admin,Templatedaten,Templatedatei) 
                values (:argName, :argTitel, 0, 0, 0, 1, '', :argUeberid,:position, 0,:templateDaten,:templateDatei);",
            array(":argName"=>$argName,":argTitel"=>$argTitel,":argUeberid"=>$argUeberid,':templateDaten'=>$serializedTemplate,':templateDatei'=>$templateFile,':position'=>$seitenPosition)
        );

        //der anlegenden Person muss das Zugriffs- und Änderungsrecht gewährt werden, damit er weiterarbeiten kann,
        // auch ein Admin erhält sie, damit die Rechte über Seiten, die er persönlich erstellt hat!
        benutzerArchive::benutzer_seitenberechtigung_erstellen($seiteId,self::$userIdent->id,true,true);

        return $seiteId;
    }

    /**
     * @param int $iSeiteid
     * @return mixed
     * @throws Exception
     */

    public static function seiten_seitendaten_laden(int $iSeiteid ){

        self::checkPagePermission($iSeiteid);

        $tSeitendaten=db::querySingle("select * from main_seiten where id=:id;",array(':id'=>$iSeiteid));
        if(db::lastRowCount()!==1)throw new Exception('Seite:'.$iSeiteid,80);

        if( $tSeitendaten["opt_edit_nur_admin"]==0  || ($tSeitendaten["opt_edit_nur_admin"]==1 and self::$userIdent->group_id==USERGROUPS::ADMIN) )
        {
            return $tSeitendaten;
        }
        else throw new Exception('Seite: '.$iSeiteid.' Userid:'.self::$userIdent->id,931);
    }


    ##  Seitendaten aendern
    ##

    /**
     * @param $iSeiteid
     * @param $sName
     * @param $sTitel
     * @param $iOptVerbergen
     * @param $iEditNurAdmin
     * @return int|NULL
     * @throws Exception
     */

    public static function seiten_seitendaten_aendern($iSeiteid, $sName, $sTitel,  $iOptVerbergen, $iEditNurAdmin)
    {
        self::checkPermission(USERGROUPS::ADMIN,true);

        $sql="update main_seiten set Name=:sName,Titel=:sTitel , opt_verbergen=:iOptVerbergen ,opt_edit_nur_admin=:iEditNurAdmin where id=:iSeiteid";
        db::update($sql,array(":iSeiteid"=>$iSeiteid,":sName"=>$sName,":sTitel"=>$sTitel,":iOptVerbergen"=>$iOptVerbergen,":iEditNurAdmin"=>$iEditNurAdmin));

        return db::lastRowCount();
    }


    /**
     * @param int $id
     * @param int $ueberId
     * @param int $neuPos
     * @return bool
     * @throws Exception
     *
     */

    public static function set_seiten_ueberid(int $id, int $ueberId, int $neuPos){

        //Rechte prüfen
        self::checkPermission(USERGROUPS::ADMIN,true);

        //Prüfen, ob die Seiten existieren
        if(!self::main_seite_existiert($id))throw new Exception($id,10);
        if(!self::main_seite_existiert($ueberId) && $ueberId!==0)throw new Exception($ueberId,10);

        $alteDaten = self::seiten_seitendaten_laden($id);
        $alteUeberId = $alteDaten['ueber_id'];
        $altePosition = $alteDaten['position'];

        //Prüfen, ob die sich die ÜberId überhaupt ändert
        if($alteUeberId!==$ueberId) {

            //Die Seiten unterhalb der alten Position hochrutschen lassen
            db::update(
                'update main_seiten set main_seiten.position=main_seiten.position-1 where main_seiten.ueber_id=:ueberId and main_seiten.position>:position',
                [
                    ':ueberId'=>$alteUeberId,
                    ':position'=>($altePosition)
                ]
            );

            //Die Seiten ab der neuen Position herunterrutschen lassen
            db::update(
                'update main_seiten set main_seiten.position=main_seiten.position+1 where main_seiten.ueber_id=:ueberId and main_seiten.position>:position_m1',
                [
                    ':ueberId'=>$ueberId,
                    ':position_m1'=>($neuPos-1)
                ]
            );

            //Über ID in der Db ändern
            $sql = "update main_seiten set ueber_id=:ueber_id, position=:position where id=:iSeiteid";
            db::update(
                $sql,
                array(
                    ":iSeiteid" => $id,
                    ":ueber_id" => $ueberId,
                    ':position'=>$neuPos

                )
            );




            return true;
        }

        //wenn nix geändert werden musste.
        return false;
    }


    /**
     * @param $iSeiteid
     * @param $iTextid
     * @return mixed
     * @throws Exception
     */
    public static  function seiten_text_laden($iTextid,$iSeiteid) {

        self::checkPagePermission($iSeiteid);

        $result = db::querySingle("select Text,id,Titel from main_texte where id=:iTextid;", array(":iTextid" => $iTextid));

        if(empty($result)){throw new Exception("Seite-Id: $iSeiteid TextId:$iTextid",11);}
        else return $result;
    }



    /**
     * @param $textId int
     * @param $text string
     * @param $textTitel string
     * @param $id int Seiten-Id
     * @throws Exception
     * @return bool
     */

    public static function seiten_text_aendern($textId, $text, $textTitel,$id){
        self::checkPagePermission($id,true);

        db::update("update main_texte set Text=:Text,Titel=:Titel where id=:iTextid;", array(":iTextid" => $textId,':Text'=>$text,':Titel'=>$textTitel));
        if(db::lastRowCount()>0)return true;
        else return false;
    }


    /**
     * @param $text
     * @param $textTitel
     * @param $id
     * @return int
     * @throws Exception
     */

    public static function seiten_text_erstellen($text, $textTitel,$id){
        self::checkPagePermission($id,true);
        $textId = db::insert(
            "insert into main_texte (Text, Titel, autor_id, aenderung_datum, seite_id, erstell_datum) values (:Text,:Titel,:autor,:date,0,:date)",
            array(':Text'=>$text,':Titel'=>$textTitel,':autor'=>self::$userIdent->id,':date'=>time())
        );
        if(db::lastRowCount()>0)return $textId;
        else throw new Exception("Fehler", 923);
    }






    /**
     * @param $iSeiteid
     * @return array
     * @throws Exception
     */
    public static function seiten_unternavigation_laden($iSeiteid)
    {
        self::checkPermission(USERGROUPS::GUEST);;

        $sql=' 	SELECT main_seiten_navigruppen.list_index, main_navigruppen_elemente.list_index, main_navigruppen.id, main_navigruppen.Name, main_navigruppen.Titel, main_navigruppen_elemente.seite_id, main_navigruppen_elemente.navicontainer_id, main_navicontainer.Text, main_navicontainer.Name, main_seiten.Titel
            FROM main_seiten_navigruppen, main_navigruppen, main_navigruppen_elemente, main_navicontainer, main_seiten
            WHERE main_seiten_navigruppen.seite_id =:iSeiteid
            AND main_seiten_navigruppen.navigruppe_id = main_navigruppen.id
            AND main_navigruppen.id = main_navigruppen_elemente.navigruppe_id
            AND main_navicontainer.id = main_navigruppen_elemente.navicontainer_id
            AND main_seiten.id = main_navigruppen_elemente.seite_id
            ORDER BY main_seiten_navigruppen.list_index, main_navigruppen_elemente.list_index
            LIMIT 0 , 30 ';

        return db::query($sql,array(":iSeiteid"=>$iSeiteid));

    }


    /**
     * @param int $id
     * @param int $gid
     * @param int $lesen
     * @param int $aendern
     * @throws Exception
     */
    public static  function changeGroupPagePermission( int  $id, int $gid, int $lesen, int $aendern)
    {
        //PRüfen, ob zu dieser Seite auch die Rechte vorliegen
        self::checkPagePermission($id,true);

        //setze die EIngaben auf orgendliche Maße
        if($lesen>0)$lesen=1;
        if($aendern>0)$aendern=1;

        //Prüfen, ob die Seite existiert
        if(count(db::query('select seite_id from main_seiten_ber where seite_id=:id and nutzergruppe_id=:gid',[':gid'=>$gid,':id'=>$id]))>0){
            db::update("update main_seiten_ber set `lesen`=:lesen,`aendern`=:aendern where `seite_id`=:id and `nutzergruppe_id`=:gid;",
                [':id'=>$id,':gid'=>$gid,':lesen'=>$lesen,':aendern'=>$aendern]);
        }else{

            db::update("insert into main_seiten_ber ( `seite_id`,`texte_id`,`nutzergruppe_id`,`benutzer_id`,`lesen`,`aendern` ) values (:id,NULL,:gid,NULL,:lesen,:aendern);",
            [':id'=>$id,':gid'=>$gid,':lesen'=>$lesen,':aendern'=>$aendern]);

        }
    }

    /**
     * @param bool $force
     * @return string
     * @throws Exception
     */

    public static function erstelle_seitenbaum($force=false){

        self::checkPermission(USERGROUPS::ADMIN);

        //Lädt für die Anzeige den Seitenbaum auf der linken Seite
        $aSortierteSeiten=self::main_seiten_seitenbaum($force);
        $tSeitenbaum="";

        //Sortiert alle Seiten nacheinander in die Ebenen ein!
        foreach($aSortierteSeiten as $aSeite)
        {
            if($aSeite["ebene"]==0)
            {
                $tSeitenbaum .= "<i class=\"fa fa-globe\"></i> {$aSeite["Name"]}<br />\n\r";
            }
            else
            {
                if(seitenArchive::get_seiten_userperm($aSeite["id"],"aendern"))
                {
                    $sEditLink="<a href=\"?mod=main_seiten&aktion=showPage&id={$aSeite["id"]}\" style=\"text-decoration: none;\">{$aSeite["Name"]}</a>";
                }
                else
                {
                    $sEditLink="{$aSeite["Name"]}";
                }
                $tSeitenbaum .= strmult("<img src=\"bilder/line.gif\" / alt=\"--\">",($aSeite["ebene"]-1))."<img src=\"bilder/join.gif\" /><i class=\"fa fa-file-text\"></i> $sEditLink<br />\n\r";
            }
        }
        return $tSeitenbaum;
    }








    /**
     * @param int $iSeiteid
     * @return bool
     * @throws Exception
     */

    public static function seiten_seite_loeschen(int $iSeiteid)
    {
        self::checkPagePermission($iSeiteid,true);

        //Wenn es unterseiten gibt, wird ein Fehler ausgegeben
        $unterSeitenAnzahl=db::querySingle("select count(id) from main_seiten where ueber_id=:id",[':id'=>$iSeiteid])["count(id)"];
        if($unterSeitenAnzahl>0)throw new Exception($unterSeitenAnzahl,207);


        //Übergeordnete Seite und Position dort laden
        $seitDaten = self::seiten_seitendaten_laden($iSeiteid);
        $ueberId=$seitDaten['ueber_id'];
        $altePosition=$seitDaten['position'];

        //Unterseiten umgruppieren

        //Die Seiten unterhalb der alten Position hochrutschen lassen
        db::update(
            'update main_seiten set main_seiten.position=main_seiten.position-1 where main_seiten.ueber_id=:ueberId and main_seiten.position>:position',
            [
                ':ueberId'=>$ueberId,
                ':position'=>($altePosition)
            ]
        );

        //Seite löschen
        $sql="delete from main_seiten where id=$iSeiteid;";											// Loesche die Datei die keine Unterseiten hat.
        db::delete($sql,array());
        if(db::lastRowCount()==1) {
            return true;
        } else return false;

    }





    /**
     * @param int|null $id
     * @return array
     * @throws Exception
     */

    public static function texte_auflisten($id=Null){

        if($id===Null){
            self::checkPermission(USERGROUPS::ADMIN);
        }else{
            self::checkPagePermission($id);
        }

        return db::query("select id, Titel from main_texte where not id = 0;");
    }





    /**
     * @return mixed
     * @throws Exception
     */

    public static function seiten_texte_anzahl(){

        return db::querySingle("select count(id) from main_texte where not id = 0;")['count(id)'];
    }




    /**
     * @param $iSeiteid
     * @param string $sModus
     * @return bool
     * @throws Exception
     */

    public static function get_seiten_userperm($iSeiteid,$sModus='lesen')
    {
        //Wenn noch nicht alle Berechtigungen geladen wurden, soll zumindest nochmal nachgesehen werden
        if(!self::$allPermissionLoaded) {
            self::load_benutzer_berechtigungen($iSeiteid,$sModus);
        }

        if(self::$userIdent->group_id==1){
            return true;
        }
        else return in_array($iSeiteid,self::$pagePermission["seitenber_{$sModus}"]);
    }




    /**
     * @throws Exception
     */

    public static function load_benutzer_berechtigungen() {
        //soll nur aufgeführt werden, wenn sei nciht ehr schon ausgeführt wurden
        if(!self::$allPermissionLoaded) {

            $aBerBereiche = array(array("seiten", "lesen"), array("seiten", "aendern")); //Ber heißt immer Berechtigung analog zu perm für permission

            foreach ($aBerBereiche as $aBerBereich) {
                self::$userPermissionQuery = 'SELECT distinct seite_id
                    FROM main_seiten_ber
                    WHERE (nutzergruppe_id= :gid or benutzer_id= :uid) and '.$aBerBereich[1].'=1';

                self::$userPermissionFromDatabase = db::query(self::$userPermissionQuery, [':gid' => self::$userIdent->group_id, ':uid' => self::$userIdent->id]);

                foreach (self::$userPermissionFromDatabase as $daEineNutzerBer) {
                    self::$pagePermission["{$aBerBereich[0]}ber_{$aBerBereich[1]}"][] = $daEineNutzerBer['seite_id'];
                }
            }
            self::$allPermissionLoaded=true;
        }
    }





    /**
     * @return array
     * @throws Exception
     */
    public static function loadUebergeordneteSeiten(){
        //TODO nach der Seitenreihenfolge sortieren
        $sql = "select distinct main_seiten.id, main_seiten.Name,main_seiten.Titel 
from main_seiten, main_seiten_ber 
where ueber_id=0 and opt_verbergen=0 and main_seiten_ber.seite_id = main_seiten.id and (main_seiten_ber.benutzer_id=:uid or  main_seiten_ber.nutzergruppe_id=:gid) and main_seiten_ber.lesen=1
order by main_seiten.position;";
        return db::query($sql,[':uid'=>self::$userIdent->id,':gid'=>self::$userIdent->group_id]);
    }





    /**
     * @return array
     * @throws Exception
     */
    public static function loadUntergeordneteSeiten(int $seitenId){

        //TODO nach der Seitenreihenfolge sortieren

        seitenArchive::checkPagePermission($seitenId);

        $sql = "select distinct main_seiten.id, main_seiten.Name,main_seiten.Titel 
from main_seiten, main_seiten_ber 
where ueber_id=:ueberId and opt_verbergen=0 and main_seiten_ber.seite_id = main_seiten.id and (main_seiten_ber.benutzer_id=:uid or  main_seiten_ber.nutzergruppe_id=:gid) and main_seiten_ber.lesen=1
order by main_seiten.position;";
        return db::query($sql,[':uid'=>self::$userIdent->id,':gid'=>self::$userIdent->group_id,'ueberId'=>$seitenId]);
    }




    #########################################################################
    ##
    ## Template-Funktionen
    ##
    #########################################################################

    /**
     * @param $id int
     * @param $template template
     * @return bool
     * @throws Exception
     */

    public static function saveTemplate($id,$template,$templateDatei){

        self::checkPagePermission($id,true);

       //Daten speichern
        db::insert(
            'update main_seiten set Templatedaten=:templateDaten, Templatedatei=:templateDatei where id=:id',
            [':templateDaten'=>serialize($template),':templateDatei'=>$templateDatei,':id'=>$id]);



        //Wenn nichts geänert wurde, dann wird false zurückgegeben, muss ja nichts passiert sein!
        return (db::lastRowCount()>0?true:false);

    }




    /**
     * @param string $templateDatei
     * @return array
     * @throws Exception
     */

    public static function loadPagesWithTemplate($templateDatei){
        return db::query('select id,Name from main_seiten where Templatedatei=:templateDatei',[':templateDatei'=>$templateDatei]);
    }




    /**
     * @param $id
     * @param $fromId
     * @return bool
     * @throws Exception
     */

    public static function inheritTemplateFromPage($id,$fromId){

        self::checkPagePermission($id,true);
        self::checkPagePermission($fromId);

        $serialTemplatedaten = self::seiten_seitendaten_laden($fromId)['Templatedaten'];
        db::insert('update main_seiten set Templatedaten = :daten where id=:id',[':id'=>$id,':daten'=>$serialTemplatedaten]);

        if(db::lastRowCount()>0)return true;
        else return false;

    }

    /**
     * @param int $id
     * @return array
     * @throws Exception
     */


    public static function getPagePermissions(int $id){

        $gruppenRechte = [];

        $nutzerGruppen = benutzerArchive::get_benutzergruppen_alle();

        $dbRechte = db::query('select main_nutzergruppen.id, main_seiten_ber.aendern, main_seiten_ber.lesen
                                    from main_nutzergruppen, main_seiten_ber 
                                    where main_nutzergruppen.id=main_seiten_ber.nutzergruppe_id and main_seiten_ber.seite_id=:id',
            [':id'=>$id]);


        foreach($nutzerGruppen as $gruppe){


            foreach($dbRechte as $recht){
                if($gruppe['id']==$recht['id']){

                    $gruppenRechte[$gruppe['id']]=[
                        'Name'=>$gruppe['Name'],
                        'aendern'=>$recht['aendern'],
                        'lesen'=>$recht['lesen']
                    ];
                    break;
                }
            }
            if(!isset($gruppenRechte[$gruppe['id']])){
                $gruppenRechte[$gruppe['id']]=[
                    'Name'=>$gruppe['Name'],
                    'aendern'=>0,
                    'lesen'=>0
                ];
            }

        }
       // die(print_r($nutzerGruppen,true).print_r($dbRechte,true).print_r($gruppenRechte,true));

        return $gruppenRechte;

    }


}
