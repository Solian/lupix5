<?php


class log {

    protected static $FEHLER=[];

    static function init($FEHLER=array())
    {
        include(__DIR__ . '/fehler.lib.php');
        self::$FEHLER = $FEHLER;
    }

    public static function fehlerExistiert(int $nummer){
        if(empty(self::$FEHLER))self::init();

        return array_key_exists($nummer,self::$FEHLER);
    }

    public static function fehler(int $nummer){
        if(self::fehlerExistiert($nummer)){
            return self::$FEHLER[$nummer];
        }
        return [];
    }

    /**
     * @param int $argNummer
     * @param string $argOffset
     * @return mixed
     * @throws Exception
     */
    public static function fehlerDaten(int $argNummer, string $argOffset)
    {
        if(array_key_exists($argOffset,self::fehler($argNummer)))return self::$FEHLER[$argNummer][$argOffset];
        elseif($argNummer>1000 && $argNummer <6000)return self::$FEHLER[1000][$argOffset];
        else throw new Exception('Fehler:'.$argNummer.'-->'.$argOffset,951);
    }


    public static function event($sFehlerkey,$iZeit,$sDatei,$sFehlernummer,$sExecCodeErgebnis,$iZeilennummer,$sEreignisverfolgung,$modul='',$userid=0,$sFehlernachricht='')
    {
        //if(!self::fehlerExistiert($sFehlernummer))throw new Exception($sFehlernummer,25);

        if(self::get_loglevel()<self::fehler($sFehlernummer)){

        try {
            $sqlabfrage = 'INSERT INTO main_logs (Fehlerkey, datum, fehlernummer, Modul, Skriptname, zeilennummer, Verfolgung, Fehlernachricht, ExecCodeAusgabe, nutzer_id, fehlerniveau )
		VALUES (:Fehlerkey,
				:datum,
				:fehlernummer,
				:Modul,
				:Skriptname,
				:zeilennummer,
				:Verfolgung,
				:Fehlernachricht,
				:ExecCodeAusgabe,
				:nutzer_id,
				:fehlerniveau);';

            db::insert($sqlabfrage, array(
                    ':Fehlerkey' => $sFehlerkey,
                    ':datum' => $iZeit,
                    ':fehlernummer' => $sFehlernummer,
                    ':Modul' => $modul,
                    ":Skriptname" => $sDatei,
                    ':zeilennummer' => $iZeilennummer,
                    ":Verfolgung" => $sEreignisverfolgung,
                    ":Fehlernachricht" => $sFehlernachricht,
                    ":ExecCodeAusgabe" => $sExecCodeErgebnis,
                    ':nutzer_id' => $userid,
                    ':fehlerniveau' => self::fehlerDaten($sFehlernummer,'NIV'))
            );

            if (db::lastRowCount() == 1){
                return true;
            }
            else{
                return false;
            }
        } catch(Throwable $e ){
            return false;
        }
        } else{
            return false;
        }


    }

    /**
     * @param int $niveau
     * @return mixed
     * @throws Exception
     */

    public static function niveauDaten(int $niveau){
        $niveauData=db::querySingle("select * from main_logs_niveaus where id=:niveau",['niveau'=>$niveau]);
        if(empty($niveauData))throw new Exception('Niveau:'.$niveau,952);
        else return $niveauData;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public static function anzahl_ungelesen()
    {
        $aAnzahl=db::querySingle("select count(*) as Anzahl from main_logs where checked_datum=0;");
        return $aAnzahl["Anzahl"];
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public static function anzahl_alle()
    {
        $aAnzahl=db::querySingle("select count(*) as Anzahl from main_logs where 1;");
        return $aAnzahl["Anzahl"];
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function lade_existniveaus(){
        return db::query("select distinct main_logs.fehlerniveau, main_logs_niveaus.Name from main_logs, main_logs_niveaus where main_logs.fehlerniveau=main_logs_niveaus.id order by main_logs.fehlerniveau asc;");
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function lade_existniveausanzahl()
    {
        $aNiveaus=self::lade_existniveaus();
        if(db::lastRowCount()==0)return array();
        else
        {
            foreach($aNiveaus as $key => $value)
            {
                $daNiveau = db::querySingle(
                    "select distinct  main_logs.fehlerniveau, count(main_logs.fehlerniveau) as Anzahl, main_logs_niveaus.Name
                        from main_logs, main_logs_niveaus
                        where main_logs.fehlerniveau=main_logs_niveaus.id and main_logs.fehlerniveau=:fehlerniveau;",
                    ['fehlerniveau'=>$value["fehlerniveau"]]);
                $aNiveaus[$key]=$daNiveau;
            }

            return $aNiveaus;
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function lade_ungeleseneniveausanzahl()
    {
        $aNiveaus = db::query("select distinct main_logs.fehlerniveau from main_logs where 1 order by main_logs.fehlerniveau asc;");
        if(db::lastRowCount()==0)return array();
        else
        {
            foreach($aNiveaus as $key => $value)
            {
                $aNiveaus[$key]=db::querySingle(
                    "select distinct  main_logs.fehlerniveau, count(main_logs.fehlerniveau) as Anzahl, main_logs_niveaus.Name 
                          from main_logs, main_logs_niveaus 
                          where main_logs.fehlerniveau=main_logs_niveaus.id and main_logs.fehlerniveau=:fehlerniveau and checked_datum=0;",
                    ['fehlerniveau'=>$value["fehlerniveau"]]);
            }

            return $aNiveaus;
        }
    }

    /**
     * @param $argNiv
     * @return array
     * @throws Exception
     */
    public static function lade_fehlernachniveau($argNiv)
    {
        return db::query("select * from main_logs where fehlerniveau=:niv order by datum desc;",array("niv"=>$argNiv));
    }

    /**
     * @param $argNiv
     * @return array
     * @throws Exception
     */
    public static function lade_ungepruefte_fehler_nach_niveau($argNiv)
    {
        return db::query("select * from main_logs where fehlerniveau=:niv and checked_datum=0 order by datum desc;",array("niv"=>$argNiv));
    }

    /**
     * @param $argKey
     * @return mixed
     * @throws Exception
     */
    public static function lade_fehlerauskey($argKey)
    {
        $lsg=db::querySingle("select * from main_logs where Fehlerkey=:key",array("key"=>$argKey));
        if(db::lastRowCount()==1)
        {
            return $lsg;
        }
        else throw new Exception("Fehler",950);
    }

    /**
     * @param $argKey
     * @param $userid
     * @return int|NULL
     * @throws Exception
     */
    public static function checke_fehler_aus($argKey,$userid)
    {
        db::insert(
            "update main_logs
                 set checked_datum=".time().", checked_admin_userid=:userid
                 where Fehlerkey=:key",array('userid'=>$userid,"key"=>$argKey));
        return db::lastRowCount();
    }

    /**
     * @param $argSuchestring
     * @return array
     * @throws Exception
     */
    public static function suche_ereignis($argSuchestring)
    {
        /*
        //erstemal pr�fen, ob das ein benutzername ist
        $aBenutzerid=self::get_benutzer_id($argSuchestring);
        if(!empty($aBenutzerid))
        {
            $argSuchestring=$aBenutzerid["id"];
            return db::query("select * from main_logs where nutzer_id=:argSuchestring
                                        order by datum desc;",array("argSuchestring"=>$argSuchestring));
        }
        else
        {*/
            return db::query(
                "select *, match (Fehlerkey) against (:argSuchestring in boolean mode) as relevanz 
                    from main_logs where Fehlerkey LIKE :argSuchestring
                    order by relevanz desc;",array("argSuchestring"=>$argSuchestring));
        //}
    }

    /**
     * @param $argSuchestring
     * @return array
     * @throws Exception
     */
    public static function suche_nutzer_ereignis($argSuchestring)
    {
        /*
        //erstemal pr�fen, ob das ein benutzername ist
        $aBenutzerid=self::get_benutzer_id($argSuchestring);
        if(!empty($aBenutzerid))
        {
            $argSuchestring=$aBenutzerid["id"];
            return db::query("select * from main_logs where nutzer_id=:argSuchestring
                                        order by datum desc;",array("argSuchestring"=>$argSuchestring));
        }
        else
        {*/
        return db::query(
            "select main_logs.*, match (main_benutzer.nickname) against (:argSuchestring in boolean mode) as relevanz 
                    from main_logs, main_benutzer where main_benutzer.nickname LIKE :argSuchestring and main_logs.nutzer_id=main_benutzer.id
                    order by relevanz desc;",array("argSuchestring"=>$argSuchestring));
        //}
    }

    /**
     * @param int $nummer
     * @return array
     * @throws Exception
     */
    public static function suche_fehlernummer(int $nummer)
    {
        return db::query("select * from main_logs where fehlernummer=:fehlernume",array("fehlernume"=>$nummer));
    }

    public static function get_loglevel() {
        global $C_LOGLEVEL;

        if (isset($C_LOGLEVEL) and ! empty($C_LOGLEVEL) and is_int($C_LOGLEVEL)) {
            return $C_LOGLEVEL;
        } else
            return 2;
    }

}

log::init();
