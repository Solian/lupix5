<?php


class templateBasicEntry{

    /**
     * @var $type string string,text,controller
     *
     */
    public $type='';

    /**
     * @var string $value Contains a static string the Text-ID or the filename of the controller file
     */
    public $value='';

    /**
     * @var string $action contains the name of the called controller action.
     */

    public $action='';

    function __construct($type,$value)
    {
        $this->type=$type;
        $this->value=$value;
    }


}
