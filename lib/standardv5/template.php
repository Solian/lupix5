<?php

class template {

    /**
     * @var string $file contains the name of the template file
     */

    public $file='';

    /**
     * @var $entries templateEntry[]
     */

    public $entries=array();

    /**
     * @var $fields string[]
     */
    public $fields=array();

    /**
     * @param $label
     * @param $type
     * @param $value
     * @param string $action
     * @throws Exception
     */
    function set_entry($label,$type,$value,$action=''){
        if(in_array($label,$this->fields)){
            $this->entries[$label]=new templateEntry($type,$value,$action);
        } else {
            throw new Exception('"'.$label.'" '.print_r($this->fields,true).'in '.$this->file,72);
        }
    }

    /**
     * @param string[] $fields
     */

    function setFields($fields){
        $this->fields=$fields;
    }

}
