<?php



class bobArchiv extends wolfi
{
    /**
     * @var array Die Standardwerte für die Textfelder
     */

    protected static $standardValues=array('string'=>'','text'=>0,'int'=>0,'set'=>0);

    /** Gibt den Standard für einen Feldtypen eines Text-Elements wieder
     * @param $type string Der Typ eines Feldes eines Text-Elements
     * @return string|int Standwert eines Feldtpys in einem Text-Element: string='', text=0 (Textid), int=0
     */
    public static function standardValue($type){
        return self::$standardValues[$type];
    }

    /** Erstellt ein leeres Text-Element
     * @return textElement
     */

    public static function createEmptyTextElement(){
        return new textElement('empty','empty','none','none');
    }

    /** Erstellt ein leeres Text-Element aus einer dafür mit einer .part.yaml-Datei konfigurierten .part.php-Datei
     * @param $filePath
     * @return textElement
     * @throws Exception
     */

    public static function emptySeitenElementFromFile($filePath){

        $configPath = self::createTextElementConfigPath($filePath);

        if(file_exists($configPath)) {

            //Erstell ein Testtempate, dass auch gleich prüft, ob die Datei vorhanden ist.
            $bauTemplate = self::empty_template_from_file($filePath.'.part');
            $configData = spyc_load_file($configPath);
            $bausatz = new textElement($configData['config']['name'],$configData['config']['desc'],$configData['config']['file'],$configData['config']['section']);

            foreach($bauTemplate->entries as $label => $entryData){

                $bausatz->addField(
                    $label,
                    $configData['fields'][$label]['type'],
                    self::standardValue($configData['fields'][$label]['type']),
                    $configData['fields'][$label]['desc'],
                    $configData['fields'][$label]['placeholder']

                );
            }

            return $bausatz;

        }else{
            throw new Exception($filePath,41);
        }
    }

    /** Erstell den Pfad für die part.yaml-Datei aus dem Namen der part.php-Datei
     * @param $filename
     * @return string
     */

    public static function createTextElementConfigPath($filename){
        return self::createTemplateBasePath().'/'.$filename.'.part.yaml';
    }

    /** Erstellt einen leeren SeitenElementText
     * @return seitenElementText
     */

    public static function emptySeitenElementText(){
        return new seitenElementText();
    }

    /**
     * @param seitenElementText $seitenElementText
     * @throws Exception
     */

    public static function saveSeitenElementText(seitenElementText $seitenElementText){

        //Wenn der SeitenElementText noch nicht gespeichert worden ist,
        //wird ein neuer Eintrag erstellt
        if($seitenElementText->isIdNull()){

            $sql='insert into main_sets (elements) values (:elements);';
            $insertId = db::insert($sql,[':elements'=>serialize($seitenElementText->getElements())]);

            if(db::lastRowCount()==0)throw new Exception('seitenTextElement:'.serialize($seitenElementText),76);
            else return $insertId;

        }
        //Wenn er bereits in der Datenbank existiert
        //wird diese einfach aktualisiert
        else{
            $sql='update main_sets set elements=:elements where id=:id;';
            db::insert($sql,[':id'=>$seitenElementText->getId(),':elements'=>serialize($seitenElementText->getElements())]);

            if(db::lastRowCount()==0)throw new Exception('seitenTextElement:'.serialize($seitenElementText),76);
            else return true;
        }

    }

    /**
     * @param int $id Id des SeitenElement-Textes
     * @throws Exception
     * @return seitenElementText
     */

    public static function loadSeitenElementText(int $id){

        $sql='select elements from main_sets where id=:id';
        $result = db::querySingle($sql,[':id'=>$id]);

        if(db::lastRowCount()==0) throw new Exception('Seitenelement-Text (id):'.$id,77);

        $seitenElementText = new seitenElementText($id);
        $seitenElementText->setElements(unserialize($result['elements']));

        return $seitenElementText;
    }

    /**
     * @param int $id
     * @return int
     * @throws Exception
     */

    public static function deleteSeitenElementText(int $id){
        db::insert('delete from main_sets where id=:id',[':id'=>$id]);
        return db::lastRowCount()==1;
    }

    /**
     * @param string $pfad
     * @return array
     */

    public static function load_elements_files($pfad='')
    {
        $result = array();

        if ($pfad === '') {
            if (CALLED_FROM_ADMIN) {
                $pfad = '../template';
            } else {
                $pfad = 'template';
            }
        }

        $cdir = scandir($pfad);
        foreach ($cdir as $key => $value) {

            //Wenn es sich nicht um die Standard-Verzeichnisse handelt
            if (!in_array($value, array(".", ".."))) {

                //Wenn es ein Verzeichnis ist
                if (is_dir($pfad . DIRECTORY_SEPARATOR . $value)) {

                    $loadesFiles = self::load_elements_files($pfad . '/' . $value);
                    if(!empty($loadesFiles))$result[$value] = $loadesFiles;
                }
                //Wenn es eine Datei ist
                else {
                    //Wird geprüft, ob es eine gültige-Datei part-Datei ist, d.h., dass sie eine configuration besitzen muss
                    $yamlConfigPath = $pfad.'/'.(substr($value,0,-9)).'.part.yaml';
                    if(strstr($value,'.part.php')  && file_exists($yamlConfigPath))
                    {
                        $result[substr($value,0,-9)] = spyc_load_file($yamlConfigPath);
                    }
                }
            }
        }

        return $result;

    }

    /**
     * @param string $pfad
     * @param array &$result
     * @return array
     */

    public static function load_elements(string $pfad='',array &$result=[])
    {

        if ($pfad === '') {
            if (CALLED_FROM_ADMIN) {
                $pfad = '../template';
            } else {
                $pfad = 'template';
            }
        }

        $cdir = scandir($pfad);
        foreach ($cdir as $key => $value) {

            //Wenn es sich nicht um die Standard-Verzeichnisse handelt
            if (!in_array($value, array(".", ".."))) {

                //Wenn es ein Verzeichnis ist
                if (is_dir($pfad . DIRECTORY_SEPARATOR . $value)) {

                    self::load_elements($pfad . '/' . $value,$result);
                }
                //Wenn es eine Datei ist
                else {
                    //Wird geprüft, ob es eine gültige-Datei part-Datei ist, d.h., dass sie eine configuration besitzen muss
                    $yamlConfigPath = $pfad.'/'.(substr($value,0,-9)).'.part.yaml';
                    if(strstr($value,'.part.php')  && file_exists($yamlConfigPath))
                    {
                        $elementConfig = spyc_load_file($yamlConfigPath);
                        if(!isset($elementConfig['config']['section']))$elementConfig['config']['section']='Allgemein';
                        $result[$elementConfig['config']['section']][substr($value,0,-9)] =$elementConfig;
                    }
                }
            }
        }

        return $result;

    }


}
