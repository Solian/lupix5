<?php


/**
 * Description of UserIdent
 *
 * @author MarkusF
 */
abstract class USERGROUPS {

    const GUEST = 0;
    const ADMIN = 1;
    const USER = 2;
    const BLOCKED = 3;

}

class User {

    public $id = 0;
    public $name = '';
    public $group_id=0;
    public $threads_created = 0;
    public $posts_created = 0;

    public function exchangeArray($data) {
        $this->id = isset($data['id']) ? $data['id'] : 0;
        $this->group_id = isset($data['nutzergruppe_id']) ? $data['nutzergruppe_id'] : USERGROUPS::GUEST;
        $this->name = isset($data['nickname']) ? $data['nickname'] : '';
        $this->threads_created = isset($data['threads_created']) ? $data['threads_created'] : 0;
        $this->posts_created = isset($data['posts_created']) ? $data['posts_created'] : 0;
    }

}

class UserIdentity extends User {

    const PW_CHANGE_SUCC = 0;
    const PW_CHANGE_UNEQUAL_PWS = 1;
    const PW_CHANGE_OLD_PW_WRONG = 2;
    const PW_CHANGE_FAIL = 3;
    const DEFAULT_STYLE = 'Xorlosch';

    public $session_key = '';
    public $style = UserIdentity::DEFAULT_STYLE;

//
//    /**
//     * Try to login the user with his name $login and the password $pw.
//     *
//     * @param string $login The user name
//     * @param string $pw The password for the user.
//     * @return bool
//     * @throws Exception
//     */
//    public function login($login, $pw) {
//        $sql = 'SELECT nickname, passwort, id, style, nutzergruppe_id, sessionkey, adminkey, salt
//                FROM main_benutzer
//                WHERE email= :mail
//                LIMIT 1';
//
//        $daten = db::querySingle($sql, [':mail' => $login]);
//
//        if (db::lastRowCount() == 0) {
//            $this->setUserToGuest();
//            throw new Exception('', 211);
//        }
//
//        // generate salted pws
//        $pw_hash = hash('sha512', $pw . $daten['salt']);
//
//        if ($pw_hash !== $daten['passwort']) {
//            //echo 'PW Falsch';
//            $this->setUserToGuest();
//            throw new Exception('', 211);
//        }
//
//        if ($daten['nutzergruppe_id'] != USERGROUPS::BLOCKED && $daten['nutzergruppe_id'] != USERGROUPS::GUEST) {
//            //echo 'LOGIN('.$daten['id'].')';
//            $this->id = $daten['id'];
//            $this->name = $daten['nickname'];
//            $this->session_key = session_create_id();
//            $this->admin_key = '';
//            $this->style = $daten['style'] != '' ? $daten['style'] : UserIdent::DEFAULT_STYLE;
//            $this->group_id = $daten['nutzergruppe_id'];
//
//            $sql = 'UPDATE main_benutzer SET sessionkey = :skey WHERE id= :uid';
//            db::insert($sql, [':skey' => $this->session_key, ':uid' => $this->id]);
//
//            //echo 'LOGIN('.$this->id.')';
//
//            if(db::lastRowCount()>0){
//                return true;
//            }else{
//                throw new Exception($this->id,204);
//            }
//
//        } elseif ($daten['nutzergruppe_id'] == USERGROUPS::BLOCKED) {
//            // handling of blocked user
//            $this->setUserToGuest();
//            throw new Exception('', 201);
//        }else{
//            return false;
//        }
//    }
//
//    /**
//     * This methode will logout the user.
//     * @throws Exception
//     */
//
//    public function logout() {
//        $sql = "UPDATE main_benutzer SET sessionkey='' WHERE id = :uid";
//        db::insert($sql, [':uid' => $this->id]);
//        $this->setUserToGuest();
//    }
//
//    /**
//     * This methode tries to identify the user by his session key.
//     *
//     * @param string $session_key The session key.
//     * @return void
//     * @throws Exception
//     */
//    public function identify($session_key = '') {
//
//        if ($session_key != '') {
//            $this->session_key = $session_key;
//        }
//
//        if ($this->session_key == '') {
//            $this->setUserToGuest();
//            return;
//        }
//
//        $sql = 'SELECT * FROM main_benutzer WHERE sessionkey = :skey';
//        $benutzerdaten = db::query($sql, [':skey' => $this->session_key]);
//
//        if (db::lastRowCount() == 0) {
//            //echo "BALDUIOM";
//            $this->setUserToGuest();
//            return;
//        }
//
//        $this->group_id = intval($benutzerdaten[0]['nutzergruppe_id']);
//        $this->id = intval($benutzerdaten[0]['id']);
//        $this->name = $benutzerdaten[0]['nickname'];
//
//
//        $sql = 'UPDATE main_benutzer SET letztbesuch = :lvisit WHERE id= :uid';
//        db::insert($sql, [':lvisit' => time(), ':uid' => $this->id]);
//    }
//
//    /**
//     * Set the state of the user to guest state.
//     */
//    public function setUserToGuest() {
//        $this->id = 0;
//        $this->name = '';
//        $this->session_key = '';
//        $this->admin_key = '';
//        $this->group_id = 0;
//        $this->style = UserIdent::DEFAULT_STYLE;
//    }
//
//    /**
//     * This method will change the password.
//     *
//     * @param string $pw_new1 The new password
//     * @param string $pw_new2 The new password repeated
//     * @param string $pw_alt The old password
//     * @return boolean
//     * @throws Exception
//     */
//    public function setNewPassword($pw_new1, $pw_new2, $pw_alt) {
//        if ($pw_new1 !== $pw_new2) {
//            return UserIdent::PW_CHANGE_UNEQUAL_PWS;
//        }
//
//        $sql = 'SELECT salt FROM main_benutzer WHERE id = :uid';
//        $daten = db::querySingle($sql, [':uid' => $this->id]);
//
//        if (db::lastRowCount() == 0) {
//            return UserIdent::PW_CHANGE_FAIL;
//        }
//
//        // generate salt
//        $salt = uniqid(mt_rand(), true);
//        // generate salted pw
//        $pw = hash('sha512', $pw_new1 . $salt);
//        $pw_alt_hash = hash('sha512', $pw_alt . $daten['salt']);
//
//        $sql = 'UPDATE main_benutzer SET passwort = :pw, salt = :salt
//                WHERE id = :uid and passwort = :pw_alt';
//        $args = [':pw' => $pw, ':salt' => $salt, ':uid' => $this->id, ':pw_alt' => $pw_alt_hash];
//
//        db::update($sql, $args);
//
//        if (db::lastRowCount()>0) {
//            return UserIdent::PW_CHANGE_SUCC;
//        }else{
//            return UserIdent::PW_CHANGE_OLD_PW_WRONG;
//        }
//    }
//
//    /**
//     * This methode will change the password.
//     *
//     * @param string $pw_new1 The new password
//     * @param string $pw_new2 The new password repeated
//     * @param string $pw_alt The old password
//     * @return boolean
//     * @throws Exception
//     */
//    public function setNewRootUser($pw,$email) {
//
//        // generate salt
//        $salt = uniqid(mt_rand(), true);
//        // generate salted pw
//        $pw = hash('sha512', $pw . $salt);
//
//        $sql = 'UPDATE main_benutzer SET passwort = :pw, salt = :salt, email= :email
//                WHERE id = 1';
//        $args = [':pw' => $pw, ':salt' => $salt, ':email' => $email];
//
//        db::update($sql, $args);
//
//        if (db::lastRowCount()>0) {
//            return UserIdent::PW_CHANGE_SUCC;
//        }else{
//            return UserIdent::PW_CHANGE_OLD_PW_WRONG;
//        }
//    }


}


class session{
    var $sessionkey='';
    var $challenge='';
    var $loginStatus = lulo::AUTH_UNIDENTIFIED;
    var $firstTime=0;
    var $userId = 0;
    var $privKey='';
    var $pubKey='';

    public function __construct($sessionkey,$challenge)
    {
        $this->sessionkey=$sessionkey;
        $this->challenge=$challenge;
        $this->firstTime=time();
    }
}



abstract class lulo {

    const AUTH_UNIDENTIFIED = 0;
    const AUTH_GOT_USER_WITH_PUB_KEY = 1;
    const AUTH_AUTHENTICATED = 2;
    const AUTH_NO_USER_FOUND = 3;
    const AUTH_WRONG_PW = 4;
    const AUTH_TO_MANY_ATTEMPTS = 5;
    const AUTH_LOGIN_TIMEOUT = 6;

    /**
     * @param session $session
     * @throws Exception
     */

    public static function deleteSession(session $session){
        db::delete('delete from main_sessions where sessionkey=:sessionkey',[':sessionkey'=>$session->sessionkey]);
        if(db::lastRowCount()>0)return true;
        else{
            throw new Exception($session->sessionkey,132);
        }
    }

    /**
     * @param session $session
     * @return bool
     * @throws Exception
     */

    public static function addNewSession(session $session){
        db::insert(
            'INSERT into main_sessions (sessionkey, challenge, loginStatus, userId, firstTime, privKey, pubKey) 
                  VALUES (:sessionkey, :challenge, :loginStatus, :userId, :firstTime, :privKey, :pubKey)',
            [
                ':sessionkey'=>$session->sessionkey,
                ':challenge'=>$session->challenge,
                ':loginStatus'=>$session->loginStatus,
                ':userId'=>$session->userId,
                ':firstTime'=>$session->firstTime,
                ':privKey'=>$session->privKey,
                ':pubKey'=>$session->pubKey]
        );
        if(db::lastRowCount()>0)return true;
        else return false;
    }

    /**
     * @param session $session
     * @return bool
     * @throws Exception
     */

    public static function saveSession(session $session){

        if(!empty(db::query('select * from main_sessions where sessionkey=:sessionkey',[':sessionkey'=>$session->sessionkey] ))) {

            db::update(
                'update main_sessions 
                set challenge=:challenge, loginStatus=:loginStatus, userId=:userId, firstTime=:firstTime, privKey=:privKey, pubKey=:pubKey
                  where sessionkey=:sessionkey',
                [
                    ':sessionkey' => $session->sessionkey,
                    ':challenge' => $session->challenge,
                    ':loginStatus' => $session->loginStatus,
                    ':userId' => $session->userId,
                    ':firstTime' => $session->firstTime,
                    ':privKey' => $session->privKey,
                    ':pubKey' => $session->pubKey]
            );

            if (db::lastRowCount() > 0) {


                return true;
//            if($session->userId>0){
//                db::update('update main_benutzer set attempts=attempts+:attempts;',[':attempts'=>$session->attempts]);
//                if(db::lastRowCount()>0)return true;
//            }else{return true;}

            } else return false;

        }else{

            return self::addNewSession($session);
        }
    }

    /**
     * @param session $session
     * @param string $newSessionkey
     * @return bool|session
     * @throws Exception
     */

    public static function saveSessionWithNewId(session $session,string $newSessionkey){
        db::update(
            'update main_sessions
                  set sessionkey=:newSessionkey, challenge=:challenge, loginStatus=:loginStatus, userId=:userId, firstTime=:firstTime, privKey=:privKey, pubKey=:pubKey 
                  where sessionkey=:sessionkey',
            [
                ':newSessionkey'=>$newSessionkey,
                ':sessionkey'=>$session->sessionkey,
                ':challenge'=>$session->challenge,
                ':loginStatus'=>$session->loginStatus,
                ':userId'=>$session->userId,
                ':firstTime'=>$session->firstTime,
                ':privKey'=>$session->privKey,
                ':pubKey'=>$session->pubKey]
        );
        $session->sessionkey=$newSessionkey;
        return $session;
    }

    public static function loadSessions(){

    }

    /**
     * @param string $sessionkey
     * @return session|false
     * @throws Exception
     */

    public static function loadSession(string $sessionkey){
        $sessionData=db::querySingle('select * from main_sessions where sessionkey=:sessionkey',[':sessionkey'=>$sessionkey]);
        if($sessionData===false)return false;
        else{
            $session = new session($sessionData['sessionkey'],$sessionData['challenge']);
            $session->loginStatus=(int) $sessionData['loginStatus'];
            $session->userId=(int) $sessionData['userId'];
            $session->firstTime=(int) $sessionData['firstTime'];
            $session->privKey=$sessionData['privKey'];
            $session->pubKey=$sessionData['pubKey'];

            return $session;
        }
    }


    /**
     * @param session $session
     * @param string $h1
     * @return int
     * @throws Exception
     */

    public static function searchForSupposedUser(session $session, string $h1){

        $userDataSet = db::query("select email,id from main_benutzer");

        foreach($userDataSet as $userData){

            //echo $userData['email'].' '.$userData['id'];

            if(hash('sha512',$userData['email'].$session->challenge)===$h1){
                //echo "richtig \n";
                return $userData['id'];
            }//else {
                //echo "\n";
            //}
        }
        return false;

    }
    /**
     * @param session $session
     * @param string $pw
     * @return bool
     * @throws Exception
     */

    public static function checkUserCredentials(session $session, string $pw){

        //echo 'CheckUserCredentials für '.$session->userId." - $pw\n";
        $userData = db::querySingle("select passwort,salt from main_benutzer where id=:userid",[':userid'=>$session->userId]);

        if($userData!==false){
            //das pw wird mit dem salt s aus der Benuzter-Tabelle gehasht h_pw=(pw,s) und mit dem hash in der Nutzer-Tabelle verglichen:
            //echo hash('sha512',$pw.$userData['salt']).'==='.$userData['passwort'];
            if(hash('sha512',$pw.$userData['salt'])===$userData['passwort']){
                return true;
            }
        }

        return false;
    }

    /**
     * @param session $session
     * @return bool
     * @throws Exception
     */
    public static function notToManyLoginAttempts(session $session){

        return (benutzerArchive::get_benutzer_daten($session->userId,'attempts')<5);

    }

    /**
     * @param session $session
     * @throws Exception
     */

    public static function addFailedAttempt(session $session){
        db::update('update main_benutzer set attempts=attempts+1 where id=:userId',[':userId'=>$session->userId]);
        if(db::lastRowCount()!==1)throw new Exception('User:'.$session->userId.' Geänderte Zeilen:'.db::lastRowCount(),133);
    }


    /**
     * @param session $session
     * @throws Exception
     */

    public static function resetLoginAttempt(session $session){
        db::update('update main_benutzer set attempts=0 where id=:userId',[':userId'=>$session->userId]);
        if(db::lastRowCount()!==1)throw new Exception('User:'.$session->userId.' Geänderte Zeilen:'.db::lastRowCount(),133);
    }

    /**
     * @return session
     * @throws Exception
     */

    public static function createNewSession(){
        $sessionkey=zufallszahl(5);
        $challenge=zufallszahl(5);

        $session = new session($sessionkey,$challenge);

        lulo::addNewSession($session);
        return $session;
    }

    /**
     * @param session $session
     * @param $h1
     * @param $g_pub
     * @return array
     * @throws Exception
     */

    public static function prepareLogin(session $session, $h1,$g_pub)
    {
        //es wird ein benutzer gesucht, der mit der Challange n den übergebenen hash-Wert h1 bildet:
        //

        $userId= lulo::searchForSupposedUser($session, $h1);

        //wird kein Benutzer gefunden,
        if ($userId == 0) {
            //der Status der Session wird auf status=AUTH_NO_USER_FOUND gesetzt
            //bleibt der Rest, damit man nicht jetzt sich schon verrät.
            $session->loginStatus = self::AUTH_NO_USER_FOUND;
        } //wird ein Benutzer gefunden,
        else {

            //wird der benutzer user_id= in der session eingetragen
            //der Status der Session wird auf status=AUTH_GOT_USER_WITH_PUB_KEY gesetzt
            $session->loginStatus = self::AUTH_GOT_USER_WITH_PUB_KEY;
            $session->userId = $userId;
        }

        //die Challenge wird von n in o geändert und mit dem g_pub verschlüsselt zu c1=enc(o,g_pub)
        //
        $session->challenge = zufallszahl(5);
        $c1 = '';
        openssl_public_encrypt($session->challenge, $c1, $g_pub);
        $c1 = base64_encode($c1);

        //Dazu generiert er ein RSA-2048bit-Schlüsselpaar k_priv, k_pub Der private SChlüssel wird in der Session gespeichert
        // Create the private and public key
        $res = openssl_pkey_new(["private_key_bits" => 2048]);

        // Extract the private key from $res to $privKey
        $privKey = '';
        openssl_pkey_export($res, $privKey);
        $session->privKey = $privKey;

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);

        //openssl_free_key($res);
        $session->pubKey = $pubKey["key"];

        //Server speichert die Session

        //der Sessionkey wird von k --> l geändert.
        $l = zufallszahl(5);
        $session = lulo::saveSessionWithNewId($session,$l);

        //Die Login-Daten ausgeben
        return [
            'l' => $l,
            'c1' => $c1,
            'k_pub' => $session->pubKey
        ];
    }

    /**
     * @param session $session
     * @param string $h2
     * @param string $c2
     * @return session
     * @throws Exception
     */

    public static function login(session $session, string $h2,string $c2){

        //wenn die Session im richtigen Zustand ist
        if($session->loginStatus===lulo::AUTH_GOT_USER_WITH_PUB_KEY) {


            //Wenn die Zeit weiter vorangeschritten ist als 5min
            if ($session->firstTime + 300 >= time()) {

                //Der Server prüft, ob die Antwort h2 mit der gespeicherten Challange o und dem verlinkten Benutzernamen übereinstimmt:
                //
                //wenn ja,
                if (lulo::searchForSupposedUser($session, $h2)) {

                    //Prüfe die ANzahl der Logins
                    //wenn es 5 sind, gibt es keinen Login mehr!
                    if (lulo::notToManyLoginAttempts($session)) {

                        //wird aus der Chiffre c2 mit dem gespeicherten privaten Schlüssel k_priv das Passwort pw entschlüsselt
                        $pw = '';
                        openssl_private_decrypt(base64_decode($c2), $pw, $session->privKey);
                        //echo 'PW:'.$pw."<br>\n";
                        if (lulo::checkUserCredentials($session, $pw)) {

                            //Bei Erfolg:
                            //
                            //wir der Sessionstatus status=AUTH_AUTHENTICATED,
                            $session->loginStatus = lulo::AUTH_AUTHENTICATED;
                            //die Sessionkey von l --> m gesetzt,
                            $m = zufallszahl(5);

                            //die Zeit session_time=time() neu gesetzt

                            return lulo::saveSessionWithNewId($session, $m);;
                        } else {
                            //lulo::addFailedAttempt($session);
                            $session->loginStatus = lulo::AUTH_WRONG_PW;
                        }
                    } else {
                        $session->loginStatus = lulo::AUTH_TO_MANY_ATTEMPTS;
                    }

                } else {
                    $session->loginStatus = lulo::AUTH_NO_USER_FOUND;
                }
            } else {
                $session->loginStatus = lulo::AUTH_LOGIN_TIMEOUT;
            }
        }

        //Die RSA-Schlüssel und die challenge o werden gelöscht
        $session->privKey = '';
        $session->pubKey = '';
        $session->challenge = '';

        lulo::saveSession($session);
        return $session;
    }

    /**
     * @param session $session
     * @return UserIdentity;
     * @throws Exception
     */
    public static function identify(session $session){
        if($session->loginStatus===lulo::AUTH_AUTHENTICATED) {

            $userIdent = new UserIdentity();
            $userIdent->id = $session->userId;
            $userIdent->group_id = (int) benutzerArchive::get_benutzer_daten($session->userId, 'nutzergruppe_id');
            $userIdent->name = benutzerArchive::get_benutzer_daten($session->userId, 'nickname');
            $userIdent->session_key = $session->sessionkey;

            //print_r($userIdent);
            return $userIdent;

        }
        else{

            $userIdent = new UserIdentity();
            $userIdent->id = 0;
            $userIdent->group_id = 0;
            $userIdent->name = benutzerArchive::get_benutzer_daten(0, 'nickname');
            $userIdent->session_key = $session->sessionkey;
            return $userIdent;

        }
    }


}
