<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 25.04.2019
 * Time: 18:57
 */

abstract class modulBaseClass
{

    protected static $MODUL;

    /**
     * @var UserIdentity
     */
    protected static $userIdent;

    /**
     * @var bool|null|array
     */

    protected static $permission;

    /**
     * @var bool|null|array
     */

    static $pagePermission;

    /**
     * @var bool
     */

    protected static $allPermissionLoaded=false;


    static public function init(UserIdentity &$userIdent)
    {
        self::$userIdent=$userIdent;

        //mit einem neuen Benutzer werden auch alle Berechtigungen gelöscht
        self::$permission=null;
        self::$pagePermission=array('seitenber_lesen'=>[],'seitenber_aendern'=>[]);
        self::$allPermissionLoaded=false;
    }

    /**
     * @param int $permissionLevel
     * @param int|null $selfId
     * @throws Exception
     */

    static protected function checkPermission(int $permissionLevel=USERGROUPS::GUEST, int $forceRecheckPermission=null) :void
    {
        ## Wenn eine parameterUserId angegeben wurde, muss das Level ADMIN sein, das nur vom eigenen Benutzer gebrochen werden darf!
        ##

        //if($parameterUserId!==null && $permissionLevel!==USERGROUPS::ADMIN){
        //    throw new Exception('Modul:'.self::$MODUL,899);
        //}

        ## Wenn die Zugangserlaubnis nicht gesetzt ist oder wenn eine parameterUserId angegeben wurde, wird sie auch erneut geladen!
        ##
        if(self::$permission===null || $forceRecheckPermission!==null) {

            ## Wenn eine Gastberechtigung gefordert wird
            ##

            ## Wenn ein Nutzer angelegt wurde
            ##
            if (self::$userIdent instanceof UserIdentity) {

                ## Wenn ein unbekannter Gast gefordert wird, kann es jeder außer einem geblockten Nutzer!
                ##

                if ($permissionLevel === USERGROUPS::GUEST && self::$userIdent->group_id !== USERGROUPS::BLOCKED) {
                    self::$permission = true;
                }

                ## Wenn eine Normale Berechtigung gefordert wird, dann darf man kein Gast oder geblockter Nutzer sein
                ##

                elseif ($permissionLevel === USERGROUPS::USER && (self::$userIdent->group_id !== USERGROUPS::BLOCKED && self::$userIdent->group_id !== USERGROUPS::GUEST)) {
                    self::$permission = true;
                }

                ## Wenn eine Adminberechtigung gefordert wird
                ##

                elseif ($permissionLevel === USERGROUPS::ADMIN) {

                    ## Wenn man Admin ist, darf man es
                    ##
                    if (self::$userIdent->group_id === 1) {
                        self::$permission = true;
                    }
                    ## sonst wird geprüft
                    ##
                    else{
                        self::$permission = self::get_benutzer_adminrecht(self::$userIdent->id, self::$MODUL);
                    }
                }
            }

            ## Wenn noch gar kein benutzer initialisiert wurde.
            ##
            else {
                throw new Exception(self::$MODUL.' '.print_r(self::$userIdent,true), 905);
            }

        }

        ## Wenn die Berechtigungen bereits geladen wurden, kann nun die eigentliche Abfrage durchgeführt werden
        ##

        if(self::$permission==false)throw new Exception(self::$MODUL.' '.print_r(self::$userIdent,true),911);

        //## Wurde der Benutzerzugriff nur durch die Seitenkanalprüfung zugelassen, muss der Zugriff wieder zruück gesetzt werden und erneut geprüft werden
        //##
        //
        //if($parameterUserId!==null)self::$permission=null;

    }

    /**
     * @param $argUId
     * @param $argModulkennung
     * @return bool
     * @throws Exception
     */

    static public function get_benutzer_adminrecht($argUId,$argModulkennung)
    {

        $result =db::querySingle(
            'select main_admin_ber.aendern
                        from main_admin_ber,main_admin_module 
                        where main_admin_ber.benutzer_id=:userid and main_admin_ber.bereich_id=main_admin_module.id and main_admin_module.Modulkennung=:modulkennung',
            [':modulkennung'=>$argModulkennung,':userid'=>$argUId]);

        ## Kein Eintrag --> Keine Berechtigung
        ##
        if ($result === false) {
            return false;
        }
        ## Eintrag ohne Berechtigung
        ##
        elseif ($result['aendern'] == 0) {
            return false;
        }
        ## Eintrag mit Berechtigung --> OK
        ##
        elseif ($result['aendern'] == 1) {
            return true;
        }else{
            return false;
        }
    }


}

