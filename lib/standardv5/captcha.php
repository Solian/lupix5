<?php
try{
/***********
 * CAPTCHA
 * v110510 Lenni
 *	- Verknüpfungen für incs aktualisiert
 **********
 * v220817 Ralf
 *	- Für die Heldenschenke5 akutalisiert
 **********
 * v101118 Ralf
 *	- Für die Heldenschenke5 akutalisiert
**********/
header("Content-type: image/png");

//ini_set("display_errors","1");
require_once('db.lib.php');

    db::connect();

	$w=400;
	$h=150;
    $im=imagecreate($w,$h);

	$bg="bdc8db";
	$col="000000";
	//if($bg[0]=="#")$bg=substr($bg,1,5);
	//if($col[0]=="#")$col=substr($col,1,5);
	$r = hexdec(substr($bg, 0, 2));
	$g = hexdec(substr($bg, 2, 2));
	$b = hexdec(substr($bg, 4, 2));
	$bg=imagecolorallocate($im,$r,$g,$b);
	//imagecolortransparent ( $im, $bg );
	$r = hexdec(substr($col, 0, 2));
	$g = hexdec(substr($col, 2, 2));
	$b = hexdec(substr($col, 4, 2));
	$col=imagecolorallocate($im,$r,$g,$b);

	$captchaid=$_GET["id"];
    $captchaDatenArray=db::querySingle("select * from main_captcha where id=:id;",array(':id'=>$_GET['id']));
	$zahl1Integer=$captchaDatenArray["zahl1"];
	$zahl2Integer=$captchaDatenArray["zahl2"];
	$captchaDatenArray["operator"]==1?$operatorString="+":$operatorString="*";

	//Koordinaten bestimmen und Überschneidungen verhindern!

	$yop=rand(50,90);
	$xop=rand(60,300);


	$yb=rand(50,90);
	$ya=rand(50,90);

	$xa=rand($xop-80,$xop+80);

	do
    {
        $xb=rand(50,320);
    }
    while(  abs($xb-$xop)<80 or abs($xb-$xa)<80);


	$fnt= __DIR__ . "/../fonts/LiberationSerif-Regular.ttf";
	//echo $fnt;

	//if(!file_exists($fnt))throw new Exception('hallo');

	imagettftext($im,50,rand(0,360),$xop,$yop,$col,$fnt,$operatorString);
	imagettftext($im,50,rand(0,90),$xa,$ya,$col,$fnt,$zahl1Integer.".");
	imagettftext($im,50,rand(0,360),$xb,$yb,$col,$fnt,$zahl2Integer.".");



	imageinterlace($im,1);
	imagepng($im);
	imagedestroy($im);
}catch(Throwable $e){

}