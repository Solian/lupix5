<?php


class adminControllerClass extends controllerBaseClass{

    /**
     * @var adminklasse
     */

    protected $main;

    /**
     * adminControllerClass constructor.
     * @param UserIdentity $userident
     * @param $main|adminklasse
     */

    function __construct(UserIdentity $userident, $main)
    {
        parent::__construct($userident, $main);
    }

    /**
     * @param $fileNameString
     * @throws Exception
     */


    public function registerTemplate($fileNameString)
    {
        parent::registerTemplate($fileNameString.'.part');
    }

    /***
     * @param string $aktion
     */

    function referTo(string $aktion,string $mod=''){
        if($mod===''){
            $this->referringParameters['mod']=$this->_MODUL;
        }else{
            $this->referringParameters['mod']=$mod;
        }

        parent::referTo($aktion);
    }

    function menuAction(string $aktion=''){

    }


}
