<?php

define('MSG_TABLE', 'main_nasy_nachrichten');
define('MSG_RECIPIENT_TABLE', 'main_nasy_empfaenger');
define('MSG_USER_TABLE', 'main_benutzer');
define('MSG_USER_ID', 'id');
define('MSG_USER_NAME', 'nickname');

abstract class MessagesTypes {

    const PRIVATE_MSG = 1;
    const SYSTEM_MSG = 2;
    const ADVENTURE_MSG = 3;
    const FORUM_MSG = 4;

}

/**
 * Description of nasy
 *
 * @author markus
 */
class nasy {

    public $messages = [];

    /**
     * Loads the last 4 messages for the user with the id $user_id
     *
     * @param integer $user_id The ID of the user for which the NavBoxMsgs should load.
     * @param integer $nbr_of_messages The number of messages which should load.
     * @throws Exception
     */
    public function loadNavBox($user_id, $nbr_of_messages = 4) {
        $sql = 'SELECT mr.message_id, message_content, message_type, ' . MSG_USER_NAME . ' AS sender
                FROM ' . MSG_RECIPIENT_TABLE . ' mr
                INNER JOIN ' . MSG_TABLE . ' AS m ON mr.message_id = m.message_id
                LEFT JOIN ' . MSG_USER_TABLE . ' AS u ON ' . MSG_USER_ID . ' = sender_id
                WHERE mr.recipient_id = :uid && delete_recipient = 0
                ORDER BY message_date DESC
                LIMIT ' . $nbr_of_messages;

        $this->loadMessages($sql, [':uid' => $user_id]);

        $this->setMessagesSubjects();
    }


    /**
     * Loads the last 4 messages for the user with the id $user_id
     *
     * @param integer $user_id The ID of the user for which the NavBoxMsgs should load.
     * @param integer $nbr_of_messages The number of messages which should load.
     * @throws Exception
     */
    public function loadUnreadNavBox($user_id, $nbr_of_messages = 4) {
        $sql = 'SELECT mr.message_id, message_content, message_type, ' . MSG_USER_NAME . ' AS sender
                FROM ' . MSG_RECIPIENT_TABLE . ' mr
                INNER JOIN ' . MSG_TABLE . ' AS m ON mr.message_id = m.message_id
                LEFT JOIN ' . MSG_USER_TABLE . ' AS u ON ' . MSG_USER_ID . ' = sender_id
                WHERE mr.recipient_id = :uid && delete_recipient = 0 && mr.was_read=0
                ORDER BY message_date DESC
                LIMIT ' . $nbr_of_messages;

        $this->loadMessages($sql, [':uid' => $user_id]);

        $this->setMessagesSubjects();
    }

    /**
     * Loads all inbox messages for the user with the id $user_id.
     *
     * @param integer $user_id The ID of the user for which the inbox should load.
     * @throws Exception
     */
    public function loadInbox($user_id) {
        $sql = 'SELECT mr.message_id, message_subject, message_date, message_type, was_read, m.sender_id, ' . MSG_USER_NAME . ' AS sender
                FROM ' . MSG_RECIPIENT_TABLE . ' mr
                INNER JOIN ' . MSG_TABLE . ' AS m ON mr.message_id = m.message_id
                LEFT JOIN ' . MSG_USER_TABLE . ' AS u ON ' . MSG_USER_ID . ' = sender_id
                WHERE mr.recipient_id = :uid && delete_recipient = 0
                ORDER BY message_date DESC';

        $this->loadMessages($sql, [':uid' => $user_id]);
    }

    /**
     * Loads all outbox messages for the user with the id $user_id
     *
     * @param integer $user_id The ID of the user for which the outbox should load.
     * @throws Exception
     */
    public function loadOutbox($user_id) {
        $sql = 'SELECT mr.message_id, message_subject, message_date, message_type, was_read, ' . MSG_USER_ID . ' AS recipient_id , ' . MSG_USER_NAME . ' AS recipient_name
                FROM ' . MSG_TABLE . ' m
                INNER JOIN ' . MSG_RECIPIENT_TABLE . ' AS mr ON mr.message_id = m.message_id
                LEFT JOIN ' . MSG_USER_TABLE . ' AS u ON ' . MSG_USER_ID . ' = recipient_id
                WHERE sender_id = :uid && delete_sender = 0
                ORDER BY message_date DESC, message_id ASC';

        $this->loadMessages($sql, [':uid' => $user_id]);
    }

    /**
     * @param $sql
     * @param $args
     * @throws Exception
     */
    private function loadMessages($sql, $args) {
        $res = db::query($sql, $args);

        foreach ($res as $row) {
            $m = new MESSAGE();
            $m->exchangeArray($row);
            $this->messages[] = $m;
        }
    }

    private function setMessagesSubjects() {
        $subject = [
            MessagesTypes::PRIVATE_MSG => 'PN von %s',
            MessagesTypes::ADVENTURE_MSG => 'Abenteuer',
            MessagesTypes::FORUM_MSG => 'Forum',
            MessagesTypes::SYSTEM_MSG => 'Systemnachricht'
        ];

        foreach ($this->messages AS $message) {
            if (!array_key_exists($message->type, $subject)) {
                continue;
            }

            switch ($message->type) {
                case MessagesTypes::PRIVATE_MSG:
                    $message->subject = vsprintf($subject[$message->type], [$message->sender]);
                    break;
                default:
                    $message->subject = $subject[$message->type];
                    break;
            }
        }
    }

    /**
     * Deletes all messages for the user with id $user_id that were specified in
     * $messages.
     *
     * @param integer[] $messages An array of IDs from messages which should be deleted.
     * @param integer $user_id The ID of the user for which the message should be deleted.
     * @throws Exception
     */
    public function deleteMessages($messages, $user_id) {
        $message = new MESSAGE();
        foreach ($messages as $msg_id) {
            $message->deleteMessage($msg_id, $user_id);
        }
    }

}

class MESSAGE {

    public $message_id = 0;
    public $sender = '';
    public $sender_id = 0;
    public $subject = '';
    public $recipients = [];
    public $date = null;
    public $text = '';
    public $type = MessagesTypes::PRIVATE_MSG;
    public $was_read = false;

    /**
     * Loads the message with the ID $message_id and check if the user with ID $user_id has the necessary rights.
     *
     * @param integer $message_id The message ID of the message to load.
     * @param integer $user_id The user ID for which the access should be checked.
     *
     * @return boolean The result if the message could be loaded.
     * @throws Exception
     */
    public function loadMessage($message_id, $user_id) {
        $this->message_id = $message_id;

        $sql = 'SELECT message_id, sender_id, message_subject, message_content, message_type, message_date, ' . MSG_USER_NAME . ' AS sender
                FROM ' . MSG_TABLE . ' m
                LEFT JOIN ' . MSG_USER_TABLE . ' AS u ON ' . MSG_USER_ID . ' = sender_id
                WHERE m.message_id = :mid';

        $args = [':mid' => $this->message_id];
        $row = db::query($sql, $args)[0];

        // load all recipients for the message
        $sql = 'SELECT recipient_id, ' . MSG_USER_NAME . ' AS recipient
                FROM ' . MSG_RECIPIENT_TABLE . '
                LEFT JOIN ' . MSG_USER_TABLE . ' AS u ON ' . MSG_USER_ID . ' = recipient_id
                WHERE message_id = :mid';

        $recipients = db::query($sql, $args);

        $key = array_search($user_id, array_column($recipients, 'recipient_id'));

        // checks if the user has the right to load the message
        if ($row['sender_id'] != $user_id && $key === FALSE) {
            return FALSE;
        }

        $row['recipients'] = $recipients;

        $this->exchangeArray($row);

        if ($key !== FALSE) {
            $this->setMessageRead($user_id);
        }

        return TRUE;
    }

    /**
     * Insert the message into the database for sender and all recipients.
     *
     * @param integer $sender_id
     * @param string $subject
     * @param string $text
     * @param int|array $recipients
     * @param int $type
     * @throws Exception
     */
    public static function sendMessage($sender_id, $subject, $text, $recipients, $type) {
        if (!is_array($recipients)) {
            $recipients = [$recipients];
        }

        $sql = 'INSERT INTO ' . MSG_TABLE . ' (sender_id, message_subject, message_content, message_type, message_date)
                VALUES(:sid, :sub, :cont, :type , NOW())';
        $args = [':sid' => $sender_id, ':sub' => $subject,
            ':cont' => $text, ':type' => $type];
        $message_id = db::insert($sql, $args);


        //test if your sending to yourself
        if(in_array($sender_id,$recipients)){
            throw new Exception($sender_id.' an ('.implode(',',$recipients).')',51);
        }

        // add an entry for all recipients
        foreach ($recipients as $r) {
            $sql = 'INSERT INTO ' . MSG_RECIPIENT_TABLE . ' (message_id, recipient_id)
                    VALUES(:mid, :rid)';
            $args = [':mid' => $message_id, ':rid' => $r];
            db::insert($sql, $args);
        }
    }

    /**
     * Delete a Message from the Database for the user with the ID $user_id.
     * Message will finally delete in database, if all recipients and sender has deleted the message.
     *
     * @param integer $message_id The ID of the message which should be deleted.
     * @param integer $user_id The ID of the user for which the message should be deleted.
     * @throws Exception
     */
    public function deleteMessage($message_id, $user_id) {
        $sql = 'SELECT sender_id, delete_sender FROM ' . MSG_TABLE . ' WHERE message_id = :mid';
        $rows = db::query($sql, [':mid' => $message_id]);

        if (count($rows) == 0) {
            return;
        }

        $sender = $rows[0];

        if ($sender['sender_id'] == $user_id) {
            $sql = 'UPDATE ' . MSG_TABLE . ' SET delete_sender = TRUE
                     WHERE message_id = :mid';
            db::insert($sql, [':mid' => $message_id]);
            $sender['delete_sender'] = 1;
        } else {
            $sql = 'UPDATE ' . MSG_RECIPIENT_TABLE . '  SET delete_recipient = TRUE
                    WHERE message_id = :mid && recipient_id = :rid';
            db::insert($sql, [':mid' => $message_id, ':rid' => $user_id]);
        }

        // counts how many recipients didn't marked the message as deleted.
        $sql = 'SELECT COUNT(delete_recipient) AS cnt_delete_recipient
                 FROM ' . MSG_RECIPIENT_TABLE . '
                 WHERE message_id = :mid && delete_recipient = FALSE';
        $recipient = db::query($sql, [':mid' => $message_id])[0];

        // delete message physical if sender and recipients has marked the message as deleted
        if ($sender['delete_sender'] == 1 && $recipient['cnt_delete_recipient'] == 0) {
            $sql = 'DELETE FROM ' . MSG_TABLE . ' WHERE message_id = :mid';
            db::query($sql, [':mid' => $message_id]);
            $sql = 'DELETE FROM ' . MSG_RECIPIENT_TABLE . ' WHERE message_id = :mid';
            db::query($sql, [':mid' => $message_id]);
        }
    }

    /**
     * Set the message read for the user with the ID $user_id.
     *
     * @param integer $user_id The ID for the user for which the message should be marked as read.
     * @throws Exception
     */
    private function setMessageRead($user_id) {
        $sql = 'UPDATE ' . MSG_RECIPIENT_TABLE . ' SET was_read = 1
                WHERE message_id = :mid && recipient_id = :rid';
        $args = [':mid' => $this->message_id, ':rid' => $user_id];
        db::insert($sql, $args);

        $this->was_read = 1;
    }

    /**
     * Exchange function to put message data from an array into the message object.
     *
     * @param mixed[] $row Array that contains message data.
     */
    public function exchangeArray($row) {
        $this->message_id = isset($row['message_id']) ? $row['message_id'] : 0;
        $this->sender = isset($row['sender']) ? $row['sender'] : 'Unbekannt';

        $this->sender_id = isset($row['sender_id']) ? $row['sender_id'] : -1;
        if ($this->sender_id == 0) {
            $this->sender = 'System';
        }

        $this->subject = isset($row['message_subject']) ? $row['message_subject'] : '';
        $this->text = isset($row['message_content']) ? $row['message_content'] : '';
        $this->recipients = isset($row['recipients']) ? $row['recipients'] : [];
        $this->date = isset($row['message_date']) ? $row['message_date'] : '';
        $this->type = isset($row['message_type']) ? $row['message_type'] : MessagesTypes::PRIVATE_MSG;
        $this->was_read = isset($row['was_read']) ? $row['was_read'] : 0;

        if (isset($row['recipient_id']) && isset($row['recipient_name'])) {
            $this->recipients[] = ['recipient_id' => $row['recipient_id'], 'recipient_name' => $row['recipient_name']];
        }
    }

}
