<?php


class uploadedFile
{
    /**
     * @var string original file name
     */
    protected $name='';

    /**
     * @var string MIME Type
     */
    protected $type='';

    /**
     * @var int size of the file
     */
    protected $size=0;

    /**
     * @var string temporary name of the uploaded File
     */
    protected $temp_name='';

    /**
     * @var int error code for occuring errors during the upload
     */
    protected $errorCode=0;

    /**
     * @param string $name
     * @param string $temp_name
     * @param int $errorCode
     */

    function __construct($name='',$temp_name='',$errorCode=0){

        $this->name=$name;

        $this->temp_name=$temp_name;

        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $this->type=$finfo->file($this->temp_name);

        $this->size=filesize($this->temp_name);

        $this->errorCode=$errorCode;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getTempName(): string
    {
        return $this->temp_name;
    }

    /**
     * @param string $temp_name
     */
    public function setTempName(string $temp_name)
    {
        $this->temp_name = $temp_name;
    }

    /**
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    /**
     * @param int $errorCode
     */
    public function setErrorCode(int $errorCode)
    {
        $this->errorCode = $errorCode;
    }

    #####################################################
    ##
    ##
    ##  Zusatzfunktionen
    ##
    ##
    #####################################################

    function isImage(){
        if(strstr($this->type,'image'))return true;
        return false;
    }

    function isText(){
        if(strstr($this->type,'text'))return true;
        return false;
    }

}
