<?php



class siteControllerBaseClass extends controllerBaseClass{

    /**
     * @var session
     */

    public $session;

    /**
     * @param $fileNameString
     * @throws Exception
     */
    public function registerPageTemplate($fileNameString)
    {
        parent::registerTemplate($fileNameString.'.tpl');
    }

    /**
     * @param $fileNameString
     * @throws Exception
     */

    public function registerPartialTemplate($fileNameString){
        parent::registerTemplate($fileNameString.'.part');
    }
}
