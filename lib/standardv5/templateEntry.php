<?php



class templateEntry extends templateBasicEntry{

    /**
     * @var string $action contains the name of the called controller action.
     */

    public $action='';

    function __construct($type,$value,$action='')
    {

        parent::__construct($type,$value);
        $this->action=$action;
    }


}
