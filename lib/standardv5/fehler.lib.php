<?php

$FEHLER = array();
//$FEHLER[Nummer] = array("Kategorie"=>"KAT","Beschreibung"=>"Fehler einen Fehlerauszulösen","Nachricht"=>"Du depp hast einen Fehler gemacht!","ExecCode"=>'echo ":-P";');

###########################################################################
##                                                                       ##
##000/100 Kritische Fehler, die die Integrität des Hauptsystems betreffen##
##                                                                       ##
###########################################################################


$FEHLER[0] = array("NIV"=>4,"KAT"=>"KRIT","Beschreibung"=>"Der Fehlerhandler wurde nicht installiert.","Nachricht"=>"Es ist ein kritischer Systemfehler aufgetreten.");
#PHP#
$FEHLER[1] = array("NIV"=>4,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_ERROR: Schwerer Fehler ist aufgetreten. Das Programm wurde unterbrochen.");
$FEHLER[2] = array("NIV"=>4,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_WARNING: Ein leichter Fehler ist aufgetreten..");
$FEHLER[3] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Beim Aufruf eines Skripts wurde ein Controller nicht initialisiert.","Nachricht"=>"Die Aktion konnte nicht ausgeführt werden.");

$FEHLER[4] = array("NIV"=>4,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_PARSE: Ein Tipp-Fehler ist aufgetreten.");
$FEHLER[5] = array("NIV"=>3,"KAT"=>"KRIT","Beschreibung"=>"Fehlerseite aus dem Cache aufgerufen!","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[6] = array("NIV"=>4,"KAT"=>"KRIT","Beschreibung"=>"Die Zugangsdaten zur Datenbank sind unvollständig.","Nachricht"=>"Das Programm konnte keine Verbindung zur Datenbank herstellen.");
$FEHLER[7] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Der Controller konnte die Templatedatei wurde nicht finden. Daher konnte das Skript nicht ordentlich ausgeführt werden.","Nachricht"=>"Die Aktion konnte nicht ausgeführt werden.");
#PHP
$FEHLER[8] = array("NIV"=>3,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_NOTICE: Ein Sicherheitshinweis wurde zu einer Funktion gemacht.");
##
$FEHLER[9] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Es wrde keine Texteid übergeben, obwohl der Template-Bereich ein Text ist.","Nachricht"=>"Die Seite konnte nicht ordnungsgemäß geladen werden.");
$FEHLER[10] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Die aufgerufene Seite existiert nicht!","Nachricht"=>"Eine Seite konnte nicht geladen werden.");
$FEHLER[11] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Die aufgerufene Seite ist unvollständig! Text existiert nicht.","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[12] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Die aufgerufene Seite ist unvollständig! Scriptdatei existiert nicht.","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[13] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Die Seite ist nicht richtig konfiguriert! Kann nicht Skript oder Text unterscheiden.","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[14] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"!!!VERALTET!!! Der aufgerufene Text gehört nicht zu dieser Seite!","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[15] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Es wurde kein Inhalt erzeugt.","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
#PHP#
$FEHLER[16] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_CORE_ERROR: Ein schwerwiegender Fehler im PHP-Kern-Programm ist aufgetreten.");
##
$FEHLER[17] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Es solten die Daten des Standardstils geladen werden, dabei wurden keine Daten herausgefunden.","Nachricht"=>"Der Standardstil konnte nicht geladen werden.");
$FEHLER[18] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Es kann keine Verbindung zur Datenbank geöffnet werden.","Nachricht"=>"Es besteht ein Problem mit der Datenbank.");
$FEHLER[19] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Es wurde keine Seitenid, nicht einmal Null angegeben.","Nachricht"=>"Die Seite konnte nicht geladen werden.");
$FEHLER[20] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Es wurde keine Ressourcenid bei einer Abfrage erzeugt, obwohl kein Fehler von Mysql gemeldet wurde!","Nachricht"=>"Ein Problem mit der Datenbank ist aufgetreten.");
$FEHLER[21] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Es sollte nur die erste Zeile ausgegeben werden, dabei gab es nicht einmal ein Ergebnis!","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[22] = array("NIV"=>4,"KAT"=>"KRIT","Beschreibung"=>"Dieser Fehler wird absichtlich geworfen. Er deutet nicht auf ein Problem hin!","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[23] = array("NIV"=>2,"KAT"=>"KRIT","Beschreibung"=>"Es sollte für ein Skript eine Variable mit unbekanntem Typ geladen werden.!","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[24] = array("NIV"=>2,"KAT"=>"KRIT","Beschreibung"=>"Es sollte für ein Skript eine Variable geladen werden, die es aber nicht gibt!","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[25] = array("NIV"=>2,"KAT"=>"KRIT","Beschreibung"=>"Die Fehlernummer ist noch nicht in der fehler.lib regisitriert!","Nachricht"=>"Weitere Informationen:");
$FEHLER[26] = array("NIV"=>2,"KAT"=>"KRIT","Beschreibung"=>"Ein Debug-Fehler wird eingebaut, damit Entwickler die Funktion ihres Codes testen können. Diese müssen vor dem aktiven Betrieb jedoch entfernt werden. Die geschah hier nicht.","Nachricht"=>"Es wurde eine Code-Stelle nicht bereinigt!");
$FEHLER[27] = array("NIV"=>2,"KAT"=>"KRIT","Beschreibung"=>"Zur Fehlerausgabe sollte das Haupttemplate geladen werden, aber dies existiert nicht.","Nachricht"=>"Die Fehlerausgabe konnte nicht gestartet werden.!");
$FEHLER[28] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Beim Aufruf einer Skriptaktion wurde ein Paramter nicht ordentlich übergeben. Daher konnte die Skriptaktion nicht aufgerufen werden.","Nachricht"=>"Die Aktion des Seitenskripts wurde nicht ordentlich geladen.");
$FEHLER[29] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Beim Aufruf einer Skriptaktion sollte eine Aktion aufgerufen werden die nicht existiert. ","Nachricht"=>"Die Aktion konnte nicht ausgeführt werden.");

$FEHLER[30] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Das Zufallspasswort konnte nicht in der Datenbank gespeichert werden.","Nachricht"=>"Es gab einen Fehler beim Erzeugen ihres neuen Zufallspassworts.");
$FEHLER[31] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Template gerendert werden, obwohl keine Templatedatei angegeben wurde.","Nachricht"=>"Template konnte nicht gerendert werden.");

#PHP#
$FEHLER[32] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_CORE_WARNING:  Ein leichter Fehler ist im PHP-Kern-Programm aufgetreten.");
##

$FEHLER[33] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Das Zufallspasswort konnte nicht per E-Mail versandt werden.","Nachricht"=>"Es gibt ein Problem mit dem Versand der E-Mail mit dem Passowrt");
$FEHLER[34] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Der Text existiert nicht, obwohl er in einer Template-Definition der Seite angegeben wurde.","Nachricht"=>"Ein Text sollte für eine Seite geladen werden. Aber er existiert nicht.");
$FEHLER[35] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Beim Aufruf einer Seite wurde das Controller-Skript nicht gefunden.","Nachricht"=>"Der angegebene Controller existiert nicht.");
$FEHLER[36] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Eine Cache-Datei sollte zwar, aber konnte nicht gelöscht werden.","Nachricht"=>"Der Cache konnte nicht aktualisiert werden.");
$FEHLER[37] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Die Datei, die das Backend-Installations-Passwort enthält, konnte nicht geschrieben werden.","Nachricht"=>"Backend-Install.php konnte nicht geschrieben werden.");
$FEHLER[40] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Eine E-Mailadresse, die bereits benutzt wird, soll verwendet werden. Dies ist nicht erlaubt.","Nachricht"=>"Diese E-Mailadresse darf nicht verwendet werden.");
$FEHLER[41] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Seitenelemente-Text sollte gerendert werden, aber es gab keine Definitions-Datei.","Nachricht"=>"Das Template kann nicht als Bausatztext verwendet werden.");
$FEHLER[42] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Benutzerdaten sollte gelesen werden, doch er existiert nicht.","Nachricht"=>"Es gibt keinen Benutzer.");



$FEHLER[50] = array("NIV"=>1,"KAT"=>"NASY","Beschreibung"=>"Nachricht-ID für Löschung wurde nicht angegeben.","Nachricht"=>"Geben Sie an, welche Nachricht sie löschen wollen.");
$FEHLER[51] = array("NIV"=>1,"KAT"=>"NASY","Beschreibung"=>"Eine Nachricht sollte u.a. an den Absender selbst geschickt werden.","Nachricht"=>"Sie dürfen keine Nachricht an sich senden.");


#PHP#
$FEHLER[64] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>'E_COMPILE_ERROR: Der Kompiler hat einen schweren Fehler gemeldet.');


$FEHLER[70] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Im Controller wurde eine Templatevariable erzeugt, die im Template nicht benötigt wird!","Nachricht"=>"Im Controller wurde eine unnötige Templatevariable erzeugt.");
$FEHLER[71] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Im Controller wurde eine Templatevariable nicht erzeugt, die im Template benötigt wird!","Nachricht"=>"Im Controller wurde eine nötige Templatevariable nicht erzeugt.");
$FEHLER[72] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Ein Template-Feld sollte geändert werden, obwohl es gar nicht im Template existiert!","Nachricht"=>"Das Templatefeld gehört nicht zum Template.");
$FEHLER[73] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Ein Seitenelemente-Text-Feld sollte hinzugefügt werden, obwohl es gar nicht in der Partial-Datei existiert!","Nachricht"=>"Das Seitenelemente-Text-Feld gehört nicht zur Partial-Datei.");
$FEHLER[74] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Ein Seitenelemente-Text-Element sollte an einer unmöglichen Position aufgerufen werden.","Nachricht"=>"An der Position existiert kein Seitenelemente-Text-Element!");
$FEHLER[75] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Ein Text-Element sollte einen Wert vom falschen Datentyp erhalten. ","Nachricht"=>"In einem Text-Element wurde ein falscher Wert gespeichert.");
$FEHLER[76] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Ein neues Seitenelement-Text konnte nicht in der Datenbank eingetragen werden. ","Nachricht"=>"Der Seitenelement-Text wurde nicht gespeichert.");
$FEHLER[77] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Ein Seitenelement-Text konnte nicht aus der Datenbank geladen werden. ","Nachricht"=>"Der Seitenelement-Text wurde nicht geladen.");
$FEHLER[78] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Ein Label sollte ein neues Seitenelement erhalten, doch es war kein Seitenelement-Text an dieser Stelle eingestellt. ","Nachricht"=>"Das Seitenelement konnte nicht hinzugefügt werden, da es kein Seitenelement-Text ist.");
$FEHLER[79] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Es sollte ein verdecktes Feld in einem SeitenElementText eingebaut werden, was aber nicht geht.","Nachricht"=>"Das SeitenElement konnte nicht geparst werden.");

$FEHLER[80] = array("NIV"=>1,"KAT"=>"TEMPLATE","Beschreibung"=>"Es sollte geprüft werden, ob eine Seite mit einer ID existiert. Aber es wurde mehr als eine Seite gefunden.","Nachricht"=>"Die Seite existiert mehrfach und kann daher nicht angezeigt werden!s");


$FEHLER[90] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Die Datenbank-Konfigurationsdatei wurde nicht gefunden.","Nachricht"=>"Keine Verbindung zur Datenbank möglich.");
$FEHLER[91] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Die Datenbank-Konfigurationsdatei konnte nicht geparst werden. Sie liegt womöglich im falschen Datenformat vor.","Nachricht"=>"Keine Verbindung zur Datenbank möglich.");
$FEHLER[92] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Die Parameter für die Datenbank-Abfrage fehlen!","Nachricht"=>"Die Abfrage konnte nicht durchgeführt werden.");


$FEHLER[100] = array("NIV"=>1,"KAT"=>"KRIT","Beschreibung"=>"Eine Controller-Action wurde nicht mit den nötigen Type-Hints versehen. Der Paramter konnte nicht ordentlich geladen werden.","Nachricht"=>"Ein Controller wurde nicht ordentlich konfiguriert.");


#PHP
$FEHLER[128] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"'E_COMPILE_WARNING: Der Kompiler hat einen leichten Fehler gemeldet.'");

$FEHLER[131] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Eine Session sollte aus der Datenbank geladen werden, doch unter dem Key konnt nichts gefunden werden.","Nachricht"=>"Die Session ist nicht verfügbar.");
$FEHLER[132] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Eine Session sollte aus der Datenbank gelöscht werden, doch unter dem Key konnt nichts gefunden werden.","Nachricht"=>"Die Session wurde nicht verworfen.");
$FEHLER[133] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Die Anzahl der Login-Versuche sollte geändert werden (+1 oder =0).","Nachricht"=>"Die Login-Aktion war unvollständig!");

$FEHLER[199] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"Ein einem Modul wurde ein Fehler erzeugt!","Nachricht"=>"Ein Fehler ist aufgetreten in einem Modul!");


###########################################################################
##                                                                       ##
##	200 Fehlbedienung durch Nutzer                                      ##
##                                                                       ##
###########################################################################

$FEHLER[200] = array("NIV"=>4,"KAT"=>"ZUGRIFF","Beschreibung"=>"Ein unbekannter Gast hat einen verbotenen Zugriff versucht!","Nachricht"=>"Ihr könnt als unbekannter Nutzer dies nicht in der Heldenschenke tun.");
$FEHLER[201] = array("NIV"=>3,"KAT"=>"ZUGRIFF","Beschreibung"=>"Gesperrter Benutzer hat einen Zugriff (ggf. Login)  versucht!","Nachricht"=>"Ihr könnt nicht eingeloggt die Heldenschenke betreten, wenn ihr gesperrt seid.");
$FEHLER[202] = array("NIV"=>3,"KAT"=>"ZUGRIFF","Beschreibung"=>"Ein eingeloggter Benutzer hat einen unerlaubten Zugriff versucht! ","Nachricht"=>"Es ist ein Fehler aufgetreten. Wendet Euch an die Administratoren.");
$FEHLER[203] = array("NIV"=>3,"KAT"=>"ZUGRIFF","Beschreibung"=>"Ein Benutzer hat versucht, auf eine Seite zuzugreifen, zu der er keine Zugriffsrechte hat.","Nachricht"=>"Ihr dürft auf diese Seite nicht zugreifen.");
$FEHLER[204] = array("NIV"=>3,"KAT"=>"ZUGRIFF","Beschreibung"=>"Ein Benutzer hat versucht, auf eine Seite zu ändern!.","Nachricht"=>"Ihr dürft diese Seite nicht ändern. Euch fehlen die Rechte.");
$FEHLER[205] = array("NIV"=>3,"KAT"=>"ZUGRIFF","Beschreibung"=>"Ein Benutzer konnte nicht eingeloggt werden.","Nachricht"=>"Sie konnten nicht in das System eingeloggt werden!");
$FEHLER[206] = array("NIV"=>3,"KAT"=>"ZUGRIFF","Beschreibung"=>"Ein Benutzer hat keine Seite angegeben. Daher wurde die oberste lesbare Seite gesucht, aber es ist auch gar keine Seite für ihn lesbar.","Nachricht"=>"Sie haben auf keine Seite dieser Webpräsenz eine Zugriffsberechtigung");
$FEHLER[207] = array("NIV"=>3,"KAT"=>"ZUGRIFF","Beschreibung"=>"Eine Seite sollte gelöscht werden, besaß aber noch Untergeordnete Seiten. Diese müssen erst entfernt werden.","Nachricht"=>"Sie können die Seite nicht löschen, bevor nicht die untergeordneten Seiten verschoben wurden.");

$FEHLER[211] = array("NIV"=>4,"KAT"=>"ZUGRIFF","Beschreibung"=>"Die eingegebenen Logindaten (E-Mail und Passwort) stimmen nicht überein,","Nachricht"=>"E-Mail und Passwort stimmen nicht überein. Versucht es nochmals, oder lasst euch ein neues Passwort zuschicken.");
$FEHLER[212] = array("NIV"=>4,"KAT"=>"ZUGRIFF","Beschreibung"=>"Die aktuellen Userdaten (ID, Name, Sessionkey) stimmen nicht überein.","Nachricht"=>"Aktuellen Sessiondaten (Id, Name, Sessionkey stimmen nicht überein. <a href=\"{$_SERVER["PHP_SELF"]}?aktion=logout\">Logout</a>");
$FEHLER[213] = array("NIV"=>2,"KAT"=>"ZUGRIFF","Beschreibung"=>"Die aktuellen Userdaten (ID, Name, Sessionkey) sind unvollständig.","Nachricht"=>"Die Sessiondaten (Id, Name, Sessionkey stimmen unvollständig. <a href=\"{$_SERVER["PHP_SELF"]}?aktion=logout\">Logout</a>");
$FEHLER[214] = array("NIV"=>1,"KAT"=>"ZUGRIFF","Beschreibung"=>"Nicht alle notwendigen Parameter, die das Skript benötigt, wurden übergeben. Daher muss die Ausführung abgebrochen werden.","Nachricht"=>"Nicht alle nötigen Informationen liegen vor, um diese Aktion auszuführen.","ExecCode"=>'echo $main->_CATCHED_USER_VARS;');


$FEHLER[220] = array("NIV"=>4,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber die maximale Upload Größe aus der php.ini wurde überschritten.","Nachricht"=>"Die Datei ist zu groß!");
$FEHLER[221] = array("NIV"=>4,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber die maximale Upload Größe aus dem Formular wurde überschritten.","Nachricht"=>"Die Datei ist zu groß!");
$FEHLER[222] = array("NIV"=>4,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber die Datei wurde nur unvollständig übertragen","Nachricht"=>"Die Datei konnte nicht hochgeladen werden!");
$FEHLER[223] = array("NIV"=>4,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber es war keine Bilddatei.","Nachricht"=>"Die Datei wird nicht heruntergeladen");
$FEHLER[224] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber es wurde keine Datei hochgeladen.","Nachricht"=>"Die Datei konnte nicht hochgeladen werden!");
$FEHLER[225] = array("NIV"=>4,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber es wurde keine unterstützte Bilddatei hochgeladen.","Nachricht"=>"Es wurde kein unterstütztes Format hochgeladen!");
$FEHLER[226] = array("NIV"=>4,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber das BMP-Format wird nicht unterstützt, daher wurde der Upload abgebrochen.","Nachricht"=>"Der Upload von *.bmp-Dateien wird nicht unterstützt. Wandelt die Datei bitte erst in ein *.png oder *.jpg um. Dies ist u.a. auf dieser Webseite möglich: <a href=\"http://www.pictureresize.org/online-images-converter.html\">http://www.pictureresize.org/online-images-converter.html</a>.");
$FEHLER[227] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber es wurde keine Bilddatei hochgeladen.","Nachricht"=>"Es gab einen Fehler beim Kopieren des Bildes in die Heldenschenke!");
$FEHLER[228] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Die hochgeladene Bild-Datei konnte von Picto nicht in den zugehörigen Ordner verschoben werden","Nachricht"=>"Es gab einen Fehler beim Kopieren des Bildes in die Heldenschenke!");
$FEHLER[229] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Die hochgeladene Bild-Datei konnte von Picto nicht beim Spieler gespeichert werden.","Nachricht"=>"Es gab einen Fehler beim Kopieren des Bildes in die Heldenschenke!");
$FEHLER[230] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Beim Upload eines Avatars konnte der alte Avatar nicht gelöscht werden!","Nachricht"=>"Es gab einen Fehler beim Kopieren des Bildes in die Heldenschenke!");

$FEHLER[231] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Die Picto-Kategorie-Stammdaten wurden nicht in der Resourcentabelle gefunden.","Nachricht"=>"Die Picto-Kategorie-Stammdaten scheint nicht zu existieren.");
$FEHLER[232] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Die Picto-Bilddaten wurden nicht in der Resourcentabelle gefunden.","Nachricht"=>"Das Bild scheint nicht in der DB zu existieren.");


#PHP#
$FEHLER[256] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_USER_ERROR: Ein schwerer Fehler ist aufgetreten.");

###########################################################################
##                                                                       ##
##	300 Fehler, die Hilfsfunktionen auslösen können                      ##
##                                                                       ##
###########################################################################

$FEHLER[301] = array("NIV"=>2,"KAT"=>"CORE","Beschreibung"=>"Die Berechtigung auf eine Seitenfunktion zuzugreifen konnte nicht durchgeführt werden, weil nicht die betroffene Seite angegeben wurde.","Nachricht"=>"Die Berechtigungsprüfung schlug fehl, weil sie falsch implementiert wurde.");

$FEHLER[310] = array("NIV"=>2,"KAT"=>"FUNK","Beschreibung"=>"Der Funktion <i>Picto</i> wurde ein falscher Parameter übergeben!","Nachricht"=>"Bitte wendet euch an die Administratoren.");

$FEHLER[321] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es wurde eine Anfrage an den LogServer geschickt, ohne einen Nachrichtentyp zu speizifizieren","Nachricht"=>"Kein Nachrichtentyp für ihre Nachricht angegeben!");
$FEHLER[322] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es wurde eine Anfrage an den LogServer geschickt, ohne eine Zeit anzugeben zu speizifizieren","Nachricht"=>'Keine Zeit für ihre Nachricht angegeben!');
$FEHLER[323] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es wurde eine Anfrage an den LogServer geschickt, ohne einen Inhalt zu speizifizieren","Nachricht"=>"'Kein Inhalt für ihre Nachricht angegeben!'");
$FEHLER[324] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es wurde eine Anfrage an den LogServer geschickt, aber der Nachrichtentyp war gar keine Anfage","Nachricht"=>"'Kein Anfrage-Nachricht an den Server gesendet! Nur dieser Typ wäre zulässig.'");

###########################################################################
##                                                                       ##
##	400 Fehler des Heldensystem                                          ##
##                                                                       ##
###########################################################################

$FEHLER[399] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter soll geladen werden, ohne dass die Id des Chars angegeben wurde.","Nachricht"=>"Sie haben nicht angegeben, welchen Charakter sie laden wollen.");
$FEHLER[400] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter soll geladen werden, ohne dass der InitKey des Chars angegeben wurde.","Nachricht"=>"Sie haben nicht angegeben, welchen Charakter sie laden wollen.");
$FEHLER[401] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter soll ohne Datenbank initialisiert werden.","Nachricht"=>"Fehler beim Initialisieren des Heldensystems.");
$FEHLER[402] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte auf Eigenschaften gewürfelt werden, obwohl diese bei diesem Held nicht initalisiert sind!","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");
$FEHLER[403] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte auf Eigenschaften gewürfelt werden, obwohl der Held noch gar nicht aktiv ist!","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");
$FEHLER[404] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte etwas gesteigert werden, obwohl der Held noch gar nicht aktiv ist!","Nachricht"=>"Das Held ist noch nicht aktiv.");
$FEHLER[405] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Energie verändert werden, obwohl der Held noch gar nicht aktiv ist!","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");
$FEHLER[406] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Energie verändert werden, obwohl der Held noch gar initialisiert wunrde","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");
$FEHLER[407] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Energie verändert werden, obwohl diese gar nicht existiert.","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");
$FEHLER[408] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine (erweitere) Eigenschaften verwendet werden obwohl diese gar nicht existiert.","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");
$FEHLER[409] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Die Attacke und Parade kann nur von Kampftechniken geladen werden. Da dies keine ist, misslingt dies!","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");
$FEHLER[410] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Die Attacke und Parade dürfen sich bei Kampftechniken maximal 5 PUnkte unterscheiden.","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");
$FEHLER[411] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Auf Attacke und Parade dürfen nicht mehr Punkte als der Talentwert verteilt werden.","Nachricht"=>"Fehler beim Zugriff auf einen Helden.");

$FEHLER[414] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Probe auf eine Eigenschaft gewürfelt werden, die es nicht gibt!","Nachricht"=>"Diese Eigenschaft gibt es nicht.");
$FEHLER[415] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Held soll eine Eigenschaft steigern, obwohl diese noch nicht geladen wurden.","Nachricht"=>"Fehler beim Steigern einer (abgeleiteten) Eigenschaft!");
$FEHLER[416] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Eigenschaft sollte gesteigert werden, aber es wurde nicht übergeben welche!","Nachricht"=>"Fehler beim Steigern einer (abgeleiteten) Eigenschaft!");
$FEHLER[417] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte auf eine unbekannte Spalte zugegriffen werden.","Nachricht"=>"Fehler beim Steigern einer (abgeleiteten) Eigenschaft!");
$FEHLER[418] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Bei einem Versuch zur Steigerung standen nicht genügend AP zur Verfügung.","Nachricht"=>"Der Charakter besitzt zu wenige Abenteuerpunkte.");
$FEHLER[419] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Sonderfertigkeit sollte gesteigert werden, die nicht gesteigert werden kann.","Nachricht"=>"Fehler beim Steigern einer Fertigkeit!");
$FEHLER[420] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Fertigkeit hinzugefügt/gelesen werden, die es nicht ind er Datenbank gibt!","Nachricht"=>"Fehler beimEintragen einer Fertigkeit!");
$FEHLER[421] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fähigkeiten bei einem nicht-initialisierten Held hinzugefügt/gelesen werden.!","Nachricht"=>"Fehler beim Eintragen einer Fertigkeit!");
$FEHLER[422] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fähigkeit sollte zu einem Helden hinzugefügt werden, der darüber aber schon verfügt!","Nachricht"=>"Fehler beim Eintragen einer Fertigkeit!");
$FEHLER[423] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fähigkeit sollte zu einem Helden hinzugefügt werden, doch sie konnte nicht in die Datenbank geschrieben werden.!","Nachricht"=>"Fehler beim Eintragen einer Fertigkeit!");
$FEHLER[424] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fähigkeit sollte geladen werden, doch der Held verfügt darüber nicht!","Nachricht"=>"Fehler beim Laden der Fertigkeit!");
$FEHLER[425] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Kampffähigkeit sollte mit einer normalen Probe behandelt werden! Dies geht jedoch noch. Diese Proben werden nur mit Gegenständen durchgeführt!","Nachricht"=>"Fehler beim Laden der Fertigkeit!");
$FEHLER[426] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Sonderfertigkeit sollte gesteigert werden, dies geht jedoch noch nicht!","Nachricht"=>"Fehler beim Laden der Fertigkeit!");
$FEHLER[427] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fähigkeit sollte gesteigert werden, doch der Held verfügt darüber nicht!","Nachricht"=>"Die zu steigernde Fähigkeit kann der Charakter nicht.");
$FEHLER[428] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fähigkeit sollte bei einem Helden verändert werden, doch sie konnte nicht in der Datenbank aktualisiert werden.!","Nachricht"=>"Fehler beim Eintragen einer Fertigkeit!");
$FEHLER[429] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Liturgie sollte gelernt/gesteigert werden, doch die Liturgiekenntnis reicht nicht aus!","Nachricht"=>"Die Liturgie-/Ritualkenntnis reicht nicht aus!");
$FEHLER[430] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fertigkeit konnte nicht gelernt werden, weil die Lernkosten nicht klar sind!","Nachricht"=>"Die Steigerungskosten können nicht bestimmt werden.");
$FEHLER[431] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fertigkeit konnte nicht gelernt werden, weil die Lernkosten zu hoch sind.","Nachricht"=>"Die Steigerungskosten sind zu hoch!");
$FEHLER[432] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Zauber konnte nicht von einem NICHT-Zauberer gelernt werden.","Nachricht"=>"Fehler beim Lernen einer Fertigkeit!");
$FEHLER[433] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Liturgie konnte nicht von einem NICHT-Geweihten gelernt werden.","Nachricht"=>"Fehler beim Lernen einer Fertigkeit!");
$FEHLER[434] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Gegenstand sollte mit einer Kampftechnik angelegt werden, deren AT/PA Werte falsch sind oder noch nicht eingetragen wurden.","Nachricht"=>"Die Attacke und Parade der verwendeten Fähigkeit stimmen nicht!");
$FEHLER[435] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Profilitem, das es nicht gibt, sollte geändert werden.","Nachricht"=>"Dieses Feld existiert nicht auf dem Heldenbrieg");
$FEHLER[436] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Fähigkeit sollte zu einem Helden hinzugefügt werden, der aber nicht inaktiv war. Er hätte sie lernen müssen.!","Nachricht"=>"Die Fähigkeit darf nicht einfach hinzugefügt, muss gelernt werden, weil der Charakter aktiv ist.");
$FEHLER[437] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Liturgie/Ritual eingegeben werden, doch die Kenntnis reicht nicht aus! Daher wurde der Wert auf den maximalen gesetzt!","Nachricht"=>"Der Wert des Rituals/der Liturgie wurde auf den maximalen Wert begrenzt!");
$FEHLER[438] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Held konnte nicht initialisiert. Daher können auch keine weiteren Heldendaten eingegeben werden.","Nachricht"=>"Versuchen Sie den Held nochmals zu speichern!");

$FEHLER[440] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Gegenstand sollte erzeugt werden, dessen Art in der DB gar nicht existiert!","Nachricht"=>"Fehler beim Laden des Gegenstands!");
$FEHLER[441] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Held sollte einen Gegenstand erhalten, der nicht ein Gegenstandsobjekt ist!","Nachricht"=>"Fehler beim Laden des Gegenstands!");
$FEHLER[442] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Held sollte einen Gegenstand erhalten, der nicht  gespeichert ist!","Nachricht"=>"Fehler beim Laden des Gegenstands!");
$FEHLER[443] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Gegenstand konnte nicht einem Helden hinzugefügt werden.","Nachricht"=>"Fehler beim Laden des Gegenstands!");
$FEHLER[444] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Gegenstand gehört zwar dem Helden, aber ist ein anderer Typ. Dieser muss falsch vorher geladen worden sein!","Nachricht"=>"Fehler beim Laden des Gegenstands!");
$FEHLER[445] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Gegenstand gehört dem Helden nicht!!","Nachricht"=>"Fehler beim Laden des Gegenstands!");
$FEHLER[446] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Gegenstand existiert nicht!!","Nachricht"=>"Fehler beim Laden des Gegenstands!");
$FEHLER[447] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Die Gegenstandsklasse des Gegenstands existiert nicht!!","Nachricht"=>"Fehler beim Laden des Gegenstands!");
$FEHLER[448] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Gegenstand konnte nicht angelegt werden, da er nicht zum Kämpfen geeignet ist!","Nachricht"=>"Fehler beim Anlegen eines Gegenstands!");
$FEHLER[449] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Beim Anlegen eines Gegenstands gab es einen Fehler, da das Talent fehlt.","Nachricht"=>"Dem Charakter fehlt eine Fähigkeit, um diesen Gegenstand zu führen.!");
$FEHLER[450] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Dem Charakter wollte mit dem falschen Talent eine Waffe führen!","Nachricht"=>"dem Charakter fehlt eine Fähigkeit, um diesen Gegenstand zu führen.!");
$FEHLER[451] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Beim Laden eines Gegenstandes wurde in ungültiger Ort angegeben.","Nachricht"=>"Ein Gegenstand ist am falschen Körperteil gesucht worden.!");
$FEHLER[452] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Geld kann nicht als einfacher Gegenstand sondern muss als Geld einem Charakter gegeben werden.","Nachricht"=>"Das Geld kann so nicht zugefügt werden.!");
$FEHLER[453] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Avatar sollte geändert werden mit einem Vorlagen-Avatar, aber die Bild-Id wurde nicht angegeben.","Nachricht"=>"Der Avatar konnte nicht geändert werden!");
$FEHLER[454] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Avatar sollte geändert werden mit einem Vorlagen-Avatar, aber die Bild-Id gehört nicht zu Avatar-Vorlagen.","Nachricht"=>"Der Avatar konnte nicht geändert werden!");

$FEHLER[460] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Attacke sollte ohne Waffe probiert werden!","Nachricht"=>"Die Attacke kann nicht ausgeführt werden.");
$FEHLER[461] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Trefferpunkte können ohne angelegt Waffe nicht ausgewürfelt werden..","Nachricht"=>"Die Trefferpunkte können nicht ausgewürfelt werden.");
$FEHLER[462] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Gegenstand sollte als Geld hinzugefügt werden.","Nachricht"=>"Das Geld konnte nicht hinzugefügt werden.");
$FEHLER[463] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Das Geld ist in der Datenbank bei dem Helden nicht vorhanden.","Nachricht"=>"Das Geld konnte nicht geladen werden.");
$FEHLER[470] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Beim Erzeugen des InitKeys ist ein Fehler aufgetreten. Daher konnte der Charakter nicht in der DB gespeichert werden.","Nachricht"=>"Der Charakter konnte nicht initialisiert werden.");
$FEHLER[471] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Charakter wurde ohne einen Mysql-Fehler zu erzeugen nicht in der Datenbank gespeichert!","Nachricht"=>"Der Charakter konnte nicht gespeichert werden.");
$FEHLER[472] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Alle Gegenstände eines Charakters sollten gelöscht werden, weil der Charakter gelöscht werden soll!","Nachricht"=>"Nicht alle Gegenstände wurden gelöscht..");
$FEHLER[473] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Alle Fertigkeiten eines Charakters sollten gelöscht werden, weil der Charakter gelöscht werden soll!","Nachricht"=>"Nicht alle Fertigkeiten wurden gelöscht..");
$FEHLER[474] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Der Charakter soll gelöscht werden!","Nachricht"=>"Der Charakter konnte nicht gelöscht werden.");

$FEHLER[480] = array("NIV"=>1,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter sollte verändert werden, obwohl dies nur der Besitzer darf!","Nachricht"=>"Sie dürfen diese Aktion am Charakter nicht ausführen.");
$FEHLER[481] = array("NIV"=>1,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter sollte verändert werden, obwohl dies nur der Spielleiter oder Besitzer dürfen!","Nachricht"=>"Sie dürfen diese Aktion am Charakter nicht ausführen.");
$FEHLER[482] = array("NIV"=>1,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter sollte verändert werden, obwohl dies nur der Gruppenleiter, Spielleiter oder Besitzer dürfen!","Nachricht"=>"Sie dürfen diese Aktion am Charakter nicht ausführen.");

###########################################################################
##                                                                       ##
##	500 Fehler des Abenteuersystems                                      ##
##                                                                       ##
###########################################################################

$FEHLER[501] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Gegenstand sollte aus einer Truhe genommen werden, aber er war gar nicht darin.","Nachricht"=>"Der Gegenstand ist nicht in der Truhe!");
$FEHLER[502] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Liste der Spielercharaktere geladen werden, obwohl es im Abenteuer noch gar keine Spielgruppe gibt.","Nachricht"=>"Es befindet sich noch keine Gruppe im Abenteuer!");
$FEHLER[503] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer gestartet werden, ohne eine Gruppe zu registrieren. Das geht nicht.","Nachricht"=>"Das Abenteuer konnte ohne Gruppe nicht gestartet werden.");
$FEHLER[504] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Szene geladen werden, die es nicht (in diesem Abenteuer) gibt.","Nachricht"=>"Die Szene existiert nicht in diesem Abenteuer.");
$FEHLER[505] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Spielerpost aus einem Meisterpost geladen werden. Dort der Spielerpost existiert nicht bei diesem Meisterpost!","Nachricht"=>"Der Post existiert so nicht!");
$FEHLER[506] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter versucht in einem Abenteuer zu antworten, doch die gewählte Szene gehört nicht zu dem Abenteuer.","Nachricht"=>"Der Held konnte nicht im Abenteuer agieren, weil die gewählte Szene nicht zu diesem Abenteuer gehört!");
$FEHLER[507] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter versucht in einem Abenteuer ein anderem Charakter zu antworten, doch sie befinden sich nicht in derselben Szene!","Nachricht"=>"Der Charakter, mit dem ihr Held interagieren soll, befindet sich nicht in derselben Szene.");
$FEHLER[508] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter versucht in einem Abenteuer zu antworten, aber die Szene ist bereits abgeschlossen. Dadurch kann man dort nichts mehr schreiben.","Nachricht"=>"Sie können keine abgeschlossene Szene mehr verändern.");
$FEHLER[509] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter versucht in einem Abenteuer zu antworten, aber die Runde wurde bereits geschlossen und durch einen neuen Meisterpost weitergesetzt. Dadurch kann man dort nichts mehr schreiben.","Nachricht"=>"Ein weiterer Meisterpost wurde bereits geschrieben. Sie können nun nicht mehr auf den vergangenen Meisterpost antworten.");
$FEHLER[510] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter versucht in einem Abenteuer einen Post zu erstellen, aber den Meisterpost oder Spielerpost gibt es nicht.","Nachricht"=>"Die Antwort konnte nicht verfasst werden!");
$FEHLER[511] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter versucht in einem Abenteuer auf den Meisterpost zu antworten, aber er hat bereits direkt geantwortet. Nun kann man nur noch Sprachhinweise notieren.","Nachricht"=>"Sie haben bereits geantwortet. Sie können nur noch Sprachantworten auf andere Spielerpost schreiben.");

#PHP#
$FEHLER[512] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_USER_WARNING: Ein leichter Fehler ist aufgetreten.");

$FEHLER[513] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter versucht in einem Abenteuer auf einen Spielerpost zu antworten. Das ist aber verboten.","Nachricht"=>"Sie können nur direkt auf den Meisterpost agieren. Auf andere Spieler können sie nur sprachlich antworten.");
$FEHLER[514] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter versucht in einem Abenteuer zu antworten, aber der Post-Typ kann nicht verarbeitet werden.","Nachricht"=>"Die Aktion des Charakters konnte vom Abenteuer nicht verarbeitet werden, da die Funktion noch nicht implementiert wurde.");
$FEHLER[515] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Post konnte eines Abenteuers konnte nicht zugeordnet werden. Er gehört weder direkt zu seinem Masterpost, noch indirekt! Ist er irgendwie dazwischen gerutscht?","Nachricht"=>"Ein Post in dieser Szene konnte nicht zugeordnet werden. Dadurch kann die Szene nicht geladen werden!");
$FEHLER[516] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte einen Helden in einem Abenteuer benutzen, der ihm gar nicht gehört.","Nachricht"=>"Der Charakter gehört ihnen nicht!");
$FEHLER[517] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Im Abenteuer sollte ein Post aktualisiert werden, der aber gar nicht in der aktuellen Szene zum letzten Meisterpost gehört.","Nachricht"=>"Der Post kann nicht mehr geändert werden!");
$FEHLER[518] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler hat eine Meisteraktion versucht, obwohl er oder sie nicht Spielleiter ist.","Nachricht"=>"Die Aktion ist verboten. Sie sind nicht Spielleiter.");
$FEHLER[519] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Extrapost (Zus. Infos zum letzten Meiterpost) geladen werden, der aber gar nicht zu diesem Abenteuer gehört.","Nachricht"=>"Dieser Post gehört nicht zu diesem Meisterpost.");
$FEHLER[520] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Extrapost (Zus. Infos zum letzten Meiterpost) geladen werden, es gibt aber gar keine Extraposts!.","Nachricht"=>"Für diesen Meisterpost gibt es keine Zusatzinformationen.");
$FEHLER[520] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spielleiter konnte einen Post weder als Extrapost an den aktuellen Masterpost eingehangen werden, noch konnte er ihn als Meisterpost speichern.","Nachricht"=>"Für diesen Meisterpost gibt es keine Zusatzinformationen.");

$FEHLER[521] = array("NIV"=>1,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler/Spielleiter wollte einen Post verändern, der aber nicht zum aktuellen Meisterpost gehört.","Nachricht"=>"Sie drüfen den Post nicht mehr ändern!");
$FEHLER[522] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler, der nicht im Abenteuer d.h. nicht in der Abenteuergruppe ist, hat versucht seinen Charakter ausgeben zu lassen.","Nachricht"=>"Sie sind nicht Mitglied der Gruppe dieses Abenteuers.");
$FEHLER[523] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter sollte zu einer Szene, die bereits geschlossen ist, eingetragen werden!","Nachricht"=>"Der Charakter kann nicht in eine bereits abgeschlossene Szene verschoben werden.");
$FEHLER[524] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Post sollte gespeichert werden in einer Szene, obwohl diese in ihm gar nicht so eingetragen wurde. ","Nachricht"=>"Der Post konnte nicht eingetragen werden. Er gehört nicht zu dieser Szene.");
$FEHLER[525] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Das Abenteuer sollte gestartet werden, doch einer der Spieler hat noch keinen Charakter! Dies gilt nicht für den Spielleiter!","Nachricht"=>"Das Abenteuer wurde nicht gestartet, da noch nicht alle Spieler einen Charakter haben.");
$FEHLER[526] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Das Abenteuer sollte gestartet werden, doch die Gruppe besteht nur aus dem Spielleiter!","Nachricht"=>"Sie dürfen nicht allein ein Abenteuer ohne Charaktere starten!");
$FEHLER[527] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Das Abenteuer sollte gestartet werden, doch es fehlt der Name für das Abenteuer.","Nachricht"=>"Geben Sie einen Namen an!");
$FEHLER[528] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Das Abenteuer sollte gestartet werden, doch es fehlt die Kurzbeschreibung für das Abenteuer.","Nachricht"=>"Geben Sie eine Kurzbeschreibung an!");
$FEHLER[529] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Das Abenteuer sollte gestartet werden, doch es fehlt die Beschreibung ür die Gruppenanforderungen für das Abenteuer.","Nachricht"=>"Geben Sie die Anfordeurngen an die Gruppe an.");
$FEHLER[530] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Das Abenteuer sollte gestartet werden, doch es fehlt die Beschreibung für das Abenteuer.","Nachricht"=>"Geben Sie eine ausführliche BEschreibung für ihre Abenteuer ein!");
$FEHLER[531] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer angesehen oder modifiziert werden.","Nachricht"=>"Es wurde nicht angegeben, auf welches Abenteuer zugegriffen werden soll.");
$FEHLER[532] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Szene sollte erstellt werden, doch es wurde nicht angegeben, wie sie heißen soll.","Nachricht"=>"Die Szene konnte nicht erstellt werden. Geben Sie an, wie die Szene heißen soll.");
$FEHLER[533] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Eine Szene sollte erstellt werden, doch es wurde nicht angegeben, wie die Szene in das Abenteuer eingefügt werden soll.","Nachricht"=>"Die Szene konnte nicht erstellt werden. Es fehlt die vorhergehende Szene.");
$FEHLER[534] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter soll von einer Szene in eine anderen verschoben werden. Es wurde aber nicht er Charakter-Schlüssel angegeben.","Nachricht"=>"Es wurde nicht angegeben, welchen Charakter sie verschieben wollen.");
$FEHLER[535] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Charakter soll von einer Szene in eine anderen verschoben werden. Es wurde aber nicht angegeben, wohin.","Nachricht"=>"Es wurde nicht angegeben, wohin der Charakter verschoben werden soll.");
$FEHLER[536] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Szene geschlossen werden, doch es wurde nicht angegebenm, welche.","Nachricht"=>"Geben Sie an, welche Szene geschlossen werden soll.");
$FEHLER[537] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte eine Szene geschlossen werden, doch es wurde nicht angegeben, welche Szene anschließend kommt.","Nachricht"=>"Geben Sie an, welche Szene an die aktuelle Szene anschließt.");
$FEHLER[538] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte auf ein Abenteuer zugegriffen werden, doch der Spieler ist weder Mitglieder der Gruppe noch der Spielleiter.","Nachricht"=>"Sie sind nicht Teil des Abenteuers. Dieses ist privat.");
$FEHLER[539] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler hat versucht eine Spielleiter-Aktion eines Abenteuers auszuführen, ohne der Spielleiter zu sein.","Nachricht"=>"Sie dürfen das nicht, weil sie nicht der Spielleiter sind.");
$FEHLER[540] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte an einer Szene gearbeitet werden, ohne die Szene-ID anzugeben.","Nachricht"=>"Geben Sie an, welche Szene sie ansehen/bearbeiten wollen!");
$FEHLER[541] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Abenteuer sollte einer Gruppe angeboten werden, obwohl es bereits eine Gruppe besitzt.","Nachricht"=>"Sie spielen bereits mit einer Gruppe, sie können keine weitere Einladen.");
$FEHLER[542] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"In einem Abenteuer sollte ein Spielerpost erstellt werden, aber es wurde nicht angegeben zu welchem Meisterpost er gehören soll.","Nachricht"=>"Geben Sie an, auf welchen Meisterpost sie antworten wollen?");


//Fälschlich zu hoch zugeteilte NUmmern
$FEHLER[670] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer gestartet werden, dabei gibt es noch gar keine Gruppe!","Nachricht"=>"Sie können das Abenteuer nicht ohne Gruppe starten.");
$FEHLER[671] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer gestartet werden, dabei sind noch gar nicht alle Helden einer Gruppe zugeordnet!","Nachricht"=>"Nicht alle Charktere haben eine Szene.");
$FEHLER[672] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer gestartet werden, dabei ist es nicht mehr im Vorbereitungsmodus!","Nachricht"=>"Das Abenteuer läuft bereits oder ist schon beendet.");

$FEHLER[680] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Bild gelöscht werden, doch es wurde kein Bild-Key angegeben.","Nachricht"=>"Es wurde kein Bild gelöscht, weil nicht angegeben werden, welches gelöscht werden soll.");



###########################################################################
##                                                                       ##
##	600 Fehler des Gruppensystems                                        ##
##                                                                       ##
###########################################################################

$FEHLER[600] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"In einer Gruppe sollte ein Charakter für ein Spieler hinzugefügt/entfernt werden. Aber es wurde kein Heldenobjekt übergeben!","Nachricht"=>"Sie haben keinen Helden angegeben, der in die Gruppe soll.");
$FEHLER[601] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"In einer Gruppe sollte ein Charakter für ein Spieler hinzugefügt/entfernt werden, dem nicht der Held gehört!","Nachricht"=>"Der Held gehört Ihnen nicht!");
$FEHLER[602] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte der Status einer Einladung bearbeitet werden, obwohl es gar keine Einladung gibt.","Nachricht"=>"Sie haben keine Einladung.");
$FEHLER[603] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Mitglied der Gruppe sollte  erneut in die Gruppe eingeladen werden. Dies geht aber nicht.","Nachricht"=>"Der Spieler ist bereits Mitglied der Gruppe-");
$FEHLER[604] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Mitglied der Gruppe sollte erneut an die Gruppe eine Anfrage stellen. Dies geht aber nicht.","Nachricht"=>"Der Spieler ist bereits Mitglied der Gruppe oder hat bereits eine Anfrage gestellt.");
$FEHLER[605] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte der Status einer Mitgliedsanfrage für eine Gruppe bearbeitet werden, obwohl es gar keine Anfrage gibt.","Nachricht"=>"Es gibt gar keine Anfrage zur Mitgliedschaft von diesem Spieler.");
$FEHLER[606] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Spieler zur Gruppe hinzugefügt werden, dabei ist die maximale Spielerzahl bereits erreicht.","Nachricht"=>"Die maximale Spielerzahl ist bereits erreicht.");
$FEHLER[607] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"In einer Gruppe sollte ein Charakter für ein Spieler eingegeben werden. Aber der Spieler ist gar kein Mitglied der Gruppe!","Nachricht"=>"Der Spieler ist nicht Teil der Gruppe.");
$FEHLER[608] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Die Gruppe versuchte die Logs zu öffnen, aber der Key war leer!","Nachricht"=>"Kein Gruppen-Log-Key gegeben!");
$FEHLER[609] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte etwas ins Log der Gruppe schreiben oder daraus lesen, aber der Spieler ist gar kein Mitglied der Gruppe!","Nachricht"=>"Sie sind nicht Teil der Gruppe und dürfen nicht hineinschreiben oder lesen.");
$FEHLER[610] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler will einen Held einer Gruppe hinzufügen/entfernen, obwohl er nicht aktiv ist.","Nachricht"=>"Der Held wurde nicht einer Gruppe zugeordnet, weil er noch inaktiv ist.");

$FEHLER[611] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Die Gruppe sollte gelöscht werden, aber es konnten nicht alle Einladen entfernt werden. Die Gruppe bleibt daher bestehen.","Nachricht"=>"Die Gruppe wurde nicht gelöscht, da nicht alle Einladungen zurückgezogen werden konnten.");
$FEHLER[612] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Die Gruppe sollte gelöscht werden, aber es konnten nicht alle Anfragen entfernt werden. Die Gruppe bleibt daher bestehen.","Nachricht"=>"Die Gruppe wurde nicht gelöscht, da nicht alle Anfragen abgelehnt werden konnten.");
$FEHLER[613] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Die Gruppe sollte gelöscht werden, aber es konnten nicht alle Mitglieder entfernt werden. Die Gruppe bleibt daher bestehen.","Nachricht"=>"Die Gruppe wurde nicht gelöscht, da nicht alle Mitgliedseinträge gelöscht werden konnten.");
$FEHLER[614] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Die Gruppe sollte gelöscht werden, was aber nicht passiert ist.","Nachricht"=>"Die Gruppe wurde nicht gelöscht!");
$FEHLER[615] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte einen Held aus einer Gruppe entfernen, obwohl er diesen Charakter gar nicht eingetragen hat.","Nachricht"=>"Der Charakter wurde nicht aus der Gruppe entfernt. Er gehört nicht dazu.");
$FEHLER[616] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte einen Held in eine Gruppe eintragen, hat aber gar nicht die Gruppen-ID angegeben.","Nachricht"=>"Der Charakter konnte nicht bei der Gruppe eingetragen werden, da gar keine Gruppe gewählt wurde");
$FEHLER[617] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte einen Held in eine Gruppe eintragen, hat aber gar keinen initKey angegeben.","Nachricht"=>"Der Charakter konnte nicht bei der Gruppe eingetragen werden, da gar kein Charakter gewählt wurde");
$FEHLER[618] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte einen Held in eine Gruppe eintragen, ist aber gar nicht Mitglied der Gruppe.","Nachricht"=>"Der Charakter konnte nicht bei der Gruppe eingetragen werden, weil Sie nicht Mitspieler in der Gruppe sind ");
$FEHLER[619] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte einen Held in eine Gruppe eintragen, ist aber gar nicht der Besitzer des Helden.","Nachricht"=>"Der Charakter konnte nicht bei der Gruppe eingetragen werden, weil es nicht ihr Charakter ist.");

$FEHLER[621] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte eine Gruppe einsehen, zu der er gar nicht gehört! Daher wird der Zugang zur Gruppe verweigert.","Nachricht"=>"Sie sind nicht Teil der Gruppe. Sie dürfen diese daher auch nicht einsehen.");
$FEHLER[622] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte eine Gruppe einsehen und gleichzeitig beitreten, da er eine Einladung hatte! Aber die maximale Anzahl an Spielern wird überschritten. Daher wird der Zugang zur Gruppe verweigert.","Nachricht"=>"Die Gruppe ist bereits voll. Bitte warten sie, bis die Gruppe mehr Spieler zulässt.");
$FEHLER[623] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte eine Gruppennachricht schreiben, hat aber gar keine Gruppe angegeben.","Nachricht"=>"Die Nachricht wurde nicht versandt, da keine Gruppe angegeben wurde.");
$FEHLER[624] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte eine Gruppennachricht schreiben, hat aber gar keine Nachricht angegeben.","Nachricht"=>"Die Nachricht wurde nicht versandt, da kein Nachrichtentext notiert wurde.");
$FEHLER[625] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler wollte eine Gruppennachricht schreiben, ist aber gar nicht Mitglied.","Nachricht"=>"Die Nachricht wurde nicht versandt,sie kein Mitglied der Gruppe sind.");

$FEHLER[631] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen-Anfrage bearbeiten, aber es wurde gar keine Gruppen_ID übertragen.","Nachricht"=>"Die Anfrage konnte bearbeitet werden, weil keine Gruppen-ID übergeben wurde.");
$FEHLER[632] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen-Anfrage bearbeiten, aber es wurde gar keine Anfrage-ID übertragen.","Nachricht"=>"Die Anfrage konnte bearbeitet werden, weil keine Anfrage-ID übergeben wurde.");
$FEHLER[633] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen-Anfrage bearbeiten, aber es wurde gar keine gültige Anfrage-ID aus der Gruppe übertragen.","Nachricht"=>"Die Anfrage konnte bearbeitet werden, weil keine gültige Anfrage-ID übergeben wurde.");
$FEHLER[634] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen-Anfrage bearbeiten, aber der Benutzer ist gar nicht Admin der Gruppe.","Nachricht"=>"Sie können keine Anfragen bearbeiten, da Sie nicht Administrator der Gruppe sind.");
$FEHLER[635] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen-Anfrage akzeptieren, aber es ist kein Platz mehr in der Gruppe.","Nachricht"=>"Es ist kein freier Platz mehr in der Gruppe.");
$FEHLER[636] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen-Einladung annehmen, aber es wurde gar keine Gruppen-ID übertragen.","Nachricht"=>"Die Einladung konnte nicht angenommen werden, weil keine Gruppen-ID übergeben wurde.");
$FEHLER[637] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen-Einladung annehmen, hatte aber gar keine bei der Gruppe.","Nachricht"=>"Die Einladung konnte nicht angenommen werden, weil eine Einladung nicht ausgesprochen wurde.");
$FEHLER[638] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen-Einladung annehmen, die bereits abgelehnt ist oder angenommen wurde.","Nachricht"=>"Die Einladung konnte nicht angenommen werden, weil sie bereits abgelehnt oder angenommen ist.");
$FEHLER[639] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Gruppenadmin wollte eine Gruppen-Einladung erstellen, aber gar keine Gruppen-ID angegeben.","Nachricht"=>"Die Einladung konnte erstellt werden, weil keine Gruppen-ID angegeben wurde.");
$FEHLER[640] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Gruppenadmin wollte eine Gruppen-Einladung erstellen, aber gar keinen Benutzernamen angegeben.","Nachricht"=>"Die Einladung konnte erstellt werden, weil kein Benutzer angegeben wurde.");
$FEHLER[731] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Gruppenadmin wollte eine Gruppen-Einladung erstellen, aber es gibt gar nicht mehr genügend Platz!","Nachricht"=>"Die Einladung konnte erstellt werden, weil Platz mehr in der Gruppe ist.");

$FEHLER[641] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen gründen, aber es wurde keine ID angegeben. Ein unbekannter Gast (userid=0) darf keine Gruppen gründen.","Nachricht"=>"Die Gruppe konnte nicht gegründet werden: es wurde kein Gruppenadmin eingetragen.");
$FEHLER[642] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen gründen, aber es wurde kein Name angegeben.","Nachricht"=>"Die Gruppe konnte nicht gegründet werden, weil kein Name für die Gruppe eingetragen wurde.");
$FEHLER[643] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen gründen, aber es wurde eine zu niedrige Spielerzahl eingetragen, mindestens zwei.","Nachricht"=>"Die Gruppe konnte nicht gegründet werden, weil mindestens zwei Mitspieler zusammenspielen sollen.");
$FEHLER[644] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen gründen, aber die Angaben für Präferenz der Region und des Settings fehlen.","Nachricht"=>"Die Gruppe konnte nicht gegründet werden, weil Angaben zur Präferenz der Region und des Settings fehlen.");
$FEHLER[645] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen gründen, aber die Kurzhinweise/Erfahrung fehlt.","Nachricht"=>"Die Gruppe konnte nicht gegründet werden, weil die Kurzhinweise zur Zusammensetzung der Gruppe fehlt.");
$FEHLER[646] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Benutzer wollte eine Gruppen gründen, aber die ausführliche Beschreibung fehlt.","Nachricht"=>"Die Gruppe konnte nicht gegründet werden, weil die ausführliche Beschreibung fehlt.");

$FEHLER[650] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es wurde eine Anfrage an den LogServer geschickt, ohne ein Kommando zu speizifizieren","Nachricht"=>"'Kein Kommando für ihre Nachricht angegeben!'");
$FEHLER[651] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es wurde eine Anfrage an den LogServer geschickt, ohne einen Inhalt zu speizifizieren","Nachricht"=>"'Kein Inhalt für ihre Nachricht angegeben!'");
$FEHLER[652] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es wurde eine Anfrage an den LogServer geschickt mit unzulässigem Kommando!","Nachricht"=>"Falsches Kommando für ihre Nachricht angegeben!");
$FEHLER[653] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es wurde ein Log-Nachricht an den LogServer geschickt ohne Textinhalt.!","Nachricht"=>"Kein Text geschickt!");
$FEHLER[654] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollten alle Nachrichten seit einer bestimmten geladen werden. Aber es wurde diese bestimmte Nachricht gar nicht angegeben!","Nachricht"=>"Die Nachrichten konnten nicht geladen werden.");

$FEHLER[655] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer für die Gruppe angenommen werden, aber es wurde keine Abenteuer-Id angegeben.","Nachricht"=>"Das Abenteuer konnte nicht hinzugefügt werden!");
$FEHLER[656] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte die Gruppe verändert werden (Mitspieler, Charaktere, Abenteuer), aber es gibt noch ein aktives Abenteuer.","Nachricht"=>"Sie dürfen die Gruppe nicht verändern, wenn noch ein Abenteuer läuft.");
$FEHLER[657] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer für die Gruppe angenommen werden, aber noch nicht alle Spieler haben einen Charakter.","Nachricht"=>"Das Abenteuer konnte nicht angenommen werden, weil nicht alle Spieler einen Charakter haben.");
$FEHLER[658] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer der Gruppe angeboten werden, aber das Abenteuer wurde bereits abgelehnt.","Nachricht"=>"Das Abenteuer wurde von der Gruppe bereits abgelehnt.");
$FEHLER[659] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler sollte aus der Gruppe ausgeschlossen werden, ist aber gar kein Mitglied!","Nachricht"=>"Der Spieler ist nicht Mitglied der Gruppe!");
$FEHLER[660] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler sollte aus einer Gruppe ausgeschlossen werden von einem Spieler, der weder Admin noch der betroffene ist. Dies geht nicht!","Nachricht"=>"Sie dürfen den Spieler nicht ausschließen!");
$FEHLER[661] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler sollte aus einer Gruppe ausgeschlossen werden, obwohl das Abenteuer noch läuft!","Nachricht"=>"So lange ein Abenteuer läuft, darf niemand ausgeschlossen werden!");
$FEHLER[662] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler sollte aus einer Gruppe ausgeschlossen werden, doch es wurde nicht angegeben, wen es betrifft!","Nachricht"=>"Sie haben nicht den Spieler angegeben, der ausgeschlossen werden soll.");
$FEHLER[663] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Ein Spieler sollte aus einer Gruppe ausgeschlossen werden, doch es konnte nicht in der Datenbank gespeichert werden!","Nachricht"=>"Der Spieler konnte nicht aus der Gruppe ausgeschlossen werden.");

$FEHLER[670] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer gestartet werden, dabei gibt es noch gar keine Gruppe!","Nachricht"=>"Sie können das Abenteuer nicht ohne Gruppe starten.");
$FEHLER[671] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer gestartet werden, dabei sind noch gar nicht alle Helden einer Gruppe zugeordnet!","Nachricht"=>"Nicht alle Charktere haben eine Szene.");
$FEHLER[672] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Abenteuer gestartet werden, dabei ist es nicht mehr im Vorbereitungsmodus!","Nachricht"=>"Das Abenteuer läuft bereits oder ist schon beendet.");

$FEHLER[680] = array("NIV"=>4,"KAT"=>"ABENT","Beschreibung"=>"Es sollte ein Bild gelöscht werden, doch es wurde kein Bild-Key angegeben.","Nachricht"=>"Es wurde kein Bild gelöscht, weil nicht angegeben werden, welches gelöscht werden soll.");


##	Fehler der Administration in den Basismodulen
##

$FEHLER[801] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber die maximale Upload Größe aus der php.ini wurde überschritten.","Nachricht"=>"Die Datei konnte nicht hochgeladen werden!");
$FEHLER[802] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber die maximale Upload Größe aus dem Formular wurde überschritten.","Nachricht"=>"Die Datei konnte nicht hochgeladen werden!");
$FEHLER[803] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber die Datei wurde nur teilweise übertragen","Nachricht"=>"Die Datei konnte nicht hochgeladen werden!");
$FEHLER[804] = array("NIV"=>3,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber es war keine Bilddatei.","Nachricht"=>"Die Datei wird nicht heruntergeladen");
$FEHLER[805] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber es wurde keine Datei hochgeladen.","Nachricht"=>"Die Datei konnte nicht hochgeladen werden!");
$FEHLER[806] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber es wurde keine unterstützte Bilddatei hochgeladen.","Nachricht"=>"Es wurde kein unterstütztes Format hochgeladen!");
$FEHLER[807] = array("NIV"=>3,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber das BMP-Format wird nicht unterstützt, daher wurde der Upload abgebrochen.","Nachricht"=>"Der Upload von *.bmp-Dateien wird nicht unterstützt. Wandelt die Datei bitte erst in ein *.png oder *.jpg um. Dies ist u.a. auf dieser Webseite möglich: <a href=\"http://www.pictureresize.org/online-images-converter.html\">http://www.pictureresize.org/online-images-converter.html</a>.");
$FEHLER[808] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde eine Datei zum hochladen per POST Anfrage gesendet, aber es wurde keine Bilddatei hochgeladen.","Nachricht"=>"Es gab einen Fehler beim Kopieren des Bildes in die Heldenschenke!");

$FEHLER[811] = array("NIV"=>3,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Bild gelöscht werden, doch es konnte nicht aus der Datenbank entfernt werden.","Nachricht"=>"Das Bild wurde nicht gelöscht.");
$FEHLER[812] = array("NIV"=>3,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Bild gelöscht werden, es ist in der Datenbank nicht mehr vorhanden.","Nachricht"=>"Das Bild wurde unvolständig gelöscht.");
$FEHLER[813] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte eine Kategorie gelöscht werden. Alle Bilder wurden bereits entfernt. Doch die Kategorie konnte nicht aus der Datenbank gelöscht werden.","Nachricht"=>"Die Kategorie wurde unvollständig gelöscht.");
$FEHLER[814] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte eine Kategorie erstellt werden.","Nachricht"=>"Die Kategorie konnte nicht in die Datenbank geschrieben.");
$FEHLER[815] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Thumbnail von einem Bild erstellt werden.","Nachricht"=>"Es wurde kein Zielpfad angegeben");
$FEHLER[815] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte überprüft werden, ein Benutzer eine Aktion in picto ausführen darf, doch es wurde keine Kategorie angegeben.","Nachricht"=>"Es wurde keine Kategorie angegeben.");
$FEHLER[816] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte überprüft werden, ein Benutzer eine Aktion in picto ausführen darf, doch es wurde keine zulässige Aktion angegeben.","Nachricht"=>"Es wurde zulässige picto-Aktion angegeben.");
$FEHLER[817] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Bild wurde von einer Kategorie in eine andere verschoben, doch die neue Kategorie und das Bild sind nicht in Ordnung nicht.","Nachricht"=>"Das Bild konnte nicht verschoben werden.");
$FEHLER[818] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Bild wurde von einer Kategorie in eine andere verschoben, das Bild konnte nicht kopiert werden.","Nachricht"=>"Das Bild konnte nicht verschoben werden.");
$FEHLER[819] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Bild wurde von einer Kategorie in eine andere verschoben, das Bild konnte in der Datenbank nicht umgeschrieben werden.","Nachricht"=>"Das Bild konnte nicht vollständig verschoben werden.");

$FEHLER[820] = array("NIV"=>2,"KAT"=>"ADMIN","Beschreibung"=>"Es sollten Daten über ein Bild ausgegeben werden, doch die Bildresource existiert nicht!","Nachricht"=>"Es gab einen Fehler beim Laden von Bilddaten.");
$FEHLER[821] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollten die Stildaten für ein Bild ausgegeben werden, doch die Daten des Stils konnten nicht geladen werden!","Nachricht"=>"Es gab einen Fehler beim Laden von Bilddaten.");
$FEHLER[822] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Picto wurde mit einem falschen Argument aufgerufen. Es konnte nicht verarbeitet werden.","Nachricht"=>"Falsches Argument für Picto");
$FEHLER[823] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollten Daten über ein Bild ausgegeben werden, doch das Offset existiert nicht!","Nachricht"=>"Es gab einen Fehler beim Laden von Bilddaten.");
$FEHLER[824] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Bild wurde von einer Kategorie in eine andere verschoben, das Bild konnte in der Datenbank nicht umgeschrieben werden.","Nachricht"=>"Das Bild konnte nicht vollständig verschoben werden.");
$FEHLER[825] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Eine Kategorie-Id sollte aus einem Bild geladen werden, doch das Bild existiert nicht.","Nachricht"=>"Die Picto-Kategorie konnte nicht geladen werden.");

$FEHLER[890] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Die Beschreibungsdaten für eine Benutzergruppe sollten geändert werden, dürfen aber nicht leer sein.","Nachricht"=>"Name und Kommentar der Benutzergruppe konnten nicht geändert werden.");
$FEHLER[891] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Die Beschreibungsdaten für eine Benutzergruppe sollten geändert werden, aber es gab keine Änderung in der Datenbank. Vielleicht hat der Benutzer nichts geändert?","Nachricht"=>"Name und Kommentar der Benutzergruppe wurden nicht geändert.");
$FEHLER[892] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Der unbekannte Gast sollte eine andere Gruppe erhalten als die des unbekannten Gastes! Dies ist nicht erlaubt!","Nachricht"=>"Die Benutzergruppe wurde nicht geändert!");
$FEHLER[893] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Der root-Benutzer sollte eine andere Gruppe als Administrator erhalten. Stattdessen sollte er gelöscht werden! Suchen Sie ihn dazu mit der Benutzersuche als root und löschen Sie ihn anschließen!","Nachricht"=>"Die Benutzergruppe wurde nicht geändert!");
$FEHLER[894] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Der unbekannte Gast sollte gelöscht werden. Dies ist jedoch nicht möglich!","Nachricht"=>"Der unbekannte Gast kann nicht gelöscht werden!");
$FEHLER[895] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Benutzer sollte gelöscht werden, doch es hat nicht funktioniert!","Nachricht"=>"Der Benutzer konnte nicht gelöscht werden!");
$FEHLER[896] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Benutzer sollte gelöscht werden, die die Seitenberechtigungen konnten nicht entfernt werden!","Nachricht"=>"Der Benutzer konnte nicht gelöscht werden!");
$FEHLER[897] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Benutzer sollte gelöscht werden, die die Administrationsberechtigungen konnten nicht entfernt werden!","Nachricht"=>"Der Benutzer konnte nicht gelöscht werden!");


$FEHLER[899] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"","Nachricht"=>"","ExecCode"=>'');

$FEHLER[900] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Das Zugriffslevel wurde nicht als USERGROUPS::ADMIN festgelegt, obwohl die Funktion nur auf den eigenen Benutzeraccount angewendet werden darf, damit nur den Admins oder dem eigenen Benutzer zusteht!","Nachricht"=>"Die Zugangsberechtigung wurde falsch konfiguriert.","ExecCode"=>'');
$FEHLER[901] = array("NIV"=>3,"KAT"=>"ADMIN","Beschreibung"=>"Falsche Zugangsdaten zur Administration eingegeben.","Nachricht"=>"FEHLER","ExecCode"=>'');
$FEHLER[902] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Ein gebannter Nutzer oder unbekannter Gast hat versucht die Administration zu betreten.","Nachricht"=> ">Heldenschenke</a> ein.","ExecCode"=>'');
$FEHLER[903] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Die aktuellen Userdaten (ID, Name, Sessionkey, Adminkey) stimmen nicht überein.","Nachricht"=>"Aktuellen Sessiondaten (Id, Name, Sessionkey, Adminkey stimmen nicht überein.","ExecCode"=>'');
$FEHLER[904] = array("NIV"=>1,"KAT"=>"ZUGRIFF","Beschreibung"=>"Es sollten Benutzerdaten zum Unbekannten Gast ohne Adminberechtigung abgerufen werden.","Nachricht"=>"Sie haben keinen Zugriff auf die Daten es unbekannten Gastes.","ExecCode"=>'echo $main->_CATCHED_USER_VARS;');
$FEHLER[905] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Admin-Modul ohne Initialisierung mit einem Benutzer aufgerufen werden.","Nachricht"=>"Fehler beim Zugriff auf das Modul.","ExecCode"=>'');
$FEHLER[906] = array("NIV"=>4,"KAT"=>"USER","Beschreibung"=>"Ein Benutzer hat versucht, zwei verschiedene Passwörter beim Registrieren eingegeben.","Nachricht"=>"Die Registrierung war nicht erfolgreich. Geben Sie zweimal das Passwort ein!");
$FEHLER[907] = array("NIV"=>4,"KAT"=>"USER","Beschreibung"=>"Das neue Passwort und der Salt konnten nicht in der Datenbank gespeichert werden!","Nachricht"=>"Das neue Passwort konnten nicht gespeichert werden.");
$FEHLER[908] = array("NIV"=>4,"KAT"=>"USER","Beschreibung"=>"Es sollte eine E-Mailadresse eingetragen werden für einen Spieler, doch sie lag nicht im orgendlich Format vor.","Nachricht"=>"Der Vorgang wurde abgebrochen, weil die E-Mailadresse nicht das richtige Format hat.");
$FEHLER[909] = array("NIV"=>4,"KAT"=>"USER","Beschreibung"=>"Es sollte ein Benutzer registriert werden, doch der Name existiert sie bereits.","Nachricht"=>"Der Vorgang wurde abgebrochen, weil der Name bereits von einem anderen verwendet wird.");
$FEHLER[910] = array("NIV"=>4,"KAT"=>"USER","Beschreibung"=>"Es sollte ein neuer Benutzerregistriert werden, doch der Name entsprach nicht den Vorgaben.","Nachricht"=>"Der Spielername darf nicht leer sein und kann Buchstaben, Zahlen, Bindestriche, Unterstriche und Leerzeichen enthalten. Zwischen den Sonderzeichen müssen mindestens zwei Zeichen liegen.");

$FEHLER[911] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Ein Nutzer hat versucht, ein Modul aufzurufen, für den er nicht die Berechtigung besitzt.","Nachricht"=>"Zugang nicht gestattet!");
$FEHLER[912] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Bei der Berechtigungsüberprüfung zur Administration ist ein Fehler aufgetreten.","Nachricht"=>"Es ist ein Fehleraufgetreten bei der Berechtigungsprüfung");
$FEHLER[913] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde kein Admin-Bereich angegeben, der aufgerufen werden soll.","Nachricht"=>"Das Modul kann nicht aufgerufen werden.");
$FEHLER[914] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Eine Seitenberechtigung konnte nicht in der DB gespeichert werden.","Nachricht"=>"Einstellungen wurden nicht gespeichert.");
$FEHLER[915] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Admin-Modul aufgerufen werden, das nicht existiert.","Nachricht"=>"Das Modul kann nicht aufgerufen werden.");
$FEHLER[916] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es soll ein Modul aufgerufen werden, das mehrfach in der Datenbank registiert ist.","Nachricht"=>"Das Modul kann nicht geladen werden.");

$FEHLER[910] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Ein bestehender verwaister Text sollte gelöscht werden, aber es wurden mehr Texte gelöscht!!!!","Nachricht"=>"Durch einen Fehler wurden mehr Texte gelöscht.");
$FEHLER[917] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Ein bestehender verwaister Text sollte gelöscht werden, wurde trotz Anweisung aber nicht gelöscht.","Nachricht"=>"Der Text konnte konnte nicht gelöscht werden.");
$FEHLER[918] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Ein bestehender verwaister Text sollte gelöscht werden, war aber gar nicht verwaist, sondern gehört noch zu einer Seite, die angegeben ist, unter den zusätzlichen Informationen","Nachricht"=>"Der Text konnte konnte nicht gelöscht werden.");
$FEHLER[919] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Ein bestehender Text konnte nicht bei der Seite eingetragen werden. Der Text ist aber in der Datenbank vorhanden und in diesem Datensatz die Seite auch schon vorgemerkt.","Nachricht"=>"Der Text konnte bei der Seite nicht eingehangen werden.");
$FEHLER[920] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Eine Seite sollte geloescht werden, obwohl eine Unterseite existierte. Dies geht nicht.","Nachricht"=>"Die Seite kann nicht geloescht werden, weil Unterseiten von ihre abhängen.");
$FEHLER[921] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Ein Skript sollte mit einer Seite verknüpft werden.","Nachricht"=>"Das Skript wurde nicht mit der Seite verknüpft.");
$FEHLER[922] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht einen Text zu speichern, was aber nicht funktioniert hat.","Nachricht"=>"Ein Text konnte nicht aktualisiert werden.");
$FEHLER[923] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht einen neuen Text zu erstellen.","Nachricht"=>"Ein Text konnte nicht erstellt werden.");
$FEHLER[924] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Beim Löschen eines Navielements wurden nicht alle Elemente wieder gespeichert, obwohl das Navielement selbst gelöscht wurde.","Nachricht"=>"Beim Löschen eines Navielements wurden nicht alle anderen Elemente wieder eingefügt.","ExecCode"=>'echo "<p>Anzahl der der eingefügten Zeilen:".$this->get_aff_rows()."<p>";');
$FEHLER[925] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, ein Navielement zu löschen.","Nachricht"=>"Das Navielement konnte nicht gelöscht werden.","ExecCode"=>'echo "<p>Mysql-Abfrage:".$this->get_abfrage()."<p>";');
$FEHLER[926] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, ein Navielement zu verschieben.","Nachricht"=>"Das Navielement konnte nicht verschoben werden.","ExecCode"=>'');
$FEHLER[927] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, ein Navielement über den Rand hinaus zu schieben.","Nachricht"=>"Das Navielement konnte nicht verschoben werden, da es schon das erste oder letzte ist und nicht höher oder tiefer geschoben werden kann.","ExecCode"=>'');
$FEHLER[928] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, eine Navigruppe über den Rand hinaus zu schieben.","Nachricht"=>"Die Navigruppe konnte nicht verschoben werden, da es schon das erste oder letzte ist und nicht höher oder tiefer geschoben werden kann.","ExecCode"=>'');
$FEHLER[929] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, eine Navigruppe zu verschieben.","Nachricht"=>"Die Navigruppe konnte nicht verschoben werden.","ExecCode"=>'');
$FEHLER[930] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Ein Seite konnte nicht gelöscht werden.","Nachricht"=>"Die Seite konnte nicht gelöscht werden.","ExecCode"=>'');
$FEHLER[931] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, eine für Admins geschützte Seite aufzurufen.","Nachricht"=>"Zugriff untersagt. Diese Seite ist für Administratoren geschützt.","ExecCode"=>'');
$FEHLER[932] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, eine Seite erneut einzuhängen.","Nachricht"=>"Die Seite konnte nicht eingehangen werden.","ExecCode"=>'');
$FEHLER[933] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, das Titelbild zuändern.","Nachricht"=>"Das Titelbild konnte nicht geändert werden.","ExecCode"=>'');
$FEHLER[934] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es sollten Text oder Skript als Seiteninhalt gespeichert werden.","Nachricht"=>"Der neue Seiteninhalt konnte nicht gespeichert werden.","ExecCode"=>'');
$FEHLER[935] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein leerer Text erstellt werden.","Nachricht"=>"Der neue Text konnte nicht erzeugt werden","ExecCode"=>'');
$FEHLER[936] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, eine neue Navigationsgruppe zu erstellen.","Nachricht"=>"Es konnte keine neue Gruppe erstellt werden.","ExecCode"=>'');
$FEHLER[937] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, ein neues Element in eine Navigationsgruppe einzuhängen.","Nachricht"=>"Das Element konnte in die Navigruppe nicht eingehangen werden.","ExecCode"=>'');
$FEHLER[938] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, einen Element in eine Navigationsgruppe einzuhängen. Doch die Gruppe existiert nicht.","Nachricht"=>"Das Element konnte in die Navigruppe nicht eingehangen werden.","ExecCode"=>'');
$FEHLER[938] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, einen Link auf einer Seite zu erzeugen. Doch sie existiert nicht.","Nachricht"=>"Der Link konnte nicht erzeugt werden.","ExecCode"=>'');
$FEHLER[940] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, einen Container in einer Navigruppe inzugängen zu erzeugen. Doch er existiert nicht.","Nachricht"=>"Der Container konnte nicht eingehangen werden.","ExecCode"=>'');
$FEHLER[941] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, einen Container zu erstellen.","Nachricht"=>"Der Container konnte nicht erstellt werden.","ExecCode"=>'');

$FEHLER[942] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, die Namen aller Stile zu laden.","Nachricht"=>"Alle Stile konnten nicht geladen werden.","ExecCode"=>'');
$FEHLER[943] = array("NIV"=>0,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht, die Daten eines Stils zu laden.","Nachricht"=>"Der Stil konnte nicht geladen werden.","ExecCode"=>'');
$FEHLER[944] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht die Stildatei zu löschen. Dies wurde verhindert.","Nachricht"=>"Der Stil konnte nicht gelöscht werden.","ExecCode"=>'');
$FEHLER[945] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es wurde versucht ein Feld eines Stiles zu leeren.","Nachricht"=>"Der Stil konnte nicht aktualisiert werden.","ExecCode"=>'');

$FEHLER[950] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Ereignis geladen werden, aber ein Ereignis mit diesem Key existiert nicht.","Nachricht"=>"Das Ereignis existiert nicht und konnte daher auch nicht dargstellt werden.");
$FEHLER[951] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Fehler geladen werden, aber ein Fehler mit dieser Nummer existiert nicht.","Nachricht"=>"Der Fehler existiert nicht und konnte daher auch nicht dargstellt werden.");
$FEHLER[952] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Es sollte ein Fehlerniveau geladen werden, aber dieses Fehlerniveau existiert nicht.","Nachricht"=>"Das Fehlerniveau konnte daher auch nicht dargstellt werden.");

$FEHLER[1000] = array("NIV"=>1,"KAT"=>"ADMIN","Beschreibung"=>"Das Datenbanksystem hat folgenden Fehler gefunden:","Nachricht"=>"Es ist ein Fehler mit dem Datenbanksystem aufgetreten.");

#PHP#
$FEHLER[1024] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_USER_NOTICE: Ein Sicherheitshinweis musste gemacht werden.");
$FEHLER[2048] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_STRICT: Ein wichtiger Hinweis zum aktuellen Programmcode wurde gemacht!");
$FEHLER[4096] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_RECOVERABLE_ERROR: Ein potentiell gefährlicher Fehler ist aufgetreten.");
$FEHLER[8192] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_DEPRECATED: Eine veraltete Programmfunktion wurde verwendet!");
$FEHLER[16384] = array("NIV"=>0,"KAT"=>"KRIT","Beschreibung"=>"N/A","Nachricht"=>"E_USER_DEPRECATED: Eine veraltete Programmfunktion wurde verwendet!");


$FEHLER[40000] = array("KAT"=>"ADMIN","Beschreibung"=>"Testfehler","Nachricht"=>"Testfehler für db_mysql_secure->secure_abfrage()!");
$FEHLER[50000] = array("KAT"=>"ADMIN","Beschreibung"=>"Testfehler","Nachricht"=>"Testfehler für Skripte");



