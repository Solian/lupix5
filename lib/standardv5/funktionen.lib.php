<?php

#################################################
##
##	 Bibliothek: Funktionen der Heldenschenke!
##
#################################################
##
##	 Name:		   	Standard Version 2
##
##	 Dateiname:    	funktionen.lib.php
##
##	 Version:	   	2.0
##
##	 Datum:			16.2.2010
##
##	 Bearbeiter: 	Remus
##
##
#################################################
## Browser bestimmen das muss doch in die bibliothek
##

function getagent($http_user_agent) {

    if (!empty($http_user_agent)) {
        $browser = get_browser(null, true);
        return $browser["browser"] . " " . $browser["version"];
    } else
        return false;
}

##	Erstellt eine Zufallszeichenkette
##
/**
 * @param int $argLongTerm
 * @return string
 */

function zufallszahl($argLongTerm = 0) {

    try {
        $aWerte = array(chr(random_int(97, 122)), chr(random_int(97, 122)), chr(random_int(97, 122)), chr(random_int(97, 122)), chr(random_int(65, 90)), chr(random_int(65, 90)), chr(random_int(65, 90)), chr(random_int(48, 57)), chr(random_int(48, 57)), chr(random_int(48, 57)));
    }catch(Throwable $e){
        $aWerte = array(chr(rand(97, 122)), chr(rand(97, 122)), chr(rand(97, 122)), chr(rand(97, 122)), chr(rand(65, 90)), chr(rand(65, 90)), chr(rand(65, 90)), chr(rand(48, 57)), chr(rand(48, 57)), chr(rand(48, 57)));
    }
    //Fügt bei einem anderen Wert als NULL bei argLongTerm jeweils argLongTerm * 3 Zeichen hinzu.
    for ($i = 0; $i < $argLongTerm; $i++) {
        try {
            $aWerte = array_merge($aWerte, array(chr(random_int(97, 122)), chr(random_int(48, 57)), chr(random_int(65, 90))));
        }catch(Throwable $e){
            $aWerte = array_merge($aWerte, array(chr(rand(97, 122)), chr(rand(48, 57)), chr(rand(65, 90))));
        }
    }

    try {
        $anzahlDerRunsInteger = random_int(0, 1000);
    }catch(Throwable $e){
        $anzahlDerRunsInteger = rand(0, 1000);
    }
    for($i=0;$i<$anzahlDerRunsInteger;$i++){shuffle($aWerte);}

    return implode("", $aWerte);

}


function generateConfirmCode($argLongTerm = 0) {

    try {
        $aWerte = array(chr(random_int(97, 122)), chr(random_int(97, 122)), chr(random_int(65, 90)), chr(random_int(65, 90)), chr(random_int(48, 57)), chr(random_int(48, 57)));
    }catch(Throwable $e){
        $aWerte = array(chr(rand(97, 122)), chr(rand(97, 122)), chr(rand(65, 90)), chr(rand(65, 90)), chr(rand(48, 57)), chr(rand(48, 57)));
    }
    //Fügt bei einem anderen Wert als NULL bei argLongTerm jeweils argLongTerm * 3 Zeichen hinzu.
    for ($i = 0; $i < $argLongTerm; $i++) {
        try {
            $aWerte = array_merge($aWerte, array(chr(random_int(97, 122)), chr(random_int(48, 57)), chr(random_int(65, 90))));
        }catch(Throwable $e){
            $aWerte = array_merge($aWerte, array(chr(rand(97, 122)), chr(rand(48, 57)), chr(rand(65, 90))));
        }
    }

    try {
        $anzahlDerRunsInteger = random_int(0, 1000);
    }catch(Throwable $e){
        $anzahlDerRunsInteger = rand(0, 1000);
    }
    for($i=0;$i<$anzahlDerRunsInteger;$i++){shuffle($aWerte);}

    return implode("", $aWerte);

}

##	Dateiname des Skripts
##

function script_name($pfad) {
    $pfad = explode("/", $pfad);

    return array_pop($pfad);
}

/*
 * Wandelt ein DateTime aus der Datenbank in das deutsche Format um.
 * @param   string	DateTime-String aus der Datenbank.
 * @return  string	Formatiertes Datum.
 */

function datetime_format($dt) {
    try {
        $date = new DateTime($dt);
        return $date->format('d.m.Y H:i') . ' Uhr';
    } catch (Exception $e) {
        return $dt;
    }
}

function zeit_format($timestamp) {

    $jetzt = time();

    $div = $jetzt - $timestamp;

    if ($timestamp == 0) {

        $timestamp = "Noch nie!";
    } else {


        $jahre = bcdiv($div, 86400 * 365, 0);
        $tage = bcdiv($div - $jahre * 86400 * 365, 86400, 0);
        $stunden = bcdiv($div - $jahre * 86400 * 365 - $tage * 86400, 3600, 0);
        $minuten = bcdiv($div - $jahre * 86400 * 365 - $tage * 86400 - $stunden * 3600, 60, 0);
        $sekunden = bcdiv($div - $jahre * 86400 * 365 - $tage * 86400 - $stunden * 3600 - $minuten * 60, 1, 0);

        if ($jahre == "0y")
            $jahre = "";
        if ($tage == "0d")
            $tage = "";
        if ($stunden == "0h")
            $stunden = "";
        if ($minuten == "0min")
            $minuten = "";
        if ($sekunden == "0s")
            $sekunden = "";

        ########## Farbanpassung
        /* if( 0 <= $timestamp && $timestamp <= 5 )$timestamp="<font color=\"green\"><b>$jahre $tage $stunden $minuten $sekunden<b></font>";
          if( 5 <= $timestamp && $timestamp <= 10 )$timestamp="<font color=\"green\">$jahre $tage $stunden $minuten $sekunden</font>";
          if( 10 <= $timestamp && $timestamp <= 30 )$timestamp="<font color=\"orange\">$jahre $tage $stunden $minuten $sekunden</font>";
          if( 30 <= $timestamp && $timestamp <= 120 )$timestamp="<font color=\"darkorange\">$jahre $tage $stunden $minuten $sekunden</font>";
          if( 120 <= $timestamp && $timestamp <= 365 )$timestamp="<font color=\"red\">$jahre $tage $stunden $minuten $sekunden</font>";
          if( 365 <= $timestamp)$timestamp="<font color=\"darkred\">$jahre $tage $stunden $minuten $sekunden</font>"; */

        $timestamp = $jahre . ' Jahre ' . $tage . ' Tage ' . $stunden . ':' . $minuten . ':' . $sekunden . '';
    }
    return $timestamp;
}

##	Erstellt eine Checkbox mit dem übergebenen Namen, die je nach dem Zustant von Wert (1,0) ein Häckchen hat oder nicht.
##

function checkbox_from_bool($name, $wert, $label = "", $insert='') {

    if ($wert == 1)
        $checked = " checked=\"checked\"";
    else
        $checked = "";

    if ($label != "")
        $label = "<div class=\"input-group-prepend\"><span class=\"input-group-text\">$label</span></div>";

    return "$label<div class=\"input-group-append\">
                <div class=\"input-group-text\">
                    <input type=\"checkbox\" name=\"$name\"$checked $insert>
                </div>
            </div>";
}


function custom_checkbox_from_bool($name, $wert, $insert='') {

    if ($wert == 1)
        $checked = " checked=\"checked\"";
    else
        $checked = "";

    return "<input type=\"checkbox\" name=\"$name\"$checked $insert />";
}

##	Die Passwortverschluesselung der Heldenschenke
##

function verschluesselung2($text) {
    return md5(str_rot13(md5(str_rot13(md5(str_rot13(md5(str_rot13(md5($text)))))))));
}

function verschluesselung($text) {
    return md5(str_rot13(md5(str_rot13(md5(str_rot13(md5(str_rot13(md5($text)))))))));
}

function get_signed_int($in) {
    $int_max = pow(2, 31) - 1;
    if ($in > $int_max) {
        $out = $in - $int_max * 2 - 2;
    } else {
        $out = $in;
    }
    return $out;
}

##	String Funktion 	Vervielfachung des Strings um den angegebenen Faktor
##

function strmult($string, $faktor) {
    if ($faktor > 0){
        return $string . strmult($string, $faktor-1);
    }
    else
        return "";
}

##	Läd aus dem Array das Elemetn i, wenn es nicht existiert, wird false zurückgegeben
##

function get_array_element(&$aArray, $mKey) {
    if (is_array($aArray)) {
        if (array_key_exists($mKey, $aArray)) {
            return $aArray[$mKey];
        } else {
            return false;
        }
    } else {
        return false;
    }
}



function adventure_textfeld(&$klasse, $feldname, $feldinhalt = "", $aboptionen = "", $pfad_zu_scripts_ordner='', $rows = "20", $cols = "95", $feldid = "textfeld") {


## Ermittlung der aktuellen Schriftfarbe
    /* $stil=$klasse->get_SITE_CONTENT("stil");
      $stil=$klasse->sabfrage("SELECT standardtxtcol FROM main_stile WHERE name='$stil'");
      $stil=$klasse->get_lsgs(1);$standardtxtcol=$stil["standardtxtcol"]; */

    ## Ausgabe der Steuerleiste
    ## HÄ?
    ## $w6=picto("%w6");
    ## $w20="<img src='bilder/w20.png' width='30' />";



    return <<<TEXTFELD
  <script src="{$pfad_zu_scripts_ordner}scripts/bbcodeeditor.js" type="text/javascript"></script>
  
<h1 class="subtitle">
    <i class="button hinweis" style="font-weight:normal;font-style:normal;text-decoration:none;" onclick="insert_text('[titel]','$feldid','[/titel]');">&Uuml;berschrift</i>
    <a class="button hinweis" style="font-weight:normal;font-style:normal;text-decoration:none;" onclick="insert_text('[p]','$feldid','[/p]');">Absatz</a>&nbsp;&nbsp;
    <a class="button hinweis" style="font-weight:normal;font-style:italic;text-decoration:none;" onclick="insert_text('[i]','$feldid','[/i]');"><span class="fa fa-comment-o"></span> ''Rede''</a>&nbsp;&nbsp; &nbsp;&nbsp;
    <a class="button hinweis" style="font-weight:normal;font-style:normal;text-decoration:none;" onclick="appendImageOptions()"><span class="fa fa-image"></span> Bild</a>
    <a class="button hinweis" style="font-weight:normal;font-style:italic;text-decoration:none;" onclick="insert_text('[karte]','$feldid','[/karte]');"><span class="fa fa-map"></span> Karte</a>&nbsp;&nbsp;
    <a class="button hinweis" style="font-weight:normal;font-style:italic;text-decoration:none;" target="_blank" href="https://karte.heldenschenke.de"><span class="fa fa-map-marker"></span>Kartenwerk</a>
</h1>
					

    <textarea id="$feldid" name="$feldname" cols="$cols" rows="$rows">$feldinhalt</textarea>


TEXTFELD;

}

## Damit kann ein Array erstellt werden, wobei die Datein gefiltert werden können.
##

/**
 * @param string $outerDir
 * @param array $filters
 * @param bool $withoutFileExtions
 * @return array
 */
function getDirectoryTree(string $outerDir, $filters = array(),bool $withoutFileExtions=false) {
    $dirs = array_diff(scandir($outerDir), array_merge(array(".", ".."), $filters));
    $dir_array = array();
    foreach ($dirs as $d)
        $dir_array[$d] = is_dir($outerDir . "/" . $d) ? getDirectoryTree($outerDir . "/" . $d, $filters) : $dir_array[$d] = ($withoutFileExtions? substr($d,0,strrpos($d,'.')):$d);
    return $dir_array;
}

function strip_nach_leertaste($argText) {
    $sText = strval($argText);
    $sbRestText = stristr($sText, " ", true);
    if (!$sbRestText)
        return $sText;
    else
        return $sbRestText;
}

function create_error_msg(Exception $e) {
    global $C_LOGLEVEL;

    $sNachricht='';
    $sBeschreibung='';
    $sFehlernachricht='';

    // Erstmal handelt es sich um kritische Fehler, wenn müssen die if's den Fehler herunterstufen
    $iFehlerlevel = 0;
    if ($e->getCode() > 999 && $e->getCode() < 5000) {
        $sBeschreibung = $e->getMessage();
        $sNachricht = "Es ist Fehler bei einer Datenbankabfrage aufgetreten.";
        $sFehlernachricht = "Mysql-Fehler " . $e->getCode();
        $sExecCodeErgebnis = $e->getMessage();
        $iFehlerlevel = 0; //DB Fehler sind grundsätzlich hochkritisch, weil sie die Konsistenz der DB gefährden.
    } elseif ($e->getCode() < 1000) {
        require("fehler.lib.php");
        /** @var array $FEHLER */
        $sBeschreibung = $FEHLER[$e->getCode()]["Beschreibung"];
        $sNachricht = $FEHLER[$e->getCode()]["Nachricht"];
        $sFehlernachricht = $e->getMessage();
        $iFehlerlevel = $FEHLER[$e->getCode()]["NIV"];
        ob_start();
        eval($FEHLER[$e->getCode()]["ExecCode"]);
        $sExecCodeErgebnis = ob_get_contents();
        ob_end_clean();
    } elseif ($e->getCode() > 5000) {
        null;
        // Hier kommen die Fehlermeldung des Moduls hin, die eben nicht zum Modul gehören.
    }

    // Ereignisverfolgung erstellen.
    $aVerfolgungsarray = $e->getTrace();
    $sEreignisverfolgung = "";
    foreach ($aVerfolgungsarray as $aEbene) {
        $sEreignisverfolgung .= '<p>' . $aEbene["file"] . '(' . $aEbene["line"] . ') - ' . $aEbene["function"] . '()</p>';
    }

    //Fehlerkey erstellen ein md5 hash aus zeit, fehlernummer, ort und nutzernummer
    $iZeit = time();

    //$sFehlerkey=md5($iZeit.$e->getCode().$e->getFile().$this->get_USER_IDENT("userid"));
    $sFehlerkey = md5($iZeit . $e->getCode() . $e->getFile());


    if (!isset($sExecCodeErgebnis))
        $sExecCodeErgebnis = "Kein Code ausgeführt.";

    if ($iFehlerlevel <= $C_LOGLEVEL) {
        null;
        //$this->events_log_setzen($sFehlerkey,$iZeit,$e->getFile(),$e->getCode(),$sExecCodeErgebnis,$e->getLine(),$sEreignisverfolgung,$iFehlerlevel,$sFehlernachricht);
    }

    $tHauptInhalt = '<body class="body">
			<h1 class="fehler">Fehler</h1>
			<h2 class="subtitle">Nachricht</h2>
			<p>' . $sNachricht . '</p>';

    //Eigentlich sollen nur Admins diese Daten einsehen können
    //if($this->get_USER_IDENT("nutzergruppe_id")==1)
    {

        $tHauptInhalt .= '<h2 class="subtitle">Beschreibung:</h2>
				<p class="body">Fehler ' . $e->getCode() . ':' . $sBeschreibung . '</p>
				<h2 class="subtitle">Ausloesende Datei - Zeilennummer</h2>
				<p>' . $e->getFile() . ' - ' . $e->getLine() . '</p>
				<h2 class="subtitle">Fehlerverfolgung mit Zeilennummer</h2>
				' . $sEreignisverfolgung . '
				<h2 class="subtitle">Zusätzliche Informationen:</h2>
				<p class="body">' . $sFehlernachricht . '</p>
				<h2 class="subtitle">Ergebnis des Fehlercodes, der ausgeführt wird, wenn der Fehler passiert.</h2>
				<p class="body">' . $sExecCodeErgebnis . '</p>';
    }

    if ($iFehlerlevel <= $C_LOGLEVEL)
        $tHauptInhalt .= '
			<h2 class="subtitle">Fehlerkey:</h2>
			<p class="body">Gebt diesen Schlüssel an, wenn Ihr Euch an die Administratoren wendet: <i>' . $sFehlerkey . '</i></p>';


    $tHauptInhalt .= '<p class="body"><a href="javascript:history.back();">Zur&uuml;ck</a> <a href="index.php?aktion=adminlogout" target="_top">Logout</a></p></body>';
    return $tHauptInhalt;
}

/**
 * @param $formel
 * @return float|int
 * @throws Exception
 */
function parseTP($formel) {

    $tpStringArray = explode('W', $formel);
    $tpStringArray = array_merge(array($tpStringArray[0]), explode('+', $tpStringArray[1]));

    empty($tpStringArray[0]) ? $tpStringArray[0] = 1 : false;
    empty($tpStringArray[1]) ? $tpStringArray[1] = 6 : false;


    return intval($tpStringArray[0]) * random_int(1, intval($tpStringArray[1])) + intval($tpStringArray[2]);
}

function translateArrayKeysIntoJavascript($array) {
    $resultArray = [];
    foreach ($array as $arrayKey => $dummyValue) {
        array_push($resultArray, "'" . $arrayKey . "'");
    }
    return implode(',', $resultArray);
}

function translateArrayIntoJavascript($array) {
    $resultArray = [];
    foreach ($array as $arrayKey => $dummyValue) {
        array_push($resultArray, "'" . $arrayKey . "':'" . $dummyValue . "'");
    }
    return '{' . implode(',', $resultArray) . '}';
}

function translateArrayValuesIntoJavascript($array) {
    $resultArray = [];
    foreach ($array as $arrayKey => $dummyValue) {
        array_push($resultArray, "'" . $dummyValue . "':'" . $dummyValue . "'");
    }
    return '{' . implode(',', $resultArray) . '}';
}

function integerToRoman($integer) {
    // Convert the integer into an integer (just to make sure)
    $integer = intval($integer);
    $result = '';

    // Create a lookup array that contains all of the Roman numerals.
    $lookup = array('M' => 1000,
        'CM' => 900,
        'D' => 500,
        'CD' => 400,
        'C' => 100,
        'XC' => 90,
        'L' => 50,
        'XL' => 40,
        'X' => 10,
        'IX' => 9,
        'V' => 5,
        'IV' => 4,
        'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($integer / $value);

        // Add the same number of characters to the string
        $result .= str_repeat($roman, $matches);

        // Set the integer to be the remainder of the integer and the value
        $integer = $integer % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

// https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php

/**
 * Check if $haystack starts with $needle.
 *
 * @param string $haystack
 * @param string $needle
 * @return bool
 */
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

/**
 * Check if $haystack ends with $needle.
 *
 * @param string $haystack
 * @param string $needle
 * @return bool
 */
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

/**
 * @param string $query
 * @param array $params
 * @return string
 */


function showQuery($query, $params)
{
    $keys = array();
    $values = array();

    if($params!==NULL) {

        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/' . $key . '/';
            } else {
                $keys[] = '/[?]/';
            }

            if (is_numeric($value)) {
                $values[] = intval($value);
            } elseif (is_null($value)) {
                $values[] = 'NULL';
            } else {
                $values[] = "'" . $value . "'";
            }
        }
    }

    $query = preg_replace($keys, $values, $query, 1, $count);
    return $query;
}


function truncateHTML($html, $length)
{
    $truncatedText = substr($html, $length);
    $pos = strpos($truncatedText, ">");
    if ($pos !== false) {
        $html = substr($html, 0, $length + $pos + 1);
    } else {
        $html = substr($html, 0, $length);
    }

    preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];

    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];

    $len_opened = count($openedtags);

    if (count($closedtags) == $len_opened) {
        return $html;
    }

    $openedtags = array_reverse($openedtags);
    for ($i = 0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</' . $openedtags[$i] . '>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }


    return $html;
}

function closetags($html)
{
    preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i = 0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</' . $openedtags[$i] . '>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
}

function sanitizeStringArray($array){
    foreach($array as $key => $value){

        if(is_array($value)){
            $array[$key]=sanitizeStringArray($value);
        }else{
            $array[$key]=htmlspecialchars($value);

        }
        return $array;

    }

}
