<?php



class frontendControllerClass extends controllerBaseClass{



    function __construct(seitenklasse $seitenObjekt)
    {
        parent::__construct($seitenObjekt->userIdenty, $seitenObjekt);
    }

    /**
     * @param $fileNameString
     * @throws Exception
     */

    public function registerTemplate($fileNameString)
    {
        parent::registerTemplate($fileNameString.'.part');
    }
}
