<?php


class uploadedFiles
{

    /**
     * @var uploadedFile[]
     */

    protected $files;

    protected $fileCount;

    function __construct(){

        $this->files=[];
        $this->fileCount=0;
    }

    /**
     * @return uploadedFile[]
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * @return int
     */

    public function fileCount(){
        return $this->fileCount;
    }

    /**
     * @param uploadedFile $file
     */

    public function addFile(uploadedFile $file){

        //ad the file to the array
        $this->files[]=$file;

        //increment the filecount
        $this->fileCount++;
    }

    public function getFile($i){

        //if negativ, get the first element
        if($i<0)$i=0;

        //if greater than the file count, get the last element.
        if($i>=$this->fileCount)$i=$this->fileCount-1;

        //load the specified file
        return $this->files[$i];

    }

}
