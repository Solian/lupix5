<?php


class textElementField extends templateBasicEntry{

    /**
     * @var string $desc describes the contents of the field
     */

    public $desc='';

    /**
     * @var string $placeholder the text which is displayed in empty input fields
     */

    public $placeholder='';

    function __construct($type,$value,$desc='',$placeholder='')
    {

        parent::__construct($type,$value);
        $this->desc=$desc;
        $this->placeholder=$placeholder;
    }

}
