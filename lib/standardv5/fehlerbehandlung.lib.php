<?php /** @noinspection PhpUnusedLocalVariableInspection */

require_once('log.php');

error_reporting(E_ALL);
if(!defined('DEVELOPING_MODE'))define('DEVELOPING_MODE',false);


/*
class error_report{

    protected $errorMessageStrings;
    protected $errorCountInteger;

    function __construct()
    {
        $this->errorCountInteger=0;
        $this->errorMessageStrings="";
    }

    function addErrorMessageString($argMsgString){
        $this->errorCountInteger++;
        $this->errorMessageStrings.='['.($this->errorCountInteger).']'.$argMsgString;
    }

    function getErrorMessageStrings(){
        return $this->errorMessageStrings;
    }

    function getErrorCountInteger(){
        return $this->errorCountInteger;
    }

}

$errorReport = new error_report();
*/

function LupixErrorHandler($argFehlercode, $argFehlertext, $argFehlerdatei, $argFehlerzeile,$argKontext=null){
    LupixFallbackTemplateParser($argFehlercode,$argFehlertext,$argFehlerdatei,$argFehlerzeile,'Error',$argKontext);
}

/**
 * @param int $argFehlercode
 * @param string $argFehlertext
 * @param string $argFehlerdatei
 * @param int $argFehlerzeile
 * @param string $argType
 * @param array $argContext
 * @param array $argTrace
 * @return mixed
 */

function LupixFallbackTemplateParser(int $argFehlercode, string $argFehlertext, string $argFehlerdatei, int $argFehlerzeile, $argType='Exception',$argContext=array(), $argTrace=array())
{
    $cached_output=[];
    while(ob_get_level()>0){
        if(DEVELOPING_MODE===true){
            $cached_output[]=ob_get_contents();
        }
        ob_end_clean();
    }
    ob_start();


    //try {
    $fehlerKey = zufallszahl(3);

    //try {
    $logGesetzt = events_log_setzen(
        $fehlerKey,
        time(),
        $argFehlerdatei,
        $argFehlercode,
        "1",
        $argFehlerzeile,
        json_encode($argTrace, 1),
        $argFehlertext);
    //} catch(Throwable $e) {
    //print_r($e);
    //$logGesetzt=false;
    //}

    //Bei PHP oder MySQL Fehlern ist die Beschreibung manchmal leer, dann wird sie durch die zusätzlichen Informationen ersetzt.
    $fehlerBeschreibung = log::fehlerDaten($argFehlercode, "Beschreibung");

    $content = '
    <h2 class="">Ereignisnummer (Fehlernummer)</h2>
    <p>' . ($logGesetzt ? $fehlerKey : 'Das Ereignis erhielt keinen Eintrag in der Datenbank.') . '</p>
    <h2 class="">Nachricht</h2>
    <p>' . log::fehlerDaten($argFehlercode, "Nachricht") . '</p>';

    if (DEVELOPING_MODE === true) {

        if (!empty($fehlerBeschreibung) && $fehlerBeschreibung !== 'N/A') {
            $content .= '<h2 class="">Beschreibung</h2>
        <p>' . $fehlerBeschreibung . '</p>';
        }


        $content .= ' <h2 class="">Informationen</h2>
    <p>' . ($argFehlertext !== '' ? $argFehlertext : '') . '</p>
    <h2 class="">Ausloesende Datei - Zeilennummer</h2>
    <p>' . $argFehlerdatei . ' - ' . $argFehlerzeile . '</p>';

        if ($argType == 'Exception') {
            if (!empty($argTrace)) {
                $content .= '<h2 class="">Stacktrace</h2><p>';

                $argTrace = json_decode(json_encode($argTrace));

                foreach ($argTrace as $tracePoint) {

                    if (isset($tracePoint->class)) {
                        if(isset($tracePoint->file)) {
                            $content .= '<b>' . $tracePoint->class . $tracePoint->type . $tracePoint->function . '(' . (isset($tracePoint->args) ? json_encode($tracePoint->args, true) : '') . ')</b> in ' . pathinfo($tracePoint->file, PATHINFO_FILENAME) . '.' . pathinfo($tracePoint->file, PATHINFO_EXTENSION) . ' in Zeile ' . $tracePoint->line . '<br>';
                        }else{
                            $content .= '<b>' . $tracePoint->class . $tracePoint->type . $tracePoint->function . '(' . (isset($tracePoint->args) ? json_encode($tracePoint->args, true) : '') . ')</b> in ??? <br>';
                        }
                    } elseif (isset($tracePoint->function)) {
                        $content .= '<b>' . $tracePoint->function . '(' . (isset($tracePoint->args) ? '<pre>' . (isset($tracePoint->args)?json_encode($tracePoint->args, true):'')  . '</pre>' : '') . ')</b> in ' . (isset($tracePoint->file) ? $tracePoint->file : 'keiner Datei') . ' in Zeile ' . (isset($tracePoint->line) ? $tracePoint->line : 'Keine') . '<br>';
                        //$content .=  '<b>'.$tracePoint->function.'('.r_implode(',',$tracePoint->args).')</b> in '.$tracePoint->file.' in Zeile '.$tracePoint->line.'<br>';
                    }
                }
                $content .=  '</p>';
            }
        } else {
            $content .= '<h2 class="">Kontext</h2><p>';
            $content .= '<pre>' . print_r($argContext, true) . '</pre></p>';
        }

        if(count($cached_output)>0) {
            $content .= '<h2 class="">Cached Output</h2><p>';
            foreach ($cached_output as $lvl => $output) {
                $content .= '<h3 class="">Level '.$lvl.'</h3><div style="border:1pt solid black;padding:4pt;">'.closetags($output).'</div>';
            }
        }
    }



    /*
     * Wenn noch kein Fehler vorher ausgegeben wurde, wird er der Seitenkopf geladen und der Inhalt ausgegeben.
     * Jeder weitere Versuch fügt nur noch Ausgaben an diesen Inhalt an.
     */



    $content .= '<p>
    <a href="javascript:history.back();">Zur&uuml;ck</a>
    <a href="?id=profil&aktion=logout" target="_top">Logout</a>
    <a href="?id=profil&aktion=loginform" target="_top">Login</a>
</p>';

    if(defined('CALLED_FROM_ADMIN'))$titleBild_ = '../template/standardv5/img/cover.png';
    else{$titleBild_ = 'template/standardv5/img/cover.png';}
    $pageTitle = 'Lupix5 - Fehler ';
    $pageName='Fehlerseite';
    $modulTitle=$pageName;
    $navi=' <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="javascript:history.back()">Zurück</a>
                </li>
                </ul>';
    $menu=$navi;
    $leftContent = '';
    $rightContent = '';
    $footer='';
    $dbStats='';
    $footerNavi='';



    if (!defined('CALLED_FROM_ADMIN')) {
        include __DIR__."/../../template/standardv5/main.tpl.php";
    } else{
        include __DIR__."/../../template/standardv5/admin.tpl.php";

    }
    $ausgabe=ob_get_contents();

    ob_end_clean();
    echo $ausgabe;

    die();

    //}catch(Throwable $thrownException){
    //    return LupixFallbackTemplateParser($thrownException->getCode(),$thrownException->getMessage(),$thrownException->getFile(),$thrownException->getLine(),$thrownException->getTrace());
    //}
}



/**
 * @param Throwable $thrownException
 * @return mixed
 */

function LupixExceptionHandler(Throwable $thrownException){
    return LupixFallbackTemplateParser($thrownException->getCode(),$thrownException->getMessage(),$thrownException->getFile(),$thrownException->getLine(),'Exception',[],$thrownException->getTrace());
}

/**
 * @param $argFehlercode
 * @param $argFehlertext
 * @param $argFehlerdatei
 * @param $argFehlerzeile
 * @param $argKontext
 * @return string
 */
function LupixSilencedErrorHandler($argFehlercode, $argFehlertext, $argFehlerdatei, $argFehlerzeile,$argKontext=null)
{

    try {
        $fehlerKey = zufallszahl(3);

        if(events_log_setzen(
            $fehlerKey,
            time(),
            $argFehlerdatei,
            $argFehlercode,
            "1",
            $argFehlerzeile,
            json_encode($argKontext, 1),
            $argFehlertext)){

            return $fehlerKey;
        }else{
            return '0';
        }


    }
    catch (Throwable $e) {
        if(!isset($fehlerKey)){
            return "KEIN FEHLERKEY!";
        }
        else{
            return $fehlerKey;
        }
    }

}


#########################################################
##                                                     ##
##  Ereignisprotkoll-Funktionen              ! OK !    ##
##                                                     ##
#########################################################
/**
 * @param $sFehlerkey
 * @param $iZeit
 * @param $sDatei
 * @param $sFehlernummer
 * @param $sExecCodeErgebnis
 * @param $iZeilennummer
 * @param $sEreignisverfolgung
 * @param $sFehlertext
 * @return bool
 */
function events_log_setzen($sFehlerkey, $iZeit, $sDatei, $sFehlernummer, $sExecCodeErgebnis, $iZeilennummer, $sEreignisverfolgung, $sFehlertext) {

    global $main, $admin;

    if ( $main instanceof seitenklasse) {

        $modul = $main->inhalt->modul;
        $userid = $main->userIdenty->id;

    }elseif($admin instanceof adminklasse) {

        $modul = $admin->inhalt->modul;
        $userid = $admin->userIdenty->id;
    }else{
        $modul = 'RESCUE';
        $userid = 0;

    }
    return log::event(
        $sFehlerkey,
        $iZeit,
        $sDatei,
        $sFehlernummer,
        $sExecCodeErgebnis,
        $iZeilennummer,
        $sEreignisverfolgung,
        $modul,
        $userid,
        $sFehlertext);

}


set_exception_handler("LupixExceptionHandler");
if("LupixExceptionHandler"!==set_exception_handler("LupixExceptionHandler")){
    LupixFallbackTemplateParser(0,'ExceptionHandler wurden nicht installiert.',__FILE__,__LINE__);
}


set_error_handler('LupixErrorHandler');
if("LupixErrorHandler"!==set_error_handler("LupixErrorHandler")){
    LupixFallbackTemplateParser(0,'ExceptionHandler wurden nicht installiert.',__FILE__,__LINE__);
}
