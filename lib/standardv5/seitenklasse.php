<?php


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

error_reporting(E_ALL);


class seitenklasse extends siteControllerBaseClass {


    /**
     * @var array
     */

    protected $_GLOBAL_VARS = array("POST" => array(), "GET" => array(), "FILES" => array(), "COOKIE" => array(), "SERVER" => array());

    /*
    protected $_PICTO_RESTAB_PFAD = "";
    protected $_PICTO_RESTAB = array();
    protected $_PICTO_RESTAB_SUMMEN = array();
        protected $_PICTO_CACHE_PFAD = "cache/picto";
    */

    /**
     * @var float|int
     */
    public $exec_time = 0;

    /**
     * @var string
     */
    protected $lupixVersion = '0.6';

    /**
     * @var UserIdentity
     */
    public $userIdenty;

    /**
     * @var siteContent
     */
    public $inhalt;



    /**
     * @var EntityManager
     */

    protected $entityManager;

    function __construct() {
        $this->exec_time = microtime(true);
        $this->userIdenty = new UserIdentity();
        $this->inhalt = new siteContent();
        parent::__construct($this->userIdenty,$this);


        // Create a simple "default" Doctrine ORM configuration for Annotations
        $isDevMode = true;
        $proxyDir = null;
        $cache = null;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/../lib"), $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

        //Load Credentials from file
        $path=__DIR__.'/../../inc/config.ini.php';

        //Wenn die Config-Datei nicht existiert, muss erstmal der Datenbankserver-Konfiguriert werden --> Anzeige der Willkommensseite
        if (!file_exists($path)){
            $this->inhalt->showWelcomePage=true;
            if(!defined('CALLED_FROM_ADMIN')){
                $this->renderPage();
            }
        }else {

            $credentials = parse_ini_file($path);


            if (!$credentials) throw new Exception($path, 91);

            if (array_key_exists('DBHOST', $credentials) && array_key_exists('DBUSER', $credentials) &&
                array_key_exists('DBPASS', $credentials)) {

                // database configuration parameters
                $conn = array(
                    'driver' => 'pdo_mysql',
                    'host' => $credentials['DBHOST'],
                    'dbname' => $credentials['DBNAME'],
                    'user' => $credentials['DBUSER'],
                    'password' => $credentials['DBPASS']
                );

                // obtaining the entity manager
                $this->setEntityManager(EntityManager::create($conn, $config));

            } else {
                if (!defined('CALLED_FROM_ADMIN')) {
                    throw new Exception($path, 91);
                }
            }


            //$this->_PICTO_RESTAB_PFAD = dirname(__FILE__) . "/../" . $this->_PICTO_CACHE_PFAD . "/resourcen_tabelle.inc.php";
            //self::$USER_AGENT = $_SERVER['HTTP_USER_AGENT'];

        }
    }

    #########################################################
    ##
    ##  Getter und Setter
    ##
    #########################################################

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    #########################################################
    ##													   ##
    ##	Haupftunktionen zum Seitenaufruf				   ##
    ##													   ##
    #########################################################

    ## Zeigt an, welches Sktipt überhaupt geladen wird!
    ##

    function registerScript($_MODUL, $_SKRIPT, $_VERSION, $_BEREICH) {
        $this->inhalt->modul = $_MODUL;
        $this->inhalt->modulskript= $_SKRIPT;
        $this->inhalt->modulversion = $_VERSION;
        $this->inhalt->modulbereich = $_BEREICH;
    }

    function catchVariables($argFangVariablen = array()) {  //Variablen, die registriert werden sollen als Inhalt
        /* $_POSTs, $_GETs abfangen!!!0 Alles andere wird ignoriert. Variablen gleichen Names werden geloescht.

          loginname, loginnpasswort, aktion, username, userid, sessionid, adminid, werden standardgemäß geladen. Alle weiteren müssen über
          den array variablen() an die funktion übergeben werden. nur diese stehen für die anderen funktionen zur Verfügung.

          Die Variablen werden dann in die Variablen USER_IDENT und SITE_CONTENT eingegeben.
         */

        //  Wenn ein json-Post-Request gesendet wird, muss die Standard-Post-Variable überschrieben werden,
        //  da sie die Daten nicht enthält. Sie ist nur auf Formulardaten geeicht (Warum auch immer man da so engstirnig ist!)
        if($_SERVER['REQUEST_METHOD']==='POST'){
            if($_SERVER['CONTENT_TYPE']==='application/json'){

                $_POST_DATA = file_get_contents('php://input', true);
                $_POST = json_decode($_POST_DATA,true);
                //die('{"type":"error","msg":"'.print_r($_POST,true).'"}');
            }
        }

        //Die Daten werden in die main-Klasse geladen (Warum?)
        $this->_GLOBAL_VARS["POST"] = $_POST;
        $this->_GLOBAL_VARS["GET"] = $_GET;
        $this->_GLOBAL_VARS["FILES"] = $_FILES;
        $this->_GLOBAL_VARS["SERVER"] = $_SERVER;

        if(array_key_exists('referrerMessage',$_COOKIE)){
            $this->_GLOBAL_VARS["COOKIE"]=[];
            $this->_GLOBAL_VARS["COOKIE"]['referrerMessage']=$_COOKIE['referrerMessage'];
        }

        $vars = array("adminkey", "sessionkey", "style",'installkey');
        foreach ($vars as $vari) {
            if (isset($_COOKIE[$vari])) {
                $this->_GLOBAL_VARS["COOKIE"][$vari] = $_COOKIE[$vari];
            } else {
                $this->_GLOBAL_VARS["COOKIE"][$vari] = "";
            }
        }

        settype($this->_GLOBAL_VARS["COOKIE"]["sessionkey"], "string");
        settype($this->_GLOBAL_VARS["COOKIE"]["adminkey"], "string");

        $vars = array("loginname", "loginpasswort");
        foreach ($vars as $vari) {
            if (isset($this->_GLOBAL_VARS["POST"][$vari])) {
                $this->_GLOBAL_VARS["LOGIN"][$vari] = $this->_GLOBAL_VARS["POST"][$vari];
            } else {
                $this->_GLOBAL_VARS["LOGIN"][$vari] = "";
            }
        }

        //Alle von außen zu fangenden Variablen werden hier geladen und gefiltert!
        if (!empty($argFangVariablen)) {
            foreach ($argFangVariablen as $vari) {
                if (!empty($this->_GLOBAL_VARS["POST"][$vari])) {
                    $this->inhalt->parameter[$vari]= $this->_GLOBAL_VARS["POST"][$vari];
                } elseif (!empty($this->_GLOBAL_VARS["GET"][$vari])) {
                    $this->inhalt->parameter[$vari] = $this->_GLOBAL_VARS["GET"][$vari];
                }
            }
        }

        $vars = array("aktion", "id");
        foreach ($vars as $vari) {
            if (!empty($this->_GLOBAL_VARS["POST"][$vari])) {
                $this->inhalt->$vari = $this->_GLOBAL_VARS["POST"][$vari];
            } elseif (!empty($this->_GLOBAL_VARS["GET"][$vari])) {
                $this->inhalt->$vari = $this->_GLOBAL_VARS["GET"][$vari];
            }else{
                $this->inhalt->$vari='';
            }
        }

        ///Erstmal ist der Zugang für einen Benutzer versperrt
        $this->inhalt->zugangVerboten = true;

        //Die ID wird aus in einen Integer gecastet
        $this->inhalt->id = (integer) $this->inhalt->id;

        /*
         * Der Darstellungsstil der Heldenschenke wird geladen.
         */

        //Stil lade
        /*
        $sql = "select main_stile.css,main_stile.schankraum from main_stile where main_stile.name='{$this->ident->style}'";
        $styleData = db::query($sql);
        if (count($styleData) == 1) {
            $stylepfad = $styleData;
        } //Wenn der Stil nicht existiert oder mehr als ein Stil gefunden wird, wird der Standardstil geladen
        else {
            $sql = "select main_stile.css,main_stile.schankraum ,main_stile.name from main_stile where main_stile.standardstil=1;";
            $stylepfad = db::query($sql);

            //Den Stil noch auf den Standard ändern
            $this->ident->style = $stylepfad[0]['name'];

            // hier muss eine Nachreicht geschrieben werden!
        }
        */

        //unset($_COOKIE, $_SERVER);
    }

    /**
     * Überprüft, ob der Benutzer identifiziert ist und ob er überhaupt auf die Seite zugreifen darf.
     * @throws Exception
     */

    function identifyUser() {



//        $login = $this->_GLOBAL_VARS['LOGIN']['loginname'];
//        $pw = $this->_GLOBAL_VARS['LOGIN']['loginpasswort'];
//        $session_key = $this->_GLOBAL_VARS['COOKIE']['sessionkey'];

        if (isset($this->_GLOBAL_VARS['POST']['k']) && $this->inhalt->aktion == 'einloggen') {
            $this->session = lulo::loadSession($this->_GLOBAL_VARS['POST']['k']);
            if ($this->session instanceof session) {
                die(json_encode(lulo::prepareLogin($this->session, $this->_GLOBAL_VARS['POST']['h1'], $this->_GLOBAL_VARS['POST']['g_pub'])));
            } else {
                die(json_encode([
                    'l' => 'errorSessionN/A',
                    'c1' => '',
                    'k_pub' => ''
                ]));
            }
        }
        elseif (isset($this->_GLOBAL_VARS['POST']['l']) && $this->inhalt->aktion == 'einloggen') {
            $this->session = lulo::loadSession($this->_GLOBAL_VARS['POST']['l']);
            if ($this->session !== false) {

                //prüft die Logindaten
                $this->session = lulo::login($this->session, $this->_GLOBAL_VARS['POST']['h2'], $this->_GLOBAL_VARS['POST']['c2']);
                //print_r($this->session);

                ## Sets the Usercookie ##
                if(DEVELOPING_MODE===true){
                    setcookie('sessionkey', $this->session->sessionkey);
                }else {
                    setcookie('sessionkey', $this->session->sessionkey, time() + 86400, '', '', true, true);
                }

                header('Location: index.php');
                die();
            }
        }
        else {

            $this->session = lulo::loadSession($this->_GLOBAL_VARS['COOKIE']['sessionkey']);

            if ($this->inhalt->aktion == 'logout' && $this->session!==false) {
                lulo::deleteSession($this->session);
                $this->session = false;
            }

        }

        //wenn es keine Session gibt, eine neue initialisieren
        if ($this->session === false) {
            $this->session = lulo::createNewSession();
        } elseif ($this->session->loginStatus === lulo::AUTH_LOGIN_TIMEOUT) {
            lulo::deleteSession($this->session);
            $this->session = lulo::createNewSession();
        }

        //benutzer identifizieren
        $this->userIdenty = lulo::identify($this->session);
        //print_r($this->ident);

        ## Sets the Usercookie ##
        if(DEVELOPING_MODE===true){
            setcookie('sessionkey', $this->session->sessionkey);
        }else {
            setcookie('sessionkey', $this->session->sessionkey, time() + 86400, '', '', true, true);
        }

        ## Initialisierung des seitenArchivs
        ##
        seitenArchive::init($this->userIdenty);

    }


    /**
     * @throws Exception|Throwable
     * @return string
     */

    function checkUserPermissions()
    {

        try {

            //Wenn die ID keinen ordentlichen Wert hat, soll sie startseite erhalten
            if (empty($this->inhalt->id) || $this->inhalt->id < 1) {

                //Prüfen, ob überhaupt Seiten exisiteren --> Lade die erste Seite, die die man lesen darf, und die unterhalb vom Globus hängt
                if (seitenArchive::main_seiten_anzahl() > 0) {
                    $this->inhalt->id = seitenArchive::get_startseite_id();
                } else {
                    $this->inhalt->showWelcomePage = true;
                    return 'Load Startseite';
                }

            } else {


                if (!seitenArchive::main_seite_existiert($this->inhalt->id)) {

                    //Wenn Seiten existieren, dann wird ein Fehler ausgegeben
                    if (seitenArchive::main_seiten_anzahl() > 0) throw new Exception('Seiten-Id:' . $this->inhalt->id, 10);

                    // Wenn keine Seite exististert, wird dies in der Auswertung der Anzeige der Seite aufgefangen. Daher wird es hier nicht weiter verfolgt.
                    else {
                        $this->inhalt->showWelcomePage = true;
                        return 'Keine Seiten';
                    }
                }

            }

            //Die aktuelle Seite wird auf ihre Berechtigung hin geladen
            //Die Berechtigung wird abgefragt
            $permission = seitenArchive::get_seiten_userperm($this->inhalt->id);

            //wenn nun weder Admin noch berechtigt, dann wir ein Fehler ausgegeben und die normale Ausführung beendet.
            if ($this->userIdenty->group_id != 1 && $permission == false) {

                //Zugang als Verboten markieren
                $this->inhalt->zugangVerboten = true;

                //Unbekannter Gast hat etwas gesucht
                if ($this->userIdenty->group_id === 0) {
                    throw new Exception('<pre>'.print_r($this->inhalt, true) . print_r($this->userident, true).print_r(seitenArchive::$pagePermission,true).print_r(seitenArchive::$userPermissionFromDatabase,true)."\n".seitenArchive::$userPermissionQuery.'</pre>', 203);
                }//Ein eingeloggter Benutzer hat das versucht
                else {
                    throw new Exception(print_r($this->inhalt, true) . print_r($this->userident, true), 202);
                }
            } else {

                //Zugang als erlaubt notieren
                $this->inhalt->zugangVerboten = false;
            }
            return 'Seitenzugang geladen!';

        }catch(Throwable $e){
            if($e->getCode()===90){
                $this->inhalt->showWelcomePage = true;
                return 'keine Seiten';
            }else{
                throw $e;
            }
        }

    }


    /**
     * @throws Exception
     */

    function renderPage():void {

        //Prüfen, ob keine Seiten existieren --> dann muss die Standard-nachricht geladen werden
        if($this->inhalt->showWelcomePage!==false) {

            //Jetzt
            $installStatus=['noPages'=>false,'noDatabase'=>false,'noRoot'=>false];


            //Prüfen ob eine Datenbankverbindung existiert!
            try{
                db::getCredentials(9);
                if (db::test_connect() !==true) {
                    $installStatus['noDatabase']=true;
                }

                else{
                    //Wenn es doch eine DB Verbindung gibt, prüfen ob ein Root user existiert!
                    if (benutzerArchive::get_benutzer_daten(1,'nickname') == null) {
                        $installStatus['noRoot']=true;
                    }

                    if(seitenArchive::main_seiten_anzahl()==0){
                        $installStatus['noPages']=true;
                    }

                }

            }catch (Throwable $exception){
                $installStatus['noDatabase']=true;
            }

            $welcomeTemplate = wolfi::empty_template_from_file('standardv5/main.tpl');
            $welcomeTemplate->entries['css'] = wolfi::create_templateEntry('string', 'template/standardv5/standard.css');
            $welcomeTemplate->entries['titleBild_'] = wolfi::create_templateEntry('string', 'template/standardv5/img/cover.png');
            $welcomeTemplate->entries['pageTitle'] = wolfi::create_templateEntry('string', 'Neues Lupix5 - Webprojekt');
            $welcomeTemplate->entries['pageName'] = wolfi::create_templateEntry('string', 'Willkommen');

            //init
            $pageLeftContentString='<h2>Wichtige Links</h2><div class="list-group m-1">';
            $pageContentString = "<h1>Herzlichen Glückwunsch!</h1><div class=\"p-2\"><p>Sie haben eine neue Instanz von Lupix aufgesetzt.</p>";

            //Wenn ein Datenbankfehler vorliegt, meldung anzeigen
            if($installStatus['noDatabase']) {
                $pageContentString .= "<div class=\"alert alert-warning\"><i class=\"text-warning fa fa-database\"></i> Die Datenbank wurde nicht installiert.</div>";
            }

            //Wenn kein Root-Nutzer vorliegt, meldung anzeigen
            if($installStatus['noRoot']) {
                $pageContentString .= "<div class=\"alert alert-warning\"><i class=\"text-warning fa fa-user-secret\"></i> Der Root-Benutzer wurde nicht eingerichtet.</div>";
            }

            //wenn root nutzer fehlt und oder db fehlt, selbe lösung einbinden
            if($installStatus['noRoot'] || $installStatus['noDatabase']) {
                $pageContentString .= '<div><p class="h7">Lösung:</p><ul>
                <li><p>Installieren Sie die Datenbank und konfigurieren Sie das Lupix5-CMS. Öffnen Sie dazu das <a href="admin/index.php?mod=install">Install-Tool</a> und folgenden Sie den Anweisungen.</p></li></ul></div>';
                $pageLeftContentString .= '<a class="list-group-item list-group-item-action" href="admin/index.php?mod=install">Install-Tool</a>';
            }

            //wenn keine seiten da sind, meldung und lösung einblenden
            if($installStatus['noPages']) {
                $pageContentString .= '<div class="alert alert-warning"><i class="text-warning fa fa-file-o"></i> Die Webseite verfügt über keine Seiten.</div>
            <div><p class="h7">Lösung:</p><ul class="alert alert-success m-2">
            <li>Öffnen Sie die  <i><a href="/admin/index.php">Administration</a></i>.
            <li>Erstellen Sie im Bereich <i>Seiten</i> eine <i>neue Seite</i>!
            <li>Wählen Sie eine neue Template-Datei aus.
            <li>Füllen Sie die Template-Felder mit Inhalt
            <li class=""><i class="text-danger fa fa-exclamation-triangle"></i><i class="text-danger fa fa-exclamation-triangle"></i> Geben Sie die Seite für die Benutzergruppe <span class="text-info">jeden</span> frei! <i class="text-danger fa fa-exclamation-triangle"></i> <i class="text-danger fa fa-exclamation-triangle"></i> <br> Dies findet sich im Editor auf der linken Seite unter dem Seitenbaum!</ul></div>';
                $pageLeftContentString.='<a class="list-group-item list-group-item-action" href="admin/">Administration</a>';

            }

            $pageLeftContentString.='</ul></div>';


            $welcomeTemplate->entries['navi'] = wolfi::create_templateEntry('string', "<ul class=\"navbar-nav\"><li class=\"nav-item\"><a class=\"nav-link\" href=\"admin/\">Administration</a></li><li class=\"nav-item\"><a class=\"nav-link\" href=\"admin/index.php?mod=install\">Install-Tool</a></li></ul>");
            $welcomeTemplate->entries['content'] = wolfi::create_templateEntry('string',$pageContentString );
            $welcomeTemplate->entries['leftContent'] = wolfi::create_templateEntry('string', $pageLeftContentString);
            $welcomeTemplate->entries['rightContent'] = wolfi::create_templateEntry('string', '');
            $welcomeTemplate->entries['footerNavi'] = wolfi::create_templateEntry('string', '');
            $welcomeTemplate->entries['footer'] = wolfi::create_templateEntry('string', 'Lupix 5 - Ein leichtgewichtiges Content Management System');
            $welcomeTemplate->entries['dbStats'] = wolfi::create_templateEntry('controller', 'statistikController','frontend');

            $this->inhalt->templateDaten=$welcomeTemplate;
            $this->inhalt->templateDatei='standardv5/main';
            die($this->parsePage('Willkommen','Willkommen zur neuen Installation'));

        }
        //Laden der Seite
        else{

            //Wenn der Zugang verboten ist
            if($this->inhalt->zugangVerboten===true){
                switch($this->userIdenty->group_id){
                    case USERGROUPS::GUEST:
                        throw new Exception('Seite-ID:' . $this->inhalt->id.' Benutzer-ID:'.$this->userIdenty->id, 200);

                    case USERGROUPS::ADMIN:
                    case USERGROUPS::USER:
                        throw new Exception('Seite-ID:' . $this->inhalt->id.' Benutzer-ID:'.$this->userIdenty->id, 202);

                    case USERGROUPS::BLOCKED:
                        throw new Exception('Seite-ID:' . $this->inhalt->id.' Benutzer-ID:'.$this->userIdenty->id, 201);

                }
            }else{

                //Template-Daten laden
                //Die Seite muss existieren, da sich bereits als
                $template = db::querySingle('select main_seiten.Name,main_seiten.Templatedatei, main_seiten.Templatedaten, main_seiten.Titel from main_seiten where main_seiten.id=:sid',[':sid'=>$this->inhalt->id]);
                $this->inhalt->templateDaten=unserialize($template['Templatedaten']);
                $this->inhalt->templateDatei=$template['Templatedatei'];

                die($this->parsePage($template['Name'],$template['Titel']));
            }
        }
    }

    /**
     * @throws Exception
     */

    protected function parsePage($seitenName,$seitenTitel,$pathOffset= __DIR__ . '/../../controller/'){

        //print_r($this->inhalt->templateDaten);
        //echo"In parsePage\n";

        //Wenn nun die Template daten geladen sind, müssen sie nun ausgeführt werden nacheinander
        $templateVars=$this->parseTemplateEntriesToTemplateVars($this->inhalt->templateDaten,$pathOffset);

        //$this->registerPageTemplateVariable('pageName',$seitenName);
        //$this->registerPageTemplateVariable('pageTitle',$seitenTitel);
        $templateVars['pageName']=$seitenName;
        $templateVars['pageTitle']=$seitenTitel;
        $this->templateVariableArray=$templateVars;
        $this->registerPageTemplate($this->inhalt->templateDatei);

        //print_r($this->templateVariableArray);
        //echo"Alles erledigt!";
        //Filse the Data into the Template und get this back
        return $this->parseTemplate();
    }

    /** parst einen seitenElementText rekursiv
     * @param int $id
     * @return string
     * @throws Exception
     */

    public function parseSet($id,$pathOffset){

        $output = '';
        $seitenElementText = bobArchiv::loadSeitenElementText($id);

        foreach ($seitenElementText->elements as $textElement) {

            $this->templateVariableArray= $this->parseTemplateEntriesToTemplateVars($textElement,$pathOffset);
            $this->registerPartialTemplate($textElement->file);
            $output .= $this->parseTemplate();
        }
        return $output;
    }

    /**
     * @param $templateOrSeitenElement
     * @param $pathOffset
     * @throws ReflectionException|Exception
     */

    public function parseTemplateEntriesToTemplateVars($templateOrSeitenElement,$pathOffset)
    {

        $templateVars = [];

        //Alle Template Variablen nacheinander durchgehen
        //throw new Exception('$this->>inhalt->templateDaten:'.print_r($this->inhalt->templateDaten,true),50000);
        foreach ($templateOrSeitenElement->entries as $templateBereich => $templateBereichsdaten) {

            //echo" $templateBereich: ";


            $templateVars[$templateBereich]=$this->parseTemplateEntry($templateBereich,$templateBereichsdaten,$pathOffset);
        }

        return $templateVars;

    }

    /**
     * @param $templateBereich
     * @param $templateBereichsdaten
     * @param $pathOffset
     * @return false|string|void
     * @throws ReflectionException
     */

    public function parseTemplateEntry($templateBereich,$templateBereichsdaten,$pathOffset){
        //Wenn es ein statischer String ist
        // TODO diese erhalten keine Cache-Funktion, weil damit auch andere Daten geschleust werden --> Lösung dafür finden
        if ($templateBereichsdaten->type == 'string') {

            return  $templateBereichsdaten->value;
        } //wenn es ein text ist
        elseif ($templateBereichsdaten->type == 'text') {

            $cacheFilePath = $this->createCachePath($this->inhalt->id, $templateBereich, 'T' . $templateBereichsdaten->value);

            if (file_exists($cacheFilePath)) {

                return  file_get_contents($cacheFilePath);

            } else {

                $text = html_entity_decode(seitenArchive::seiten_text_laden($templateBereichsdaten->value, $this->inhalt->id)['Text']);
                if (db::lastRowCount() > 0) {

                    file_put_contents($cacheFilePath, $text);
                    return $text;

                } else {
                    throw new Exception('Text-ID:' . $templateBereichsdaten->value, 34);
                }

            }
        } //Führt die einzelnen Controller aus und lädt ihren Inhalt in den angebeben Bereich
        elseif ($templateBereichsdaten->type == 'controller') {

            //echo"Controller";
            //ob_start();
            $controllerFileName = $pathOffset . $templateBereichsdaten->value . '.php';


            if (file_exists($controllerFileName)) {
                require_once($controllerFileName);
                $classname=$templateBereichsdaten->value;
                $controller = new $classname($this);

            } else {

                throw new Exception($controllerFileName, 35);
            }


            //ob_end_clean();
            //Ausnahme werfen, wenn kein Controller in der Datei initialisiert wurde, dies wird für Standard-Controller erwartet
            /** @var controllerBaseClass $controller */
            if (!isset($controller)) {
                throw new Exception($controllerFileName . '<br>' . print_r($templateBereichsdaten, true), 3);
            }

            //Die Seitenaktion soll hier verwertet werden
            if ($templateBereichsdaten->action === '%') {
                return $controller->start($this->inhalt->aktion);

            }


            //Eine vordefinierte Aktion soll hier verwertet werden
            else {
                return $controller->start($templateBereichsdaten->action);

            }

        } //parst rekursiv die seitenElementTexte
        elseif ($templateBereichsdaten->type == 'set') {

            $setId = (int)$templateBereichsdaten->value;

            return $this->parseSet($setId,$pathOffset);

        }
    }


    /**
     * @param string $variableName
     * @param string $variablenTyp
     * @param bool $variableIsOptional
     * @return bool|int|mixed
     * @throws Exception
     */

    public function loadParameter(string $variableName, string $variablenTyp, bool$variableIsOptional ){

        $inputValue = 0;
        $varIsSet = false;

        if($variablenTyp!=='uploadedFile' && $variablenTyp!=='uploadedFiles') {

            //Wert einer Postvariable bestimmen
            if (array_key_exists($variableName, $this->get_("POST"))) {

                if($variablenTyp=='array'){
                    $inputValue=sanitizeStringArray($this->_GLOBAL_VARS["POST"][$variableName]);
                }else {
//                  $pretext.='<div><h1>'.$variableName.'</h1><div style="border:2pt red dottet;">'.$_POST[$variableName].'</div></div>';
                    if($variablenTyp == 'string'){

                        $inputValue = htmlspecialchars( $this->_GLOBAL_VARS["POST"][$variableName] );

                    } else {
                        $inputValue = filter_var(
                            $this->_GLOBAL_VARS["POST"][$variableName],

                            $variablenTyp == 'bool' ? FILTER_VALIDATE_BOOLEAN : (
                            $variablenTyp == 'float' ? FILTER_VALIDATE_FLOAT : FILTER_VALIDATE_INT
                            )


                        );
                    }

                }

                $varIsSet = true;
//                    $pretext.='<div><h1>'.$variableName.' sanitzed</h1><div style="border:2pt red dottet;">'.$inputValue.'</div></div>';
            }

            //Wert einer GET Variable ohne POST bestimmen
            if (array_key_exists($variableName, $this->get_("GET")) && !array_key_exists($variableName, $this->get_("POST"))) {

                if($variablenTyp=='array'){
                    $inputValue=sanitizeStringArray($this->_GLOBAL_VARS["GET"][$variableName]);
                }else {

                    if($variablenTyp == 'string'){

                        $inputValue = htmlspecialchars( $this->_GLOBAL_VARS["GET"][$variableName] );

                    } else {
                        $inputValue = filter_var(
                            $this->_GLOBAL_VARS["GET"][$variableName],

                            $variablenTyp == 'bool' ? FILTER_VALIDATE_BOOLEAN : (
                            $variablenTyp == 'float' ? FILTER_VALIDATE_FLOAT : FILTER_VALIDATE_INT
                            )


                        );
                    }
                }
                $varIsSet = true;
            }
        }
        //Wert einer FILES VARIABLE ohne GET Variable und  ohne POST bestimmen
        elseif (array_key_exists($variableName, $this->_GLOBAL_VARS["FILES"])) {

            //TODO Check for multiple files array
            if($variablenTyp==='uploadedFile') {

                // Undefined | Multiple Files | $_FILES Corruption Attack
                // If this request falls under any of them, treat it invalid.
                if (!isset($this->_GLOBAL_VARS["FILES"][$variableName]['error']) || is_array($this->_GLOBAL_VARS["FILES"][$variableName]['error'])
                ) {
                    throw new Exception("Invalid error parameters for Upload $variableName." . print_r($_FILES[$variableName]));
                }

                // Check $_FILES[$variableName]['error'] value.
                switch ($this->_GLOBAL_VARS["FILES"][$variableName]['error']) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new Exception('No file sent.',224);
                    case UPLOAD_ERR_INI_SIZE:
                        throw new Exception('Exceeded filesize limit.',221);
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new Exception('Exceeded filesize limit.',220);
                    default:
                        throw new Exception('Unknown errors.',222);
                }

                $inputValue = new uploadedFile($this->_GLOBAL_VARS["FILES"][$variableName]['name'], $this->_GLOBAL_VARS["FILES"][$variableName]['tmp_name'], $this->_GLOBAL_VARS["FILES"][$variableName]['error']);
                $varIsSet = true;

            }elseif($variablenTyp==='uploadedFiles') {

                $inputValue = new uploadedFiles();

                // Undefined | $_FILES Corruption Attack
                // If this request falls under any of them, treat it invalid.
                if (!isset($this->_GLOBAL_VARS["FILES"][$variableName]['error'])) {
                    throw new Exception("Invalid error parameters for Upload $variableName." . print_r($_FILES[$variableName]));
                }

                $fileCount = count($this->_GLOBAL_VARS["FILES"][$variableName]['name']);

                for($i=0;$i<$fileCount;$i++) {

                    if(!empty($this->_GLOBAL_VARS["FILES"][$variableName]['name'][$i])) {

                        // Check $_FILES[$variableName]['error'] value.
                        switch ($this->_GLOBAL_VARS["FILES"][$variableName]['error'][$i]) {
                            case UPLOAD_ERR_OK:
                                break;
                            case UPLOAD_ERR_NO_FILE:
                                throw new Exception('No file sent. (Filename:' . $this->_GLOBAL_VARS["FILES"][$variableName]['name'][$i] . ')', 224);
                            case UPLOAD_ERR_INI_SIZE:
                                throw new Exception('Exceeded filesize limit.', 221);
                            case UPLOAD_ERR_FORM_SIZE:
                                throw new Exception('Exceeded filesize limit.', 220);
                            default:
                                throw new Exception('Unknown errors.', 222);
                        }

                        $inputValue->addFile(
                            new uploadedFile(
                                $this->_GLOBAL_VARS["FILES"][$variableName]['name'][$i],
                                $this->_GLOBAL_VARS["FILES"][$variableName]['tmp_name'][$i],
                                $this->_GLOBAL_VARS["FILES"][$variableName]['error'][$i])
                        );
                    }
                }
                $varIsSet = true;

            }else{

                $varIsSet = false;
            }
        }


        //Wenn eine Variable nicht gesetzt ist, aber sie ist boolsch, wird sie auf false gestellt.
        if(!$varIsSet && $variablenTyp=='bool'){
            $inputValue=false;
            $varIsSet=true;
        }

        //Wenn die Variable nicht gesetzt ist und sie ist nicht optional --> Fehler
        if (!$varIsSet && !$variableIsOptional) {
            throw new Exception('<i>' . $variablenTyp . '</i> '.$variableName, 28);
        }

        //Sie ist nicht gsetzt aber auch nur optional --> NIX

        return [$varIsSet,$inputValue];

    }


    /**
     * @param string $global_var_sektion   Wählen sie aus POST, GET, SERVER, COOKIE
     * @param string $offset Die zu ladende Variable
     * @return bool|mixed
     */

    function get_($global_var_sektion, $offset = "") {
        if (array_key_exists($global_var_sektion, $this->_GLOBAL_VARS)) {
            if (!empty($offset)) {
                if (array_key_exists($offset, $this->_GLOBAL_VARS[$global_var_sektion])) {
                    return $this->_GLOBAL_VARS[$global_var_sektion][$offset];
                } else {
                    return false;
                }
            } else {
                return $this->_GLOBAL_VARS[$global_var_sektion];
            }
        } else {
            return false;
        }
    }

    /**
     * @return string
     */

    public function getLupixVersion(){
        return $this->lupixVersion;
    }

    public function getCacheFolder(){
        return __DIR__.'/../../cache/parts/';
    }

    /**
     * @param int $pageId
     * @param string $templateLabel
     * @param string $contentKey
     * @return string
     */

    public function createCachePath(int $pageId, string $templateLabel, string  $contentKey){

        /*
        if( $this->basePath ==='') {
            $filePathArray = explode('/', __DIR__);
            array_pop($filePathArray);
            array_pop($filePathArray);
            $this->basePath = implode('/',$filePathArray);
        }
            $this->basePath
        */
        return $this->getCacheFolder().hash('sha3-512', $pageId.','.$templateLabel.','.$contentKey);

    }

    /**
     * @param int $pageId
     * @param string $templateLabel
     * @param string $contentKey
     * @return bool
     * @throws Exception
     */

    public function deleteCachePath(int $pageId, string $templateLabel, string  $contentKey)
    {
        $cacheFilePath = $this->createCachePath($pageId, $templateLabel, $contentKey);
        echo $cacheFilePath;

        if (file_exists($cacheFilePath)){
            if(!unlink($cacheFilePath))throw new Exception("$pageId, $templateLabel, $contentKey",0);
            else return true;
        }

        return false;
    }

    public function deleteCache(){
        // Variable deklarieren
        $dir = __DIR__.'/../../cache/parts/';
        // Variable deklarieren und Verzeichnis öffnen
        $verz = opendir($dir);
        // Verzeichnisinhalt auslesen
        while ($file = readdir ($verz))
        {
            // "." und ".." bei der Ausgabe unterdrücken
            if($file != "." && $file != ".." && $file != 'placeholder.txt')
            {
                // File löschen
                unlink($dir.$file);
            }
        }
        // Verzeichnis schließen
        closedir($verz);
    }


    /**
     * @return array Die Namen der Controller als Keys und die Dateinamen der ControllerDateien als Schlüssel
     */

    public function getControllerListe(){
        $aController=[];
        $aControllerConfigs = getDirectoryTree(__DIR__."/../../controller", array(),true);

        foreach($aControllerConfigs as $aControllerConfigFile){
            $yamlPath = __DIR__.'/../../controller/'.$aControllerConfigFile.'.yaml';
            $controllerPath= __DIR__.'/../../controller/'.$aControllerConfigFile.'.php';
            if(file_exists($yamlPath)){
                $controllerData = spyc_load_file($yamlPath);
                //print_r($controllerData);
                if(!array_key_exists($controllerData['name'],$aController) && file_exists($controllerPath)) {
                    $aController[$controllerData['name']] = $aControllerConfigFile;
                }
            }
        }
        return $aController;
    }



}



