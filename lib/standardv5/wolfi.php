<?php /** @noinspection PhpIncludeInspection */

/**
 * Created by PhpStorm.
 * User: micro
 * Date: 19.04.2019
 * Time: 22:40
 */

/**
 * Class wolfi
 */

class wolfi{

    /**
     * @var array $templateVariableArray Eine Liste der in der aktuell verarbeiteten Templatedatei vorhandenen Felder (Labels) als Schlüssel, mit dem anzuzeigenden Inhalt als Werte
     */

    private static $templateVariableArray=[];

    /**
     * @var string $templateFile Pseudo-Dateiname, dem entweder ein .part oder ein .tpl angehängt wird. Die Endung .php wirda automatisch ergänzt!
     */

    private static $templateFile='';

    /** Die Funktion generiert aus der Datei (angegeben als Pseudodateiname unterhalb des template Ordners ohne .php) und den angegebenen Einträgen den HTMl-Code
     * @param string $tempFileName
     * @param array $tempVarValues
     * @return false|string
     * @throws Exception
     */

    static function parseTemplate(string $tempFileName='',array $tempVarValues=[])
    {
        //GGF aus der Funktion Übernehmen
        if($tempFileName!='')self::$templateFile=$tempFileName;
        if(!empty($tempVarValues))self::$templateVariableArray=$tempVarValues;

        if(self::$templateFile=='')throw new Exception('',31);

        ## Vergleich der Dateivariablen mit dem Array ggf leere variable erzeugen
        ##
        $emptyTemplate = self::empty_template_from_file(self::$templateFile);
        $emptyFields = $emptyTemplate->fields;

        //Template Variablen laden
        foreach($emptyTemplate->fields as $wolfiTemplateVariableName){

            //Variable erzeugen, wenn sie registriert wurde
            if(array_key_exists($wolfiTemplateVariableName,self::$templateVariableArray)){
                $$wolfiTemplateVariableName=self::$templateVariableArray[$wolfiTemplateVariableName];
                unset($emptyFields[$wolfiTemplateVariableName]);
            }
            //Wenn eine Variable vom Template gefordert wird, aber nicht übergeben wurde soll sie mit sich selbst eingefügt werden
            else{
                $$wolfiTemplateVariableName='###'.$wolfiTemplateVariableName.'###';
            }
        }

        ob_start();

        //Template laden und von PHP parsen lassen
        include(self::createTemplatePath(self::$templateFile));

        //Variablen löschen
        foreach($emptyTemplate->fields as $wolfiTemplateVariableName){
            unset($$wolfiTemplateVariableName);
        }

        //Templatedaten leeren
        self::$templateFile='';
        self::$templateVariableArray=[];

        //Ausgabepuffer ausgeben lassen
        return ob_get_clean();

    }

    /** Generiert aus dem übergebenen Seitenelement-Text den Quellcode und gibt ihn zurück-
     * @param seitenElementText $seitenElementText
     * @return string generierter Quellcode.
     * @throws Exception
     */

    public static function parseSeitenElementText(seitenElementText $seitenElementText){

        // Initialisiere den Ausgabepuffer
        $output = '';

        //alle Einträge durchgehen und dann den Output speichern
        foreach($seitenElementText->elements as $textElement){

            //lade die einzelnen Einträge mit dem Namen und dem Wert in die Templatevariabeln
            $templateVariables = [];
            //Gehe alle Felder dieses Elements durch
            foreach($textElement->entries as $textElementFieldName => $textElementField){
                $templateVariables[$textElementFieldName]= $textElementField->value;
            }

            // Generiere mit der angegebenen Datei den aktuellen Quellcode.
            // Ergänze den Ausgabepuffer um den aktuell generierten Quellcode
            $output.=self::parseTemplate($textElement->getFile().'.part',$templateVariables);
        }

        return $output;

    }

    /**
     * @return array
     */

    public static function getTemplateVariableArray(): array
    {
        return self::$templateVariableArray;
    }

    /**
     * @param array $templateVariableArray
     */
    public static function setTemplateVariableArray(array $templateVariableArray)
    {
        self::$templateVariableArray = $templateVariableArray;
    }

    /**
     * @return string
     */
    public static function getTemplateFile(): string
    {
        return self::$templateFile;
    }

    /**
     * @param string $templateFile
     */
    public static function setTemplateFile(string $templateFile)
    {
        self::$templateFile = $templateFile;
    }

    public static function registerTemplateVariable($name,$value){
        self::$templateVariableArray[$name]=$value;
    }

    public static function createTemplateBasePath(){
        return  __DIR__.'/../../template';
    }

    /**
     * @param $filename
     * @return string
     */

    public static function createTemplatePath($filename){
        return self::createTemplateBasePath().'/'.$filename.'.php';
    }

    /** Lädt aus einer Templatedatei (angegeben durch Pseudodateipfade Unterhalb des template-Ordners ohne .php
     * @param $filename string Ein Dateinameverweis, der nur Ordner und den ersten Namensbestandteil und die Sub-Endung der Datei einthält ohne die Endung .php. Es muss also von der Datei *.tpl.php nur *.tpl und von der Datei *.part.php nur *.part  eingetragen werden!
     * @return template
     * @throws Exception
     */

    public static function empty_template_from_file(string $filename){

        $path = self::createTemplatePath($filename);
        $variablen=array();

        //Templatetext laden
        if(!file_exists($path))throw new Exception($filename.'=>'.$path,7);
        $quelltext = htmlentities(file_get_contents($path));

        //Quelltext nach Variablen durchsuchen
        preg_match_all('/\$[a-zA-Z][_]?[\x7f-\xff]?[a-zA-Z0-9_\x7f-\xff]*/',$quelltext, $variablen);

        //Template-Objekt initialisieren und eindeutige Liste erstellen
        $template = new template();
        $template->file=preg_replace('/\.tpl/','',preg_replace('/\.part/','',$filename));
        $variablen[0]=array_unique($variablen[0]);

        //All Variablen laden und ihnen Einträge gaben
        foreach($variablen[0] as $variable) {
            $variableName = substr($variable,1);
            $template->fields[]=$variableName;
            $template->entries[$variableName] = new templateEntry('string', '###'.$variableName.'###');
        }

        //Nach inludes suchen
        $includes = array();
        if(preg_match_all('/include\([A-Za-z.\'\"_\-\/]*\);/',$quelltext, $includes)!==FALSE) {


            foreach ($includes[0] as $includeEntry) {

                //Aus dem Dateifad dem include Befehl filtern
                $includePath = substr(
                    $includeEntry,
                    strpos($includeEntry, '(') + 2,
                    strpos($includeEntry, ')') - strpos($includeEntry, '(') - 3 - 4);

                //Wenn ein Pfad vorangestellt ist, muss der Templateverweis daraus gefiltert werden
                if(strstr($includePath,'/')!==false)
                {
                    //GGf. den absoluten Pfad abschneiden
                    $basicPathPosition = strpos(BASIC_PATH,$includePath);
                    if($basicPathPosition !== false){
                        $includePath=substr($includePath,$basicPathPosition+strlen(BASIC_PATH));
                    }

                    //den Pfad vor dem templateOrdner abschneiden
                    $templatePathPosition = strpos('template',$includePath);
                    if($templatePathPosition !== false){
                        $includePath=substr($includePath,$templatePathPosition+8);
                    }

                    $includeTemplate = self::empty_template_from_file($includePath);
                }
                //Wenn kein Pfad vorangestllt wurde, wird einfach so der eigene pfad verwendet
                else
                {
                    //template daraus laden
                    $includeTemplate = self::empty_template_from_file(substr($filename, 0, strrpos($filename, '/')) . '/' . $includePath);
                }

                //die Felder dem Basistemplate hinzufügen
                $template->fields = array_merge($template->fields, $includeTemplate->fields);
                $template->entries = array_merge($template->entries, $includeTemplate->entries);
            }
        }


        return $template;
    }

    /**
     * @param string $type string, text, controller
     * @param string $value für Strings ist es einfach der Text, bei test die TextId und bei controllern der Name der controller-Datei
     * @param string $action nur für Controller nötig, wenn es leer ist oder mit einer Zeichenkette ... versehen, wird die im Controller die Funktion ...Action aufgerufen, wenn ein % steht wird die aktion aus seitenklasse->inhalt->aktion geladen
     * @return templateEntry
     */

    static function create_templateEntry(string $type,string $value, string $action='')
    {
        return  new templateEntry($type,$value,$action);
    }

    static function load_template_files($pfad='')
    {
        $result = array();

        if ($pfad === '') {
            if (CALLED_FROM_ADMIN) {
                $pfad = '../template';
            } else {
                $pfad = 'template';
            }
        }

        $cdir = scandir($pfad);
        foreach ($cdir as $key => $value) {

            //Wenn es sich nicht um die Standard-Verzeichnisse handelt
            if (!in_array($value, array(".", ".."))) {

                //Wenn es ein Verzeichnis ist
                if (is_dir($pfad . DIRECTORY_SEPARATOR . $value)) {

                    $loadesFiles = self::load_template_files($pfad . '/' . $value);
                    if(!empty($loadesFiles))$result[$value] = $loadesFiles;
                }
                //Wenn es eine Datei ist
                else {
                    //Wird geprüft, ob es eine tpl-Datei ist
                    if(strstr($value,'tpl.php')) {
                        $result[substr($value,0,-8)] = 'file';
                    }
                }
            }
        }

        return $result;

    }


}
