<?php


class benutzerArchive extends modulBaseClass{

    protected static $MODUL='main_benutzer';

    protected static $lastUserdata=['id'=>NULL];

    /**
     * @param string $argOrderBy
     * @param string $argSortierung
     * @return array
     * @throws Exception
     */

    static public function get_benutzer_alle_mitgruppe($argOrderBy="main_benutzer.regdatum",$argSortierung="asc")
    {
        self::checkPermission(USERGROUPS::USER);

        $argSortierung = strtoupper($argSortierung) == "ASC" ? "ASC" : "DESC";
        return db::query("SELECT main_benutzer.*, main_nutzergruppen.Name as Gruppenname, main_nutzergruppen.id as Gruppenid
                           FROM main_benutzer, main_nutzergruppen
                          WHERE main_nutzergruppen.id=main_benutzer.nutzergruppe_id and not main_benutzer.id=1
                          ORDER BY {$argOrderBy} {$argSortierung};");
    }

    /**
     * @param string $argAufsteigend
     * @param string $order
     * @return array
     * @throws Exception
     */

    static public function get_benutzergruppen_alle($argAufsteigend="asc",$order='id')
    {
        self::checkPermission();

        if($argAufsteigend!=='desc' && $argAufsteigend!=='asc')$argAufsteigend='asc';
        if($order!=='id' && $order!=='Name')$order='id';
        $order='order by '.$order;


        return db::query("select id, Name from main_nutzergruppen {$order} {$argAufsteigend}");

    }

    /**
     * @param int $userId
     * @return bool
     * @throws Exception
     */


    static function benutzer_loeschen(int $userId){
        self::checkPermission(USERGROUPS::ADMIN,$userId);

        if($userId===0)throw new Exception('User:'.self::$userIdent->id,894);

        $adminBerCount = db::querySingle('select count(benutzer_id) from main_admin_ber where benutzer_id = :userid',[':userid'=>$userId])['count(benutzer_id)'];
        db::delete('Delete from main_admin_ber where benutzer_id = :userid',[':userid'=>$userId]);
        if(db::lastRowCount()==$adminBerCount){
            $seitenBerCount = db::querySingle('select count(benutzer_id) from main_seiten_ber where benutzer_id = :userid',[':userid'=>$userId])['count(benutzer_id)'];
            db::delete('Delete from main_seiten_ber where benutzer_id = :userid',[':userid'=>$userId]);
            if(db::lastRowCount()==$seitenBerCount) {
                db::delete('Delete from main_benutzer where id = :userid',[':userid'=>$userId]);
                if(db::lastRowCount()==1){
                    return true;

                }else{
                    if($userId===0)throw new Exception('User:'.self::$userIdent->id,895);
                }
            }else{
                if($userId===0)throw new Exception('User:'.self::$userIdent->id,896);
            }
        }else{
            if($userId===0)throw new Exception('User:'.self::$userIdent->id,897);
        }

        return false;
    }

    /**
     * @param $argNutzerId
     * @param $argGruppenId
     * @throws Exception
     */

    static public function set_benutzer_gruppe($argNutzerId,$argGruppenId)
    {
        self::checkPermission(USERGROUPS::ADMIN);
        db::insert("update main_benutzer set nutzergruppe_id=".$argGruppenId." where id=".$argNutzerId.";");
    }

    /**
     * @param int $argUId
     * @param string $argNickname
     * @param string $argRealerName
     * @param int $argZeig_Pers_Daten
     * @param int $argNeue_Pns_Anzeigen
     * @param int $argNeue_Pns_Mail
     * @param string $argEMail
     * @return int|NULL
     * @throws Exception
     */

    static public function set_benutzer_daten( int $argUId, string $argNickname, string $argRealerName, int $argZeig_Pers_Daten, int $argNeue_Pns_Anzeigen, int $argNeue_Pns_Mail, string $argEMail)
    {
        self::checkPermission(USERGROUPS::ADMIN,$argUId);

        if($argEMail==''){
            db::insert(
                "update main_benutzer
                 set nickname=:nickname, realername=:realername, zeig_pers_daten=:zeig_pers_daten, neue_pns_anzeigen=:neue_pns_anzeigen, neue_pns_mail=:neue_pns_mail
                 where id={$argUId}",
                [':nickname'=>$argNickname,':realername'=>$argRealerName,':zeig_pers_daten'=>$argZeig_Pers_Daten,':neue_pns_anzeigen'=>$argNeue_Pns_Anzeigen,':neue_pns_mail'=>$argNeue_Pns_Mail]
            );
        }else{

            ##### Ob die eMail schon existiert
            db::query("SELECT email FROM main_benutzer WHERE email=:email or new_email=:email;",[':email'=>$argEMail]);

            ##### Wenn er der erste mit dieser Mail ist
            if (db::lastRowCount() == 0) {

                db::insert(
                    "update main_benutzer
                 set nickname=:nickname, realername=:realername, zeig_pers_daten=:zeig_pers_daten, neue_pns_anzeigen=:neue_pns_anzeigen, neue_pns_mail=:neue_pns_mail, email=:email
                 where id={$argUId}",
                    [':nickname' => $argNickname, ':realername' => $argRealerName, ':zeig_pers_daten' => $argZeig_Pers_Daten, ':neue_pns_anzeigen' => $argNeue_Pns_Anzeigen, ':neue_pns_mail' => $argNeue_Pns_Mail, ':email' => $argEMail]
                );
            }else{
                throw new Exception('Handelnder User:'.self::$userIdent->id.' Betroffener Benutzer:'.$argUId,40);
            }
        }
        return db::lastRowCount();
    }

    /**
     * @param integer $argUserId Die Id des Benutzers
     * @return bool Bei Erfolg wird true zurückgegeben.
     * @throws Exception
     */

    static public function set_benutzer_zufallspasswort($argUserId)
    {
        self::checkPermission(USERGROUPS::ADMIN,$argUserId);
        $sNeuesPW = zufallszahl();
        $aNutzerDaten=db::querySingle('select nickname, email from main_benutzer where id=:userid',[':userid'=>$argUserId]);

        $empfaenger = $aNutzerDaten["email"];
        $betreff = 'Passwort gändert';
        $nachricht = <<<EMAIL
Travia zum Gruße, {$aNutzerDaten["nickname"]}!


Für Euch wurde eine neues Passwort generiert. Es lautet: $sNeuesPW.


Wenn Ihr das ändert möchtet, benutzt in den persönlichen Einstellungen
die Funktion "Passwort ändern".


Mit freundlichen Grüßen
Der Wirt und seine Helfer
EMAIL;
        $header = 'From: derwirt@heldenschenke.de' . "\r\n" .
            'Reply-To: derwirt@heldenschenke.de' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        if(mail($empfaenger, $betreff, $nachricht, $header)) {

            if(!self::setUserPasswort($argUserId,$sNeuesPW)) {
                throw new Exception($argUserId, 30);
            }else {
                return true;
            }
        }else {
            throw new Exception($argUserId,32);
        }
    }

    /**
     * @param string $nickname
     * @param string $passwort
     * @param string $email
     * @return array
     * @throws Exception
     */

    public static function create_benutzer($nickname,$passwort,$email){

        self::checkPermission(USERGROUPS::ADMIN);

        if (preg_match('/\S+@\S+\.\S+/', $email)) {

            #### Wenn alle Eingaben richtig bzw. nicht leer sind
            ##### Ob der User schon exestiert
            db::query("select nickname from main_benutzer where nickname=:nickname;",[':nickname'=>$nickname]);

            ##### Wenn er der erste mit dies dem Namen ist
            if (db::lastRowCount() == 0) {
                ##### Ob die eMail schon existiert
                db::query("SELECT email FROM main_benutzer WHERE email='" . addslashes($email) . "' or new_email='" . addslashes($email) . "';");

                ##### Wenn er der erste mit dieser Mail ist
                if (db::lastRowCount() == 0) {

                    ##Dann wird jetzt der Freischaltungscode für die E-Mailadresse erzeugt
                    $neueEMailKeyString = zufallszahl(20);
                    $neueEMailDatumInt = time();

                    ## Benutzer mit der E-Mailadresse erstellen
                    ##

                    $userID = db::insert("INSERT INTO main_benutzer (`nickname`,`new_email_key`,`new_email_date`,`new_email`,`nutzergruppe_id`,`regdatum`)" .
                        " VALUES ('$nickname','$neueEMailKeyString',$neueEMailDatumInt,'$email',2,$neueEMailDatumInt)");

                    ## Passwort erstellen
                    ##

                    $salt = uniqid(mt_rand(), true);
                    //beachte, dass die Passwörter bereits verschlüsselt sind.
                    $pw = hash('sha512', $passwort . $salt);

                    ## Passwort speichern
                    ##

                    $sql = 'UPDATE main_benutzer SET passwort = :pw, salt = :salt WHERE new_email_key = :new_email_key';
                    $args = [':pw' => $pw, ':salt' => $salt, ':new_email_key' => $neueEMailKeyString];
                    db::insert($sql, $args);

                    if (db::lastRowCount() > 0) {
                        return [$userID,$neueEMailKeyString];
                    }else{
                        //benutzer konnte nicht gespeichert werden
                        throw new Exception($nickname.''.$email,907);
                    }
                }else{
                    //Bereits E-Mail verbraucht
                    throw new Exception($nickname.''.$email,40);
                }
            }else{
                //Bereits benutzer mit diesem namen
                throw new Exception($nickname.''.$email,909);
            }
        }else{
            //Die E-Mail hat das falsche Format
            throw new Exception($nickname.''.$email,908);
        }
    }

    /**
     * @param $userId
     * @param $newPassword
     * @return bool
     * @throws Exception
     */

    protected static function setUserPasswort($userId,$newPassword){
        self::checkPermission(USERGROUPS::ADMIN,$userId);

        // generate salt
        $salt = uniqid(mt_rand(), true);
        // generate salted pw
        $pw = hash('sha256', $newPassword . $salt);

        $sql = 'UPDATE main_benutzer SET passwort = :pw, salt = :salt
                WHERE id = :uid';
        $args = [':pw' => $pw, ':salt' => $salt, ':uid' => $userId];

        db::update($sql, $args);

        if (db::lastRowCount()>0) {
            return true;
        }else{
            throw new Exception('User:'.$userId,907);
        }

    }

    /**
     * @param int $argUId
     * @return array|mixed
     * @throws Exception
     */

    static public function get_benutzer_alle_daten(int $argUId){
        self::checkPermission(USERGROUPS::ADMIN,$argUId);
        if(self::$lastUserdata['id']!==$argUId){
            self::$lastUserdata = self::getBenutzerDatenFromDB($argUId);
        }
        return self::$lastUserdata;
    }

    /**
     * @param $argUId
     * @return mixed
     * @throws Exception
     */

    static protected function getBenutzerDatenFromDB($argUId)
    {
        $response = db::querySingle("SELECT * FROM main_benutzer WHERE id=:userid;", [':userid' => $argUId]);
        //Wenn der Benutezr nicht gefunden werden konnte, muss ein Fehler geworfen werden
        if($response===false)throw new Exception($argUId,42);
        return $response;
    }

    /**
     * @param int $argUId
     * @param string $argParameter
     * @return mixed
     * @throws Exception
     */


    static public function get_benutzer_daten($argUId=0,$argParameter="nickname")
    {
        //Den Benutzernamen muss man auch so bekommen können, ohne Passwort abfrage
        if($argParameter!="nickname" && $argParameter!='nutzergruppe_id' && $argParameter!='attempts')self::checkPermission();

        //andere Benutzerdaten als dem Nicknamen zum unbekannten Gast können nicht abgerufen werden
        if($argUId==0 && $argParameter!="nickname" && $argParameter!='nutzergruppe_id' )throw new Exception('',904);

        //Wenn es noch keinen letzten User oder den unbekannten Gast gab, werden die Daten neu geladen
        if(self::$lastUserdata['id']!==$argUId){
            self::$lastUserdata = self::getBenutzerDatenFromDB($argUId);
        }

        return self::$lastUserdata[$argParameter];
    }

    /**
     * @param $argName
     * @return mixed
     * @throws Exception
     *
     */
    static public function get_benutzer_id($argName)
    {
        self::checkPermission();
        return db::querySingle("select id from main_benutzer where nickname=:argName;",array(":argName"=>$argName));
    }

    /**
     * @param $query
     * @return array
     * @throws Exception
     */
    static public function suche_benutzer($query){
        self::checkPermission();
        //return db::query('SELECT main_benutzer.*, main_nutzergruppen.Name as Gruppenname from main_benutzer, main_nutzergruppen where match (main_benutzer.nickname) against (:query in boolean mode) and main_benutzer.nutzergruppe_id=main_nutzergruppen.id;',[':query'=>$query]);
        return db::query('SELECT main_benutzer.*, main_nutzergruppen.Name as Gruppenname from main_benutzer, main_nutzergruppen where main_benutzer.nickname LIKE :query and main_benutzer.nutzergruppe_id=main_nutzergruppen.id;',[':query'=>$query]);
    }

    /**
     * @param $argUId
     * @param $argSeiteId
     * @return mixed
     * @throws Exception
     */
    static public function get_benutzer_seitenrecht($argUId,$argSeiteId)
    {
        self::checkPermission(USERGROUPS::ADMIN,$argUId);
        ## Diese ist für alle nutzerbar
        ##

        return db::querySingle(
            "select lesen, aendern 
            from main_seiten_ber 
            where seite_id = :seitenId and benutzer_id=:userid and not seite_id=0;",
            ['userid'=>$argUId,'seitenId'=>$argSeiteId]);
    }

    /**
     * @param $argUId
     * @return array
     * @throws Exception
     */
    static public function get_benutzer_seitenrechte($argUId)
    {
        self::checkPermission(USERGROUPS::ADMIN,$argUId);
        return db::query("select main_seiten.id, main_seiten.Name, main_seiten_ber.lesen, main_seiten_ber.aendern  from main_seiten, main_seiten_ber where main_seiten_ber.seite_id = main_seiten.id and main_seiten_ber.benutzer_id={$argUId} and not id=0;");
    }

    /**
     * @param int $uid
     * @return array
     * @throws Exception
     */
    public static function get_benutzer_unberechtigteSeiten(int $uid)
    {
        self::checkPermission(USERGROUPS::ADMIN,$uid);

        return db::query("SELECT id, Name FROM main_seiten WHERE NOT id = any( SELECT seite_id FROM main_seiten_ber WHERE benutzer_id =:uid and not id=0);", [':uid' => $uid]);
    }

    /**
     * @param int $uid
     * @return array
     * @throws Exception
     */
    public static function benutzer_seitenrechte_anzeigen(int $uid){

        self::checkPermission();

        $aRechte=self::get_benutzer_seitenrechte($uid);

        $aLeereRechte=self::get_benutzer_unberechtigteSeiten($uid);

        $aAlleRechte = array();

        foreach ( $aRechte as $aRecht )
        {
            array_push($aAlleRechte,array('id'=>$aRecht["id"],'Name'=>$aRecht["Name"],'lesen'=>$aRecht["lesen"],'aendern'=>$aRecht["aendern"]));
        }

        foreach ( $aLeereRechte as $aRecht )
        {
            array_push($aAlleRechte,array('id'=>$aRecht["id"],'Name'=>$aRecht["Name"],'lesen'=>0,'aendern'=>0));
        }

        asort($aAlleRechte);

        return $aAlleRechte;
    }

    /**
     * @param $argUId
     * @throws Exception
     */
    static public function benutzer_seitenberechtigungen_loeschen($argUId)
    {
        self::checkPermission(USERGROUPS::ADMIN,$argUId);
        db::insert("delete from main_seiten_ber where benutzer_id=$argUId;");
    }

    /**
     * @param $argSeiteId
     * @param $argBenutzerId
     * @param $argLesen
     * @param $argAendern
     * @return bool
     * @throws Exception
     */

    static public function benutzer_seitenberechtigung_erstellen( $argSeiteId, $argBenutzerId, $argLesen, $argAendern)
    {
        self::checkPermission(USERGROUPS::ADMIN);
        db::insert(
            "insert into main_seiten_ber ( `seite_id`,`benutzer_id`,`lesen`,`aendern` ) values (:seitenId,:userid,:lesen,:aendern)",
            [':seitenId'=>$argSeiteId,':userid'=>$argBenutzerId,':lesen'=>$argLesen,':aendern'=>$argAendern]);
        if(db::lastRowCount()==0){
            throw new Exception("$argSeiteId, $argBenutzerId, $argLesen, $argAendern",914);
        }else{
            return true;
        }
    }


    /**
     * @param $argUId
     * @return array
     * @throws Exception
     */

    static public function get_benutzer_adminrechte($argUId)
    {
        self::checkPermission(USERGROUPS::ADMIN,$argUId);
        $unsortedRechte=db::query("select main_admin_module.id, main_admin_module.Name, main_admin_ber.aendern  from main_admin_module, main_admin_ber where main_admin_ber.bereich_id = main_admin_module.id and main_admin_ber.benutzer_id=:userid and not id=0",[':userid'=>$argUId]);

        $sortedRechte = [];

        foreach($unsortedRechte as $recht){
            $sortedRechte[$recht['id']]=$recht;
        }

        unset($unsortedRechte);

        return $sortedRechte;
    }

    /**
     * @param $argUId
     * @throws Exception
     */

    static public function benutzer_adminberechtigungen_loeschen($argUId)
    {
        self::checkPermission(USERGROUPS::ADMIN,$argUId);
        db::insert("delete from main_admin_ber where benutzer_id=:userId",[':userId'=>$argUId]);
    }

    /**
     * @param $argBereichId
     * @param $argBenutzerId
     * @param $argAendern
     * @throws Exception
     */
    static public function benutzer_adminberechtigung_setzen($argBereichId,$argBenutzerId,$argAendern)
    {
        self::checkPermission(USERGROUPS::ADMIN);
        db::insert("insert into main_admin_ber ( `bereich_id`,`benutzer_id`,`aendern` ) values ( :bereichId, :userid, :aendern)",[':bereichId'=>$argBereichId, ':userid'=>$argBenutzerId, ':aendern'=>$argAendern] );

    }

    /**
     * @param int $userId
     * @param string $neueEMail
     * @return bool|string
     * @throws Exception
     */

    static public function set_benutzer_neue_mail(int $userId, string $neueEMail){
        self::checkPermission(USERGROUPS::ADMIN,$userId);
        //Key generieren
        $neueEMailKeyString = zufallszahl(20);
        $neueEMailDatumInt = time();

        //speichere alles ab!
        db::insert(
            "update main_benutzer set new_email=:neue_email, new_email_key=:key, new_email_date=:date where id=:userId;",
            ['userId'=>$userId,':neue_email'=>$neueEMail,':key'=>$neueEMailKeyString,':date'=>$neueEMailDatumInt]);

        if(db::lastRowCount() > 0)return $neueEMailKeyString;
        else return false;
    }

    /**
     * @param $emailkey
     * @return bool
     * @throws Exception
     */
    static function confirm_benutzer_mail($emailkey){
        self::checkPermission();
        //Link-Daten laden
        $eMailDatenArray=db::querySingle(
            "select id,new_email_date, new_email from main_benutzer where new_email_key=:key;",
            [':key'=>$emailkey]);
        if (db::lastRowCount() > 0) {

            //Wenn der Link zu alt ist, soll er nicht mehr verwendet werden.
            $aktuellerZeitstempelInteger = time();
            if ($eMailDatenArray["new_email_date"] < ($aktuellerZeitstempelInteger - 86400)) {
                return false;

            } else {
                //Die E-Mail ist ok und wird nun eingetragen
                db::insert("update main_benutzer set new_email_date=0, new_email='', new_email_key='', email=:neue_mail where id=:userId;",
                    [':neue_mail'=>$eMailDatenArray['new_email'], ':userId'=>$eMailDatenArray['id']]);

                //Wenn es erfolgreich war.
                if(db::lastRowCount()==1){
                    return true;

                } // Wenn es nicht erfolgreich war
                else{
                    return false;
                }
            }
        } else {
            //Wenn es keinen Link gibt, war es das.
            return false;
        }


    }

    /**
     * @return int
     * @throws Exception
     */

    static public function get_benutzer_anzahl() {

        self::checkPermission();

        ## Laden die Anzahl der Benutzer
        ##

        $aAnzahl = db::querySingle("select count(*) as Anzahl from main_benutzer where 1;");

        ## Ziehe Root und unbekannten Gast ab
        ##
        return ($aAnzahl["Anzahl"] - 2);
    }

    /**
     * @param int $groupId
     * @return array
     * @throws Exception
     */

    static public function get_benutzergruppe_rechte(int $groupId)
    {
        self::checkPermission();

        return db::query(
            "select main_seiten.id, main_seiten.Name, main_seiten_ber.lesen, main_seiten_ber.aendern 
            from main_seiten, main_seiten_ber 
            where main_seiten_ber.seite_id = main_seiten.id and main_seiten_ber.nutzergruppe_id=:groupId;",
            [':groupId'=>$groupId]);
    }

    /**
     * @param int $gid
     * @return mixed
     * @throws Exception
     */
    static public function nutzergruppe_daten_laden(int $gid){
        self::checkPermission(USERGROUPS::ADMIN);
        return db::querySingle("select * from main_nutzergruppen where id=:gid",[':gid'=>$gid]);
    }

    /**
     * @param int $gid
     * @param string $name
     * @param string $kommentar
     * @return bool
     * @throws Exception
     */

    static public function nutzergruppe_daten_aendern(int $gid,string $name, string $kommentar)
    {
        self::checkPermission(USERGROUPS::ADMIN);
        if ($name == "" || $kommentar == "") {
            throw new Exception('Gruppenname:' . $name . ' Kommentar:' . $kommentar, 890);
        } else {
            db::insert("update main_nutzergruppen set main_nutzergruppen.Name=:name, main_nutzergruppen.Kommentar=:kommentar where id=:gid", [':name' => $name, ':kommentar' => $kommentar, ':gid' => $gid]);
            if (db::lastRowCount() == 1) return true;
            else throw new Exception('Gruppenname:' . $name . ' Kommentar:' . $kommentar, 891);
        }
    }

    /**
     * @param int $gid
     * @return array
     * @throws Exception
     *
     */

    public static function nutzergruppe_seitenrechte_anzeigen(int $gid)
    {
        self::checkPermission(USERGROUPS::ADMIN);

        $aGruppenRechte=self::get_benutzergruppe_rechte($gid);

        $aLeereGruppenRechte=self::get_nutzergruppe_unberechtigteSeiten($gid);

        $aAlleGruppenRechte = array();

        foreach ( $aGruppenRechte as $aRecht )
        {
            array_push($aAlleGruppenRechte,array('id'=>$aRecht["id"],'Name'=>$aRecht["Name"],'lesen'=>$aRecht["lesen"],'aendern'=>$aRecht["aendern"]));
        }

        foreach ( $aLeereGruppenRechte as $aRecht )
        {
            array_push($aAlleGruppenRechte,array('id'=>$aRecht["id"],'Name'=>$aRecht["Name"],'lesen'=>0,'aendern'=>0));
        }

        asort($aAlleGruppenRechte);

        return $aAlleGruppenRechte;

    }

    /**
     * @param int $gid
     * @return array
     * @throws Exception
     */

    public static function get_nutzergruppe_unberechtigteSeiten(int $gid)
    {
        self::checkPermission();

        return db::query("SELECT id, Name FROM main_seiten WHERE NOT id = any( SELECT seite_id FROM main_seiten_ber WHERE nutzergruppe_id =:gid and not id=0);", [':gid' => $gid]);
    }

    /**
     * @param int $gid
     * @throws Exception
     */

    public static  function nutzergruppe_seitenberechtigungen_loeschen(int $gid)
    {
        self::checkPermission(USERGROUPS::ADMIN);

        db::delete("delete from main_seiten_ber where nutzergruppe_id=:gid;",[':gid'=>$gid]);
    }

    /**
     * @param int $id
     * @param int $gid
     * @param int $lesen
     * @param int $aendern
     * @throws Exception
     */
    public static  function nutzergruppe_seitenberechtigung_setzen( int  $id, int $gid, int $lesen, int $aendern)
    {
        self::checkPermission(USERGROUPS::ADMIN);

        db::delete("insert into main_seiten_ber ( `seite_id`,`texte_id`,`nutzergruppe_id`,`benutzer_id`,`lesen`,`aendern` ) values (:id,NULL,:gid,NULL,:lesen,:aendern);",
            [':id'=>$id,':gid'=>$gid,':lesen'=>$lesen,':aendern'=>$aendern]);
    }

    /**
     * @param int $id
     * @param int $uid
     * @param int $lesen
     * @param int $aendern
     * @throws Exception
     */

    public static  function benutzer_seitenberechtigung_setzen( int  $id, int $uid, int $lesen, int $aendern)
    {
        self::checkPermission(USERGROUPS::ADMIN);

        db::delete("insert into main_seiten_ber ( `seite_id`,`texte_id`,`nutzergruppe_id`,`benutzer_id`,`lesen`,`aendern` ) values (:id,NULL,NULL,:uid,:lesen,:aendern);",
            [':id'=>$id,':uid'=>$uid,':lesen'=>$lesen,':aendern'=>$aendern]);
    }

    /**
     * @param $pw
     * @param $email
     * @return int
     * @throws Exception
     */
    public static function set_new_root($pw,$email) {

        // generate salt
        $salt = uniqid(mt_rand(), true);
        // generate salted pw
        $Lol="$pw = ".hash('sha512', "$pw" . "$salt");
        $pw = hash('sha512', "$pw" . "$salt");

        $sql = 'UPDATE main_benutzer SET passwort = :pw, salt = :salt, email= :email, attempts=0
                WHERE id = 1';
        $args = [':pw' => $pw, ':salt' => $salt, ':email' => $email];

        db::update($sql, $args);


        if (db::lastRowCount()>0) {
            return UserIdentity::PW_CHANGE_SUCC;
        }else{
            return UserIdentity::PW_CHANGE_OLD_PW_WRONG;
        }
    }

}
