<?php



class siteContent {


    /**
     * @var int
     */
    public $id=0;
    /**
     * @var bool
     */
    public $showWelcomePage=false;

    /**
     * @var bool
     */
    //Wird grundsätzlich erstmal gesperrt
    public $zugangVerboten=true;

    /**
     * @var string
     */
    public $aktion='';

    /**
     * @var template
     */
    public $templateDaten=null;

    /**
     * @var string
     */

    public $templateDatei='';

    /**
     * @var string
     */
    public $modul='';
    /**
     * @var
     */
    public $modulskript;
    /**
     * @var
     */
    public $modulversion;
    /**
     * @var
     */
    public $modulbereich;
    /**
     * @var array
     */
    public $parameter=[];


}
