<?php

require_once(__DIR__.'/../spyc/Spyc.php');



class textElement extends template{

    /**
     * @var int|null $id interne ID des Text-Elements, null wenn noch keine vergeben wurde!
     */

    protected $id=null;

    /**
     * @var string $name Name des Text-Elements, der auch in der Übersicht angezeigt werden sollte.
     */

    protected $name='';

    /**
     * @var string $desc Die Beschreibung, wie das Text-Element eingesetzt werden sollte
     */

    protected $desc='';

    /**
     * @var string $section Die Gruppe, in der das Text-Element eingruppiert werden sollte.
     */

    protected $section='';

    /**
     * @var $entries textElementField[] Die Einträge zu den Feldern eines Elements
     */

    public $entries=array();

    /**
     * textElement constructor.
     * @param string $name
     * @param string $desc
     * @param string $file
     * @param string $section
     */

    public function __construct(string $name, string $desc, string $file, string $section='')
    {
        $this->name=$name;
        $this->desc=$desc;
        $this->file=$file;
        $this->section=$section;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDesc(): string
    {
        return $this->desc;
    }

    /**
     * @param string $desc
     */
    public function setDesc(string $desc): void
    {
        $this->desc = $desc;
    }

    /**
     * @return string
     */
    public function getSection(): string
    {
        return $this->section;
    }

    /**
     * @param string $section
     */
    public function setSection(string $section): void
    {
        $this->section = $section;
    }

    /**
     * @return textElementField[]
     */
    public function getEntries(): array
    {
        return $this->entries;
    }

    /**
     * @param textElementField[] $entries
     */
    public function setEntries(array $entries): void
    {
        $this->entries = $entries;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile(string $file): void
    {
        $this->file = $file;
    }


    /** Fügt ein Text-Element inklusive Inhalt hinzu!
     * @param $label string Name des Feldeintrages
     * @param $type string Typ des FEldeintrages
     * @param $value string|int Wert des Eintrags
     * @param $desc string Beschreibung des Eintrages zur Bedeutung und Nutzung
     * @param $placeholder string Der Platzhaltertext wird in leeren Eingabefeldern angezeigt
     * @throws Exception
     */

    public function addField($label,$type,$value,$desc='',$placeholder=''){
        $this->fields[]=$label;
        $this->addEntry($label,$type,$value,$desc,$placeholder);
    }

    /** Fügt einen Feldeintrag eines Text-Elements hinzu, wenn das Feld existiert
     * @param $label string Name des Eintrags
     * @param $type string Typ des Eintrages
     * @param $value string|int Wert des Eintrags
     * @param string $desc string Beschreibung
     * @param string $placeholder Der Platzhaltertext wird in leeren Eingabefeldern angezeigt
     * @throws Exception
     */

    public function addEntry($label,$type,$value,$desc='',$placeholder=''){
        if(in_array($label,$this->fields)){
            $this->entries[$label]=new textElementField($type,$value,$desc,$placeholder);
        }else{
            throw new Exception('"'.$label.'" '.print_r($this->fields,true).'in '.$this->file,72);
        }
    }

    /** Speichert einen Wert für einen vorhandenen Eintrag
     * @param $label string Name des Feldes im Text-Element
     * @param $value string|int Der neue Wert für das Feld im TExt-Element
     * @throws Exception
     */

    public function setEntryValue($label,$value){

        if(in_array($label,$this->fields)){

            switch($this->entries[$label]->type){
                case 'text':
                    if(gettype($value)!=='integer')throw new Exception(gettype($value).' statt einer Text-ID(integer)',75);
                    $this->entries[$label]->value=(int) $value;
                    break;
                case 'int':
                    if(gettype($value)!=='integer')throw new Exception(gettype($value).' statt integer',75);
                    $this->entries[$label]->value=(int) $value;
                    break;
                case 'string':
                    if(gettype($value)!=='string')throw new Exception(gettype($value).' statt string',75);
                    $this->entries[$label]->value=(string) $value;
                    break;
                default:
                    break;
            }


        }else{
            throw new Exception('"'.$label.'" '.print_r($this->fields,true).'in '.$this->file,72);
        }

    }
}
