<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lupix5 ~ <?php /** @noinspection PhpUndefinedVariableInspection */
        echo $pageName; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="apple-touch-icon" sizes="180x180" href="template/standardv5/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="template/standardv5/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="template/standardv5/img/favicon-16x16.png">
    <link rel="manifest" href="template/standardv5/img/site.webmanifest">
    <link rel="mask-icon" href="template/standardv5/img/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="template/standardv5/img/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="template/standardv5/img/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="template/standardv5/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="template/standardv5/scripts/jquery-ui/jquery-ui.css">

    <script src="template/standardv5/scripts/jquery.js"></script>
    <script src="template/standardv5/scripts/jquery-ui/jquery-ui.js"></script>
    <script src="template/standardv5/scripts/popper.min.js"></script>
    <script src="template/standardv5/scripts/bootstrap.min.js"></script>

    <script src="template/standardv5/scripts/funktionen.js" type="text/javascript"></script>

    <link rel="stylesheet" href="template/standardv5/fonts/fontawesome/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="template/standardv5/css/fonts.css" type="text/css" />
    <link rel="stylesheet" href="template/standardv5/css/standard.css" type="text/css" />
    <link rel="stylesheet" href="../template/standardv5/css/buttons.css" type="text/css" />


    <style>
        .fakeimg {
            height: 200px;
            background: #aaa;
        }
    </style>
</head>
<body>

<div class="jumbotron text-center" style="margin-bottom:0;background-image:url(<?php /** @noinspection PhpUndefinedVariableInspection */
echo $titleBild_; ?>);background-size:contain;background-repeat:no-repeat;background-position: center;">

    <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
</div>

<nav id="navi" class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-center shadow">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
        <?php /** @noinspection PhpUndefinedVariableInspection */
        echo $navi; ?>
    </div>
</nav>


<div class="container-xl" style="margin-top:15pt;margin-bottom:120pt;">
    <div class="row">
        <div class="col-md-3 mb-5"  id="left">
            <div class="rounded shadow" id="left-content">
                <?php /** @noinspection PhpUndefinedVariableInspection */
                echo $leftContent; ?>
            </div>
        </div>
        <div class="col-md-9 rounded mb-5" id="center">
            <h1 class="shadow"><?php /** @noinspection PhpUndefinedVariableInspection */ echo $pageTitle ?></h1>
            <div class="rounded shadow"  id="center-content">
            <?php /** @noinspection PhpUndefinedVariableInspection */

            echo $content; ?>

            </div>
        </div>
        <!--
        <div class="col-md-2 rounded shadow mb-5" id="right">
            <?php /** @noinspection PhpUndefinedVariableInspection */
            echo $rightContent; ?>
        </div>
        -->
    </div>
</div>

<div id="footer" class="container-fluid fixed-bottom">
    <div class="row">
        <div class="col">
    <?php /** @noinspection PhpUndefinedVariableInspection */
    echo $footer;?>
        </div></div>
    <div class="row">
        <div class="col">
    <?php
    /** @noinspection PhpUndefinedVariableInspection */
    echo $dbStats; ?>        </div></div>
    <div class="row">
        <div class="col">
    <?php /** @noinspection PhpUndefinedVariableInspection */
    echo $footerNavi; ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="https://pixelio.de">Hintergrundbild von Margit Völz / pixelio.de</a>
        </div>
    </div>
</div>

</body>
</html>
