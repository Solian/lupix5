<ul class="nav nav-tabs nav-justified">
    <?php
    foreach($menuEntries as $menuEntry) {
        echo '<li class="nav-item">
        <a href="'.$menuEntry['link'].'" class="nav-link ' . ($navFlag === $menuEntry['navFlag'] ? 'active' : '') . '">'.$menuEntry['title'].'</a>
    </li>';
    }
    ?>
</ul>

<?php




/** @var string $nachricht */
if($nachricht!==''){
    ?>
        <script>
            $(document).ready(function(){
                $('.toast').html('<div class="toast-header">\n' +
                    '        Hinweis\n' +
                    '    </div>\n' +
                    '    <div class="toast-body">\n' +
                    '        <?=$nachricht;?>\n' +
                    '    </div>').toast({delay:5000}).toast('show');
            });
        </script>
        <?php
}?>
