<h4>Datenbank auswählen</h4>

<div class="alert alert-warning">Geben Sie die Verbindungsdaten für den Datenbankserver ein</div>
<form action="index.php" method="post" name="">
    <div class="form-group">
        <label for="sel1">Datenbanken</label>
        <select class="form-control" id="dbName" name="dbName">
            <?php
            /** @noinspection PhpUndefinedVariableInspection */
            foreach($datenbanken as $db){
                echo '<option>'.$db['Database'].'</option>';
            }
            ?>
        </select>
    </div>
    <input type="hidden" name="aktion" value="installDataBase">
    <input type="hidden" name="mod" value="install">
    <button type="submit" class="lupix-btn btn-primary">Verbinden</button>
</form>

