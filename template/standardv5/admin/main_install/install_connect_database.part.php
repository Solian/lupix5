        <h4>Datenbank einrichten</h4>
<div class="alert alert-warning">Geben Sie die Verbindungsdaten für den Datenbankserver ein</div>
<form action="index.php?mod=install&aktion=connectDatabase" method="post" name="">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Server</span>
        </div>
        <input type="text" class="form-control" placeholder="Name oder IP-Adresse der Datenbank" name="dbServer">
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Benutzername</span>
        </div>
        <input type="text" class="form-control" placeholder="Benutzername des Datenbankbenutzers" name="dbUser">
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Passwort</span>
        </div>
        <input type="password" class="form-control" placeholder="Passwort für den Datenbankbenutzer" name="dbPass">
    </div>
    <button type="submit" class="lupix-btn btn-primary">Verbinden</button>
</form>

