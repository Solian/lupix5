        <h4>Login</h4>
<div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> Geben Sie das Passwort für das Installations-Tool ein!</div>

<form action="index.php?mod=install&aktion=installToolLogin" method="post" name="">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Passwort</span>
        </div>
        <input type="password" class="form-control" placeholder="Passwort für das Installations-Tool" name="installPasswort">
    </div>
    <button type="submit" class="lupix-btn btn-primary">Einloggen</button>
</form>
