<h4>Passwort für den Root-Benutzer</h4>
<div class="admin-modul">
<div class="admin-modul-content">
    <script src="../template/standardv5/scripts/jsencrypt/jsencrypt.min.js"></script>
    <script>
        function setEmailEmpty(){
            document.registerbox.email.value='';
        }

        function sendRegistration() {

            var passwRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
            var emailRegex = /\S+@\S+\.\S+/;
            $("#nicknametxt").removeClass("ui-state-error").text("Benutzername");
            $("#neuespw2txt").removeClass("ui-state-error").text("Bitte wiederholen Sie das Passwort!");
            $("#neuespw1txt").removeClass("ui-state-error").text("Passwort");
            $("#emailtxt").removeClass("ui-state-error").text("Adresse");


            if (document.registerbox.passwort1.value.match(passwRegex)) {
                if (document.registerbox.passwort1.value === document.registerbox.passwort2.value) {
                    if (document.registerbox.email.value !== 'XXX@XXX.XXX') {
                        if (document.registerbox.email.value.match(emailRegex)) {

                            //er verschlüsselt das Passwort c2=enc(pw,k_pub)
                            // Encrypt with the public key...
                            var encrypt2 = new JSEncrypt();
                            encrypt2.setPublicKey('<?=$k_pub;?>');
                            $('#inputC2').val(encrypt2.encrypt($('#loginpasswort').val()));

                            //er sendet l,c2,h2 an den Server, neue Seite wird aufgerufen, die RSA-SChlüssel gehen verloren!
                            document.registerbox.submit();
                        } else {
                            $("#emailtxt").addClass("ui-state-error");
                            $('#dialog').html("<p class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 2em;\">Sie müssen eine E-Mailadresse in der gültigen Form angeben ...<strong> @ </strong>...<strong> . </strong>... !</p>").dialog('open');
                        }
                    } else {
                        $("#emailtxt").addClass("ui-state-error");
                        $('#dialog').html("<p class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">Die E-Mailadresse darf nicht XXX@XXX.XXX lauten!</p>").dialog('open');
                        document.registerbox.email.value = '';
                    }

                } else {
                    $("#neuespw2txt").addClass("ui-state-error");
                    $('#dialog').html("<p class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">Die Passwörter müssen übereinstimmen. Geben Sie das Passwort ein weiteres Mal ein!</p>").dialog('open');
                }
            } else {
                $("#neuespw1txt").addClass("ui-state-error");
                $('#dialog').html("<p class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">Das Passwort muss mindestens bestehen aus:</p><ul><li>aus 8-15 Zeichen<li> einem Kleinbuchstaben<li>einen Großbuchstaben<li>einer Zahl<li>einem Sonderzeichen</ul>").dialog('open');
            }

        }

        $(function() {
            $("#dialog").dialog({
                modal:true,
                autoOpen: false,
                title:'Fehler',
                width: 400,
                buttons: [
                    {
                        text: "Abbrechen",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
        });
    </script>


    <form action="<?=$form_action;?>" method="post" name="registerbox">
        <table class="table table-hover" align="center">
            <tr>
                <td id="neuespw1txt">Passwort</td><td><input type="password" id='loginpasswort' name="passwort1"></td>
            </tr>
            <tr>
                <td id="neuespw2txt">Bitte wiederholen Sie das Passwort!</td><td><input type="password" name="passwort2"></td>
            </tr>
            <tr>
                <td id="emailtxt">Adresse</td>
                <td><input type="text" name="email" value="XXX@XXX.XXX" onclick="javascript:setEmailEmpty()"></td>
            </tr>
            <tr>
                <td colspan="2" class="table-info"><i class="fa fa-info-circle"></i> An den Benutzer wird eine E-Mail mit einem Link zur Bestätigung gesendet wird. Dieser wird nach 24 Stunden ungültig!</td>
            </tr>
            <tr>
                <td colspan="2"><input type="Button" onclick="javascript:sendRegistration()" value="Nutzer erstellen"></td>
            </tr>
        </table>
        <input type="hidden" value="" name="c2" id="inputC2">
    </form>
</div>
</div>
