<h4>Datenbank auswählen</h4>
<div class="alert alert-info">Wählen Sie die Datenbank aus, mit der sie sich verbinden möchten!</div>
<form action="index.php?mod=install&aktion=chooseDatabase" method="post" name="">
    <div class="form-group">
        <label for="sel1">Datenbanken</label>
        <select class="form-control" id="dbName" name="dbName">
            <?php
            /** @noinspection PhpUndefinedVariableInspection */
            print_r($datenbanken);
            foreach($datenbanken as $db){
                echo '<option>'.$db['Database'].'</option>';
            }
            ?>
        </select>
    </div>
    <button type="submit" class="lupix-btn btn-primary">Verbinden</button>
</form>


