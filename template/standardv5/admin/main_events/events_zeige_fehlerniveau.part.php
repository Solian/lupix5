<h4><?= /** @noinspection PhpUndefinedVariableInspection */
    $ueberschriftName; ?></h4>
<p class="alert"><strong>Legende:</strong> <span class="alert alert-warning fa fa-lightbulb-o"> Ungeprüfte Ereignisse</span> <span class="alert alert-success fa fa-check-circle-o"> Geprüfte Ereignisse</span> <span class="alert alert-info fa fa-clock-o"> Ereignisse mit falschem Datum (<0 ?!)</span></p>
<table class="table" width="100%">
    <tr>
        <td class=""></td>
        <td class=""><h6>Modul</h6></td>
        <td class=""><h6>(Fehler) Beschreibung</h6></td>
        <td class=""><h6>Informationen</h6></td>
        <td class=""><h6>Ort</h6></td>
        <td class=""><h6>Auslös. Nutzer</h6></td>
        <td class=""><h6>Fehlerkey</h6></td>
    </tr>


<?php
/** @noinspection PhpUndefinedVariableInspection */
foreach($fehler as $aFehler) {
        if ($aFehler["checked_datum"] == 0){

            /** @noinspection PhpUndefinedVariableInspection */
            $sChecked = '<a class="button btn-primary fa fa-lightbulb-o" href="?mod=main_events&aktion=mark_fehler_checked&key='.$aFehler["Fehlerkey"].'&niveau='.$niveau.'&unmarked=unmarked"></a>';
            $class="alert-warning   ";
        } elseif ($aFehler["checked_datum"] > 0) {
            $sChecked ='<span class="fa fa-check-square-o"></span>';
            $class="alert-success";
        }
        else{
            $sChecked = '<span class="fa fa-clock-o"></span>';
            $class="alert-info";
        }
        ?>
        <tr class="error-table-entry <?=$class;?>" style="cursor:pointer;" onclick="window.location='?mod=main_events&aktion=zeige_fehler&key=<?=$aFehler["Fehlerkey"]; ?>'">
            <td class=""><?=$sChecked;?></td>
            <td class=""><?=$aFehler["Modul"]; ?></td>
            <td class="">(<?=$aFehler["fehlernummer"]; ?>) <?=$aFehler["Beschreibung"]; ?></td>
            <td class=""><?=$aFehler["Fehlernachricht"]; ?></td>
            <td class=""><?=$aFehler["Skriptname"]; ?> <?=$aFehler["zeilennummer"]; ?></td>
            <td class=""><?=$aFehler["nutzer_id"]; ?> <?= $aFehler["nutzername"]==''?'':'('.$aFehler["nutzername"].')'; ?></td>
            <td class=""><?=$aFehler["Fehlerkey"]; ?></td>
        </tr>
        <?php
    }
    if(count($fehler)==0){
        ?>
    <tr class="">
        <td class="alert alert-success" colspan="7"><span class="fa fa-check"> Kein Ereignis gefunden.</span></td>
    </tr><?php
    }
?>
</table>
</div>
</div>
