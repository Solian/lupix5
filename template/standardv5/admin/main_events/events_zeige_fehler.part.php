<h4>Ereignis <?= /** @noinspection PhpUndefinedVariableInspection */
    $key ; ?></h4>
<table class="table table-striped">
    <tr><td class="subtitle" style="width:25%;">Ereignislevel</td><td class="body"><?= /** @noinspection PhpUndefinedVariableInspection */ $fehlerlevelName ; ?> (Level <?= /** @noinspection PhpUndefinedVariableInspection */ $fehlerlevel ; ?>)</td></tr>
    <tr><td class="subtitle" style="width:25%;">Fehlernummer</td><td class="body"><?= /** @noinspection PhpUndefinedVariableInspection */                $ereignisDaten["fehlernummer"] ; ?></td></tr>
    <tr><td class="">Auslösender Nutzer</td><td class="">(<?= $ereignisDaten["nutzer_id"] ; ?>) <?= /** @noinspection PhpUndefinedVariableInspection */
                $benutzername ; ?></td></tr>
    <tr><td class="">Zeitpunkt</td><td class=""><?= date("d. F Y H:i:s", $ereignisDaten["datum"]) ; ?></td></tr>
    <tr><td class="">Nachricht</td><td class=""><?= /** @noinspection PhpUndefinedVariableInspection */
                $nachricht ; ?></td></tr>
    <tr><td class="">Beschreibung</td><td class=""><?= /** @noinspection PhpUndefinedVariableInspection */
                $beschreibung ; ?></td></tr>
    <tr><td class="">Informationen</td><td class=""><?= $ereignisDaten['Fehlernachricht']==''?'keine':$ereignisDaten['Fehlernachricht'] ; ?></td></tr>
    <tr><td class="">Ausloesende Datei - Zeilennummer</td><td class="body"><?= $ereignisDaten["Skriptname"] ; ?> - <?= $ereignisDaten["zeilennummer"] ; ?></td></tr>
    <tr><td class="">Ereignisverfolgung</td><td class="body">
            <?php
                $traceArray = json_decode($ereignisDaten["Verfolgung"]);
                if(!is_null($traceArray)) {
                    foreach ($traceArray as $tracePoint) {
                        if (isset($tracePoint->class)) {

                            //echo '<b>' . $tracePoint->class . $tracePoint->type . $tracePoint->function . '(' . json_encode($tracePoint->args, true) . ')</b> in ' . pathinfo($tracePoint->file, PATHINFO_FILENAME) . '.' . pathinfo($tracePoint->file, PATHINFO_EXTENSION) . ' in Zeile ' . $tracePoint->line . '<br>';
                            echo '<b>' . $tracePoint->class . $tracePoint->type . $tracePoint->function . '(' . (isset($tracePoint->args) ? json_encode($tracePoint->args, true) : '') . ')</b> in ' . pathinfo($tracePoint->file, PATHINFO_FILENAME) . '.' . pathinfo($tracePoint->file, PATHINFO_EXTENSION) . ' in Zeile ' . $tracePoint->line . '<br>';

                        } else {

                            //echo '<b>' . $tracePoint->function . '(' . implode(',', $tracePoint->args) . ')</b> in ' . $tracePoint->file . ' in Zeile ' . $tracePoint->line . '<br>';
                            echo '<b>' . $tracePoint->function . '(' . (isset($tracePoint->args) ? '<pre>' . (isset($tracePoint->args) ? json_encode($tracePoint->args, true) : '') . '</pre>' : '') . ')</b> in ' . (isset($tracePoint->file) ? $tracePoint->file : 'keiner Datei') . ' in Zeile ' . (isset($tracePoint->line) ? $tracePoint->line : 'Keine') . '<br>';
                        }
                    }
                }?></td></tr>
    <tr><td class="subtitle">Ereignis geprüft?</td><td class="body">
<?php
            if ($ereignisDaten["checked_datum"] == 0) echo '<p class="hinweis"> <a class="button fa fa-info" href=""></a>Dieser Fehler ist ungeprüft. <a class="button" href="?mod=main_events&aktion=mark_fehler_checked&key='.$key.'">Von mir als geprüft markieren.</a>';
            else {
                /** @noinspection PhpUndefinedVariableInspection */
                echo '<p class="erfolg"> von ' . $checkedAdminName . ' am ' . date("d. F Y H:i:s", $ereignisDaten["checked_datum"]) . ' überprüft. <a class="button" href="?mod=main_events&aktion=mark_fehler_checked&key='.$key.'">Nochmal als geprüft markieren!</a> </p>';
            }
?>
            </td></tr>
</table>
</div>
</div>
