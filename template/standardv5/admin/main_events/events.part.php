<script src="../template/standardv5/scripts/chart.js-2.8.0/Chart.bundle.js"></script>
<?php
$uebersichtDerFehlerniveaus="";
$chartDataFehlerniveaus=[];
$chartDataFehlerniveauHaeufigkeit=[];
$chartDataUngepruefteFehlerniveauHaeufigkeit=[];
/** @noinspection PhpUndefinedVariableInspection */
foreach($fehlerniveausMitAnzahl as $aNiveau)
{
    /** @noinspection PhpUndefinedVariableInspection */
    $iRelativeHaeufigkeit= intval($aNiveau["Anzahl"])/$anzahlAllerEregnisse;
    $irelH100=floor($iRelativeHaeufigkeit*10000)/100;
    $irelHBalken=floor($iRelativeHaeufigkeit*600);
    $uebersichtDerFehlerniveaus.= ' <a href="?mod=main_events&aktion=zeige_niveau&niveau='.$aNiveau["fehlerniveau"].'" title="'.$aNiveau["Anzahl"].' Ereignisse ('.$irelH100.'%)" class="form-control lupix-btn btn-secondary">'.$aNiveau["Name"].' ('.$aNiveau["Anzahl"].')</a> ';
    //$uebersichtDerFehlerniveaus.= '<a href="?mod=main_events&aktion=zeige_niveau&niveau='.$aNiveau["fehlerniveau"].'" title="'.$aNiveau["Anzahl"].' Ereignisse"><span class="button" style="margin-right:50pt;height:20pt;width:'.$irelHBalken.'pt">'.$irelH100.'%</span></a>'.$aNiveau["Name"].'<br>';
    $chartDataFehlerniveaus[]=$aNiveau['Name'];
    $chartDataFehlerniveauHaeufigkeit[]=$aNiveau["Anzahl"];
}


$uebersichtDerFehlerniveausUngelesenerEreignisse="";

/** @noinspection PhpUndefinedVariableInspection */
foreach($ungepruefterFehlerniveausMitAnzahl as $aNiveau) {
    if ($aNiveau['Anzahl'] > 0) {
        /** @noinspection PhpUndefinedVariableInspection */
        $iRelativeHaeufigkeit = intval($aNiveau["Anzahl"]) / $anzahlUngelesenerEregnisse;
        $irelH100 = floor($iRelativeHaeufigkeit * 10000) / 100;
        $irelHBalken = floor($iRelativeHaeufigkeit * 600);
        $uebersichtDerFehlerniveausUngelesenerEreignisse .= ' <a href="?mod=main_events&aktion=zeige_niveau_unmarked&niveau='.$aNiveau["fehlerniveau"].'" title="'.$aNiveau["Anzahl"].' Ereignisse ('.$irelH100.'%)" class="lupix-btn btn-secondary form-control">'.$aNiveau["Name"].' ('.$aNiveau["Anzahl"].')</span></a> ';
    }else{
        $iRelativeHaeufigkeit = 0;
        $irelH100 = 0;
        $irelHBalken =20;
        $uebersichtDerFehlerniveausUngelesenerEreignisse.='';
    }
    //$uebersichtDerFehlerniveausUngelesenerEreignisse .= '<a  href="?mod=main_events&aktion=zeige_niveau_unmarked&niveau=' . $aNiveau["fehlerniveau"] . '" title="' . $aNiveau["Anzahl"] . ' Ereignisse"><span class="button" style="margin-right:50pt;width:' . $irelHBalken . 'pt">' . $irelH100 . '% </span></a>' . $aNiveau["Name"] . '<br>';
    $chartDataUngepruefteFehlerniveauHaeufigkeit[]=$aNiveau["Anzahl"];
}

?>
    <h4 >Überblick über die Logs</h4>
<table class="table table-striped" width="100%">
    <tr>
        <td width="25%" class="">Mindestniveau f&uuml;r einen Logeintrag<a href="" class="info" title="Dieses Fehlereinstufung dürfen die Ereignisse haben, die geloggt werden sollen. Beachten Sie, je niedriger, desto schlimmer.">?</a></td>
        <td class="body" class=""><span class="badge badge-primary badge-pill align-right"><?= /** @noinspection PhpUndefinedVariableInspection */
                $maxLogLevel;?></span></td>
    </tr>
    <?php if($anzahlUngelesenerEregnisse>0){ ?>
   <tr>
        <td class="subtitle"><?= /** @noinspection PhpUndefinedVariableInspection */
            $anzahlUngelesenerEregnisse ;?> unbearbeiteten Einträge<br>(<?= /** @noinspection PhpUndefinedVariableInspection */
            round($anzahlUngelesenerEregnisse/$anzahlAllerEregnisse*10000)/100; ?>%)</td>
        <td class="body"><div class="input-group"><?=$uebersichtDerFehlerniveausUngelesenerEreignisse ;?></div>
            <canvas id="ungepruefteFehlerChart" width="500" height="100"></canvas>
            <script>
                var ctxGesamt = document.getElementById('ungepruefteFehlerChart').getContext('2d');
                var unFehlerScatter = new Chart(ctxGesamt, {
                    type: 'bar',
                    data: {
                        labels:<?=json_encode($chartDataFehlerniveaus);?>,
                        datasets: [{
                            label: 'Ungeprüfte Fehler auf Niveaus',
                            data: <?=json_encode($chartDataUngepruefteFehlerniveauHaeufigkeit);?>,
                            backgroundColor: 'rgba(99, 132, 255,0.5)',
                            lineTension:0}
                        ]},
                    options: {}
                });
                unFehlerScatter.defaults.global.elements.point.hoverRadius=40;
            </script>
        </td>
    </tr>
    <?php } ?>
    <tr>
        <td class="subtitle"><?=$anzahlAllerEregnisse ;?> Ereignisse</p></td>
        <td class="body">
            <div class="input-group"><?php echo  $uebersichtDerFehlerniveaus ;?></div>
            <canvas id="fehlerChart" width="500" height="100"></canvas>
            <script>

                var ctxGesamt = document.getElementById('fehlerChart').getContext('2d');
                var fehlerScatter = new Chart(ctxGesamt, {
                    type: 'bar',
                    data: {
                        labels:<?=json_encode($chartDataFehlerniveaus);?>,
                        datasets: [{
                            label: 'Fehler in den Niveaus',
                            data: <?=json_encode($chartDataFehlerniveauHaeufigkeit);?>,
                            backgroundColor: 'rgba(255, 99, 132,0.5)',
                            lineTension:0},
                        ]},
                    options: {}
                });
                fehlerScatter.defaults.global.elements.point.hoverRadius=140;

            </script>
        </td>
    </tr>
</table>
</div>
</div>
