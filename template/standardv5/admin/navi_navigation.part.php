<?php
$tBasismodule="";
$tErweiterungsmodule="";
$sModulTextVars=array("tErweiterungsmodule","tBasismodule");
if(!empty($modulDaten)) {
    foreach ($modulDaten as $aModul) {
        ${$sModulTextVars[$aModul["opt_main_modul"]]} .= "<li class=\"nav-item\"><a class=\"nav-link\" href=\"index.php?mod={$aModul["Modulkennung"]}\">{$aModul["Name"]}</a></li>\n";
    }
    ?>
    <ul class="navbar-nav">
    <li class="nav-item"><a class="nav-link" href="index.php?mod=start">Überblick</a></li>
    <?= $tBasismodule; ?>
    <li><a class="nav-link" href="#">|</a></li>
    <?= $tErweiterungsmodule; ?>
    <li><a class="nav-link" href="#">|</a></li>
    <li class="nav-item"><a class="nav-link" href="index.php?aktion=logout">
            <i class="fa fa-sign-out" aria-hidden="true"></i>
            Logout</a></li>
    </ul>
    <?php

}else{
    echo '<ul class="navbar-nav">
    <li class="nav-item"><a class="nav-link" href="index.php?mod=start">Lupix Administration</a></li>
    </ul>';
}

