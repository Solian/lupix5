<?php

$sAction=$sAction;
$formAktionVar=$formAktionVar;
$tSpaltentitle= $tSpaltentitle;
$SeitenbaumSpaltentitle= $SeitenbaumSpaltentitle;



//Wenn ein Textbearbeitet wird, muss die TextId mit gegeben werden zum Speichern, sonst müsste man sie quälend immer mitladen.
if(isset($iTextId)){$placeHolderForTextIdSnippet='<input type="hidden" name="textId" value="'.$iTextId.'"/>';}
//Wennd dagegen ein Text erstelllt wird, hat man sie noch nicht. Daher braucht man sie dann auch noch nicht.
else{ $placeHolderForTextIdSnippet='';}

//Wenn ein Text erzeugt wird, gibt es noch keinen Titel
if(!isset($textTitel))$textTitel='';
if(!isset($textInhalt))$textInhalt='';
if(!isset($tSeitendaten))$tSeitendaten='';
if(!isset($textMeldung))$textMeldung='';

/** @noinspection PhpUndefinedVariableInspection */
/** @noinspection PhpUndefinedVariableInspection */
$tSeitendaten = <<<LOL
$tSeitendaten
$textMeldung 
<form id="textEditor" action="$sAction" method="POST">
    <table style="width:720px;">
    
        
        <tr><td colspan="4" >
            <div class="input-group mb-3">
                 <div class="input-group-prepend">
                    <span class="input-group-text">Titel</span>
                    </div>
                <input type="text" style="min-width: 60%;"  class="form-control"  name="textTitel" value="$textTitel"/>
                <input type="text" class="lupix-btn btn-success form-control" onclick="seitenNewPageSaveContentAndSubmit();" value="Speichern" />
                <input type="text" class="lupix-btn btn-warning form-control" onclick="window.location='?mod=main_seiten&aktion=showPage&id=$id';" value="Abbrechen">
            </div>
        </td></tr>
        <tr><td colspan="4" >
LOL;
                //htmc_textfeld(&$klasse,$feldname,$feldinhalt="",$aboptionen="",$pfad_zu_scripts_ordner,$rows="20",$cols="95",$feldid="textfeld")
                //$tSeitendaten .= htmc_textfeld($admin,"text","","","../",50,130);
/** @noinspection PhpUndefinedVariableInspection */
$tSeitendaten .=<<<LOL
                <script src="../template/standardv5/scripts/tinymce/tinymce.min.js"></script>
                <script>
                    function seitenNewPageSaveContentAndSubmit(){
                            if(tinymce.activeEditor.isDirty()){
                                //Speichere den Inhalt aus dem div als Text im input
                                $('#text').val(escapeHtml($('#textfeld').html()));
                                //Schicke das Formular ab
                                $('#textEditor').submit();
                            }else{
                                alert('Bitte ändern Sie den Text, bevor sie ihn speichern. Wenn sie den Text nicht speichern wollen, dann brechen Sie ab!');
                            }
                        }
                        
                    $(function(){
                        tinymce.init({
                            selector: '#textfeld',
                            plugins:"lists link image table code",
                            branding: false,
                            toolbar: 'undo redo | formatselect | forecolor backcolor | bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist |link image |table | code ',
                            menubar:'file edit insert view format table tools help',
                            inline:true,
                            extended_valid_elements :'script[type=text/javascript]'
                        });
                        
                    });
                </script>
                <div id="textfeld" style="border:1px dotted black;">$textInhalt</div>
                <input id="text" name="text" type="hidden" cols="130" rows="50">

            </td></tr><tr><td  colspan="3">&nbsp;</td>
            </tr><tr>
            <td>
             <div class="input-group mb-3">
                <input type="text" class="lupix-btn btn-success form-control" onclick="seitenNewPageSaveContentAndSubmit();" value="Speichern" />
                <input type="text" class="lupix-btn btn-warning form-control" onclick="window.location='?mod=main_seiten&aktion=showPage&id=$id';" value="Abbrechen">
            </div>
            <input type="hidden" name="mod" value="main_seiten">
                <input type="hidden" name="id" value="$id">
                <input type="hidden" name="label" value="$label">
                <input type="hidden" name="aktion" value="$formAktionVar">
                $placeHolderForTextIdSnippet
                </td>
        </tr>
    </table>
</form>
<div id="textEditorMeldung"></div>
LOL;

include('seiten.part.php');