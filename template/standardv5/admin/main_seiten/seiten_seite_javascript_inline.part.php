<script>
    $(function() {
        $('#dialog').dialog(
            {
                modal: false,
                buttons: {
                    'Abbrechen': function () {
                        $(this).dialog("close");
                        }
                    },
                autoOpen: false
            }
        );

        $('[data-toggle="tooltip"]').tooltip();

    });

    function saveTemplateString(templateLabel,setId,setNumber){
        if(setId!==null && setNumber!==null){
            var url='index.php?mod=main_seiten&aktion=saveSETStringInline&id=<?=$id;?>&setId='+parseInt(setId)+'&setNumber='+parseInt(setNumber)+'&label=' + templateLabel + '&value=' + encodeURIComponent($('#' + templateLabel + '_' + setId + '_' + setNumber + 'StringValue').val());
            window.location = url;
        }else {
            window.location = 'index.php?mod=main_seiten&aktion=saveTemplateStringInline&id=<?=$id;?>&label=' + templateLabel + '&value=' + encodeURIComponent($('#' + templateLabel + '__StringValue').val());
        }
    }

    function choosePageController(templateLabel){

        $('#dialog').dialog('option','title','Wählen Sie den Controller aus').html('<ul><?php

            foreach ($aController as $sSkript => $sSkriptname) {
                echo "<li><a target=\"_top\" data-toggle=\"tooltip\"  data-placement=\"left\"  title=\"$sSkriptname\" href=\"?mod=main_seiten&aktion=saveController&id=" . $id . "&label='+templateLabel+'&controller=$sSkriptname\">$sSkript</a></li>";
            }
            ?></ul>').dialog('open');
        $('[data-toggle="tooltip"]').tooltip();
    }

    function savePageController(id,label,controller,action){
        window.location='index.php?mod=main_seiten&aktion=saveControllerInline&id='+id+'&label='+label+'&controller='+controller+'&action='+action;
    }

    function createSeitenElementText(templateLabel){

    }

</script>