<script>

function saveTemplateString(templateLabel,setId,setNumber){
        window.location = 'index.php?mod=main_seiten&aktion=saveTemplateString&id=<?=$id;?>&label=' + templateLabel + '&value=' + encodeURIComponent($('#' + templateLabel + '__StringValue').val());

}

    function choosePageController(templateLabel){
        var dialog = $('#dialog');
        dialog.attr('title','Wählen Sie den Controller aus').html('<ul><?php

            foreach ($aController as $sSkript => $sSkriptname) {
                echo "<li><a data-toggle=\"tooltip\"  data-placement=\"left\"  title=\"$sSkriptname\" href=\"?mod=main_seiten&aktion=saveController&id=" . $id . "&label='+templateLabel+'&controller=$sSkriptname\">$sSkript</a></li>";
            }
            ?></ul>').dialog('open');

        $('[data-toggle="tooltip"]').tooltip();
    }

    function savePageController(id,label,controller,action){
        window.location='index.php?mod=main_seiten&aktion=saveController&id='+id+'&label='+label+'&controller='+controller+'&action='+action;
    }



    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();

        var dialog = $('#dialog');
        dialog.dialog(
            {
                modal:true,
                autoOpen:false,
                buttons: {
                    'Abbrechen': function() {
                        $( this ).dialog( "close" );
                    }
                }
            }
        );
    });

</script>
<script>

    var sidebarWrapper = $('#sidebar-wrapper');
    var sidebuttonWrapper = $('#sidebutton-wrapper');

    $('#main_seiten_edit_page_close_button').click(function(){
        sidebarWrapper.animate({opacity:0,'margin-left':"-=250pt"},500,function() {sidebarWrapper.toggle();});
        sidebuttonWrapper.toggle();
        sidebuttonWrapper.animate({opacity:1,'margin-left':"+=250pt"},500);
    });

    sidebuttonWrapper.click(function(){
        sidebuttonWrapper.animate({opacity:0,'margin-left':"-=250pt"},500,function() {sidebuttonWrapper.toggle();});
        sidebarWrapper.toggle();
        sidebarWrapper.animate({opacity:1,'margin-left':"+=250pt"},500);

    });

    var menubarWrapper = $('#menubar-wrapper');
    var menubuttonWrapper = $('#menubutton-wrapper');

    $('#main_seiten_edit_properties_close_button').click(function(){
        menubarWrapper.animate({opacity:0,'margin-right':"-=350pt"},500,function() {menubarWrapper.toggle();});
        menubuttonWrapper.toggle();
        menubuttonWrapper.animate({opacity:1,'margin-right':"+=350pt"},500);
    });

    menubuttonWrapper.click(function(){
        menubuttonWrapper.animate({opacity:0,'margin-right':"-=350pt"},500,function() {menubuttonWrapper.toggle();});
        menubarWrapper.toggle();
        menubarWrapper.animate({opacity:1,'margin-right':"+=350pt"},500);

    });

    var topbarWrapper = $('#topbar-wrapper');
    var topbuttonWrapper = $('#topbutton-wrapper');

    $('#main_seiten_edit_navi_close_button').click(function(){
        topbarWrapper.animate({opacity:0,'margin-top':"-=100pt"},500,function() {topbarWrapper.toggle();});
        topbuttonWrapper.toggle();
        topbuttonWrapper.animate({opacity:1,'margin-top':"+=100pt"},500);
    });

    topbuttonWrapper.click(function(){
        topbuttonWrapper.animate({opacity:0,'margin-top':"-=100pt"},500,function() {topbuttonWrapper.toggle();});
        topbarWrapper.toggle();
        topbarWrapper.animate({opacity:1,'margin-top':"+=100pt"},500);

    });



</script>



