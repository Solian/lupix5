<?php


/**
* The function converts the array into an string formatted with treeJS Markup
* @param array $pageTree
* @return string
*/
/**
* @param array $pageTree
* @return string
*/
function createPageTreeListFromArrayViewHelper(array $pageTree){
$html='<ul>';

    foreach($pageTree as $nameDotDotID => $childPagesArray) {
        list($name, $id) = explode(':', $nameDotDotID);

        if ($id == 0) {
            $html .= '<li data-jstree=\'{"icon":"fa fa-globe"}\' id=\'' . $id . '\'>' . $name;
        } else {
            $html .= '<li data-jstree=\'{"icon":"fa fa-file-text"}\' id=\'' . $id . '\'>' . $name;
        }

        if (!empty($childPagesArray)) {
            $html .= createPageTreeListFromArrayViewHelper($childPagesArray);
        }
        $html .= '</li>';
    }

    $html.='</ul>';
return $html;
}

$tSeitenbaum = '<div id="pageTree">'.createPageTreeListFromArrayViewHelper($aSeitenbaum).'</div>';
