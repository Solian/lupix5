<?php

$aSeitenbaum =$aSeitenbaum;
$tSpaltentitle =$tSpaltentitle;
$tSpaltentitle = $tSpaltentitle;
$tSeitendaten=$tSeitendaten;
$SeitenbaumSpaltentitle=$SeitenbaumSpaltentitle;
include('seiten_seitenbaum_generator.part.php');

$tSeitenbaum='<div class="alert alert-info"><ul class="mb-0"><li>Wählen Sie eine Seite aus!</li><li>Verschieben Sie die Seiten!</li></ul></div>'.$tSeitenbaum;

##  Hauptanzeite wird geladen
##

include('seiten.part.php');


##  TreeJS wird initialisiert
##

?>
<script>
    $(function () {
        treeview = $('#pageTree');

        treeview
            .jstree({"core":{"multiple" : false,"check_callback" : true,'dnd':{'inside_pos':'first'}},plugins:['dnd']})
            .jstree('open_all')
            .on('changed.jstree', function (e, data) {
                window.location='index.php?mod=main_seiten&aktion=showPage&id='+encodeURI(data.instance.get_node(data.selected[0]).id);
            })
            .on('move_node.jstree',function(e,data){
                if(data.parent==='#'){
                    window.location='index.php?mod=main_seiten';
                }else {
                    window.location = 'index.php?mod=main_seiten&aktion=setUeberSeite&id=' + data.node.id + '&ueberId=' + data.parent + '&neuPos=' + data.position + '&callBackAction=Standard';
                }
        });
    });


</script>
