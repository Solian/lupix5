<?php

$aSeitenbaum =$aSeitenbaum;
$tSpaltentitle =$tSpaltentitle;
$tSpaltentitle = $tSpaltentitle;
$tSeitendaten=$tSeitendaten;
$SeitenbaumSpaltentitle=$SeitenbaumSpaltentitle;

include('seiten_seitenbaum_generator.part.php');

echo '<a class="button" href="?aktion=newPageChooseHook&mod=main_seiten" title="Neue Seite"><i class="fa fa-file-o"></i></a><hr>';

echo $tSeitenbaum;


##  TreeJS wird initialisiert
##

?>
<script>
    $(function () {
        treeview = $('#pageTree');

        treeview
            .jstree({"core":{"multiple" : false,"check_callback" : true,'dnd':{'inside_pos':'first'}},plugins:['dnd']})
            .jstree('open_all')
            .on('changed.jstree', function (e, data) {
                window.location='index.php?mod=main_seiten&aktion=showPage&id='+encodeURI(data.instance.get_node(data.selected[0]).id);
            })
            .on('move_node.jstree',function(e,data){
                if(data.parent==='#'){
                    window.location='index.php?mod=main_seiten&aktion=showPage&id=<?=$id?>';
                }else{
                    window.location='index.php?mod=main_seiten&aktion=setUeberSeite&id='+data.node.id+'&ueberId='+data.parent+'&neuPos='+data.position;
                }
            });
    });


</script>

<hr>

<table class="table table-sm">
    <tr><td></td><td><i class="fa fa-eye"></i></td><td><i class="fa fa-edit"></i></td></tr>
    </tr>
<?php

    foreach($gruppenSeitenRechte as $gruppenId =>$gruppenRecht){
        if($gruppenId!=1) {
            echo '<tr>
<td class="small">' . $gruppenRecht['Name'] . '</td>
<td>
<script>
function setLesen'.$id.'_'.$gruppenId.'_'.$gruppenRecht['lesen'].'(){
     window.location=\'index.php?mod=main_seiten&aktion=setPagePermission&id='.$id.'&groupId='.$gruppenId.'&lesen=' . ($gruppenRecht['lesen'] == 1 ? '0' : '1') .'&aendern='.$gruppenRecht['aendern'].'\';
}
</script>
<div class="custom-control custom-switch">
    <input onclick="setLesen'.$id.'_'.$gruppenId.'_'.$gruppenRecht['lesen'].'()" type="checkbox" class="custom-control-input" id="lesen'.$id.'_'.$gruppenId.'_'.$gruppenRecht['lesen'].'" ' . ($gruppenRecht['lesen'] == 1 ? 'checked' : '') . '>
    <label class="custom-control-label" for="lesen'.$id.'_'.$gruppenId.'_'.$gruppenRecht['lesen'].'"></label>
  </div></td>
<td>
<script>
function setAendern'.$id.'_'.$gruppenId.'_'.$gruppenRecht['aendern'].'(){
     window.location=\'index.php?mod=main_seiten&aktion=setPagePermission&id='.$id.'&groupId='.$gruppenId.'&aendern=' . ($gruppenRecht['aendern'] == 1 ? '0' : '1') .'&lesen='.$gruppenRecht['lesen'].'\';
}
</script>
<div class="custom-control custom-switch">
    <input onclick="setAendern'.$id.'_'.$gruppenId.'_'.$gruppenRecht['aendern'].'()" type="checkbox" class="custom-control-input" id="aendern'.$id.'_'.$gruppenId.'_'.$gruppenRecht['aendern'].'" ' . ($gruppenRecht['aendern'] == 1 ? 'checked' : '') . '>
    <label class="custom-control-label" for="aendern'.$id.'_'.$gruppenId.'_'.$gruppenRecht['aendern'].'"></label>
  </div></td></tr>';
        }
    }

    ?>
</table>