    <ul class="nav nav-tabs nav-justified">
        <li class="nav-item">
            <a href="?mod=main_seiten" class="nav-link <?= /** @var string $navFlag */ $navFlag==='uebersicht'?'active':''; ?>">Übersicht</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link <?= /** @var string $navFlag */ $navFlag==='seite'?'active':''; ?>"><i class="fa fa-file-text"></i>&nbsp;&nbsp;<?= /** @var string $SeitenTitel */ $navFlag==='seite'?$SeitenTitel:'Lade Seite'; ?></a>
        </li>
        <li class="nav-item">
            <a href="?mod=main_seiten&aktion=newPageChooseHook" class="nav-link <?= /** @var string $navFlag */ $navFlag==='neueSeite'?'active':''; ?>"><i class="fa fa-file-o"></i>&nbsp;&nbsp;Neue Seite</a>
        </li>
    </ul>
    <?php
        /** @var string $nachricht */
        if($nachricht!==''){
        ?>
        <script>
            $(function(){
                $('.toast').html('<div class="toast-header">\n' +
                    '        Seiten aktualisiert!\n' +
                    '    </div>\n' +
                    '    <div class="toast-body">\n' +
                    '        <?=$nachricht;?>\n' +
                    '    </div>').toast({delay:5000}).toast('show');
            });
        </script>
    <?php
    }?>
