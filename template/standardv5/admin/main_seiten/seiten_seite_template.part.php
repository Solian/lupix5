<?php

$aSeitenbaum =$aSeitenbaum;
$tSpaltentitle =$tSpaltentitle;
$tSpaltentitle = $tSpaltentitle;
$tSeitendaten=$tSeitendaten;
$SeitenbaumSpaltentitle=$SeitenbaumSpaltentitle;



/** @noinspection PhpUndefinedVariableInspection */
$tSeitendaten =<<<LOL

        <form action="$sAction" method="POST">
		<table style="width:720px;margin-left:10pt;">
		<tr><td class=""><b>Template</b></td><td class="" colspan="3"><input type="text" id="templateFile" name="templateFile" size="40" value="$oldTemplateFile"></td></tr>
		<tr>
		<td></td><td class="">
		    <p class="alert alert-warning fa fa-warning"> Wenn Sie ein neues Template speichern, werden alle aktuellen Daten verworfen.<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gelöschte Daten können nicht mehr wiederhergestellt werden.</p>
		</td></tr><tr>
		<td></td><td class=""><input type="hidden" name="id" value="$id">
		            <input type="hidden" name="aktion" value="pageSetContentType">
		            <input type="hidden" name="mod" value="{$modul}">
		            <input type="Submit" value="Speichern" /></td>
		</tr>
		</table>
		</form>
		<hr>
		<h2>Template-Vorschau</h2>
    <iframe id="vorschau" src="{$iFramePreviewLink}" style="width:100%;height:500pt;"></iframe>
<br>
<br>
LOL;

/**
 * The function converts the array into an string formatted with treeJS Markup
 * @param array $pageTree
 * @return string
 */

function createPageTreeListFromArrayViewHelper(array $pageTree){
    $html='<ul>';

    foreach($pageTree as $name => $fileOrFolderArray){
        if(is_array($fileOrFolderArray)){
            $html.='<li data-jstree=\'{"icon":"fa fa-folder"}\'>'.$name.createPageTreeListFromArrayViewHelper($fileOrFolderArray).'</li>';
        }else{
            $html.='<li data-jstree=\'{"icon":"fa fa-pagelines"}\'>'.$name.'</li>';
        }
    }

    $html.='</ul>';
    return $html;
}
/** @var array $aTemplateDateien */
$tSeitenbaum = '<div id="pageTree">'.createPageTreeListFromArrayViewHelper($aTemplateDateien).'</div>';



##  Hauptanzeite wird geladen
##

include('seiten.part.php');


##  TreeJS wird initialisiert
##

?>
<script>
    $(function () { $('#pageTree')
        .on('changed.jstree', function (e, data) {
            if(!data.instance.is_parent(data.selected[0])){
                var path = data.instance.get_path(data.selected[0],"/");
                $('#templateFile').val(path);
                $('#vorschau').attr('src','index.php?mod=main_seiten&aktion=show_template&file='+encodeURI(path))
                //

            }else{
                $('#pageTree').jstree().open_node(data.selected[0],500);
            }
    })
        .jstree(); });

</script>
