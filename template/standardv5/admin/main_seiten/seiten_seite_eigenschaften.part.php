<div class="sidebar-header">
    <h5><i class="fa fa-chevron-circle-right" id="main_seiten_edit_properties_close_button"></i> Eigenschaften
        <a href="?mod=main_seiten&aktion=showPageSettingsList&id=<?=$seitenId;?>" class="lupix-btn"><i class="fa fa-list-ul"></i></a></h5>
</div>


<hr>
<script>
    function inherit_template_from_file(id,fromId,fromTitle){
        $('#dialog').html('Wollen Sie wirklich den Template-Datensatz von der Seite <strong>'+fromTitle+'</strong> übernehmen. Die Änderung kann nicht mehr rückgängig gemacht werden!');
        $('#dialog').dialog(
            {
                title:'Template übernehmen',
                buttons:
                {
                    OK: function(){
                        window.location='?mod=main_seiten&aktion=inheritTemplate&id='+id+'&fromId='+fromId;
                    },
                    'Abbruch':function(){
                        $( this ).dialog( "close" );
                    }
                }
            }
        ).dialog('open');
    }
</script>
<?php

$dateienMitTemplate='';
foreach($sameTemplateFiles as $seite){
    if($seite['id']!==$seitenId)$dateienMitTemplate.='<li><a class="dropdown-item" onclick="inherit_template_from_file('.$seitenId.','.$seite['id'].',\''.$seite['Name'].'\')" class="">'.$seite['Name'].'</a></li> ';
}

?>

<form action="" method="POST"  width="100%">


    <div class="input-group mt-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Name</span>
        </div>
        <input type="text" class="form-control" id="Name" name="Name" value="<?=$seitenName;?>" aria-describedby="seitenNameHelp">
    </div>
    <small id="seitenNameHelp" class="form-text text-muted">für Kurz-Navigation (Breadcrumb...) und die Browser-Titel.</small>



    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">Titel</span>
        </div>
        <input type="text" class="form-control" id="Titel" name="Titel" value="<?=$seitenTitel;?>" aria-describedby="seitenTitelHelp">
    </div>
    <small id="seitenTitelHelp" class="form-text text-muted">für die Navigation und Überschriften</small>

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">Template</span>
        </div>
        <a class="lupix-btn form-control" href="?mod=main_seiten&aktion=changeTemplateForm&id=<?=$seitenId;?>">Ändern</a>
        <ul class="dropdown-menu">
            <?=$dateienMitTemplate;?>
        </ul>
        <button class="lupix-button dropdown-toggle form-control pr-5" type="button" data-toggle="dropdown">
            Erben
        </button>
    </div>
    <small id="seitenTitelHelp" class="form-text text-muted">Ändert die Templatevorlage oder erbt den Inhalt einer anderen Seite</small>

    <div class="input-group mt-2">
        <div class="input-group-prepend">
            <span class="input-group-text">Sichtbarkeit</span>
        </div>
        <div class="custom-control custom-switch form-control pt-2 pl-5">
            <?=$verbergen;?>
            <label class="custom-control-label" for="opt_verbergen">unsichtbar</label>
        </div>
    </div>
    <small class="form-text text-muted">Unsichtbare Seiten werden im Menü verborgen, man wird zur übergeordneten Seite umgeleitet.</small>

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">Admin-Schutz</span>
        </div>
        <div class="custom-control custom-switch form-control pt-2 pl-5">
            <?=$admin_edit;?>
            <label class="custom-control-label" for="opt_edit_nur_admin">geschützt</label>
        </div>
    </div>
    <small class="form-text text-muted">Nur Administrator dürfen die Seite ändern.</small>

    <input type="hidden" name="id" value="<?=$seitenId;?>"><input type="hidden" name="aktion" value="set_seiten_daten">
    <div class="input-group">
        <input type="submit" class="form-control" value="Speichern" />
        <a class="lupix-btn form-control" href="?mod=main_seiten&aktion=seite_loeschen&id=<?=$seitenId;?>">Löschen <i class="fa fa-times-circle-o"></i></a>
    </div>
</form>
<hr>
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Oberseite</span>
    </div>
    <input class="form-control" disabled value="<?=$ueberSeiteName;?>(<?=$ueberSeiteId;?>)"><a class="lupix-btn form-control" href="?mod=main_seiten"><i class="fa fa-edit"></i></a>
</div>
<hr>
<h5>Verborgene Felder</h5>
<ul>
<?php
foreach($verborgeneTemplateFelder as $label =>$templateEntry){
    echo '<li>'.substr($label,0,-1).' '.$templateEntry->value;
}?>
</ul>
