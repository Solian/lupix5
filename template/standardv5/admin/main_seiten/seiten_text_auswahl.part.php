<?php /** @noinspection PhpUndefinedVariableInspection */
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.12.19
 * Time: 10:06
 */

$sAction=$sAction;
$formAktionVar=$formAktionVar;
$tSpaltentitle= $tSpaltentitle;
$SeitenbaumSpaltentitle= $SeitenbaumSpaltentitle;

$tSeitenbaum='<ul class="list-group">';
/* @noinspection PhpUndefinedVariableInspection */
foreach($aAlleTexte as $aEinTexte)
{

    /* @noinspection PhpUndefinedVariableInspection */
    $tSeitenbaum.= '<li class="list-group-item"><a href="?mod=main_seiten&aktion=saveTextToPageLabel&textId='.$aEinTexte["id"].'&id='.$id.'&label='.$label.'">('.$aEinTexte["id"].') '.$aEinTexte["Titel"].'</a></li>';
}
$tSeitenbaum.='</ul>';


$tSeitendaten .= <<<LOL
					<div class="alert alert-info">
					<p>Wählen sie aus:</p>
					<ul><li>Wählen Sie <strong>rechts</strong> einen Text aus, der auf dieser Seite im Feld '$label' angezeigt werden soll,</li>
					<li>oder schreiben Sie <strong>unten</strong> einen neuen Text für das Feld '$label' </li>
					</ul>
					</div>
					<h3>Neuer Text</h3>

LOL;

include('seiten_text_editor.part.php');

//include('seiten.part.php');
