<script src="../template/standardv5/scripts/tinymce/tinymce.min.js"></script>
<script>

    function seitenNewPageCheckContentAndSubmit<?=$label.'_'.$setId.'_'.$setNumber;?>() {
        if (tinymce.EditorManager.get('textfeld<?=$label.'_'.$setId.'_'.$setNumber;?>').isDirty()) {
            seitenNewPageSaveContentAndSubmit<?=$label.'_'.$setId.'_'.$setNumber;?>();
        } else {
            $('#dialog').dialog().html('Bitte ändern Sie den Text, bevor sie ihn speichern. Wenn sie den Text nicht speichern wollen, dann brechen Sie ab!').dialog('open');
        }
    }

    function seitenNewPageSaveContentAndSubmit<?=$label.'_'.$setId.'_'.$setNumber;?>() {
        //$('#text<?=$label.'_'.$setId.'_'.$setNumber;?>').val(escapeHtml($('#textfeld<?=$label.'_'.$setId.'_'.$setNumber;?>').html()));
        $('#text<?=$label.'_'.$setId.'_'.$setNumber;?>').val($('#textfeld<?=$label.'_'.$setId.'_'.$setNumber;?>').html());
        $('#textEditor<?=$label.'_'.$setId.'_'.$setNumber;?>').submit();
    }

    $(function() {
        tinymce.init({
            selector: '#textfeld<?=$label.'_'.$setId.'_'.$setNumber;?>',
            plugins: "lists link image table code save",
            branding: false,
            toolbar: 'undo redo | save | formatselect | forecolor backcolor | bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist |link image |table | code ',
            menubar: false,
            inline: <?=$inline;?>,
            extended_valid_elements: 'script[type=text/javascript]',
            save_enablewhendirty: true,
            save_onsavecallback: function () { seitenNewPageSaveContentAndSubmit<?=$label.'_'.$setId.'_'.$setNumber;?>(); },

        });

    });


</script>
<div id="textfeld<?=$label.'_'.$setId.'_'.$setNumber;?>" style="border:1px dotted black;"><?=$textInhalt;?></div>
<form id="textEditor<?=$label.'_'.$setId.'_'.$setNumber;?>" action="" method="POST">

<input id="text<?=$label.'_'.$setId.'_'.$setNumber;?>" name="text" type="hidden">
<input type="hidden" name="mod" value="main_seiten">
<input type="hidden" name="id" value="<?=$id;?>">
<input type="hidden" name="label" value="<?=$label;?>">
<input type="hidden" name="aktion" value="<?=$formAktionVar;?>">
<input type="hidden" name="textId" value="<?=$textId;?>">
<input type="hidden" name="textTitel" value="<?=$textTitel; ?>"/>
</form>
<div id="textEditorMeldung"></div>
