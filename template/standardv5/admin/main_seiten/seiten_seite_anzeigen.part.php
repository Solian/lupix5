<?php
/**
 * Created by PhpStorm.
 * User: Solian
 * Date: 27.11.19
 * Time: 07:07
 */

$aSeitenbaum =$aSeitenbaum;
$tSpaltentitle =$tSpaltentitle;
$tSpaltentitle = $tSpaltentitle;
$tSeitendaten=$tSeitendaten;
$SeitenbaumSpaltentitle=$SeitenbaumSpaltentitle;
/**
 * @noinspection PhpUndefinedVariableInspection
 * @var $template template
 * @var  $feldName string
 * @var  $feldDaten templateEntry
 */


##########################################################
##
##  Seitenbaum generieren
##
##########################################################

include('seiten_seitenbaum_generator.part.php');




##########################################################
##
##  Spaltentitel
##
##########################################################

$tSpaltentitle="(".$aSeitendaten["id"].") ".$aSeitendaten["Name"];




##########################################################
##
##  Seitenoptionen
##
##########################################################


$cachen=checkbox_from_bool("opt_cachen",$aSeitendaten["opt_cachen"],"cachen<a href=\"\" title=\"Ist dies aktiviert, wird die Seite gecached und bei jedem Auruf nicht neu kompiliert sondern aus dem Cache geladen.\" class=\"form-control\">?</a>");

if($userGroupId==1)
{
    $verbergen=checkbox_from_bool("opt_verbergen",$aSeitendaten["opt_verbergen"],"verbergen<a href=\"\" title=\"Ist dies aktiviert, wie die Seite im Menü verborgen. Bei einem Aufruf wird man zur drüberliegenden Seite umgeleitet.\"  class=\"info\">?</a>");
    $admin_edit=checkbox_from_bool("opt_edit_nur_admin",$aSeitendaten["opt_edit_nur_admin"],"sch&uuml;tzen<a href=\"\" title=\"Ist dies aktiviert, kann die Seite nur durche einen Administrator geändert werden. Auch dann, wenn ein anderer Nutzer die Berechtigung besitzt.\"  class=\"info\">?</a>");
}else{
    $verbergen='';
    $admin_edit='';
}







## Javascript für die Änderung eines Strings

$tTemplateFelder='<script>
    function saveTemplateString(templateLabel){
        window.location=\'index.php?mod=main_seiten&aktion=saveTemplateString&id='.$aSeitendaten["id"].'&label=\'+templateLabel+\'&value=\'+encodeURIComponent($(\'#\'+templateLabel+\'StringValue\').val());
    }';

## Javascript für die Auswahl der Controller

$tTemplateFelder.='    
    function choosePageController(templateLabel){
    var dialog = $(\'#dialog\');
    dialog.attr(\'title\',\'Wählen Sie den Controller aus\').html(\'<ul>';

foreach ($aController as $sSkript=> $sSkriptname) {
    $tTemplateFelder .= "<li><a href=\"?mod=main_seiten&aktion=saveController&id=".$aSeitendaten["id"]."&label='+templateLabel+'&controller=$sSkriptname\">$sSkript</a></li>";
}

$tTemplateFelder.='</ul>\');
    dialog.dialog(
        {
            modal:true,buttons: {
                \'Abbrechen\': function() {
                    $( this ).dialog( "close" );
                }
            }
        }
    );
}
    
</script>
';




//Templatefelder darstellen und Änderungsbuttons generieren

$tTemplateFelder.=<<<Table
    <table class=" table table-striped">
        <thead>
          <tr>
            <th>Feld</th>
            <th>Typ</th>
            <th>Wert</th>
          </tr>
        </thead>
        <tbody>
Table;

//Lade für jedes Template-Feld die Optionen und Einstellungen
foreach($template->entries as $feldName => $feldEintrag) {
    //Nur einen Button anzeigen, wenn es nciht Name und Titel sind. Die werden zusätzlich automatisch generiert und können immer verwendet werden
    if($feldName !=='pageName' && $feldName!=='pageTitle') {

        //Zeilenanfang
        $tTemplateFelder .='<tr>
            <td>' . $feldName . '</td>
            <td>' . $changeTypeDropdownButton[$feldName] . '</td>';

        //Fallunterscheidung je nach Typ
        if($feldEintrag->type=='string')
        {
            $tTemplateFelder .= '<td>
        <div class="input-group mb-3">
            <input type="text" id="'.$feldName.'StringValue" value="' . $feldEintrag->value . '" class="form-control" />
            <input type="button" class="lupix-btn btn-secondary form-control" value="Speichern" onclick="saveTemplateString(\''.$feldName.'\')">
        </div></td>';
        }
        elseif($feldEintrag->type=='text')
        {
           $tTemplateFelder .= '<td>
 <div class="input-group mb-3">
    <input type="text" class="form-control" disabled value="' . $templateTextTitel[$feldEintrag->value] . ' ('.$feldEintrag->value.')"><input type="button" class="lupix-btn btn-secondary form-control" value="Editieren" onclick="window.location=\'?mod=main_seiten&aktion=edit_text&id='.$aSeitendaten["id"].'&tid='.$feldEintrag->value.'\'"></td>';
        }
        elseif($feldEintrag->type=='controller')
        {
            $tTemplateFelder .= '<td class="">
            <div class="input-group mb-3">
            <input type="text" id="'.$feldName.'StringValue" value="' . $feldEintrag->value . '" disabled class="form-control" />
                <div class="dropdown d-inline">
                <button type="button" class="lupix-btn btn-secondary dropdown-toggle" data-toggle="dropdown">'.($feldEintrag->action !== '' ? $feldEintrag->action : 'Standard').'</button>
                <div class="dropdown-menu">';
            foreach($templateControllerActions[$feldName] as $aktion){
                if($aktion==''){
                    $aktionsName = 'Standard';
                }elseif($aktion=='%') {
                    $aktionsName='Seitenaktion (%)';
                }else{
                        $aktionsName=$aktion;
                }
                $tTemplateFelder.='<a class="dropdown-item" href="?mod=main_seiten&aktion=saveController&id='.$aSeitendaten["id"].'&label='.$feldName.'&controller='.$feldEintrag->value.'&action='.$aktion.'">'.$aktionsName.'</a>';
            }

            $tTemplateFelder .= '</div></div></div></td>';
        }

        //Zeilenende
        $tTemplateFelder.='</tr>';
    }
}
$tTemplateFelder.='</tbody></table>';

$dateienMitTemplate='';

foreach($sameTemplateFiles as $seite){
    if($seite['id']!==$aSeitendaten['id'])$dateienMitTemplate.='<li><a class="dropdown-item" onclick="inherit_template_from_file('.$aSeitendaten['id'].','.$seite['id'].',\''.$seite['Name'].'\')" class="">'.$seite['Name'].'</a></li> ';
}

##########################################################
##
##  Haupttemplate generieren
##
##########################################################

// Länge des Titels optimal anpassen
$iCountTitel = strlen($aSeitendaten["Titel"])*2;


//Das Haupttemplate wird in den Seitendaten untergebracht
$tSeitendaten = <<<LOL
                
                <div id="ueber_seite_aendern" style="display:none">
                <table class="" style=""><tr><td class="body">
                    <p class="">1. Skript oder Text</p>
                    <p class="">
                            <input type="radio" name="opt_inhaltstyp" value="none" checked>nichts
                            <input type="radio" name="opt_inhaltstyp" value="skript" onclick="window.location='?mod=main_seiten&aktion=changePageController&id={$aSeitendaten["id"]}'">Skript
                            <input type="radio" name="opt_inhaltstyp" value="text"  onclick="document.getElementById('container_verlinken').style.display='block';document.getElementById('seite_verlinken').style.display='none';">Text
                        </p>
                    </td></tr></table>
                    
                    <div id="seite_verlinken" style="display:none;">
s                    </div>
                    <div id="container_verlinken" style="display:none;">
                        <table class="" style=""><tr><td class="body">
                            <p class="">2. Wollen Sie einen neuen Text oder einen bestehenden Text auf der Seite anzeigen?</p>
                            <p class="">
                                <input type="radio" name="opt_textwahl" value="besteheneden_einfuegen" onclick="window.location='?mod=main_seiten&aktion=changePageText&id={$aSeitendaten["id"]}'">Bestehenden Text
                                <input type="radio" name="opt_textwahl" value="neuen_einfuegen"  onclick="window.location='?mod=main_seiten&aktion=createNewTextForPage&id={$aSeitendaten["id"]}'">Neuen Text
                            </p>
                        </td></tr></table>
                    </div>
                </div>
                <script>

                
                function open_ueber_seite_aendern()
                {
                    window.location='?mod=main_seiten&aktion=pageUeberSeiteForm&id={$aSeitendaten["id"]}';
                }
                

                
                </script>
                    
                
                
                <table style="width:100%;height:100%;padding:1px;">
                <tr style="height:100px"><td colspan="2" style="padding:1px;">
                
                    <form id="pageSettingsForm" action="" method="POST"  width="100%">
                    <table width="100%">
                    <tr>
                        <td>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">Name</span></div>
                                <input type="text" name="Name" class="form-control" value="{$aSeitendaten["Name"]}" />
                                <div class="input-group-prepend"><span class="input-group-text">Title</span></div>
                                <input type="text" name="Titel" class="form-control" value="{$aSeitendaten["Titel"]}" size="$iCountTitel" />
                                $verbergen
                                $admin_edit
                            </div>
                        </td>
                     </tr>
                    <tr>
                        
                    </tr>
                    <!--<tr><td >Titelbild <a href="" title="Wird kein Bild angegeben, wird das Standardbild des Stils geladen!" class="info">?</a></td><td  colspan="3"><input type="text" name="Titelbild" value="{$aSeitendaten["Titelbild"]}" /> <a href="" onclick="javascript:open_titelbild_aendern();">&rarr; Bild auswählen</a></td></tr>-->
                    <tr>
                         <td class="pt-2">
                            <input type="hidden" name="id" value="{$aSeitendaten["id"]}"><input type="hidden" name="aktion" value="set_seiten_daten_list"><a class="lupix-btn" onclick="$('#pageSettingsForm').submit()"><i class="fa fa-floppy-o"></i> Speichern</a>&nbsp;
                            <a class="lupix-btn" href="?mod=main_seiten&aktion=seite_loeschen&id={$aSeitendaten['id']}"><i class="fa fa-times-circle-o"></i> Löschen</a> &nbsp;
                            <a class="lupix-btn" href="?mod=main_seiten&aktion=showPage&id={$aSeitendaten['id']}"><i class="fa fa-edit"></i> Vorschau</a>
                        </td>
                    </tr>
                    </table>
                    </form>
                
                </td></tr>
                <tr style="padding:1px;"><td style="padding:1px;"><h5>Template-Daten</h5></td></tr>
                <tr>
                    <td><div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text">Template-Datei</span></div><input type="text" class="form-control" value="{$aSeitendaten["Templatedatei"]}"><a class="lupix-btn" href="?mod=main_seiten&aktion=changeTemplateForm&id={$aSeitendaten["id"]}">Ändern</a></td>
                    </tr>
                    <tr style="padding:1px;"><td style="padding:1px;">
                     <div class="dropdown">
                        <button class="lupix-btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Template Erben
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                $dateienMitTemplate
                            </ul>
                        </div> 
                        </td>
                    </tr>
                <tr style="padding:1px;"><td style="padding:1px;">
                
                $tTemplateFelder
                </td></tr>
                <tr style="padding:1px;"><td style="padding:1px;">
                <h5>Vorschau</h5>
                    <iframe src="index.php?mod=main_seiten&aktion=preview_page&id={$aSeitendaten["id"]}" style="width:100%;height:500pt;"></iframe>
                </td>
                </tr>
                </table>
   <br>
<br>
LOL;

//  TreeJS wird initialisiert
?>
    <script>
        $(function () { treeview = $('#pageTree');
            treeview.on('changed.jstree', function (e, data) {
                window.location='index.php?mod=main_seiten&aktion=showPageSettingsList&id='+encodeURI(data.instance.get_node(data.selected[0]).id);
            }).jstree({"core":{"multiple" : false}}).jstree('open_all');
        });
    </script>
<?php

include('seiten.part.php');


