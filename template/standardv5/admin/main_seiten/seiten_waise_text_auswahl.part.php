<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.12.19
 * Time: 10:06
 */

$aSeitenbaum =$aSeitenbaum;
$tSpaltentitle =$tSpaltentitle;
$tSpaltentitle = $tSpaltentitle;
$tSeitendaten=$tSeitendaten;
$SeitenbaumSpaltentitle=$SeitenbaumSpaltentitle;

$tTextliste='';
/** @noinspection PhpUndefinedVariableInspection */
if(count($aAlleTexte)>0)
foreach($aAlleTexte as $aEinTexte)
{
    $tTextliste.= '('.$aEinTexte["id"].') '.($aEinTexte["Titel"]===''?'[Kein Titel]':$aEinTexte["Titel"]).'&nbsp;&nbsp;&nbsp;<a href="?mod=main_seiten&aktion=deleteOrphanText&textId='.$aEinTexte["id"].'"><i class="button fa fa-trash"></i></a><br />'."\n\r";
}else{
    $tTextliste='<p class="hinweis">Es gibt keine unzugeordneten Texte!</p>';
}


$tSeitendaten .= <<<LOL
					<p>Wählen Sie einen Text aus, der gelöscht werden soll!</p>
					<h3>Liste der Texte</h3>
					<ul>$tTextliste</ul>
LOL;


include('seiten.part.php');