<ul class="list-group">



    <li class="list-group-item list-group-item-dark">
        <h4>Webseite</h4>
    </li>



    <li class="list-group-item d-flex justify-content-between align-items-center">
        Domain
        <span class="badge badge-primary badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */ $domain;?></span>
    </li>



    <li class="list-group-item list-group-item-dark">
        <h4>CMS</h4>
    </li>



    <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span class="badge badge-primary badge-pill">Lupix5</span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        Version
        <span class="badge badge-primary badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */
            $lupixVersion;?></span>
    </li>



    <li class="list-group-item list-group-item-dark">
        <h4>Fehlermeldungen</h4>
    </li>



    <li class="list-group-item d-flex justify-content-between align-items-center">
        Ungeprüfte Fehler
        <span class="badge badge-<?= /** @noinspection PhpUndefinedVariableInspection */($fehlermeldungen>0?'danger':'success')?> badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */
            $fehlermeldungen;?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        Anzahl aller Fehler
        <span class="badge badge-primary badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */
            $gesamtFehlermeldungen;?></span>
    </li>



    <li class="list-group-item list-group-item-dark">
        <h4>Speicher</h4>
    </li>



    <li class="list-group-item d-flex justify-content-between align-items-center">
        Belegter Speicher / Verfügbarer Speicher
        <div class="badge badge-success badge-pill" style="width:50%">
            <div class="progress">
                <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" style="width:<?= /** @noinspection PhpUndefinedVariableInspection */round($usedSpaceRaw/$totalSpaceRaw*100,2);?>%">
                    <?= /** @noinspection PhpUndefinedVariableInspection */
                    $usedSpace;?>
                </div>
                <div class="progress-bar bg-success" style="width:<?=round((1-$usedSpaceRaw/$totalSpaceRaw)*100,2);?>%">
                    <?= /** @noinspection PhpUndefinedVariableInspection */
                    $availableSpace;?>
                </div>
            </div>
        </div>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        Gesamtspeicher
        <span class="badge badge-primary badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */
            $totalSpace;?></span>
    </li>



    <li class="list-group-item list-group-item-dark">
        <h4>Benutzer</h4>
    </li>



    <li class="list-group-item d-flex justify-content-between align-items-center">
       Anzahl der Benutzer
        <span class="badge badge-primary badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */
            $benutzerZahl; ?></span>
    </li>



    <li class="list-group-item list-group-item-dark">
        <h4>Seiten</h4>
    </li>



    <li class="list-group-item d-flex justify-content-between align-items-center">
        Anzahl der Seiten
        <span class="badge badge-primary badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */
            $seitenZahl; ?></span>
    </li>
</ul>
<p style="margin-top:30pt;"></p>

<style>
    .admin-modul-content{
        overflow:hidden;
    }
</style>
