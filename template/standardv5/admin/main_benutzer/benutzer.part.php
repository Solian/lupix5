<h4>Übersicht</h4>
<ul class="list-group list-group-flush">
    <li class="list-group-item">
        <h5>Benutzer</h5>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        Anzahl der Benutzer
        <span class="badge badge-primary badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */ $benutzerZahl;?></span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        Anzahl der Gruppen
        <span class="badge badge-primary badge-pill"><?= /** @noinspection PhpUndefinedVariableInspection */ count($benutzerGruppen);?></span>
    </li>
    <li class="list-group-item">
        <h5>Benutzergruppen</h5>
        <ul class="list-group list-group-flush">
            <?php
            /** @noinspection PhpUndefinedVariableInspection */
            foreach($benutzerGruppen as $gruppe){
                echo '<li class="list-group-item">'.$gruppe['Name'].'</li>';
            }

            ?>
        </ul>
    </li>
</ul>

