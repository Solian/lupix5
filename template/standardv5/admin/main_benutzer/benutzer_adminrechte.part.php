    <h4>Administrationsrechte von <?= /** @noinspection PhpUndefinedVariableInspection */
        $nickname;?></h4>
<form action="" method="POST">

<table class="table" width="800" class="body">
    <tr><td width="60%" class="subtitle">Adminmodul</td><td class="subtitle">Benutzerrecht</td><td class="subtitle"><a title="Diese Rechte erhält man durch die Mitgliedschaft in der Admingruppe">Vererbtes Recht</a></td></tr>
    <?php

    /** @noinspection PhpUndefinedVariableInspection */
    foreach ($alleRechte as $recht) {
    echo '<tr><td width="60%" class="subtitle">(' . $recht['id'] . ') ' . $recht['Name'] . '</td><td class="subtitle">' . checkbox_from_bool("aendern" . $recht['id'], $recht['recht']) . '</td><td class="subtitle">' . checkbox_from_bool("geerbtes_aendern" . $recht['id'], $recht['recht_admingroup'],'','disabled="disabled"') . '</td></tr>';
    }

    ?>
    <tr><td width="60%" class="subtitle"></td><td class="subtitle"><input type="submit" value="Speichern" class="ui-button ui-corner-all ui-widget"></td><td width="20%" class="subtitle"><input type="hidden" name="userId" value="<?= /** @noinspection PhpUndefinedVariableInspection */
            $userId;?>"><input type="hidden" name="aktion" value="set_user_adminrechte"></td><td></td></tr>
</table>
</form>
