    <h4>Seitenrechte von <?= /** @noinspection PhpUndefinedVariableInspection */
        $nutzerdaten['nickname'];?></h4>
<form action="" method="POST">
<table class="table" class="body">
    <tr><td width="" class=""><h6>Seite</h6></td><td class=""><h6>Leserecht (Gruppenrecht)</h6></td><td width="" class=""><h6>Schreibrecht (Gruppenrecht)</h6></td></tr>
    <?php

    if(!empty($rechte)) {


    foreach ($rechte as $aRecht) {
        //Gruppenrecht heraussuchen

        //Lade das aktuelle Gruppenrecht

        /** @noinspection PhpUndefinedVariableInspection */
        foreach($gruppenRechte as $gruppenRecht){
            if($gruppenRecht['id']==$aRecht['id']){
                break;
            }
        }

        //Wenn aber gar kein Gruppenrechte vorhanden ist dazu, muss es erstellt werden
        if(!isset($gruppenRecht) || empty($gruppenRecht)){
            $gruppenRecht=array('id'=>$aRecht['id'],'lesen'=>0,'aendern'=>0 );
        }elseif(!is_array($gruppenRecht)){
            $gruppenRecht=array('id'=>$aRecht['id'],'lesen'=>0,'aendern'=>0 );
        }
        //Wenn das letzte Gruppenrecht nicht die gleiche ID hat, ist die schleife einfach durchgelaufen und es gibt gar kein Recht => neu erstellen!
        elseif($gruppenRecht['id']!==$aRecht['id']){
            $gruppenRecht=array('id'=>$aRecht['id'],'lesen'=>0,'aendern'=>0 );
        }

        /** @noinspection PhpUndefinedVariableInspection */
        echo '<tr><td width="" class=""><p>' . $aRecht['Name'] . '</p></td><td class="">' . checkbox_from_bool("lesen" . $aRecht['id'], $aRecht['lesen']) . ' (' . checkbox_from_bool("lesenGruppe" . $gruppenRecht['id'], $gruppenRecht['lesen'], '','disabled') . ')</td><td width="" class="">' . checkbox_from_bool("aendern" . $aRecht['id'], $aRecht['aendern']) . ' (' . checkbox_from_bool("aendernGruppe" . $gruppenRecht['id'], $gruppenRecht['aendern'], '','disabled') . ')</td></tr>';
    }


    ?>
    <tr><td width="" class=""></td><td class=""><input type="submit" value="Speichern" class="ui-button ui-corner-all ui-widget"></td><td width="" class=""><input type="hidden" name="uid" value="<?=$nutzerdaten['id'];?>"><input type="hidden" name="aktion" value="set_benutzer_rechte"></td></tr>
    <?php

    }else{
    echo '<tr><td colspan="3"><p class="hinweis fa fa-info-circle"> '.$nutzerdaten['nickname'].' hat keine Rechte auf Seiten zuzugreifen.</p></td></tr>';
    }

?></table>
</form>

