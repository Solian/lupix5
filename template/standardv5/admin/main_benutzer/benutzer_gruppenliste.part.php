    <?php
    if(!isset($sAufsteigend))$sAufsteigend="ASC"?>
    <h4>Liste der Benutzergruppen</h4>
<table class="table table-hover" width="100%" border="0" cellspacing="1" cellpadding="0"><tr>

        <th class="table-row table-head" style="text-align:center;"><h6>ID <a href="?mod=main_benutzer&aktion=liste_der_benutzergruppen&order=id&asc=<?=$sAufsteigend;?>"><i class="fa fa-chevron-down"></i></a></h6></th>

        <td class="" style="text-align:center;width:*"><h6>Name <a href="?mod=main_benutzer&aktion=liste_der_benutzergruppen&order=Name&asc=<?=$sAufsteigend;?>"><i class="fa fa-chevron-down"></i></a></h6></td>

        <td class="" style="text-align:center;"><h6>Einstellungen</h6></td>
    </tr>
<?php
/** @noinspection PhpUndefinedVariableInspection */
foreach ($alleGruppen as $aGruppe) {
        echo <<<LOL
    <tr style="text-align:center"><td class="body">{$aGruppe["id"]}</td><td class="body">{$aGruppe["Name"]}</td><td class="body"><a href="?mod=main_benutzer&aktion=show_gruppen_daten&gid={$aGruppe["id"]}" class="button"><i class="fa fa-address-card-o" title="Gruppendaten"></i></a></a> <a href="?mod=main_benutzer&aktion=show_gruppen_rechte&gid={$aGruppe["id"]}" class="button"><i class="fa fa-list-ul" title="Gruppenrechte"></i></a></a></td></tr>
LOL;
    }
    ?>
</table>
