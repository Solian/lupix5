
<script>

    $(function() {
        $("#dialog").dialog({
            autoOpen: false,
            width: 400,
            buttons: [
                {
                    text: "Abbrechen",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });
    });

    function openGroupChangeDialog(userId,userGroupId){
        $('#dialog').dialog('option','title','Benutzergruppe ändern').html('<p>Wählen Sie die neue Benutzergruppe aus:</p>'+
            <?php  /** @noinspection PhpUndefinedVariableInspection */
            foreach($alleGruppen as $gruppe){
                    ?> '<a id="benutzerListe_groupdchangedialog_<?=$gruppe['id']?>" class="button" href="?mod=main_benutzer&aktion=benutzergruppe_aendern&amp;userId='+userId+'&amp;gruppeId=<?=$gruppe['id']?>"><?=$gruppe['Name']?></a><br>'+ <?php
            } ?>
        '');
        $('#benutzerListe_groupdchangedialog_'+userGroupId).addClass('ui-state-active');
        $('#dialog').dialog('open');
        $('#benutzerListe_groupdchangedialog_'+userGroupId).focus();
    }
</script>
<style>


</style>
<h4>Benutzerliste</h4>
<table class="table table-hover" border="0" cellspacing="1" cellpadding="0" style="margin:4pt;width:99%;"><tr>

        <th class="table-head" style="text-align:center;width:20%"><h6>Username <a href="?mod=main_benutzer&aktion=benutzerliste&orderBy=main_benutzer.nickname&asc=<?= /** @noinspection PhpUndefinedVariableInspection */
                $aufAb; ?>"><i class="fa fa-chevron-down"></i></a></h6></th>

        <td class="" style="text-align:center;width:20%"><h6>Registerdatum <a href="?mod=main_benutzer&aktion=benutzerliste&orderBy=main_benutzer.regdatum&asc=<?=$aufAb; ?>"><i class="fa fa-chevron-down"></i></a</h6></td>

        <td class="" style="text-align:center;width:20%"><h6>Einstellungen</td>

        <td class="" style="text-align:center;width:*"><h6>Gruppe <a href="?mod=main_benutzer&aktion=benutzerliste&orderBy=main_nutzergruppen.id&asc=<?=$aufAb; ?>"><i class="fa fa-chevron-down"></i></a></h6></td>

        <td class="" style="text-align:center;width:20%"><h6>Letzter Login <a href="?mod=main_benutzer&aktion=benutzerliste&orderBy=main_benutzer.letztbesuch&asc=<?=$aufAb; ?>"><i class="fa fa-chevron-down"></i></a></h6></td>
    </tr>

    <?php  /** @noinspection PhpUndefinedVariableInspection */

    foreach($alleBenutzer as $aBenutzer)
        {

            $aBenutzer["letztbesuch"]=zeit_format($aBenutzer["letztbesuch"]);
            $aBenutzer["regdatum"] =zeit_format($aBenutzer["regdatum"]);

            ?>
            <tr class="user_row" style="text-align:center">

                <td class="body"><a href="?mod=main_benutzer&aktion=show_user_daten&amp;userId=<?=$aBenutzer["id"];?>" class="fa fa-id-card-o" style="text-decoration:none;" title="Benutzerdaten"> <?=$aBenutzer["nickname"];?></a></td>

                <td class="body"><?=$aBenutzer["regdatum"];?></td>

                <td class="body">
                    <a href="?mod=main_benutzer&aktion=show_user_rechte&amp;userId=<?=$aBenutzer["id"];?>" class="button fa fa-file-text-o" title="Seitenrechte"></a>
                    <a href="?mod=main_benutzer&aktion=show_user_adminrechte&amp;userId=<?=$aBenutzer["id"];?>" class="button fa fa-cogs" title="Adminrechte"></a>

                </td>

                <td class="body"><a href="javascript:openGroupChangeDialog(<?=$aBenutzer["id"];?>,<?=$aBenutzer["Gruppenid"];?>);"><?=$aBenutzer["Gruppenname"];?></a></td>

                <td class="body"><?=$aBenutzer["letztbesuch"];?></td>

            </tr>
            <?php
        }
?></table>
</div>
</div>
