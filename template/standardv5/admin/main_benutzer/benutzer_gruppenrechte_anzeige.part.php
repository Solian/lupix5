
    <h4>Berechtigung für Seitenzurgiffe von <?= /** @noinspection PhpUndefinedVariableInspection */
        $aGruppendaten['Name'];?></h4>
    <form action="" method="POST">
    <table width="800" class="table">
        <tr><td width="60%" class=""><h6>Seite</h6></td><td class=""><h6>Leserecht</h6></td><td width="20%" class=""><h6>Schreibrecht</h6></td></tr>

        <?php

        /** @noinspection PhpUndefinedVariableInspection */
        foreach($aAlleRechte as $aRecht)
        {
        echo '<tr><td width="60%" class="">'.$aRecht['Name'].'</td><td class="">'.checkbox_from_bool("lesen".$aRecht['id'],$aRecht['lesen']).'</td><td class="">'.checkbox_from_bool("aendern".$aRecht['id'],$aRecht['aendern']).'</td></tr>';
        }

        ?>
        <tr><td width="60%" class=""></td><td class=""><input type="submit" value="Speichern" class="ui-button ui-corner-all ui-widget"></td><td  class=""><input type="hidden" name="gid" value="<?= /** @noinspection PhpUndefinedVariableInspection */
                $gid;?>"><input type="hidden" name="mod" value="main_benutzer"><input type="hidden" name="aktion" value="set_gruppen_rechte"></td></tr>
    </table>
    </form>
