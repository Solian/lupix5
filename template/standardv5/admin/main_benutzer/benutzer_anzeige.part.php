<script>
    $(function() {
        $("#dialog").dialog({
            modal:true,
            autoOpen: false,
            width: 400,
            buttons: [
                {
                    text: "Löschen",
                    click: function () {
                        window.location='?mod=main_benutzer&aktion=benutzer_loeschen&userId=<?=/** @noinspection PhpUndefinedVariableInspection */ $benutzer["id"];?>';
                    }
                },
                {
                    text: "Abbrechen",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });
    });

    function benutzerDeleteDialog(userId,userGroupId){
        $('#dialog').dialog('option','title','Benutzer loeschen').html('<p class="ui-state-error ui-corner-all" style="padding: 0 .7em;">\n' +
            '\t\t<span class="ui-icon ui-icon-alert"></span>\n' +
            '\t\t<strong>Achtung:</strong> Sind Sie sicher, dass Sie den Benutzer <i><?=$benutzer["nickname"];?></i> löschen wollen\n' +
            '\t</p>');
        $('#dialog').dialog('open');
        $('#benutzerListe_groupdchangedialog_'+userGroupId).focus();
    }
</script>

    <h4>Benutzerdaten von <?= /** @noinspection PhpUndefinedVariableInspection */
        $benutzer['nickname'];?></h4>
<form action="?mod=main_benutzer" method="POST">
<table class="table table-striped table-sm" style="margin:4pt;">
    <tr>
        <td width="60%" class="subtitle">Name</td>
        <td class=""><input type="text" name="nickname" value="<?=$benutzer['nickname'];?>"/></td>
        <td width="20%" class=""></td>
    </tr>
    <tr>
        <td class="subtitle">Passwort</td>
        <td class="">***</td>
        <td width="20%" class=""><a href="?mod=main_benutzer&aktion=new_user_pw&userId=<?=$benutzer['id'];?>">&rarr; Neues Passwort</a></td>
    </tr>

    <?php

    ## Die E-Mail an sich.
    ##

    //wenn noch eine E-Mail vorliegt
    if(!empty($benutzer['email'])){
    ?>

    <tr>
        <td class="subtitle">E-Mail</td>
        <td class=""><input type="text" name="email" value="<?=$benutzer['email'];?>"/></td>
        <td width="20%" class=""></td>
    </tr>
    <?php
    }else{

        ?>
        <tr>
            <td class="subtitle">E-Mail</td>
            <td class="fehler"><p class="fa fa-info-circle"> Noch nicht bestätigt</p></td>
            <td width="20%" class=""></td>
        </tr>
        <?php
    }

    ## Die zu bestätigende E-Mail
    ##

    //wenn noch eine E-Mail vorliegt
    if(!empty($benutzer['new_email'])){
        ?>

        <tr>
            <td class="subtitle">Neue E-Mail</td>
            <td class="hinweis"><p class="fa fa-info-circle"> <?=$benutzer['new_email'];?><br>Link gülitg bis <?=date('H:i:s',$benutzer['new_email_date']+86400) ?></p></td>
            <td width="20%" class=""><a href="?mod=main_benutzer&aktion=confirm_mail&mailkey=<?=$benutzer['new_email_key'] ;?>">&rarr; E-Mail bestätigen</a></td>
        </tr>
        <?php
    }


    ?>

    <tr>
        <td class="subtitle">Regdatum</td>
        <td class=""><?=$benutzer['regdatum'];?></td>
        <td width="20%" class=""></td>
    </tr>
    <tr>
        <td class="subtitle">Letzter Besuch</td>
        <td class=""><?=$benutzer['letztbesuch'];?></td>
        <td width="20%" class=""></td>
    </tr>
    <tr>
        <td class="subtitle">Realer Name</td>
        <td class=""><input type="text" name="realername" value="<?=$benutzer['realername'];?>"/></td>
        <td width="20%" class=""></td>
    </tr>
    <tr>
        <td class="subtitle">Avatar</td>
        <td class=""><img style="width:100pt;" src="<?=($benutzer['avatar']==''?'../template/standardv5/img/profile.png':$benutzer['avatar']);?>"/></td>
        <td width="20%" class=""><a title="TODO">&rarr; Avatarverwaltung</a></td>
    </tr>
    <tr>
        <td class="subtitle">Persönliche Beschreibung anzeigen</td>
        <td class=""><?=$benutzer['zeig_pers_daten'];?></td>
        <td width="20%" class=""></td>
    </tr>
    <tr>
        <td class="subtitle">neue PNs per Popup anzeigen</td>
        <td class=""><?=$benutzer['neue_pns_anzeigen'];?></td>
        <td width="20%" class=""></td>
    </tr>
    <tr>
        <td class="subtitle">über neue PNs per Mail benachrichtigen</td>
        <td class=""><?=$benutzer['neue_pns_mail'];?></td>
        <td width="20%" class=""></td>
    </tr>
    <tr>
        <td></td>
        <td class=""><input type="submit" value="Speichern" class="ui-button ui-corner-all ui-widget"><input type="hidden" name="aktion" value="set_user_daten"><input type="hidden" name="userId" value="<?=$benutzer['id'];?>"></td>
        <td width="20%"><a class="button" href="javascript:benutzerDeleteDialog();"><i class="ui-icon ui-icon-alert"></i> Loeschen</a></td>
    </tr>
</table></form>
