    <h4>Suche nach Benutzern</h4>
    <form action="?mod=main_benutzer" method="post">
        <p>Benutzername: <input type="text" name="query"/><input type="hidden" name="aktion" value="suche_nutzer"> <input type="submit" class="ui-button ui-corner-all ui-widget" name="Suchen" value="Suchen"></p>
    </form>
    <?php
    /** @var string  $suchName */
    if($suchName!=='')
    {
        ?>
        <h5>Suchergebnisse für '<?=$suchName;?>'</h5>

        <table class="table" border="0" cellspacing="1" cellpadding="0" style="margin:4pt;width:99%;">
            <tr>

                <td class="subtitle" style="text-align:center;width:20%">Username</a></td>

                <td class="subtitle" style="text-align:center;width:20%">Registriert seit</a></td>

                <td class="subtitle" style="text-align:center;width:20%">Einstellungen</td>

                <td class="subtitle" style="text-align:center;width:*">Gruppe</a></td>

                <td class="subtitle" style="text-align:center;width:20%">Letzter Login</td>
            </tr>

        <?php
        /** @var array $ergebnisListe */
        if(is_array($ergebnisListe)) {
            foreach ($ergebnisListe as $aBenutzer) {

                $aBenutzer["letztbesuch"] = zeit_format($aBenutzer["letztbesuch"]);
                $aBenutzer["regdatum"] = zeit_format($aBenutzer["regdatum"]);

                ?>
                <tr class="user_row" style="text-align:center">

                    <td class="body"><a
                                href="?mod=main_benutzer&aktion=show_user_daten&amp;userId=<?= $aBenutzer["id"]; ?>"
                                class="fa fa-id-card-o" style="text-decoration:none;"
                                title="Benutzerdaten"> <?= $aBenutzer["nickname"]; ?></a></td>

                    <td class="body"><?= $aBenutzer["regdatum"]; ?></td>

                    <td class="body">
                        <a href="?mod=main_benutzer&aktion=show_user_rechte&amp;userId=<?= $aBenutzer["id"]; ?>"
                           class="button fa fa-cog" title="Seitenrechte"></a>
                        <a href="?mod=main_benutzer&aktion=show_user_adminrechte&amp;userId=<?= $aBenutzer["id"]; ?>"
                           class="button fa fa-cogs" title="Adminrechte"></a>
                    </td>

                    <td class="body"><a
                                href="?mod=main_benutzer&aktion=benutzergruppe_aendern&userId=<?= $aBenutzer["id"]; ?>"><?= $aBenutzer["Gruppenname"]; ?></a>
                    </td>

                    <td class="body"><?= $aBenutzer["letztbesuch"]; ?></td>

                </tr>
                <?php
            }
        }else{
            echo '<tr><td colspan="5" class="hinweis"><p>Kein Ergebnis für <b>'.$suchName.'</b></p</td></tr>';
        }
        ?></table><?php




    }
    ?>
