<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap 4 Website Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/font-awesome.min.css" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style>
        .fakeimg {
            height: 200px;
            background: #aaa;
        }
    </style>
</head>
<body>

<div class="jumbotron text-center" style="margin-bottom:0">
    <h1>My First Bootstrap 4 Page</h1>
    <p>Resize this responsive page to see the effect!</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-center">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
        </ul>
    </div>
</nav>


<div class="container-xl" style="margin-top:30px">
    <div class="row">
        <div class="col-md-3">
            <h2>About Me</h2>
            <h5>Photo of me:</h5>
            <div class="fakeimg">Fake Image</div>
            <p>Some text about me in culpa qui officia deserunt mollit anim..</p>
            <h3>Some Links</h3>
            <p>Lorem ipsum dolor sit ame.</p>
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Active</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
            <hr class="d-sm-none">
        </div>
        <div class="col-sm-7">
            <h2>TITLE HEADING</h2>
            <h5>Title description, Dec 7, 2017</h5>
            <div class="fakeimg">Fake Image</div>
            <p>Some text..</p>
            <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
            <br>
            <h2>TITLE HEADING</h2>
            <h5>Title description, Sep 2, 2017</h5>
            <div class="fakeimg">Fake Image</div>
            <p>Some text..</p>
            <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
        </div>
        <div class="col-md-2">
            <h2>About Me</h2>
            <h5>Photo of me:</h5>
            <div class="fakeimg">Fake Image</div>
            <p>Some text about me in culpa qui officia deserunt mollit anim..</p>
            <h3>Some Links</h3>
            <p>Lorem ipsum dolor sit ame.</p>
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Active</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
            <hr class="d-sm-none">
        </div>
    </div>
</div>

<div class="jumbotron text-center" style="margin-bottom:0">
    <p>Footer</p>
</div>












<div id="topbar-wrapper" class="p-3 border-bottom border-left border-right border-primary shadow rounded-bottom" style="width:65%;position: fixed;text-align:center;left:15%;top:0;background-color:#e7f4f9;">

    <nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-center">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
            </ul>
        </div>
    </nav>
    <div id="main_seiten_edit_navi_close_button"><i class="fa fa-chevron-circle-up"></i></div>
</div>

<div id="topbutton-wrapper" class="" style="width:100%;position: fixed;text-align:center;margin:0;top:0;margin-top:-100pt;display:none;">
    <i class="fa fa-chevron-circle-down" id="main_seiten_edit_navi_open_button" style="cursor:pointer"></i>
</div>





<div id="sidebar-wrapper" class="p-3 border-right border-primary shadow rounded-right" style="width:15%;min-width:100pt;max-width:250pt;height:100%;position: fixed;left:0;top:0;background-color:#e7f4f9;">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Seitenbaum <i class="fa fa-close" id="main_seiten_edit_page_close_button"></i></h3>
        </div>

        <ul class="list-unstyled components">
            <p>Dummy Heading</p>
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Home 1</a>
                    </li>
                    <li>
                        <a href="#">Home 2</a>
                    </li>
                    <li>
                        <a href="#">Home 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">About</a>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="#">Page 1</a>
                    </li>
                    <li>
                        <a href="#">Page 2</a>
                    </li>
                    <li>
                        <a href="#">Page 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">Portfolio</a>
            </li>
            <li>
                <a href="#">Contact</a>
            </li>
        </ul>
    </nav>
</div>

<div id="sidebutton-wrapper" class="p-3  border-right border-bottom border-primary  shadow rounded-right rounded-bottom" style="position: fixed;left:0;top:0;background-color:#e7f4f9;display: none;margin-left:-250pt;">
    <i class="fa fa-file-o" id="main_seiten_edit_page_open_button" style="cursor:pointer"></i>
</div>








<div id="menubar-wrapper" class="p-3 border-left border-primary shadow rounded-left" style="width:25%;min-width:100pt;max-width:250pt;height:100%;position: fixed;right:0;top:0;background-color:#e7f4f9;">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Seitenbaum <i class="fa fa-close" id="main_seiten_edit_properties_close_button"></i></h3>
        </div>

        <ul class="list-unstyled components">
            <p>Dummy Heading</p>
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Home 1</a>
                    </li>
                    <li>
                        <a href="#">Home 2</a>
                    </li>
                    <li>
                        <a href="#">Home 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">About</a>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="#">Page 1</a>
                    </li>
                    <li>
                        <a href="#">Page 2</a>
                    </li>
                    <li>
                        <a href="#">Page 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">Portfolio</a>
            </li>
            <li>
                <a href="#">Contact</a>
            </li>
        </ul>
    </nav>
</div>

<div id="menubutton-wrapper" class="p-3  border-left border-bottom border-primary  shadow rounded-left rounded-bottom" style="position: fixed;right:0;top:0;background-color:#e7f4f9;display: none;margin-right:-350pt;">
    <i class="fa fa-align-justify" id="main_seiten_edit_properties_open" style="cursor:pointer"></i>
</div>


<script>
    var sidebarWrapper = $('#sidebar-wrapper');
    var sidebuttonWrapper = $('#sidebutton-wrapper');

    $('#main_seiten_edit_page_close_button').click(function(){
        sidebarWrapper.animate({opacity:0,'margin-left':"-=250pt"},500,function() {sidebarWrapper.toggle();});
        sidebuttonWrapper.toggle();
        sidebuttonWrapper.animate({opacity:1,'margin-left':"+=250pt"},500);
    });

    sidebuttonWrapper.click(function(){
        sidebuttonWrapper.animate({opacity:0,'margin-left':"-=250pt"},500,function() {sidebuttonWrapper.toggle();});
        sidebarWrapper.toggle();
        sidebarWrapper.animate({opacity:1,'margin-left':"+=250pt"},500);

    });

    var menubarWrapper = $('#menubar-wrapper');
    var menubuttonWrapper = $('#menubutton-wrapper');

    $('#main_seiten_edit_properties_close_button').click(function(){
        menubarWrapper.animate({opacity:0,'margin-right':"-=350pt"},500,function() {menubarWrapper.toggle();});
        menubuttonWrapper.toggle();
        menubuttonWrapper.animate({opacity:1,'margin-right':"+=350pt"},500);
    });

    menubuttonWrapper.click(function(){
        menubuttonWrapper.animate({opacity:0,'margin-right':"-=350pt"},500,function() {menubuttonWrapper.toggle();});
        menubarWrapper.toggle();
        menubarWrapper.animate({opacity:1,'margin-right':"+=350pt"},500);

    });

    var topbarWrapper = $('#topbar-wrapper');
    var topbuttonWrapper = $('#topbutton-wrapper');

    $('#main_seiten_edit_navi_close_button').click(function(){
        topbarWrapper.animate({opacity:0,'margin-top':"-=100pt"},500,function() {topbarWrapper.toggle();});
        topbuttonWrapper.toggle();
        topbuttonWrapper.animate({opacity:1,'margin-top':"+=100pt"},500);
    });

    topbuttonWrapper.click(function(){
        topbuttonWrapper.animate({opacity:0,'margin-top':"-=100pt"},500,function() {topbuttonWrapper.toggle();});
        topbarWrapper.toggle();
        topbarWrapper.animate({opacity:1,'margin-top':"+=100pt"},500);

    });
</script>










</body>
</html>
