<script>
function setLoginnameEmpty(){
    document.loginbox.loginname.value='';
}
function setLoginPasswortEmpty(){
    document.loginbox.loginpasswort.value='';
}
function sendLogin(){
    document.loginbox.loginpasswort.value=verschluesselung2(document.loginbox.loginpasswort.value);
    document.loginbox.submit();
}
</script>
<?php

if(isset($mit_ueberschrift)){
    echo '<h1>Anmelden</h1>';
}


?>
<form action="index.php" method="post" name="loginbox">
    <input type="hidden" name="ort" value="?id=' . ((!isset($_tpl_login_form_id))?1:$_tpl_login_form_id) . '" />
    <input type="hidden" name="aktion" value="einloggen" />
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">E-Mail</span>
        </div>
        <input type="text" class="form-control" placeholder="registrierte E-Mailadresse" name="loginname">
        &nbsp;&nbsp;&nbsp;&nbsp;<div class="input-group-prepend">
            <span class="input-group-text">Passwort</span>
        </div>
        <input type="password" class="form-control" placeholder="Passwort" name="loginpasswort">
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="lupix-btn btn-primary form-control" value="Login" onclick="javascript:sendLogin()"/>
    </div>
</form>