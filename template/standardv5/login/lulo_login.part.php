<?php

if(isset($mit_ueberschrift)){
    echo '<h1>Anmelden</h1>';
}

?>
<script src="template/standardv5/scripts/js-sha512/sha512.min.js"></script>
<script src="template/standardv5/scripts/jsencrypt/jsencrypt.min.js"></script>
<div>
    <script type="text/javascript">
        function startLogin() {
            //erstellt aus Challange n und Benutzernamen u den hash h1=h(u,n).
            let h1 = sha512($('#loginname').val() + '<?=$challenge;?>');

            //Dazu generiert er ein RSA-1024bit-Schlüsselpaar g_priv, g_pub

            let encrypt = new JSEncrypt({default_key_size: 1536});
            encrypt.getKey();


            //liefert k, h1, g_pub an den Server zurück!
            $.post("index.php", {
                'aktion': "einloggen",
                'g_pub': encrypt.getPublicKey(),
                'h1': h1,
                'k': '<?=$sessionkey;?>'
            }).done(function (answer) {
                answer = JSON.parse(answer);
                $('#inputL').val(answer.l);

                //Der Client entschlüsselt mit seinem privaten RSA-SChlüssel g_priv aus dem Chiffrat c1 --> die Challange o
                let o = encrypt.decrypt(answer.c1);

                //er stellt erneut die Antwort h2=h(u,o) aus dem Benutzernamen und der neuen Challange o
                $('#inputH2').val(sha512($('#loginname').val() + o));

                //er verschlüsselt das Passwort c2=enc(pw,k_pub)
                // Encrypt with the public key...
                var encrypt2 = new JSEncrypt();
                encrypt2.setPublicKey(answer.k_pub);
                $('#inputC2').val(encrypt2.encrypt($('#loginpasswort').val()));

                //er sendet l,c2,h2 an den Server, neue Seite wird aufgerufen, die RSA-SChlüssel gehen verloren!
                $('#loginbox').submit();
            });
        }


    </script>
    <input type="hidden" name="aktion" value="einloggen"/>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">E-Mail</span>
        </div>
        <input type="text" class="form-control" placeholder="registrierte E-Mailadresse" id="loginname" name="loginname">
        <div class="input-group-prepend">
            <span class="input-group-text">Passwort</span>
        </div>
        <input type="password" class="form-control" placeholder="Passwort" id="loginpasswort" name="loginpasswort">
        <input type="button" class="lupix-btn btn-primary form-control" value="Login" onclick="startLogin()"/>
    </div>
    <form action="index.php" method="post" id="loginbox" name="loginbox">
        <input type="hidden" id="inputL" name="l" value="">
        <input type="hidden" id="inputC2" name="c2" value="">
        <input type="hidden" id="inputH2" name="h2" value="">
        <input type="hidden" name="aktion" value="einloggen">
    </form>
</div>



