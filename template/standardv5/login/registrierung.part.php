
<h1 class="subtitle">Grußwort des Wirtes</h1>
<p>Tragt bitte alle geforderten Daten und löst die Rechenaufgabe am Ende der Seite, damit wir sichergehen können, dass ihr kein Computer seid.<p />
<h1 class="subtitle">Angaben</h1>

<script>
    function setEmailEmpty(){
        document.registerbox.email.value='';
    }

    function sendRegistration(){
        var nicknameRegex=/^([a-zA-Z0-9]+([a-zA-Z0-9]+(_|-| )[a-zA-Z0-9]+)*[a-zA-Z0-9]+){2,20}/;
        var passwRegex =   /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
        var emailRegex = /\S+@\S+\.\S+/;
        $("#nicknametxt").removeClass("fehler").text("Spielername (Nicht der Name ihres Charakters!)");
        $("#neuespw2txt").removeClass("fehler").text("Bitte wiederholen Sie das Passwort!");
        $("#neuespw1txt").removeClass("fehler").text("Passwort");
        $("#emailtxt").removeClass("fehler").text("Adresse");
        $("#antworttxt").removeClass("fehler").text("Antwort");

        if(document.registerbox.nickname.value.match(nicknameRegex)){
            if(document.registerbox.passwort1.value.match(passwRegex)){
                if(document.registerbox.passwort1.value==document.registerbox.passwort2.value){
                    if(document.registerbox.email.value!=='XXX@XXX.XXX'){
                        if(document.registerbox.email.value.match(emailRegex)){
                            if( parseInt(document.registerbox.antwort.value)>=0){
                                document.registerbox.passwort1.value=verschluesselung2(document.registerbox.passwort2.value);
                                document.registerbox.passwort2.value=verschluesselung2(document.registerbox.passwort2.value);
                                document.registerbox.submit();
                            } else {
                                $("#antworttxt").addClass("fehler").text("Geben Sie eine Zahl großer oder gleich Null ein!");
                            }
                        } else {
                            $("#emailtxt").addClass("fehler").text("Die E-Mailadresse muss die Form  [...]@[...].[...] haben!");
                        }
                    }else{
                        $("#emailtxt").addClass("fehler").text("Die E-Mailadresse darf nicht XXX@XXX.XXX lauten!");
                        document.registerbox.email.value='';
                    }

                } else {

                    $("#neuespw2txt").addClass("fehler").text("Geben Sie das Passwort ein zweites Mal ein.");
                }
            } else {
                $("#neuespw1txt").addClass("fehler").text("Das Passwort muss aus 8-15 Zeichen bestehen und dabei mindestens einen Kleinbuchstaben, einen Großbuchstaben, eine Zahl und ein Sonderzeichen enthalten.");
            }

        } else {
            $("#nicknametxt").addClass("fehler").text("Der Spielername darf nicht leer sein und kann nur Buchstaben, Zahlen, Bindestriche und Unterstriche enthalten. Zwischen Bindestrichen und Unterstrichen müssen sich mindestens zwei Zeiche befinden!");
        }

    }
</script>

<form action="index.php?id=profil&aktion=spieler_speichern" method="post" name="registerbox">
    <table align="center">
        <tr>
            <td class="body" id="nicknametxt">Spielername (Kein Heldenname!)</td>
            <td><input type="text" name="nickname" value=""></td>
        </tr>
        <tr>
            <td class="body" id="neuespw1txt">Passwort</td><td><input type="password" name="passwort1"></td>
        </tr>
        <tr>
            <td class="body" id="neuespw2txt">Bitte wiederholen Sie das Passwort!</td><td><input type="password" name="passwort2"></td>
        </tr>
        <tr>
            <td class="body" id="emailtxt">Adresse</td>
            <td><input type="text" name="email" value="XXX@XXX.XXX" onclick="javascript:setEmailEmpty()"></td>
        </tr>
        <tr>
            <td class="body">Aufgabe<p class="smalltext">(Lösen Sie bitte die Aufgabe. Sie müssen entweder addieren (+) oder multiplizieren (*). Die 6 und 9 werden durch einen Punkte unterschieden.)</p></td>
            <td><img src="lib/captcha.php?id=<?=$captchaid; ?>"></td>
        </tr>
        <tr>
            <td class="body" id="antworttxt">Antwort</td>
            <td><input type="text" name="antwort">
                <input type="hidden" name="captchaid" value="<?= $captchaid; ?>"></td>
        </tr>
        <td colspan="2" class="body"><hr>[ X ] Ich erkl&auml;re mich mit meiner Anmeldung einverstanden mit den Bedingungen zur Nutzung der dieser Webseite.</td>
        </tr>
        </tr>
        <td colspan="2" class="body">[ X ] Die Betreiber der Heldenschenke dürfen mich bezogen auf meine Tätigkeiten in der Heldenschenke per E-Mail kontaktieren.</td>
        </tr>
        <tr>
            <td colspan="2"><input type="Button" onclick="javascript:sendRegistration()" value="Lokalrunde geben"> <input type="reset"></td>
        </tr>
    </table>
</form>