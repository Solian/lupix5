<h1 class="">Hinweis</h1>

<ul class="hinweis">
    <li>Bitte beachten Sie, dass Sie bereits als Benutzer <?= /** @noinspection PhpUndefinedVariableInspection */
        $username; ?>  angemeldet sind.</li>
    <li>Wenn Sie eine andere Person sind, können Sie sich hier <a href="?aktion=logout">abmelden</a>.</li>
    <li>Ihre persönlichen Einstellungen nehmen sie im Bereich 'Spielerprofil' vor.</li>
</ul>