<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lupix5 ~ <?php /** @noinspection PhpUndefinedVariableInspection */
        echo $pageName; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--    FAVICON         -->
    <link rel="apple-touch-icon" sizes="180x180" href="../template/standardv5/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../template/standardv5/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../template/standardv5/img/favicon-16x16.png">
    <link rel="manifest" href="../template/standardv5/img/site.webmanifest">
    <link rel="mask-icon" href="../template/standardv5/img/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../template/standardv5/img/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../template/standardv5/img/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <!--    STYLE-SHEETS         -->
    <link rel="stylesheet" href="../template/standardv5/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../template/standardv5/scripts/jquery-ui/jquery-ui.css">

    <link rel="stylesheet" href="../template/standardv5/fonts/fontawesome/font-awesome.css" type="text/css" />
    <link rel="stylesheet" href="../template/standardv5/css/fonts.css" type="text/css" />
    <link rel="stylesheet" href="../template/standardv5/css/standard.css" type="text/css" />
    <link rel="stylesheet" href="../template/standardv5/css/buttons.css" type="text/css" />
    <link rel="stylesheet" href="../template/standardv5/css/jstree/default/style.css" type="text/css" />


    <!--   JAVASCRIPTE          -->
    <script src="../template/standardv5/scripts/jquery.js"></script>
    <script src="../template/standardv5/scripts/jquery-ui/jquery-ui.js"></script>
    <script src="../template/standardv5/scripts/popper.min.js"></script>
    <script src="../template/standardv5/scripts/bootstrap.min.js"></script>
    <script src="../template/standardv5/scripts/jstree.min.js"></script>
    <script src="../template/standardv5/scripts/funktionen.js" type="text/javascript"></script>

</head>
<body style="overflow:hidden;">

<iframe src="index.php?id=<?= /** @var string $pageId_ */ $pageId_;?>&mod=main_seiten&aktion=editPage" width="100%" height="100%" style="border:0;"></iframe>

<div id="topbar-wrapper" class="pl-1 pr-1 shadow rounded-bottom" style="width:60%;position: fixed;text-align:center;left:16%;top:0;background:linear-gradient(0deg,rgb(234, 237, 249) , rgba(234,237,249,0.5)7% ,rgba(234,237,249,0.5)93%,rgb(234, 237, 249)),url(../template/standardv5/img/material.jpeg);z-index:10000">

    <nav class="navbar navbar-expand-sm justify-content-center p-0">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
            <ul class="navbar-nav" style="position:absolute;left:0;">
                <li class="nav-item">
                    <a class="nav-link" id="main_seiten_edit_navi_close_button"><i class="fa fa-chevron-circle-up"></i></a>
                </li>
            </ul>
            <?php /** @var string $navi */
            echo $navi; ?>
            <ul class="navbar-nav" style="position:absolute;right:0;">
                <li class="nav-item">
                    <a class="nav-link" target="_blank" href="../index.php?id=<?= /** @var string $pageId_ */ $pageId_;?>"><i class="nav-link fa fa-external-link"></i> Vorschau</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div id="topbutton-wrapper" class=" shadow rounded-bottom" style="width:20pt;position: fixed;left:calc(50% - 52pt);text-align:center;margin:0;top:0;margin-top:-100pt;display:none;z-index:10000;background:linear-gradient(0deg,rgb(234, 237, 249) , rgba(234,237,249,0.5) 30% ,rgba(234,237,249,0.5) 70%,rgb(234, 237, 249)),url(../template/standardv5/img/material.jpeg);">
    <i class="fa fa-chevron-circle-down" id="main_seiten_edit_navi_open_button" style="cursor:pointer"></i>
</div>





<div id="sidebar-wrapper" class="p-3 shadow rounded-right" style="width:15%;min-width:100pt;max-width:250pt;height:100%;position: fixed;left:0;top:0;background:linear-gradient(90deg,rgb(234, 237, 249) , rgba(234,237,249,0.5)2% ,rgba(234,237,249,0.5)98%,rgb(234, 237, 249)), url(../template/standardv5/img/material.jpeg);z-index:10000;overflow: hidden;">
    <!-- Sidebar -->
    <nav id="">
        <div class="">
            <h5>Seitenbaum <i class="fa fa-chevron-circle-left" id="main_seiten_edit_page_close_button"></i></h5>
        </div>
        <?php /** @var string $pagetree */ echo $pagetree; ?>

    </nav>
</div>

<div id="sidebutton-wrapper" class="p-3 shadow  rounded-right rounded-bottom" style="position: fixed;left:0;top:20pt;background:linear-gradient(270deg,rgb(234, 237, 249) , rgba(234,237,249,0.5)15% ,rgba(234,237,249,0.5)85%,rgb(234, 237, 249)), url(../template/standardv5/img/material.jpeg);display: none;margin-left:-250pt;z-index:10000">
    <i class="fa fa-file-o" id="main_seiten_edit_page_open_button" style="cursor:pointer"></i>
</div>








<div id="menubar-wrapper" class="p-3 shadow rounded-left" style="width:25%;min-width:100pt;max-width:250pt;height:100%;position: fixed;right:0;top:0;background:linear-gradient(90deg,rgb(234, 237, 249) , rgba(234,237,249,0.5)2% ,rgba(234,237,249,0.5)98%,rgb(234, 237, 249)), url(../template/standardv5/img/material.jpeg);z-index:10000;overflow: scroll;">
    <!-- Sidebar -->
    <nav id="sidebar">

        <?php /** @var string $seitenEigenschaften */
        echo $seitenEigenschaften; ?>

</div>

<div id="menubutton-wrapper" class="p-3 shadow rounded-left rounded-bottom" style="position: fixed;right:0;top:20pt;background:linear-gradient(90deg,rgb(234, 237, 249) , rgba(234,237,249,0.5)15% ,rgba(234,237,249,0.5)85%,rgb(234, 237, 249)), url(../template/standardv5/img/material.jpeg);;display: none;margin-right:-350pt;z-index:10000">
    <i class="fa fa-align-justify" id="main_seiten_edit_properties_open" style="cursor:pointer"></i>
</div>

<?php /** @var string $seitenJavascript */
echo $seitenJavascript; ?>

<div id="dialog" title="">
</div>

<div class="toast fixed-bottom">
    <div class="toast-header">
    </div>
    <div class="toast-body">
    </div>
</div>
</body>
</html>
