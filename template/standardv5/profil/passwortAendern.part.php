<?php include('../admin/showDispatchMessageAsText.part.php'); ?>

<h1>Passwort ändern</h1>

<p>Geben Sie hier ihr ihr neues Passwort zweimal ein, um Tippfehler zu vermeiden.</p>
<form action="index.php" method="post" name="change_pw_box">
    <table width="100%" style="padding:5pt;">
        <tr><td id="neuespw1txt" class="body" width="50%">Neues Passwort</td><td><input type="password" name="neuespw1"></td></tr>
        <tr><td id="neuespw2txt" class="body" width="50%">Wiederholung</td><td><input type="password" name="neuespw2"></td></tr>
        <!--<tr><td class="subtitle" colspan="3">&nbsp;</td></tr>-->
        <tr><td class="body" width="50%" id="altespwtxt">Aktuelles Passwort</td><td><input type="password" name="altespw"></td></tr>
        <!--<tr><td class="subtitle" colspan="3">&nbsp;</td></tr>-->
        <tr><td class="body"><input type="hidden" name="aktion" value="pw_speichern"><input type="hidden" name="id" value="profil"></td>
            <td class="body"><input type="button" name="Speichern" value="Passwort &Auml;ndern" onclick="javascript:sendPasswort()">&nbsp;<input type="reset" name="Reset" value="Reset"></td></tr>
    </table>
</form>


<script>
    function checkPasswort(inputtxt){
        var passw =   /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
        if(inputtxt.match(passw)){
            return true;
        }else {
            return false;
        }
    }
    function sendPasswort(){

        document.change_pw_box.neuespw1.value=verschluesselung2(document.change_pw_box.neuespw1.value);
        document.change_pw_box.neuespw2.value=verschluesselung2(document.change_pw_box.neuespw2.value);
        document.change_pw_box.altespw.value=verschluesselung2(document.change_pw_box.altespw.value);
        document.change_pw_box.submit();
        return null;

        if(document.change_pw_box.neuespw1.value==""){
            $("#neuespw2txt").removeClass("fehler").text("Wiederholung");
            $("#altespwtxt").removeClass("fehler").text("Aktuelles Passwort");
            $("#neuespw1txt").addClass("fehler").text("Geben Sie das neue Passwort ein!");
        } else {
            if(checkPasswort(document.change_pw_box.neuespw1.value)){
                if(document.change_pw_box.neuespw1.value==document.change_pw_box.neuespw2.value){
                    if(document.change_pw_box.altespw.value!=""){
                        if(!(document.change_pw_box.neuespw1.value==document.change_pw_box.altespw.value)){
                            document.change_pw_box.neuespw1.value=verschluesselung2(document.change_pw_box.neuespw1.value);
                            document.change_pw_box.neuespw2.value=verschluesselung2(document.change_pw_box.neuespw2.value);
                            document.change_pw_box.altespw.value=verschluesselung2(document.change_pw_box.altespw.value);
                            document.change_pw_box.submit();
                        } else {
                            $("#neuespw2txt").removeClass("fehler").text("Wiederholung");
                            $("#altespwtxt").removeClass("fehler").text("Aktuelles Passwort");
                            $("#neuespw1txt").addClass("fehler").text("Das muss sich vom aktuellen Passwort unterscheiden!");
                        }
                    } else {
                        $("#neuespw2txt").removeClass("fehler").text("Wiederholung");
                        $("#altespwtxt").addClass("fehler").text("Geben Sie das aktuelle Passwort ein.");
                        $("#neuespw1txt").removeClass("fehler").text("Neues Passwort");
                    }
                } else {
                    $("#neuespw1txt").removeClass("fehler").text("Neues Passwort");
                    $("#altespwtxt").removeClass("fehler").text("Aktuelles Passwort");
                    $("#neuespw2txt").addClass("fehler").text("Geben Sie das neue Passwort ein zweites Mal ein.");
                }
            } else {
                $("#neuespw2txt").removeClass("fehler").text("Wiederholung");
                $("#altespwtxt").removeClass("fehler").text("Aktuelles Passwort");
                $("#neuespw1txt").addClass("fehler").text("Das Passwort muss aus 8-15 Zeichen bestehen und dabei mindestens einen Kleinbuchstaben, einen Großbuchstaben, eine Zahl und ein Sonderzeichen enthalten.");
            }

        }
    }
</script>