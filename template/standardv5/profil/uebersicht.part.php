<?php include('../admin/showDispatchMessageAsText.part.php'); ?>

<h1 class="subtitle">Einstellungen</h1>
<ul>
    <li><a href="?id=<?= /** @noinspection PhpUndefinedVariableInspection */
        $seiteId; ?>&aktion=pw_aendern">Passwort ändern</a></li>
    <li><a href="?id=<?=$seiteId; ?>&aktion=profil_aendern">Einstellungen ändern</li>
    <li><a href="?id=<?=$seiteId; ?>&aktion=avatar_hochladen">Avatar hochladen</a></li>
</ul>
<?php if(isset($box)) echo $box; ?>

<h1 class="subtitle">Autorenrechte</h1>
<p>Liste aller Dokumente, die man bearbeiten darf!</p>

<h1 class="subtitle">Hinweise zum Spielen</h1>
<p>Liste aller Charaktere, die man besitzt darf!</p>