<?php include('../admin/showDispatchMessageAsText.part.php'); ?>
<h1>Avatar hochladen</h1>
<table>
    <tr>
        <td><h1 class="subtitle">Aktuell</h1></td>
        <td><h1 class="subtitle">Aktueller Avatar</h1></td>
    </tr>
    <tr>
        <td><?= /** @noinspection PhpUndefinedVariableInspection */
            $avatarPicto; ?></td>
        <td><form action="index.php" method="post" enctype="multipart/form-data">
            <p>W&auml;hlen Sie eine Bildatei aus:</p>
            <p><input type="hidden" name="MAX_FILE_SIZE" value="506000" /><input name="neues_bild" type="file" size="50" /></p>
            <p>Mit dem Upload erklären Sie, dass sie die Rechte an diesem Bild besitzen. Andernfalls müssen wir das Bild gegebenfalls aus unserer Datenbank entfernen!</p>
            <hr>
            <p class="smalltext hinweis">&bullet; Je kleiner die Datei auf dem PC vorliegt, desto schneller wird sie hochgeladen.<br>
                &bullet; Es werden nur Bilder mit einer Maximalgröße von 500kiB=560kB angenommen.
            </p>
            <p><input type="hidden" name="aktion" value="avatar_speichern" /><input type="hidden" name="id" value="profil" /><input type="submit" name="Senden" value="Avatar hochladen"></p>
            </form>
        </td>
    </tr>
</table>