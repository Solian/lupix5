<?php include('../admin/showDispatchMessageAsText.part.php'); ?>
<h2>Passwort vergessen?</h2>
<p>Wenn Ihr vergessen habt, wie Euer Passwort lautete, dann gebt einfach euren Nutzernamen und die passende E-Mail-Adresse ein und klickt auf absenden. Ihr erhaltet dann ein Ersatzpasswort per E-Mail zugesandt.</p>
<form action="index.php?id=profil&aktion=neues_passwort_senden" method="post">
    <p style="text-align:center;">Benutzername: <input type="text" name="nickname"> &nbsp;&nbsp;&nbsp; E-Mail-Adresse: <input type="text" name="email"></p>
    <p style="text-align:right"><input type="submit" name="Submit" value="Passwort anfordern"></p>
</form>