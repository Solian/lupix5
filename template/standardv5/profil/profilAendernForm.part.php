<?php include('../admin/showDispatchMessageAsText.part.php'); ?>

<?php

/*
//Select Element generieren, um es dann ins Template einzufügen
$stilAuswahlString = '<select name="stil">';
    for ($i = 0; $i < count($listeDerStileNamenArray); $i++) {
    $stilAuswahlString .= '<option ';
    if ($benutzerDatenArray['style'] == $listeDerStileNamenArray[$i]["name"]) {
    $stilAuswahlString .= 'selected="selected"';
    }
    $stilAuswahlString .= '>' . $listeDerStileNamenArray[$i]["name"] . "</option>\n\n";
    }
    $stilAuswahlString .= '</select>';
*/

?>


<script>
    function checkProfilData(){

        var emailregex = /\S+@\S+\.\S+/;


        if($("#email").attr("value")==""){
            $("#emailtxt").text("Geben Sie eine E-Mail-Adresse ein!").addClass("fehler");
            $("#stiltxt").removeClass("fehler").text("Aktueller Stil");
        } else {
            if(!$("#email").attr("value").match(emailregex)){
                $("#emailtxt").text("Die E-Mail muss der gültigen Form entsprechen: [Nutzername]@[Domainname].[xxx]").addClass("fehler");
                $("#stiltxt").removeClass("fehler").text("Aktueller Stil");
            } else {
                if($("#stiltxt").attr("value")==""){
                    $("#emailtxt").text("E-Mail-Adresse").removeClass("fehler");
                    $("#stiltxt").addClass("fehler").text("Sie müssen einen Stil auswählen!");
                } else {
                    document.change_profil_data.submit();
                }
            }
        }
    }

</script>
<form action="index.php" method="post" name="change_profil_data">
    <input type="hidden" name="stil" value="">
    <table width="100%" style="padding:5pt;">
        <tr><td colspan="2"><hr></td></tr>
        <tr><td id="emailtxt" class="body" width="50%"><p>E-Mail-Adresse</p></td><td><input type="text" id="email" name="email" value="<?= /** @noinspection PhpUndefinedVariableInspection */
                $email;?>" size="<?= /** @noinspection PhpUndefinedVariableInspection */
                $lengthOfMail; ?>"></td></tr>
        <tr><td colspan="2"><p class="hinweis smalltext">Wenn Sie Ihre E-Mail Adresse ändern wollen, müssen Sie diese bestätigen. Dazu wird Ihnen eine E-Mail mit einem Bestätigungslink zugesandt.</p></td></tr>
        <!--<tr><td id="stiltxt" class="body" width="50%">Aktueller Stil</td><td><?= /** @noinspection PhpUndefinedVariableInspection */
        $stilAuswahlString; ?></td></tr>-->
        <tr><td class="body" width="50%" id="rea"><p>Realer Name</p></td><td><input type="text" id="realername" name="realername" value="<?= /** @noinspection PhpUndefinedVariableInspection */
                $realerName ;?>"></td></tr>
        <tr><td colspan="2"><hr></td></tr>
        <tr style="border:2pt solid #000;"><td class="body"><input type="hidden" name="aktion" value="profil_speichern"><input type="hidden" name="id" value="profil"></td>
            <td class="body"><input type="button" name="Speichern" value=" Speichern" onclick="javascript:checkProfilData()"></td></tr>
    </table>
</form>