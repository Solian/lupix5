<h1>Fehler beim E-Mail-Versand</h1>
<p class="fehler">Leider konnte Euch die E-Mail unter der Adresse "<?= /** @noinspection PhpUndefinedVariableInspection */
    $email;?>" nicht zugesendet werden.</p>
<p class="hinweis"><b>Lösung:</b> Bitte sendet eine E-Mail mit Eurem Spielernamen und dem Betreff "Registrierung" direkt an derwirt@heldenschenke.de.</p>
