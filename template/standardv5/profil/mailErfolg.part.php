<h1>E-Mail bestätigung</h1>
<p class="erfolg">Die Anmeldung war erfolgreich!</p>
<p style="display:none">Der Wirt der Heldenschenke ist leider etwas pingelig und muss &uuml;berpr&uuml;fen, ob Eure angegebene
    Adresse stimmt. Erst wenn Ihr auf seinen f&ouml;rmlichen Brief geantwortet habt, dürft Ihr Euch wirklich am Stammtisch niederlassen.</p>
<p>Wir eine Registrierungsmail an "<?= /** @noinspection PhpUndefinedVariableInspection */
    $email; ?>" gesendet, um die E-Mail-Adresse zu bestätigen.</p>
<ul>
    <li> Öffnet den Link in der E-Mail<br><b>oder</b><br> kopiert den dort angegebenen Link in Eure Adresszeile des Browsers und best&auml;tigt mit "Enter".</li>
    <li> Dann wird der Account entg&uuml;ltig freigeschaltet.</li>
</ul>
<p</p>
<p class="hinweis"><span class="ui-icon ui-icon-alert"></span> Nach einem Tagen erlischt der Link! Kontaktiert in diesem Fall die Administratoren.</p>