<?php




/**
 * Send a GET requst using cURL
 * @param string $url to request
 * @param array $get values to send
 * @param array $options for cURL
 * @param int $timeOut
 * @return string
 */
function curl_get($url, array $get = array(), array $options = array(),int $timeOut=10)
{
    $defaults = array(
        CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get),
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_TIMEOUT => $timeOut,
    );


    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if( ! $result = curl_exec($ch))
    {
        echo (curl_error($ch));
    }
    curl_close($ch);
    return $result;
}


$falseInt = file_put_contents('latest.zip', curl_get('https://lupix5.canior.de/latest.zip'));
if(!file_exists('download'))mkdir('download');

$zip = new ZipArchive;
if ($zip->open('latest.zip')) {
    if($zip->extractTo(__DIR__)){
        $zip->close();
        header('Location: admin/index.php?mod=install');

    }else{
        if($zip->getStatusString()=='No error'){
            header('Location: admin/index.php?mod=install');
        }else{
            echo "Die Datei wurde nicht entpackt:".$zip->getStatusString();
        }
    }

} else {
    echo 'Die Datei konnte nicht geöffnet werden:'.$zip->getStatusString();
}