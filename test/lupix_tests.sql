-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 05. Aug 2018 um 18:41
-- Server-Version: 10.1.34-MariaDB
-- PHP-Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `hs_tests`
--
CREATE DATABASE IF NOT EXISTS `lupix_tests` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `lupix_tests`;

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_admin_ber`
--

DROP TABLE IF EXISTS `main_admin_ber`;
CREATE TABLE `main_admin_ber` (
  `benutzer_id` int(11) NOT NULL,
  `bereich_id` int(11) NOT NULL,
  `lesen` tinyint(1) NOT NULL,
  `aendern` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_admin_ber`
--

TRUNCATE TABLE `main_admin_ber`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_admin_module`
--

DROP TABLE IF EXISTS `main_admin_module`;
CREATE TABLE `main_admin_module` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Modulkennung` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `opt_main_modul` tinyint(1) NOT NULL DEFAULT '0',
  `Scriptdatei` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Version` varchar(8) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `opt_nur_admin` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_admin_module`
--

TRUNCATE TABLE `main_admin_module`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_benutzer`
--

DROP TABLE IF EXISTS `main_benutzer`;
CREATE TABLE `main_benutzer` (
  `id` int(11) NOT NULL,
  `nickname` varchar(40) COLLATE latin1_german2_ci DEFAULT NULL,
  `style` varchar(25) COLLATE latin1_german2_ci DEFAULT NULL,
  `email` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `new_email_key` varchar(200) COLLATE latin1_german2_ci NOT NULL DEFAULT '""',
  `new_email_date` int(11) NOT NULL DEFAULT '0',
  `new_email` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `passwort` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `salt` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `adminkey` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `sessionkey` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `nutzergruppe_id` int(11) DEFAULT '1',
  `regdatum` varchar(11) COLLATE latin1_german2_ci DEFAULT NULL,
  `letztbesuch` int(11) DEFAULT NULL,
  `realername` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `avatar` text COLLATE latin1_german2_ci NOT NULL,
  `preavatar` text COLLATE latin1_german2_ci NOT NULL,
  `avatar_ok` varchar(1) COLLATE latin1_german2_ci NOT NULL,
  `opt_flash_navi` tinyint(1) NOT NULL DEFAULT '0',
  `zeig_pers_daten` tinyint(1) NOT NULL DEFAULT '0',
  `neue_pns_anzeigen` tinyint(1) NOT NULL DEFAULT '1',
  `neue_pns_mail` tinyint(1) NOT NULL DEFAULT '0',
  `threads_created` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `posts_created` mediumint(8) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 CHECKSUM=1 COLLATE=latin1_german2_ci PACK_KEYS=0;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_benutzer`
--

TRUNCATE TABLE `main_benutzer`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_captcha`
--

DROP TABLE IF EXISTS `main_captcha`;
CREATE TABLE `main_captcha` (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `zeit` int(11) NOT NULL,
  `operator` int(11) NOT NULL,
  `zahl1` int(11) NOT NULL,
  `zahl2` int(11) NOT NULL,
  `ergebnis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_captcha`
--

TRUNCATE TABLE `main_captcha`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_forum_posts`
--

CREATE TABLE IF NOT EXISTS `main_forum_posts` (
  `post_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8_bin NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_forum_posts`
--

TRUNCATE TABLE `main_forum_posts`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_forum_threads`
--

CREATE TABLE IF NOT EXISTS `main_forum_threads` (
  `thread_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(30) COLLATE utf8_bin NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `state` tinyint(4) NOT NULL DEFAULT '0',
  `author_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`thread_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_forum_threads`
--

TRUNCATE TABLE `main_forum_threads`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_logs`
--

DROP TABLE IF EXISTS `main_logs`;
CREATE TABLE `main_logs` (
  `Fehlerkey` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `datum` int(11) NOT NULL,
  `fehlernummer` int(11) NOT NULL,
  `Modul` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Skriptname` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Fehlernachricht` varchar(512) COLLATE latin1_german2_ci NOT NULL,
  `zeilennummer` int(11) NOT NULL,
  `Verfolgung` varchar(2000) COLLATE latin1_german2_ci NOT NULL,
  `ExecCodeAusgabe` varchar(1000) COLLATE latin1_german2_ci NOT NULL,
  `nutzer_id` int(11) NOT NULL,
  `fehlerniveau` int(11) NOT NULL,
  `checked_datum` int(11) NOT NULL DEFAULT '0',
  `checked_admin_userid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_logs`
--

TRUNCATE TABLE `main_logs`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_logs_niveaus`
--

DROP TABLE IF EXISTS `main_logs_niveaus`;
CREATE TABLE `main_logs_niveaus` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Kommentar` varchar(2500) COLLATE latin1_german2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_logs_niveaus`
--

TRUNCATE TABLE `main_logs_niveaus`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_nasy_empfaenger`
--

DROP TABLE IF EXISTS `main_nasy_empfaenger`;
CREATE TABLE `main_nasy_empfaenger` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `recipient_id` int(10) UNSIGNED NOT NULL,
  `was_read` tinyint(1) NOT NULL DEFAULT '0',
  `delete_recipient` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_nasy_empfaenger`
--

TRUNCATE TABLE `main_nasy_empfaenger`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_nasy_nachrichten`
--

DROP TABLE IF EXISTS `main_nasy_nachrichten`;
CREATE TABLE `main_nasy_nachrichten` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `message_subject` varchar(50) COLLATE utf8_bin NOT NULL,
  `message_content` text COLLATE utf8_bin NOT NULL,
  `message_date` datetime NOT NULL,
  `message_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `delete_sender` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_nasy_nachrichten`
--

TRUNCATE TABLE `main_nasy_nachrichten`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_navicontainer`
--

DROP TABLE IF EXISTS `main_navicontainer`;
CREATE TABLE `main_navicontainer` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Text` text COLLATE latin1_german2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_navicontainer`
--

TRUNCATE TABLE `main_navicontainer`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_navigruppen`
--

DROP TABLE IF EXISTS `main_navigruppen`;
CREATE TABLE `main_navigruppen` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Titel` varchar(255) COLLATE latin1_german2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_navigruppen`
--

TRUNCATE TABLE `main_navigruppen`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_navigruppen_elemente`
--

DROP TABLE IF EXISTS `main_navigruppen_elemente`;
CREATE TABLE `main_navigruppen_elemente` (
  `navigruppe_id` int(11) DEFAULT NULL,
  `seite_id` int(11) DEFAULT NULL,
  `navicontainer_id` int(11) DEFAULT NULL,
  `list_index` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_navigruppen_elemente`
--

TRUNCATE TABLE `main_navigruppen_elemente`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_nutzergruppen`
--

DROP TABLE IF EXISTS `main_nutzergruppen`;
CREATE TABLE `main_nutzergruppen` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Kommentar` text COLLATE latin1_german2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci DELAY_KEY_WRITE=1;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_nutzergruppen`
--

TRUNCATE TABLE `main_nutzergruppen`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_picto_bilder`
--

DROP TABLE IF EXISTS `main_picto_bilder`;
CREATE TABLE `main_picto_bilder` (
  `id` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Bildtitel` varchar(500) COLLATE latin1_german2_ci NOT NULL,
  `Beschriftung` varchar(500) COLLATE latin1_german2_ci NOT NULL,
  `picto_kategorie` int(11) NOT NULL,
  `Cssid` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Pfad` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Alternativtext` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `size` int(11) NOT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_picto_bilder`
--

TRUNCATE TABLE `main_picto_bilder`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_picto_kategorien`
--

DROP TABLE IF EXISTS `main_picto_kategorien`;
CREATE TABLE `main_picto_kategorien` (
  `id` int(11) NOT NULL,
  `Name` varchar(300) COLLATE latin1_german2_ci NOT NULL,
  `Kommentar` text COLLATE latin1_german2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_picto_kategorien`
--

TRUNCATE TABLE `main_picto_kategorien`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_seiten`
--

DROP TABLE IF EXISTS `main_seiten`;
CREATE TABLE `main_seiten` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `Titel` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `navigruppe_id` int(11) NOT NULL,
  `text_id` int(11) NOT NULL,
  `opt_cachen` tinyint(1) NOT NULL DEFAULT '0',
  `opt_verbergen` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 Sichtbar, 1 Verborgen (sehen nur Admins im editieren modus oder so)',
  `opt_zus_navi_position` tinyint(11) NOT NULL DEFAULT '1' COMMENT '1 oben (Standard), 2 drunter, 0 unterdrücke Navicontainer',
  `Skriptdatei` text COLLATE latin1_german2_ci NOT NULL,
  `Titelbild` text COLLATE latin1_german2_ci NOT NULL,
  `ueber_id` int(11) NOT NULL,
  `opt_edit_nur_admin` tinyint(11) NOT NULL COMMENT '1 Nur Admins dürfen aendern'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_seiten`
--

TRUNCATE TABLE `main_seiten`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_seiten_ber`
--

DROP TABLE IF EXISTS `main_seiten_ber`;
CREATE TABLE `main_seiten_ber` (
  `seite_id` int(11) DEFAULT NULL,
  `texte_id` int(11) DEFAULT NULL,
  `nutzergruppe_id` int(11) DEFAULT NULL,
  `benutzer_id` int(11) DEFAULT NULL,
  `lesen` tinyint(1) DEFAULT '1',
  `aendern` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_seiten_ber`
--

TRUNCATE TABLE `main_seiten_ber`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_seiten_navigruppen`
--

DROP TABLE IF EXISTS `main_seiten_navigruppen`;
CREATE TABLE `main_seiten_navigruppen` (
  `seite_id` int(11) NOT NULL,
  `navigruppe_id` int(11) NOT NULL,
  `list_index` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_seiten_navigruppen`
--

TRUNCATE TABLE `main_seiten_navigruppen`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_stile`
--

DROP TABLE IF EXISTS `main_stile`;
CREATE TABLE `main_stile` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE latin1_german2_ci NOT NULL,
  `standardstil` int(1) NOT NULL DEFAULT '0',
  `css` varchar(20) COLLATE latin1_german2_ci NOT NULL,
  `font` varchar(20) COLLATE latin1_german2_ci NOT NULL,
  `fs` int(5) NOT NULL,
  `standardbgcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `standardtxtcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `link` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `visited` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `hover` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subtbgcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subttxtcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subtlink` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subtvlink` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subthover` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subtfs` int(14) NOT NULL,
  `subhbgcol` varchar(141) COLLATE latin1_german2_ci NOT NULL,
  `subhtxtcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subhlink` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subhvlink` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subhhover` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `subhfs` int(14) NOT NULL,
  `smalltextfs` int(14) NOT NULL,
  `bigfs` int(14) NOT NULL DEFAULT '15',
  `altbgcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `alttxtcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `navtxtcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `navbgcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `fehlerbgcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `fehlertxtcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `fehlerfs` int(5) NOT NULL,
  `formbgcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `formtxtcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `bordercol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `befont` varchar(20) COLLATE latin1_german2_ci NOT NULL,
  `befs` int(5) NOT NULL,
  `schankraum` varchar(20) COLLATE latin1_german2_ci NOT NULL,
  `fbeitborderspace` int(5) NOT NULL,
  `fbeitcol` varchar(14) COLLATE latin1_german2_ci NOT NULL,
  `fsa` int(5) NOT NULL,
  `fsb` int(5) NOT NULL,
  `fsc` int(5) NOT NULL,
  `fsd` int(5) NOT NULL,
  `fse` int(5) NOT NULL,
  `fsf` int(5) NOT NULL,
  `fsg` int(5) NOT NULL,
  `fsh` int(5) NOT NULL,
  `fsi` int(5) NOT NULL,
  `fsj` int(5) NOT NULL,
  `fsk` int(5) NOT NULL,
  `fsl` int(5) NOT NULL,
  `fsm` int(5) NOT NULL,
  `fsn` int(5) NOT NULL,
  `fso` int(5) NOT NULL,
  `fsp` int(5) NOT NULL,
  `fsq` int(5) NOT NULL,
  `fsr` int(5) NOT NULL,
  `fss` int(5) NOT NULL,
  `fst` int(5) NOT NULL,
  `fsu` int(5) NOT NULL,
  `fsv` int(5) NOT NULL,
  `fsw` int(5) NOT NULL,
  `fsx` int(5) NOT NULL,
  `fsy` int(5) NOT NULL,
  `fsz` int(5) NOT NULL,
  `fsä` int(5) NOT NULL,
  `fsö` int(5) NOT NULL,
  `fsü` int(5) NOT NULL,
  `xka` int(5) NOT NULL,
  `xkb` int(5) NOT NULL,
  `xkc` int(5) NOT NULL,
  `xkd` int(5) NOT NULL,
  `xke` int(5) NOT NULL,
  `xkf` int(5) NOT NULL,
  `xkg` int(5) NOT NULL,
  `xkh` int(5) NOT NULL,
  `xki` int(5) NOT NULL,
  `xkj` int(5) NOT NULL,
  `xkk` int(5) NOT NULL,
  `xkl` int(5) NOT NULL,
  `xkm` int(5) NOT NULL,
  `xkn` int(5) NOT NULL,
  `xko` int(5) NOT NULL,
  `xkp` int(5) NOT NULL,
  `xkq` int(5) NOT NULL,
  `xkr` int(5) NOT NULL,
  `xks` int(5) NOT NULL,
  `xkt` int(5) NOT NULL,
  `xku` int(5) NOT NULL,
  `xkv` int(5) NOT NULL,
  `xkw` int(5) NOT NULL,
  `xkx` int(5) NOT NULL,
  `xky` int(5) NOT NULL,
  `xkz` int(5) NOT NULL,
  `xkä` int(5) NOT NULL,
  `xkö` int(5) NOT NULL,
  `xkü` int(5) NOT NULL,
  `yka` int(5) NOT NULL,
  `ykb` int(5) NOT NULL,
  `ykc` int(5) NOT NULL,
  `ykd` int(5) NOT NULL,
  `yke` int(5) NOT NULL,
  `ykf` int(5) NOT NULL,
  `ykg` int(5) NOT NULL,
  `ykh` int(5) NOT NULL,
  `yki` int(5) NOT NULL,
  `ykj` int(5) NOT NULL,
  `ykk` int(5) NOT NULL,
  `ykl` int(5) NOT NULL,
  `ykm` int(5) NOT NULL,
  `ykn` int(5) NOT NULL,
  `yko` int(5) NOT NULL,
  `ykp` int(5) NOT NULL,
  `ykq` int(5) NOT NULL,
  `ykr` int(5) NOT NULL,
  `yks` int(5) NOT NULL,
  `ykt` int(5) NOT NULL,
  `yku` int(5) NOT NULL,
  `ykv` int(5) NOT NULL,
  `ykw` int(5) NOT NULL,
  `ykx` int(5) NOT NULL,
  `yky` int(5) NOT NULL,
  `ykz` int(5) NOT NULL,
  `ykä` int(5) NOT NULL,
  `ykö` int(5) NOT NULL,
  `ykü` int(5) NOT NULL,
  `ringid` int(5) NOT NULL,
  `naviid` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_stile`
--

TRUNCATE TABLE `main_stile`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `main_texte`
--

DROP TABLE IF EXISTS `main_texte`;
CREATE TABLE `main_texte` (
  `id` int(11) NOT NULL,
  `seite_id` int(11) NOT NULL,
  `autor_id` int(11) NOT NULL,
  `erstell_datum` int(11) NOT NULL,
  `aenderung_datum` int(11) NOT NULL,
  `Titel` text COLLATE latin1_german2_ci NOT NULL,
  `Text` text COLLATE latin1_german2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- TRUNCATE Tabelle vor dem Einfügen `main_texte`
--

TRUNCATE TABLE `main_texte`;
-- --------------------------------------------------------

--
-- Indizes für die Tabelle `main_admin_module`
--
ALTER TABLE `main_admin_module`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `main_benutzer`
--
ALTER TABLE `main_benutzer`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `main_logs`
--
ALTER TABLE `main_logs`
  ADD PRIMARY KEY (`Fehlerkey`);

--
-- Indizes für die Tabelle `main_nasy_empfaenger`
--
ALTER TABLE `main_nasy_empfaenger`
  ADD UNIQUE KEY `message_recioient_id` (`message_id`,`recipient_id`);

--
-- Indizes für die Tabelle `main_nasy_nachrichten`
--
ALTER TABLE `main_nasy_nachrichten`
  ADD PRIMARY KEY (`message_id`);

--
-- Indizes für die Tabelle `main_navicontainer`
--
ALTER TABLE `main_navicontainer`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `main_navigruppen`
--
ALTER TABLE `main_navigruppen`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `main_nutzergruppen`
--
ALTER TABLE `main_nutzergruppen`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `main_picto_bilder`
--
ALTER TABLE `main_picto_bilder`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `main_picto_bilder` ADD FULLTEXT KEY `Volltextindex` (`Name`,`Bildtitel`,`Beschriftung`,`Alternativtext`);

--
-- Indizes für die Tabelle `main_picto_kategorien`
--
ALTER TABLE `main_picto_kategorien`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indizes für die Tabelle `main_seiten`
--
ALTER TABLE `main_seiten`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `main_stile`
--
ALTER TABLE `main_stile`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `main_texte`
--
ALTER TABLE `main_texte`
  ADD PRIMARY KEY (`id`);

-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `main_admin_module`
--
ALTER TABLE `main_admin_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_benutzer`
--
ALTER TABLE `main_benutzer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_nasy_nachrichten`
--
ALTER TABLE `main_nasy_nachrichten`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_navicontainer`
--
ALTER TABLE `main_navicontainer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_navigruppen`
--
ALTER TABLE `main_navigruppen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_nutzergruppen`
--
ALTER TABLE `main_nutzergruppen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_picto_kategorien`
--
ALTER TABLE `main_picto_kategorien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_seiten`
--
ALTER TABLE `main_seiten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_stile`
--
ALTER TABLE `main_stile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `main_texte`
--
ALTER TABLE `main_texte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
