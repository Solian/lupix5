<?php

/**
 * Description of testBasic
 *
 * @author markus
 */
require_once(__DIR__ . '/../lib/standardv5/db.lib.php');

class testBasics {

    /**
     * @param string $test
     * @throws Exception
     */
    public static function generateTestDB($test = '') {

        print("\nInit db for test class $test!\n");

        echo "\n\nGITLAB gesetzt:".(defined('GITLAB')==TRUE?'Jap':'Nope')."\n";

        if(defined('GITLAB')){
            db::setConfigPath(__DIR__.'/gitlab.config.ini.php');
        }

        db::connect();


        require_once(__DIR__.'/../admin/lupix.sql.php');

        $sql = DbInstallQuery('lupix_tests');
        db::query($sql);
        db::query('use lupix_tests');
        db::query('SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = "lupix_tests"');


    }

}
