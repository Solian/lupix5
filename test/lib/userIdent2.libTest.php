<?php

use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../testBasics.php');
require_once(__DIR__ . '/../../lib/standardv5/funktionen.lib.php');
require_once(__DIR__ . '/../../lib/standardv5/userIdent.lib.php');
require_once(__DIR__ . '/../../lib/standardv5/benutzer.lib.php');

/**
 * Test
 */
class UserIdentTest2 extends TestCase {

    /**
     * Called before the first test of the test case class is run.
     * @throws Exception
     */
    public static function setUpBeforeClass():void {
        testBasics::generateTestDB(get_called_class());
        $userIdent = new UserIdentity();
        $userIdent->id=1;
        $userIdent->group_id=1;
        benutzerArchive::init($userIdent);
    }

    /**
     * This method is called before a test is executed
     * @throws Exception
     */
    protected function setUp() :void{
        db::query('TRUNCATE main_benutzer');
        $this->insertTestUsers();
    }

    /**
     * This method is called after a test is executed.
     */
    protected function tearDown():void {

    }

    /**
     * @throws Exception
     */
    protected function insertTestUsers() {

        //Der unbekannte Gast ist Pflicht!
        db::insert("insert main_benutzer (id,nickname,nutzergruppe_id) values (0,'unbekannter Gast',0) ");

        list($userIdHans,$eMailIdString)=benutzerArchive::create_benutzer('Hans','Test','hans@test.de');
        benutzerArchive::confirm_benutzer_mail($eMailIdString);
        benutzerArchive::set_benutzer_gruppe($userIdHans,USERGROUPS::ADMIN);

        list($userIdKurt,$eMailIdString)=benutzerArchive::create_benutzer('Kurt','Test','kurt@test.de');
        benutzerArchive::confirm_benutzer_mail($eMailIdString);
        benutzerArchive::set_benutzer_gruppe($userIdKurt,USERGROUPS::USER);

        list($userIdMax,$eMailIdString)=benutzerArchive::create_benutzer('Max','Test','max@test.de');
        benutzerArchive::confirm_benutzer_mail($eMailIdString);
        benutzerArchive::set_benutzer_gruppe($userIdMax,USERGROUPS::BLOCKED);

    }

    public function testCreateSessionWithUnknownUser(){

        $newSession = lulo::createNewSession();
        $this->assertInstanceOf('session',$newSession);

    }

    /**
     * @throws Exception
     */
    public function testRecogniseSessionOfAnUnknownUser(){
        $newSession = lulo::createNewSession();
        $loadedSession = lulo::loadSession($newSession->sessionkey);
        $this->assertEquals($newSession,$loadedSession);
        $this->assertEquals(lulo::AUTH_UNIDENTIFIED,$loadedSession->loginStatus);
    }

    /**
     * @throws Exception
     */

    public function testPrepareLogin(){

        $session = lulo::loadSession(lulo::createNewSession()->sessionkey);

        //let h1 = sha512($('#loginname').val() + '<?=$challenge;
        $h1 = hash('sha512','kurt@test.de'.$session->challenge);


        //
        //  Dazu generiert er ein RSA-1024bit-Schlüsselpaar g_priv, g_pub
        //  let encrypt = new JSEncrypt({default_key_size: 2048});
        //  encrypt.getKey();

        $res = openssl_pkey_new(["private_key_bits" => 2048]);
        $g_priv = '';
        openssl_pkey_export($res, $g_priv);

        // Extract the public key from $res to $pubKey
        $g_pub = openssl_pkey_get_details($res);
        $g_pub = $g_pub["key"];

        //Der Server sendet l,c1,k_pub
        $loginPreperationCredetials = lulo::prepareLogin($session,$h1,$g_pub);

        openssl_private_decrypt(base64_decode($loginPreperationCredetials['c1']), $o, $g_priv);

        //Die Sessiondaten müssen übereinstimmen
        $this->assertEquals($session->challenge,$o);
        $this->assertEquals($session->sessionkey,$loginPreperationCredetials['l']);
        $this->assertEquals(lulo::AUTH_GOT_USER_WITH_PUB_KEY,$session->loginStatus);
    }

    /**
     * @throws Exception
     */

    public function testSavePreparedSessionToDatabase(){
        $session = lulo::loadSession(lulo::createNewSession()->sessionkey);

        //let h1 = sha512($('#loginname').val() + '<?=$challenge;
        $h1 = hash('sha512','kurt@test.de'.$session->challenge);

        //
        //  Dazu generiert er ein RSA-1024bit-Schlüsselpaar g_priv, g_pub
        //  let encrypt = new JSEncrypt({default_key_size: 2048});
        //  encrypt.getKey();

        $res = openssl_pkey_new(["private_key_bits" => 2048]);
        $g_priv = '';
        openssl_pkey_export($res, $g_priv);

        // Extract the public key from $res to $pubKey
        $g_pub = openssl_pkey_get_details($res);
        $g_pub = $g_pub["key"];

        //Der Server sendet l,c1,k_pub
        $loginPreperationCredetials = lulo::prepareLogin($session,$h1,$g_pub);

        openssl_private_decrypt(base64_decode($loginPreperationCredetials['c1']), $o, $g_priv);

        //LAden der Daten aus der Datenbank
        $session = lulo::loadSession($session->sessionkey);

        $this->assertNotFalse($session);

        //Die Sessiondaten müssen übereinstimmen
        $this->assertEquals($session->challenge,$o);
        $this->assertEquals($session->sessionkey,$loginPreperationCredetials['l']);
        $this->assertEquals(lulo::AUTH_GOT_USER_WITH_PUB_KEY,$session->loginStatus);
    }

    /**
     * @throws Exception
     */

    public function testLoginUserWithPasswortSaveToDatabase(){
        $session = lulo::loadSession(lulo::createNewSession()->sessionkey);

        //let h1 = sha512($('#loginname').val() + '<?=$challenge;
        $h1 = hash('sha512','kurt@test.de'.$session->challenge);

        //  Dazu generiert er ein RSA-1024bit-Schlüsselpaar g_priv, g_pub
        //  let encrypt = new JSEncrypt({default_key_size: 2048});
        //  encrypt.getKey();

        $res = openssl_pkey_new(["private_key_bits" => 2048]);
        $g_priv = '';
        openssl_pkey_export($res, $g_priv);

        // Extract the public key from $res to $pubKey
        $g_pub = openssl_pkey_get_details($res);
        $g_pub = $g_pub["key"];

        //Der Server sendet l,c1,k_pub
        $loginPreperationCredetials = lulo::prepareLogin($session,$h1,$g_pub);

        //Der Client entschlüsselt mit seinem privaten RSA-SChlüssel g_priv aus dem Chiffrat c1 --> die Challange o
        //let o = encrypt.decrypt(answer.c1);
        openssl_private_decrypt(base64_decode($loginPreperationCredetials['c1']), $o, $g_priv);

        //er stellt erneut die Antwort h2=h(u,o) aus dem Benutzernamen und der neuen Challange o
        //let h1 = sha512($('#loginname').val() + '<?=$challenge;
        $h2 = hash('sha512','kurt@test.de'.$o);

        //er verschlüsselt das Passwort c2=enc(pw,k_pub)
        //Encrypt with the public key...
        //var encrypt2 = new JSEncrypt();
        //encrypt2.setPublicKey(answer.k_pub);
        //$('#inputC2').val(encrypt2.encrypt($('#loginpasswort').val()));
        $c2 = '';
        openssl_public_encrypt('Test', $c2, $loginPreperationCredetials['k_pub']);
        $c2 = base64_encode($c2);

        $session = lulo::loadSession($loginPreperationCredetials['l']);
        // sendet l,c2,h2 an den Server, neue Seite wird aufgerufen, die RSA-SChlüssel des Clienten gehen verloren!
        //('#loginbox').submit();
        $session = lulo::login($session, $h2,$c2);
        $this->assertInstanceOf('session',$session);
        $this->assertEquals(lulo::AUTH_AUTHENTICATED,$session->loginStatus);

        //der neue Sessionkey m wird als Cookie gesetzt und die Standardseite geladen!
        //setcookie('sessionkey', $session->sessionkey);

    }

    /**
     * @throws Exception
     */
    public function testIdentifyUnknownUser()
    {
        $session = lulo::loadSession(lulo::createNewSession()->sessionkey);
        $userIdent = lulo::identify($session);

        $this->assertEquals(0,$userIdent->id);
        $this->assertEquals(0,$userIdent->group_id);
    }

    /**
     * @throws Exception
     */
    public function testIdentifyBlockedUser()
    {
        $session = lulo::loadSession(lulo::createNewSession()->sessionkey);
        $session->userId=3;
        $session->loginStatus=lulo::AUTH_AUTHENTICATED;
        $userIdent = lulo::identify($session);

        $this->assertEquals(3,$userIdent->id);
        $this->assertEquals(3,$userIdent->group_id);
    }

    /**
     * @throws Exception
     */

    public function testIdentifyLoggedInUser(){

        $session = lulo::loadSession(lulo::createNewSession()->sessionkey);

        //let h1 = sha512($('#loginname').val() + '<?=$challenge;
        $h1 = hash('sha512','kurt@test.de'.$session->challenge);

        //  Dazu generiert er ein RSA-1024bit-Schlüsselpaar g_priv, g_pub
        //  let encrypt = new JSEncrypt({default_key_size: 2048});
        //  encrypt.getKey();

        $res = openssl_pkey_new(["private_key_bits" => 2048]);
        $g_priv = '';
        openssl_pkey_export($res, $g_priv);

        // Extract the public key from $res to $pubKey
        $g_pub = openssl_pkey_get_details($res);
        $g_pub = $g_pub["key"];

        //Der Server sendet l,c1,k_pub
        $loginPreperationCredetials = lulo::prepareLogin($session,$h1,$g_pub);

        //Der Client entschlüsselt mit seinem privaten RSA-SChlüssel g_priv aus dem Chiffrat c1 --> die Challange o
        //let o = encrypt.decrypt(answer.c1);
        openssl_private_decrypt(base64_decode($loginPreperationCredetials['c1']), $o, $g_priv);

        //er stellt erneut die Antwort h2=h(u,o) aus dem Benutzernamen und der neuen Challange o
        //let h1 = sha512($('#loginname').val() + '<?=$challenge;
        $h2 = hash('sha512','kurt@test.de'.$o);

        //er verschlüsselt das Passwort c2=enc(pw,k_pub)
        //Encrypt with the public key...
        //var encrypt2 = new JSEncrypt();
        //encrypt2.setPublicKey(answer.k_pub);
        //$('#inputC2').val(encrypt2.encrypt($('#loginpasswort').val()));
        $c2 = '';
        openssl_public_encrypt('Test', $c2, $loginPreperationCredetials['k_pub']);
        $c2 = base64_encode($c2);

        $session = lulo::loadSession($loginPreperationCredetials['l']);
        // sendet l,c2,h2 an den Server, neue Seite wird aufgerufen, die RSA-SChlüssel des Clienten gehen verloren!
        //('#loginbox').submit();
        $session = lulo::login($session, $h2,$c2);
        $this->assertEquals(lulo::AUTH_AUTHENTICATED,$session->loginStatus);

        //der neue Sessionkey m wird als Cookie gesetzt und die Standardseite geladen!
        //setcookie('sessionkey', $session->sessionkey);

        $userIdent = lulo::identify($session);

        $this->assertEquals(2,$userIdent->id);
        $this->assertEquals(2,$userIdent->group_id);
    }


}
