<?php

use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../testBasics.php');
require_once (__DIR__. '/../../lib/standardv5/standard.lib.php');
require_once(__DIR__ . '/../../lib/standardv5/template.lib.php');
require_once(__DIR__ . '/../../lib/standardv5/controllerBaseClass.php');



class controllerTestBaseClass extends frontendControllerClass
{

    function __construct($userident, $main)
    {
        parent::__construct($userident, $main);
    }

    /**
     * @param string $testString
     */

    function testFuncAction(string $testString)
    {
        echo "Hallo Welt:$testString";
    }

    /**
     * @param int $testInt
     */

    function testIntegerValueAction(int $testInt)
    {
        echo "Zahl:$testInt " . gettype($testInt);
    }

    /**
     * @param string $testString
     * @param string $optionalTestString
     */

    function testFuncNoOptionalParametersAction(string $testString, string $optionalTestString = "Preset Value")
    {
        echo "Hallo Welt:$testString   Preset Value:$optionalTestString";
    }

    /**
     * @throws Exception
     */
    function testFuncUseTemplateWithoutVariablesAction()
    {
        $this->registerTemplate('standardv5/test/controller_test');
    }

    /**
     * @throws Exception
     */
    function testFuncUseTemplateWithVariablesAction()
    {
        $this->registerTemplate('standardv5/test/controller_test_with_variable');
        $this->registerTemplateVariable('wort', 'richtig');
    }

    /**
     * @throws ReflectionException|Exception
     */
    function testFuncDispatchingResetsTemplateAction()
    {
        $this->registerTemplate('standardv5/test/controller_test_with_variable');
        $this->registerTemplateVariable('wort', 'falsch');
        $this->dispatch('testFuncUseTemplateWithVariables');
    }

    /**
     * @throws ReflectionException|Exception
     */

    function testFuncDispatchingWithDispathingMessageAction()
    {
        $this->setDispatchingMessage('Important Message');
        $this->dispatch('testFuncDispatchedWithDispathingMessage');
    }

    function testFuncDispatchedWithDispathingMessageAction()
    {
        echo $this->getDispatchingMessage();
    }
}

class controllerTestPreloadBaseClass extends frontendControllerClass
{

    public function preload()
    {
        echo "LOL";
    }

    public function testFuncDispatchingWithPreloadFunctionAction()
    {
        echo "Hallo";
    }

}

class controllerTestPreloadTemplateBaseClass extends frontendControllerClass
{
    /**
     * @throws Exception
     */

    public function preload()
    {
        $this->registerTemplate('standardv5/test/controller_test_with_variable');
        $this->registerTemplateVariable('wort', 'richtig');
    }


    public function testFuncDispatchingWithPreloadFunctionAndTemplateAction()
    {

    }

}

class mockMain extends stdClass
{

    public $_GLOBAL_VARS = array();


    function __construct()
    {
        //parent::__construct();
        $this->_GLOBAL_VARS['POST'] = $_POST;
        $this->_GLOBAL_VARS['GET'] = $_GET;
        $this->_GLOBAL_VARS['SERVER'] = $_SERVER;
        $this->_GLOBAL_VARS['FILES'] = $_FILES;
        $this->_GLOBAL_VARS['COOKIE'] = [];
    }

    function get_SITE_CONTENT()
    {
        return 1;
    }

    function get_($div, $offset = '')
    {
        if ($offset != '') {
            if (array_key_exists($offset, $this->_GLOBAL_VARS[$div])) {
                return $this->_GLOBAL_VARS[$div][$offset];
            } else {
                return false;
            }
        } else {
            if (array_key_exists($div, $this->_GLOBAL_VARS)) {
                return $this->_GLOBAL_VARS[$div];
            } else {
                return false;
            }
        }
    }


    /**
     * @param string $variableName
     * @param string $variablenTyp
     * @param bool $variableIsOptional
     * @return bool|int|mixed
     * @throws Exception
     */

    public function loadParameter(string $variableName, string $variablenTyp, bool$variableIsOptional ){
        $inputValue =0;
        $varIsSet=false;
        //Wert einer Postvariable bestimmen
        if (array_key_exists($variableName, $this->get_("POST"))) {
//                    $pretext.='<div><h1>'.$variableName.'</h1><div style="border:2pt red dottet;">'.$_POST[$variableName].'</div></div>';
            $inputValue = filter_var(
                $_POST[$variableName],
                ($variablenTyp == 'string' ? FILTER_SANITIZE_STRING : (
                $variablenTyp == 'bool' ? FILTER_VALIDATE_BOOLEAN : FILTER_VALIDATE_INT)
                )
            );
            $varIsSet=true;
//                    $pretext.='<div><h1>'.$variableName.' sanitzed</h1><div style="border:2pt red dottet;">'.$inputValue.'</div></div>';
        }

        //Wert einer GET Variable ohne POST bestimmen
        if (array_key_exists($variableName, $this->get_("GET")) && !array_key_exists($variableName, $this->get_("POST"))) {
            $inputValue = filter_var(
                $_GET[$variableName],
                ($variablenTyp == 'string' ? FILTER_SANITIZE_STRING : (
                $variablenTyp == 'bool' ? FILTER_VALIDATE_BOOLEAN : FILTER_VALIDATE_INT)
                )
            );
            $varIsSet=true;
        }

        //TODO: Wert einer FILES Variable bestimmen

        //Wenn eine Variable nicht gesetzt ist, aber sie ist boolsch, wird sie auf null gestellt.
        if(!$varIsSet && $variablenTyp=='bool'){
            $inputValue=false;
            $varIsSet=true;
        }

        //Wenn die Variable nicht gesetzt ist und sie ist nicht optional --> Fehler
        if (!$varIsSet && !$variableIsOptional) {
            throw new Exception(' fehlerhafter Paramter:' . $variableName .
                ' erwarteter Wert:' . $variablenTyp .
                ' übergebener Wert: $_POST:' . (print_r($this->get_("POST"), true)) . '$_GET:' . (print_r($this->get_("GET"), true)), 28);

        }

        //Sie ist nicht gsetzt aber auch nur optional --> NIX

        return [$varIsSet,$inputValue];

    }
}


/**
 * Tests for the database library.
 */
class controllerBaseClassTest extends TestCase
{


    /**
     * @throws ReflectionException|Exception
     */
    function testDispatchFunction()
    {
        $_SERVER=[];$_COOKIE=[];
        $_GET['testString'] = 'hallo';

        $testObject = new stdClass();
        //$mockMain = new mockMain();
        $main = new seitenklasse();
        $main->catchVariables();


        $controller = new controllerTestBaseClass($testObject, $main);

        self::assertEquals('Hallo Welt:hallo', $controller->dispatch('testFunc'));
    }
    /**
     * @throws ReflectionException|Exception
     */

    function testDispatchInt()
    {
        $_SERVER=[];$_COOKIE=[];
        $_GET['testInt'] = 1;

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        self::assertEquals('Zahl:1 integer', $controller->dispatch('testIntegerValue'));
    }
    /**
     * @throws ReflectionException|Exception
     */

    function testStringCannotBeParsedToInteger()
    {
        $_SERVER=[];$_COOKIE=[];
        $_GET['testInt'] = "Hallo074456";

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        self::assertEquals('Zahl:0 integer', $controller->dispatch('testIntegerValue'));
    }

    function testActionDoesNotExist()
    {
        $_SERVER=[];$_COOKIE=[];
        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        try {
            $controller->dispatch('test_Func_029384489283');
        } catch (Throwable $e) {
            self::assertEquals(29, $e->getCode());
        }
    }

    function testNotAllNecessaryParamtersAreGiven()
    {
        $_SERVER=[];$_COOKIE=[];
        unset($_GET['testString']);

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        try {
            $controller->dispatch('testFunc');
        } catch (Throwable $e) {
            self::assertEquals(28, $e->getCode());
        }
    }
    /**
     * @throws ReflectionException|Exception
     */
    function testAllNecessaryButNoOptionalParamtersAreGiven()
    {
        $_SERVER=[];$_COOKIE=[];
        $_GET['testString'] = 'hallo';

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        self::assertEquals('Hallo Welt:hallo   Preset Value:Preset Value', $controller->dispatch('testFuncNoOptionalParameters'));
    }
    /**
     * @throws ReflectionException|Exception
     */
    function testAllNecessaryAndOptionalParamtersAreGiven()
    {
        $_GET['testString'] = 'hallo';
        $_GET['optionalTestString'] = 'Gulasch';
        $_SERVER=[];$_COOKIE=[];

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        self::assertEquals('Hallo Welt:hallo   Preset Value:Gulasch', $controller->dispatch('testFuncNoOptionalParameters'));
    }
    /**
     * @throws ReflectionException|Exception
     */
    function testUseTemplateWithoutVariables()
    {
        $_SERVER=[];$_COOKIE=[];

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        self::assertEquals('<h1>Meldung</h1><p>Template funktioniert!</p>', $controller->dispatch('testFuncUseTemplateWithoutVariables'));
    }
    /**
     * @throws ReflectionException|Exception
     */
    function testUseTemplateWithVariables()
    {
        $_SERVER=[];$_COOKIE=[];

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        self::assertEquals('<h1>Meldung</h1><p>Template funktioniert richtig!</p>', $controller->dispatch('testFuncUseTemplateWithVariables'));
    }
    /**
     * @throws ReflectionException|Exception
     */
    function testDispatchingResetsTemplate()
    {
        $_SERVER=[];$_COOKIE=[];

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);

        self::assertEquals('<h1>Meldung</h1><p>Template funktioniert richtig!</p>', $controller->dispatch('testFuncDispatchingResetsTemplate'));
    }
    /**
     * @throws ReflectionException|Exception
     */
    function testDispatchingWithDispathingMessage()
    {
        $_SERVER=[];$_COOKIE=[];

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestBaseClass($testObject, $main);
        self::assertEquals('Important Message', $controller->dispatch('testFuncDispatchingWithDispathingMessage'));
    }
    /**
     * @throws ReflectionException|Exception
     */
    function testDispatchingWithPreloadFunction()
    {
        $_SERVER=[];$_COOKIE=[];

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestPreloadBaseClass($testObject, $main);

        self::assertEquals('LOLHallo', $controller->dispatch('testFuncDispatchingWithPreloadFunction'));
    }
    /**
     * @throws ReflectionException|Exception
     */
    function testDispatchingWithPreloadFunctionAndTemplate()
    {
        $_SERVER=[];$_COOKIE=[];

        $testObject = new stdClass();
        $main = new seitenklasse();
        $main->catchVariables();

        $controller = new controllerTestPreloadTemplateBaseClass($testObject, $main);

        self::assertEquals('<h1>Meldung</h1><p>Template funktioniert richtig!</p>', $controller->dispatch('testFuncDispatchingWithPreloadFunctionAndTemplate'));
    }



}
