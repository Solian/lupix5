<?php

use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../testBasics.php');
require_once(__DIR__ . '/../../lib/standardv5/textElement.lib.php');


/**
 * Tests for the database library.
 */
class bausatzTest extends TestCase
{

    /**
     * Called before the first test of the test case class is run.
     * @throws Exception
     */
    public static function setUpBeforeClass():void {
        testBasics::generateTestDB(get_called_class());
    }

    public function testErstelleLeeresTextElment(){

        $textElement = bobArchiv::createEmptyTextElement();

        $this->assertEmpty($textElement->fields);
        $this->assertEquals('empty',$textElement->getName());
        $this->assertEquals('empty',$textElement->getDesc());
        $this->assertEquals('none',$textElement->getFile());
        $this->assertEquals('none',$textElement->getSection());



    }

    public function testErzeugeFehlerBeiNormalenPartials(){

        try{
            $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/wrong_demo');
        }catch(Throwable $e){
            $this->assertEquals(41,$e->getCode());
            $this->assertEquals('standardv5/test/wrong_demo',$e->getMessage());
        }

    }

    public function testErzeugeFehlerBeiNormalenTemplates(){

        try{
            $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo_template');
        }catch(Throwable $e){
            $this->assertEquals(41,$e->getCode());
            $this->assertEquals('standardv5/test/demo_template',$e->getMessage());
        }

    }

    /**
     * @throws Exception
     */

    public function testAendereEinTextElement(){

        $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo');

        $this->assertEquals('Einfacher Abschnitt',$textElement->getName());
        $this->assertEquals('Dieser Text ist als einfache Demonstration gedacht.',$textElement->getDesc());
        $this->assertEquals('standardv5/test/demo',$textElement->getFile());
        $this->assertEquals('Test',$textElement->getSection());

        $textElement->setEntryValue('titel','Hallo');
        $textElement->setEntryValue('text',1);

        $this->assertEquals('string',$textElement->entries['titel']->type);
        $this->assertEquals('Die Überschrift des Artikelabsatzes',$textElement->entries['titel']->desc);
        $this->assertEquals('Titel des Textes',$textElement->entries['titel']->placeholder);
        $this->assertEquals('Hallo',$textElement->entries['titel']->value);

        $this->assertEquals('text',$textElement->entries['text']->type);
        $this->assertEquals('Inhalt des Artikels, der auch unterüberschriften enthalten kann.',$textElement->entries['text']->desc);
        $this->assertEquals('Dies ist ein toller Text...',$textElement->entries['text']->placeholder);
        $this->assertEquals( 1,$textElement->entries['text']->value);

    }

    /**
     * @throws Exception
     */

    public function testErstelleEinTextElement(){

        $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo');

        $this->assertEquals('Einfacher Abschnitt',$textElement->getName());
        $this->assertEquals('Dieser Text ist als einfache Demonstration gedacht.',$textElement->getDesc());
        $this->assertEquals('standardv5/test/demo',$textElement->getFile());
        $this->assertEquals('Test',$textElement->getSection());

        $this->assertEquals('string',$textElement->entries['titel']->type);
        $this->assertEquals('Die Überschrift des Artikelabsatzes',$textElement->entries['titel']->desc);
        $this->assertEquals('Titel des Textes',$textElement->entries['titel']->placeholder);
        $this->assertEquals('',$textElement->entries['titel']->value);

        $this->assertEquals('text',$textElement->entries['text']->type);
        $this->assertEquals('Inhalt des Artikels, der auch unterüberschriften enthalten kann.',$textElement->entries['text']->desc);
        $this->assertEquals('Dies ist ein toller Text...',$textElement->entries['text']->placeholder);
        $this->assertEquals( 0,$textElement->entries['text']->value);

    }

    /**
     * @throws Exception
     */

    public function testErrorWennTextfeldMitEinemStringGespeichertWerdenSoll(){

        $errorThrown=false;

        $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo');

        $textElement->setEntryValue('text',1);
        $this->assertEquals( 1,$textElement->entries['text']->value);
        try{
            $textElement->setEntryValue('text','Hallo');
        }catch (Throwable $e){
            $this->assertEquals( 75,$e->getCode());
            echo $e->getMessage();
            $errorThrown=true;
        }

        $this->assertTrue($errorThrown);

    }


    /**
     * @throws Exception
     */

    public function testErrorWennStringMitEinemIntegerGespeichertWerdenSoll(){

        $errorThrown=false;

        $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo');

        $textElement->setEntryValue('titel','LOL');
        $this->assertEquals( 'LOL',$textElement->entries['titel']->value);
        try{
            $textElement->setEntryValue('titel',0);
        }catch (Throwable $e){
            $this->assertEquals( 75,$e->getCode());
            $errorThrown=true;
        }

        $this->assertTrue($errorThrown);

    }

    /**
     * @throws Exception
     */

    public function testAendereEinenReinesStringTextElement(){

        $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo_string_only');

        $this->assertEquals('Einfacher Abschnitt',$textElement->getName());
        $this->assertEquals('Dieser Text ist als einfache Demonstration gedacht.',$textElement->getDesc());
        $this->assertEquals('standardv5/test/demo_string_only',$textElement->getFile());
        $this->assertEquals('Test',$textElement->getSection());

        $textElement->setEntryValue('titel','Großer Aufsatz');
        $textElement->setEntryValue('text','<p>Damit beginnt die gesamte Geschichte!</p>');

        $this->assertEquals('string',$textElement->entries['titel']->type);
        $this->assertEquals('Die Überschrift des Artikelabsatzes',$textElement->entries['titel']->desc);
        $this->assertEquals('Titel des Textes',$textElement->entries['titel']->placeholder);
        $this->assertEquals('Großer Aufsatz',$textElement->entries['titel']->value);

        $this->assertEquals('string',$textElement->entries['text']->type);
        $this->assertEquals('Inhalt des Artikels, der auch unterüberschriften enthalten kann.',$textElement->entries['text']->desc);
        $this->assertEquals('Dies ist ein toller Text...',$textElement->entries['text']->placeholder);
        $this->assertEquals( '<p>Damit beginnt die gesamte Geschichte!</p>',$textElement->entries['text']->value);

    }

    /**
     * @throws Exception
     */


    public function testErstelleEinenBTextMitLeeremTextElementNurMitStrings(){

        $textElementeText = bobArchiv::emptySeitenElementText();

        $textElementeText->addSet(bobArchiv::emptySeitenElementFromFile('standardv5/test/demo_string_only'));

        $textElement = $textElementeText->getElementAtPosition(0);

        $textElement->setEntryValue('titel','Großer Aufsatz');
        $textElement->setEntryValue('text','<p>Damit beginnt die gesamte Geschichte!</p>');

        $this->assertEquals('string',$textElement->entries['titel']->type);
        $this->assertEquals('Die Überschrift des Artikelabsatzes',$textElement->entries['titel']->desc);
        $this->assertEquals('Titel des Textes',$textElement->entries['titel']->placeholder);
        $this->assertEquals('Großer Aufsatz',$textElement->entries['titel']->value);

        $this->assertEquals('string',$textElement->entries['text']->type);
        $this->assertEquals('Inhalt des Artikels, der auch unterüberschriften enthalten kann.',$textElement->entries['text']->desc);
        $this->assertEquals('Dies ist ein toller Text...',$textElement->entries['text']->placeholder);
        $this->assertEquals( '<p>Damit beginnt die gesamte Geschichte!</p>',$textElement->entries['text']->value);

    }


    /**
     * @throws Exception
     */


    public function testAendertEinElementEinesBTextMitLeeremTextElementNurMitStrings(){

        $textElementeText = bobArchiv::emptySeitenElementText();

        $textElementeText->addSet(bobArchiv::emptySeitenElementFromFile('standardv5/test/demo_string_only'));

        //Variante 1
        $textElementeText->getElementAtPosition(0)->setEntryValue('titel','Großer Aufsatz');

        //Variante 2
        $textElement = $textElementeText->getElementAtPosition(0);
        $textElement->entries['text']->value='<p>Damit beginnt die gesamte Geschichte!</p>';
        $textEntry = $textElement->entries['text'];
        $textEntry->value='<p>Damit beginnt die gesamte neue Geschichte!</p>';

        $this->assertEquals('string',$textElementeText->getElementAtPosition(0)->entries['titel']->type);
        $this->assertEquals('Die Überschrift des Artikelabsatzes',$textElementeText->getElementAtPosition(0)->entries['titel']->desc);
        $this->assertEquals('Titel des Textes',$textElementeText->getElementAtPosition(0)->entries['titel']->placeholder);
        $this->assertEquals('Großer Aufsatz',$textElementeText->getElementAtPosition(0)->entries['titel']->value);

        $this->assertEquals('string',$textElementeText->getElementAtPosition(0)->entries['text']->type);
        $this->assertEquals('Inhalt des Artikels, der auch unterüberschriften enthalten kann.',$textElementeText->getElementAtPosition(0)->entries['text']->desc);
        $this->assertEquals('Dies ist ein toller Text...',$textElementeText->getElementAtPosition(0)->entries['text']->placeholder);
        $this->assertEquals( '<p>Damit beginnt die gesamte neue Geschichte!</p>',$textElementeText->getElementAtPosition(0)->entries['text']->value);

    }

    /**
     * @throws Exception
     */

    public function testSpeichereUndLadeEineSeitenElementText(){

        $seitenElementText = bobArchiv::emptySeitenElementText();
        $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo_string_only');

        $textElement->setEntryValue('titel','Großer Aufsatz');
        $textElement->setEntryValue('text','<p>Damit beginnt die gesamte Geschichte!</p>');

        $seitenElementText->addSet(bobArchiv::emptySeitenElementFromFile('standardv5/test/demo_string_only'));

        $idDesTextElementText = bobArchiv::saveSeitenElementText($seitenElementText);
        $theSameTextElementeText =  bobArchiv::loadSeitenElementText($idDesTextElementText);

        $this->assertEquals($seitenElementText,$theSameTextElementeText);

        foreach(array('text','titel') as $elementField) {
            $this->assertEquals($seitenElementText->getElementAtPosition(0)->entries[$elementField]->type, $theSameTextElementeText->getElementAtPosition(0)->entries[$elementField]->type);
            $this->assertEquals($seitenElementText->getElementAtPosition(0)->entries[$elementField]->desc, $theSameTextElementeText->getElementAtPosition(0)->entries[$elementField]->desc);
            $this->assertEquals($seitenElementText->getElementAtPosition(0)->entries[$elementField]->placeholder, $theSameTextElementeText->getElementAtPosition(0)->entries[$elementField]->placeholder);
            $this->assertEquals($seitenElementText->getElementAtPosition(0)->entries[$elementField]->value, $theSameTextElementeText->getElementAtPosition(0)->entries[$elementField]->value);
        }
        $theSameTextElementeText->getElementAtPosition(0)->setEntryValue('titel','Super Aufsatz!');

        $this->assertTrue(bobArchiv::saveSeitenElementText($theSameTextElementeText));
        $theSameSameTextElementeText =  bobArchiv::loadSeitenElementText($theSameTextElementeText->getId());

        $this->assertEquals('Super Aufsatz!', $theSameSameTextElementeText->getElementAtPosition(0)->entries['titel']->value);


    }

    /**
     * @throws Exception
     */

    public function testRendereSeitenElementText(){

        $titel1 = 'Großer Aufsatz';
        $titel2 = 'Nächstes Kapitel';
        $text1='<p>Damit beginnt die gesamte Geschichte!</p>';
        $text2='<p>Damit geht die gesamte Geschichte weiter.</p>';

        $seitenElementText = bobArchiv::emptySeitenElementText();
        $textElement = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo_string_only');
        $textElement->setEntryValue('titel',$titel1);
        $textElement->setEntryValue('text',$text1);
        $seitenElementText->addSet($textElement);

        $textElement2 = bobArchiv::emptySeitenElementFromFile('standardv5/test/demo_string_only');
        $textElement2->setEntryValue('titel',$titel2);
        $textElement2->setEntryValue('text',$text2);
        $seitenElementText->addSet($textElement2);

        $expectedText= wolfi::parseTemplate('standardv5/test/demo_string_only.part',['titel'=>$titel1,'text'=>$text1]).
            wolfi::parseTemplate('standardv5/test/demo_string_only.part',['titel'=>$titel2,'text'=>$text2]);

        $actualText = wolfi::parseSeitenElementText($seitenElementText);

        $this->assertEquals($expectedText,$actualText);


    }

    public function testRendereSeitenElementTextEditor(){
        print_r(bobArchiv::load_elements(__DIR__.'/../../template'));
    }

}