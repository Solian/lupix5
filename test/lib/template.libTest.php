<?php

use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../testBasics.php');

/**
 * Test
 */
class templateTest extends TestCase {

    /**
     * Called before the first test of the test case class is run.
     * @throws Exception
     */
    public static function setUpBeforeClass():void {
        testBasics::generateTestDB(get_called_class());
    }

    /**
     * This method is called before a test is executed.
     */
    protected function setUp():void {
        
    }

    /**
     * This method is called after a test is executed.
     */
    protected function tearDown():void {
        
    }

    /**
     *
     */
    public function testTemplate() {
        self::assertTrue(TRUE);
    }

    /**
     * @throws Exception
     */

    public function testDatabase() {
        $row = db::query('SELECT COUNT(message_id) AS msg_count FROM main_nasy_nachrichten');
        self::assertEquals(0, $row[0]['msg_count']);
    }

}
