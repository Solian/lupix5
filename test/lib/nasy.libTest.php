<?php

use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../testBasics.php');
require_once(__DIR__ . '/../../lib/standardv5/db.lib.php');
require_once(__DIR__ . '/../../lib/standardv5/nasy.lib.php');

class nasyTest extends TestCase {

    /**
     * @var nasy $nasy
     */
    protected $nasy;
    protected $sender_id = 1;
    protected $recipient_id = 2;

    /**
     * Called before the first test of the test case class is run.
     * @throws Exception
     */
    public static function setUpBeforeClass():void {
        testBasics::generateTestDB(get_called_class());
    }

    /**
     * This method is called before a test is executed.
     * @throws Exception
     * @throws Exception
     * @throws Exception
     * @throws Exception
     */
    protected function setUp():void {
        $this->nasy = new nasy();

        db::insert('INSERT INTO main_nasy_nachrichten (sender_id, message_subject, message_content, message_date)
                    VALUES (1, "loadInbox", "loadInboxContent", NOW())');

        db::insert('INSERT INTO main_nasy_empfaenger (message_id, recipient_id)
                    VALUES (1, 2)');

        db::insert('INSERT INTO main_nasy_nachrichten (sender_id, message_subject, message_content, message_date)
                    VALUES (2, "loadOutbox", "loadOutboxContent", NOW())');

        db::insert('INSERT INTO main_nasy_empfaenger (message_id, recipient_id)
                    VALUES (2, 1)');
    }

    /**
     * @throws Exception
     */

    protected function setMessageTypeTestData():void {

        $id1 = db::insert('INSERT INTO main_nasy_nachrichten (sender_id, message_subject, message_content, message_type, message_date)
                    VALUES (1, "messageType1", "messageType1Content", ' . MessagesTypes::PRIVATE_MSG . ', DATE_SUB(NOW(), INTERVAL 1 HOUR))');
        db::insert('INSERT INTO main_nasy_empfaenger (message_id, recipient_id)
                    VALUES (' . $id1 . ', 2)');

        $id2 = db::insert('INSERT INTO main_nasy_nachrichten (sender_id, message_subject, message_content, message_type, message_date)
                    VALUES (1, "messageType2", "messageType2Content", ' . MessagesTypes::SYSTEM_MSG . ', DATE_SUB(NOW(), INTERVAL 2 HOUR))');
        db::insert('INSERT INTO main_nasy_empfaenger (message_id, recipient_id)
                    VALUES (' . $id2 . ', 2)');

        $id3 = db::insert('INSERT INTO main_nasy_nachrichten (sender_id, message_subject, message_content, message_type, message_date)
                    VALUES (1, "messageType3", "messageType3Content", ' . MessagesTypes::ADVENTURE_MSG . ' , DATE_SUB(NOW(), INTERVAL 3 HOUR))');
        db::insert('INSERT INTO main_nasy_empfaenger (message_id, recipient_id)
                    VALUES (' . $id3 . ', 2)');

        $id4 = db::insert('INSERT INTO main_nasy_nachrichten (sender_id, message_subject, message_content, message_type, message_date)
                    VALUES (1, "messageType4", "messageType4Content", ' . MessagesTypes::FORUM_MSG . ', DATE_SUB(NOW(), INTERVAL 4 HOUR))');
        db::insert('INSERT INTO main_nasy_empfaenger (message_id, recipient_id)
                    VALUES (' . $id4 . ', 2)');
    }

    /**
     * This method is called after a test is executed.
     * @throws Exception
     * @throws Exception
     */
    protected function tearDown():void {
        db::query('TRUNCATE main_nasy_nachrichten');
        db::query('TRUNCATE main_nasy_empfaenger');
    }

    /**
     * @covers MessagesTypes
     */
    public function testMessageTypes() {
        $expectedCount = 5;

        $this->setMessageTypeTestData();
        $this->nasy->loadOutbox($this->sender_id);

        self::assertCount($expectedCount, $this->nasy->messages);
        self::assertEquals(MessagesTypes::PRIVATE_MSG, $this->nasy->messages[1]->type);
        self::assertEquals(MessagesTypes::SYSTEM_MSG, $this->nasy->messages[2]->type);
        self::assertEquals(MessagesTypes::ADVENTURE_MSG, $this->nasy->messages[3]->type);
        self::assertEquals(MessagesTypes::FORUM_MSG, $this->nasy->messages[4]->type);
    }

    /**
     * @covers nasy::loadInbox
     */
    public function testLoadInbox() {
        $expectedCount = 1;

        $this->nasy->loadInbox($this->recipient_id);

        self::assertCount($expectedCount, $this->nasy->messages);
        self::assertEquals('loadInbox', $this->nasy->messages[0]->subject);
    }

    /**
     * @covers nasy::loadOutbox
     */
    public function testLoadOutbox() {
        $expectedCount = 1;

        $this->nasy->loadOutbox($this->recipient_id);

        self::assertCount($expectedCount, $this->nasy->messages);
        self::assertEquals('loadOutbox', $this->nasy->messages[0]->subject);
    }

    /**
     * @covers nasy::loadNavBox
     */
    public function testLoadNavBox() {
        $expectedCount = 1;

        $this->nasy->loadNavBox($this->recipient_id);
        self::assertCount($expectedCount, $this->nasy->messages);
    }

    /**
     * @covers nasy::deleteMessages
     * @throws Exception
     */
    public function testDeleteMessages() {
        $expectedCount = 0;

        $this->nasy->deleteMessages([1], $this->sender_id);

        $this->nasy->loadOutbox($this->sender_id);
        self::assertCount($expectedCount, $this->nasy->messages);
    }

    /**
     * @covers MESSAGE::sendMessage
     * @throws Exception
     */
    public function testSendMessage() {
        $expectedCount = 2;

        MESSAGE::sendMessage($this->sender_id, 'sendMessage', 'sendMessageContent', $this->recipient_id, MessagesTypes::PRIVATE_MSG);

        $this->nasy->loadOutbox($this->sender_id);
        self::assertCount($expectedCount, $this->nasy->messages);
    }

    /**
     * @covers MESSAGE::deleteMessage
     * @throws Exception
     */
    public function testDeleteMessage() {
        $expectedCount = 0;
        $message = new MESSAGE();

        $message->deleteMessage(1, $this->sender_id);

        $this->nasy->loadOutbox($this->sender_id);
        self::assertCount($expectedCount, $this->nasy->messages);
    }

    /**
     * @covers MESSAGE::setMessageRead
     * @throws Exception
     * @throws Exception
     */
    public function testSetMessageRead() {
        $message_sender = new MESSAGE();
        $message_recipient = new MESSAGE();

        $this->nasy->loadInbox($this->sender_id);
        self::assertEquals(0, $this->nasy->messages[0]->was_read);

        $message_sender->loadMessage(2, $this->sender_id);
        self::assertEquals(1, $message_sender->was_read);

        $this->nasy->loadInbox($this->recipient_id);
        self::assertEquals(0, $this->nasy->messages[0]->was_read);

        $message_recipient->loadMessage(1, $this->recipient_id);
        self::assertEquals(1, $message_sender->was_read);
    }

    /**
     * @covers MESSAGE::exchangeArray
     */
    public function testExchangeArray() {
        $message = new MESSAGE();
        $message->exchangeArray(['sender_id' => $this->sender_id,
            'sender' => 'Test',
            'message_subject' => 'exchangeArray',
            'message_content' => 'exchangeArrayContent']);

        self::assertEquals(0, $message->message_id);
        self::assertEquals($this->sender_id, $message->sender_id);
        self::assertEquals('Test', $message->sender);
        self::assertEquals('exchangeArray', $message->subject);
        self::assertEquals('exchangeArrayContent', $message->text);
        self::assertEquals([], $message->recipients);
        self::assertEquals(MessagesTypes::PRIVATE_MSG, $message->type);
    }

}
