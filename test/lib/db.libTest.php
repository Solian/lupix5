<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../testBasics.php';

/**
 * Tests for the database library.
 */
class dbTest extends TestCase {

    /**
     * Called before the first test of the test case class is run.
     * @throws Exception
     */
    public static function setUpBeforeClass():void {
        testBasics::generateTestDB(get_called_class());
    }

    /**
     * This method is called before a test is executed.
     */
    protected function setUp():void {
        
    }

    /**
     * @throws Exception
     */
    protected function setTestUser() {
        $sql = "INSERT INTO main_benutzer (nickname) VALUES ('Hans')";
        db::insert($sql);

        $sql = "INSERT INTO main_benutzer (nickname) VALUES ('Kurt')";
        db::insert($sql);
    }

    /**
     * This method is called after a test is executed.
     * @throws Exception
     */
    protected function tearDown():void {
        db::query('TRUNCATE main_benutzer');
    }

    /**
     * Tests a query with the wrong amount of parameters.
     */
    public function testWrongQueryParameters() {
        $this->expectExceptionCode(92);
        $sql = 'SELECT nickname FROM main_benutzer WHERE id = :id';
        db::query($sql);
    }

    /**
     * Tests an insert query with the wrong amount of parameters.
     */
    public function testWrongInsertParameters() {

        $this->expectExceptionCode(92);
        $sql = 'INSERT INTO main_benutzer (nickname) VALUES (:username)';
        db::insert($sql);
    }

    /**
     * Tests if the insert method returns the id of the last inserted entry.
     * @throws Exception
     */
    public function testInsertID() {
        $sql = "INSERT INTO main_benutzer (nickname) VALUES ('HansWurt')";
        $id = db::insert($sql);

        self::assertEquals(1, $id);
    }

    /**
     * Tests if the query returns the correct amount of rows.
     * @throws Exception
     */
    public function testQuery() {
        $this->setTestUser();

        $sql = 'SELECT * FROM main_benutzer';
        $res = db::query($sql);
        self::assertEquals(2, count($res));
    }

    /**
     * Tests if the querySingle returns only one row or false if multiple rows selected.
     * @throws Exception
     * @throws Exception
     */
    public function testSingleQuery() {
        $this->setTestUser();

        $sql = "SELECT * FROM main_benutzer WHERE nickname = 'Hans'";
        $res = db::querySingle($sql);
        self::assertEquals('Hans', $res['nickname']);

        $sql = 'SELECT * FROM main_benutzer';
        $res = db::querySingle($sql);
        self::assertFalse($res);
    }

}
