<?php

use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../testBasics.php');
require_once( __DIR__ . '/../../lib/standardv5/fehlerbehandlung.lib.php');
require_once(__DIR__ . '/../../lib/standardv5/funktionen.lib.php');
require_once(__DIR__ . '/../../lib/standardv5/userIdent.lib.php');

/**
 * Test
 */
class UserIdentTest extends TestCase {

    /** @var UserIdentity */
    protected $ident;

    /**
     * Called before the first test of the test case class is run.
     * @throws Exception
     */
    public static function setUpBeforeClass():void {
        testBasics::generateTestDB(get_called_class());
    }

    /**
     * This method is called before a test is executed
     * @throws Exception
     */
    protected function setUp() :void{
        db::query('TRUNCATE main_benutzer');
        $this->ident = new UserIdentity();
        $this->insertTestUsers();
    }

    /**
     * This method is called after a test is executed.
     */
    protected function tearDown():void {

    }

    /**
     * @throws Exception
     */
    protected function insertTestUsers() {
        $sql_u1 = 'INSERT INTO main_benutzer (id, nickname, passwort, nutzergruppe_id) VALUES (1, "Hans", "Test", 1)';
        db::insert($sql_u1);

        $salt = uniqid(mt_rand(), true);
        $pw = hash('sha512', 'Test' . $salt);

        $sql_u2 = 'INSERT INTO main_benutzer (id, nickname, email, passwort, salt, nutzergruppe_id, sessionkey) VALUES (2, "Kurt", "kurt@test.de", :pw, :salt, 2, "skey")';
        db::insert($sql_u2, [':pw' => $pw, ':salt' => $salt]);

        $sql_u3 = 'INSERT INTO main_benutzer (id, nickname, email, passwort, salt, nutzergruppe_id) VALUES (3, "Max", "max@test.de" , :pw, :salt, 3)';
        db::insert($sql_u3, [':pw' => $pw, ':salt' => $salt]);
    }

    /**
     * Test login as blocked user.
     * @throws Exception
     */
    public function testBlockedUser() {
        $this->expectExceptionCode(201);
        $this->ident->login('max@test.de', 'Test');
    }

    /**
     * Test login with wrong password.
     * @throws Exception
     */
    public function testUserWithWrongPw() {
        $this->expectExceptionCode(211);
        $this->ident->login('Hans', 'Wurst');
    }

    /**
     * Tests if the identification will fail with wrong session key.
     *
     * @throws Exception
     */
    public function testUserFailIdentify() {
        $this->ident->identify('wrongKey');
        self::assertEquals(USERGROUPS::GUEST, $this->ident->group_id, 'User must be set to guest!');
    }

    /**
     * @throws Exception
     * Tests if the user could identify correct.
     */
    public function testUserIdentify() {
        $this->ident->identify('skey');
        self::assertEquals('Kurt', $this->ident->name, 'User identiy fail!');
    }

    /**
     * Tests if the user could login correct.
     * @throws Exception
     */
    public function testCorrectLogin() {
        $this->ident->login('kurt@test.de', 'Test');

        self::assertEquals(2, $this->ident->id, 'User login failed!');
    }

    /**
     * Negative test for setting new password.
     * @throws Exception
     */
    public function testSetNewPasswordWrong() {
        $this->ident->login('kurt@test.de', 'Test');
        $result1 = $this->ident->setNewPassword('Test1', 'Test2', 'Test');

        self::assertEquals(UserIdentity::PW_CHANGE_UNEQUAL_PWS, $result1);

        $result2 = $this->ident->setNewPassword('Test1', 'Test1', 'Test2');

        self::assertEquals(UserIdentity::PW_CHANGE_OLD_PW_WRONG, $result2);
    }

    /**Positive test for setting new password.
     * @throws Exception
     */
    public function testSetNewPassword() {
        $this->ident->login('kurt@test.de', 'Test');
        $result = $this->ident->setNewPassword('Test1', 'Test1', 'Test');

        self::assertEquals(UserIdentity::PW_CHANGE_SUCC, $result);
    }

}
